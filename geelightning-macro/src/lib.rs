extern crate proc_macro;

use proc_macro2::{TokenStream, TokenTree};
use quote::quote;

#[proc_macro]
pub fn script(script: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let script: TokenStream = script.into();
    let mut pushes = Vec::new();
    let mut token_trees = script.into_iter();
    while let Some(token_tree) = token_trees.next() {
        match token_tree {
            TokenTree::Ident(ident) => {
                pushes.push(quote! {
                    let builder = builder.push_opcode(bitcoin::blockdata::opcodes::all::#ident);
                });
            }
            TokenTree::Punct(ref punct) if punct.as_char() == '#' => {
                let ident = match token_trees.next() {
                    Some(TokenTree::Ident(ident)) => ident,
                    _ => panic!("expected ident after '#'"),
                };
                pushes.push(quote! {
                    let builder = SerializeToScript::push_to_script(&#ident, builder);
                });
            }
            _ => panic!("unexpected token"),
        }
    }
    let output = quote! {{
        use crate::scripts::SerializeToScript;
        let builder = bitcoin::blockdata::script::Builder::new();
        #(#pushes)*
        builder.into_script()
    }};
    output.into()
}

#[allow(clippy::cognitive_complexity)]
#[proc_macro_attribute]
pub fn ext_trait(
    _attr: proc_macro::TokenStream,
    item_impl: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let item_impl = syn::parse_macro_input!(item_impl as syn::ItemImpl);
    let syn::ItemImpl {
        attrs,
        defaultness,
        unsafety,
        impl_token,
        generics,
        trait_,
        self_ty,
        brace_token: _brace_token,
        items,
    } = item_impl;
    if defaultness.is_some() {
        panic!("#[ext_trait] cannot be used on default trait impls");
    }
    if unsafety.is_some() {
        panic!("#[ext_trait] cannot be used on unsafe trait impls");
    }
    let (bang, path, for_token) = match trait_ {
        Some(x) => x,
        None => panic!("#[ext_trait] must be given a trait name"),
    };
    if bang.is_some() {
        panic!("#[ext_trait] does not support negative impls");
    }

    let mut item_signatures = Vec::new();
    let mut item_impls = Vec::new();
    for item in &items {
        match item {
            syn::ImplItem::Const(impl_item_const) => {
                let syn::ImplItemConst {
                    attrs,
                    vis,
                    defaultness,
                    const_token,
                    ident,
                    colon_token,
                    ty,
                    eq_token,
                    expr,
                    semi_token,
                } = impl_item_const;
                match vis {
                    syn::Visibility::Inherited => (),
                    _ => panic!("#[ext_trait] impl items cannot have visibility qualifiers"),
                }
                if defaultness.is_some() {
                    panic!("#[ext_trait] impl items cannot be default");
                }
                item_signatures.push(quote! {
                    #(#attrs)* #const_token #ident #colon_token #ty #semi_token
                });
                item_impls.push(quote! {
                    #(#attrs)* #const_token #ident #colon_token #ty #eq_token #expr #semi_token
                });
            }
            syn::ImplItem::Method(impl_item_method) => {
                let syn::ImplItemMethod {
                    attrs,
                    vis,
                    defaultness,
                    sig,
                    block,
                } = impl_item_method;
                match vis {
                    syn::Visibility::Inherited => (),
                    _ => panic!("#[ext_trait] impl items cannot have visibility qualifiers"),
                }
                if defaultness.is_some() {
                    panic!("#[ext_trait] impl items cannot be default");
                }
                item_signatures.push(quote! {
                    #(#attrs)* #sig;
                });
                item_impls.push(quote! {
                    #(#attrs)* #sig #block
                });
            }
            syn::ImplItem::Type(impl_item_type) => {
                let syn::ImplItemType {
                    attrs,
                    vis,
                    defaultness,
                    type_token,
                    ident,
                    generics,
                    eq_token,
                    ty,
                    semi_token,
                } = impl_item_type;
                match vis {
                    syn::Visibility::Inherited => (),
                    _ => panic!("#[ext_trait] impl items cannot have visibility qualifiers"),
                }
                if defaultness.is_some() {
                    panic!("#[ext_trait] impl items cannot be default");
                }
                item_signatures.push(quote! {
                    #(#attrs)* #type_token #ident #generics #semi_token
                });
                item_impls.push(quote! {
                    #(#attrs)* #type_token #ident #generics #eq_token #ty #semi_token
                });
            }
            syn::ImplItem::Macro(..) => panic!("#[ext_trait] does not support associated macros"),
            _ => panic!("unexpected tokens in impl block"),
        }
    }

    let output = quote! {
        #(#attrs)*
        pub trait #path {
            #(#item_signatures)*
        }

        #(#attrs)*
        #impl_token #generics #path #for_token #self_ty {
            #(#item_impls)*
        }
    };
    output.into()
}
