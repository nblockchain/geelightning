use super::*;

pub fn alloc_single<T>(value: T) -> *mut T {
    unsafe {
        let ptr = libc::malloc(mem::size_of::<T>()) as *mut T;
        ptr::write(ptr, value);
        ptr
    }
}

pub fn alloc_array<T>(values: Vec<T>) -> (usize, *mut T) {
    let len = values.len();
    unsafe {
        let ptr = libc::malloc(mem::size_of::<T>() * values.len()) as *mut T;
        for (i, value) in values.into_iter().enumerate() {
            ptr::write(ptr.add(i), value);
        }
        (len, ptr)
    }
}

pub fn alloc_slice<T: Copy>(values: &[T]) -> (usize, *mut T) {
    let len = values.len();
    unsafe {
        let ptr = libc::malloc(mem::size_of::<T>() * values.len()) as *mut T;
        for (i, value) in values.iter().enumerate() {
            ptr::write(ptr.add(i), *value);
        }
        (len, ptr)
    }
}

pub unsafe fn free_array<T>(len: usize, ptr: *mut T) -> Vec<T> {
    let mut values = Vec::with_capacity(len);
    unsafe {
        for i in 0..len {
            let value = ptr::read(ptr.add(i));
            values.push(value);
        }
        libc::free(ptr as *mut c_void);
        values
    }
}

#[repr(C)]
pub struct CChannelInfo {
    pub channel_id: *mut u8,
    pub capacity_sat: u64,
    pub confirmed_balance_msat: u64,
    pub unconfirmed_balance_msat: u64,
    pub funding_outpoint: COutPoint,
    pub phase: CChannelPhaseInfo,
    pub minimum_depth: u32,
}

pub type CChannelPhaseInfo = u32;
pub const C_CHANNEL_PHASE_INFO_UNCONFIRMED: CChannelPhaseInfo = 0;
pub const C_CHANNEL_PHASE_INFO_LOCAL_CONFIRMED: CChannelPhaseInfo = 1;
pub const C_CHANNEL_PHASE_INFO_ACTIVE: CChannelPhaseInfo = 3;

impl CChannelInfo {
    pub fn from_rust(channel_id: ChannelId, channel_info: ChannelInfo) -> CChannelInfo {
        let (_, channel_id) = alloc_slice(channel_id.as_bytes());
        let capacity_sat = channel_info.capacity.to_u64();
        let unconfirmed_balance_msat = channel_info.unconfirmed_balance.to_u64();
        let confirmed_balance_msat = channel_info.confirmed_balance.to_u64();
        let funding_outpoint = COutPoint::from_rust(channel_info.funding_outpoint);
        let (phase, minimum_depth) = match channel_info.phase {
            ChannelPhaseInfo::Unconfirmed { minimum_depth } => {
                (C_CHANNEL_PHASE_INFO_UNCONFIRMED, minimum_depth)
            },
            ChannelPhaseInfo::LocalConfirmed => (C_CHANNEL_PHASE_INFO_LOCAL_CONFIRMED, 0),
            ChannelPhaseInfo::Active => (C_CHANNEL_PHASE_INFO_ACTIVE, 0),
        };
        CChannelInfo {
            channel_id,
            capacity_sat,
            confirmed_balance_msat,
            unconfirmed_balance_msat,
            funding_outpoint,
            phase,
            minimum_depth,
        }
    }
}

#[repr(C)]
pub struct COutPoint {
    pub txid: *mut u8,
    pub vout: u32,
}

impl COutPoint {
    pub fn from_rust(out_point: OutPoint) -> COutPoint {
        let txid = {
            let (_, txid) = alloc_slice(out_point.txid.as_ref());
            txid
        };
        let vout = out_point.vout;
        COutPoint { txid, vout }
    }

    pub fn into_rust(self) -> OutPoint {
        let txid = unsafe {
            let bytes = slice::from_raw_parts(self.txid, sha256d::Hash::BYTES_LEN);
            let txid = Txid::from(sha256d::Hash::from_slice(bytes).unwrap());
            free_array(sha256d::Hash::BYTES_LEN, self.txid);
            txid
        };
        let vout = self.vout;
        OutPoint { txid, vout }
    }
}

#[repr(C)]
pub struct CScript {
    len: usize,
    data: *mut u8,
}

impl CScript {
    pub fn from_rust(script: Script) -> CScript {
        let script_bytes = script.into_bytes();
        let (len, data) = alloc_array(script_bytes);
        CScript { len, data }
    }

    pub fn into_rust(self) -> Script {
        let script_bytes = unsafe { free_array(self.len, self.data) };
        Script::from(script_bytes)
    }
}
