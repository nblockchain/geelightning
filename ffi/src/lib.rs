#![deny(unsafe_op_in_unsafe_fn)]
#![allow(non_snake_case)]
#![type_length_limit = "3000000"]
#![allow(clippy::missing_safety_doc)]
#![allow(clippy::let_and_return)]
#![allow(clippy::unused_unit)]

// NOTE: Some types in this file are commented-out because feature(type_alias_impl_trait) is still
// not stable. Once it is, we can switch to the commented versions which avoid using dyn pointers
// and explictly wrapping `HandleImpl`, resulting in better type-safety and more efficient code.

macro_rules! try_ {
    ($block:block) => (
        ((|| Ok($block))())
    );
}

use crate::{
    addr::SocketAddrExt,
    callbacks::{drop_handle, next_generator_state, run_future, HandleImpl},
    tx::{alloc_array, alloc_single, alloc_slice, CChannelInfo, COutPoint, CScript},
    util::{err_to_c_string, ErrorString, Smuggle, SmuggleMut},
};
use bitcoin::{
    hash_types::Txid,
    hashes::{sha256d, Hash},
    OutPoint, Script, Network,
};
use failure::Fail;
use futures::{Future, FutureExt, Stream, StreamExt, TryStreamExt};
use geelightning::{
    async_generator::{
        AsyncGeneratorExt, FusedAsyncGenerator, GeneratorState,
        TryAsyncGeneratorExt, TryFutureExt as _,
    },
    bitcoin_ext::Sha256dHashExt,
    channel::drop_channel::{DropReceiver, DropSender},
    ln::{
        channel::{
            Balance, ChannelId, ChannelInfo, ChannelPhaseInfo, TemporaryChannelId,
            CHANNEL_ID_BYTES_LEN,
        },
        endpoint::{Endpoint, LocalEndpoint},
        peer::{PeerId, PeerSecretKey, PEER_ID_BYTES_LEN, PEER_SECRET_KEY_BYTES_LEN},
    },
    units::{Sat64, SatPerKw32},
    Node,
};
use libc::{c_char, c_void};
use net_literals::*;
use pin_utils::pin_mut;
use std::{
    collections::HashMap,
    ffi::CStr,
    fmt, mem,
    net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6},
    path::PathBuf,
    pin::Pin,
    ptr, slice,
};
use tokio::runtime::Runtime;

mod addr;
mod callbacks;
mod tx;
mod util;

pub type Geelightning = Runtime;
pub type ErrorStream = Pin<Box<dyn Stream<Item = ErrorString> + Send + 'static>>;

fn new_error_stream<E, S>(stream: S) -> *mut ErrorStream
where
    E: Into<ErrorString>,
    S: Stream<Item = E> + Send + 'static,
{
    Box::into_raw(Box::new(Box::pin(stream.map(|e| e.into())) as ErrorStream))
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_init(error_out: *mut *mut c_char) -> *mut Geelightning {
    let runtime = match Runtime::new() {
        Ok(runtime) => runtime,
        Err(err) => {
            unsafe {
                *error_out = err_to_c_string(err);
            }
            return ptr::null_mut();
        },
    };
    Box::into_raw(Box::new(runtime))
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_quit(geelightning: *mut Geelightning) {
    let runtime = unsafe { Box::from_raw(geelightning) };
    drop(runtime)
}

//pub type NodeNewFuture = impl Handle;

#[repr(transparent)]
pub struct NodeNewFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_new(
    geelightning: *mut Geelightning,
    base_directory: *const c_char,
    peer_secret_key: *const u8,
    callback: extern "C" fn(*const c_void, *mut Node, *mut ErrorStream, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeNewFuture {
    let runtime = unsafe { &mut *geelightning };
    let base_directory_res: Result<PathBuf, ErrorString> = try_!({
        let base_directory = unsafe { CStr::from_ptr(base_directory) };
        let base_directory = base_directory.to_str()?;
        PathBuf::from(base_directory)
    });
    let peer_secret_key_res: Result<PeerSecretKey, ErrorString> = try_!({
        let peer_secret_key =
            unsafe { ptr::read(peer_secret_key as *const [u8; PEER_SECRET_KEY_BYTES_LEN]) };
        PeerSecretKey::try_from_bytes(peer_secret_key)?
    });
    let userdata = Smuggle(userdata);
    let ret = Box::into_raw(Box::new(NodeNewFuture(run_future(
        runtime,
        async move {
            let base_directory = base_directory_res?;
            let peer_secret_key = peer_secret_key_res?;
            let local_endpoint = LocalEndpoint {
                peer_secret_key,
                addr: addr!("0.0.0.0:0"),
            };
            let (node, error_stream) = Node::new(base_directory, &local_endpoint).await?;
            Ok((node, error_stream))
        },
        move |res: Result<_, ErrorString>| match res {
            Ok((node, error_stream)) => {
                let node = Box::into_raw(Box::new(node));
                let error_stream = new_error_stream(error_stream);
                callback(userdata.as_ptr(), node, error_stream, ptr::null_mut());
            },
            Err(err) => {
                let err = err_to_c_string(err);
                callback(userdata.as_ptr(), ptr::null_mut(), ptr::null_mut(), err);
            },
        },
    ))));
    ret
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_new_finish(
    geelightning: *mut Geelightning,
    node_new_future: *mut NodeNewFuture,
) {
    unsafe { drop_handle(geelightning, node_new_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_free(geelightning: *mut Geelightning, node: *mut Node) {
    let node = SmuggleMut(node);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node) = node;
        let _node = unsafe { Box::from_raw(node) };
    });
}

//pub type ErrorStreamNextFuture = impl Handle;

#[repr(transparent)]
pub struct ErrorStreamNextFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_error_stream_next(
    geelightning: *mut Geelightning,
    error_stream: *mut ErrorStream,
    callback: extern "C" fn(*const c_void, *mut c_char),
    userdata: *const c_void,
) -> *mut ErrorStreamNextFuture {
    let runtime = unsafe { &mut *geelightning };
    let error_stream = SmuggleMut(error_stream);
    let userdata = Smuggle(userdata);
    let ret = Box::into_raw(Box::new(ErrorStreamNextFuture(run_future(
        runtime,
        async move {
            let error_stream_mut = unsafe { error_stream.as_mut() };
            let error_opt = error_stream_mut.next().await;
            error_opt
        },
        move |res: Option<ErrorString>| match res {
            Some(err) => {
                let err = err_to_c_string(err);
                callback(userdata.as_ptr(), err);
            },
            None => {
                callback(userdata.as_ptr(), ptr::null_mut());
            },
        },
    ))));
    ret
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_error_stream_next_finish(
    geelightning: *mut Geelightning,
    error_stream_next_future: *mut ErrorStreamNextFuture,
) {
    unsafe { drop_handle(geelightning, error_stream_next_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_error_stream_free(
    geelightning: *mut Geelightning,
    error_stream: *mut ErrorStream,
) {
    let error_stream = SmuggleMut(error_stream);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(error_stream) = error_stream;
        let _error_stream = unsafe { Box::from_raw(error_stream) };
    });
}

/*
pub type NodeOpenChannelGeneratorInner =
    impl AsyncGenerator<
        Yield = ErrorString,
        Return = Result<(TemporaryChannelId, Script), ErrorString>,
    >
    + FusedAsyncGenerator
    + Send
    + 'static;
pub type NodeOpenChannelGenerator = Pin<Box<NodeOpenChannelGeneratorInner>>;
*/
pub type NodeOpenChannelGenerator = Pin<Box<
    dyn FusedAsyncGenerator<
        Yield = ErrorString,
        Return = Result<(TemporaryChannelId, Script), ErrorString>,
    >
    + Send
    + 'static
>>;

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_open_channel(
    _geelightning: *mut Geelightning,
    node: *mut Node,
    peer_id: *const u8,
    funding: u64,
    feerate: u32,
    network_magic: u32,
) -> *mut NodeOpenChannelGenerator {
    let node = SmuggleMut(node);

    let peer_id_res: Result<_, ErrorString> = try_!({
        let peer_id = unsafe { ptr::read(peer_id as *const [u8; PEER_ID_BYTES_LEN]) };
        PeerId::try_from_bytes(peer_id)?
    });

    let future = async move {
        let peer_id = peer_id_res?;
        let funding = Sat64::from_u64(funding);
        let feerate = SatPerKw32::from_u32(feerate);
        let node = unsafe { node.as_mut() };
        let open_channel_generator = node.open_channel(peer_id, funding, feerate, unwrap!(Network::from_magic(network_magic)));
        Ok(open_channel_generator
            .map_err(|e| e.into())
            .map_yield(|e| e.into()))
    };
    let generator = future.try_flatten_async_generator().fuse();
    Box::into_raw(Box::new(Box::pin(generator)))
}

//pub type NodeOpenChannelResumeFuture = impl Handle;

#[repr(transparent)]
pub struct NodeOpenChannelResumeFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_open_channel_resume(
    geelightning: *mut Geelightning,
    node_open_channel_generator: *mut NodeOpenChannelGenerator,
    callback: extern "C" fn(*const c_void, *mut u8, *mut CScript, *mut c_char, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeOpenChannelResumeFuture {
    let userdata = Smuggle(userdata);
    let map = move |state: GeneratorState<_, Result<(TemporaryChannelId, Script), _>>| match state {
        GeneratorState::Complete(Ok((temporary_channel_id, funding_script_pubkey))) => {
            let (_, c_temporary_channel_id) = alloc_slice(temporary_channel_id.as_bytes());
            let c_funding_script_pubkey = CScript::from_rust(funding_script_pubkey);
            let c_funding_script_pubkey = alloc_single(c_funding_script_pubkey);
            callback(
                userdata.as_ptr(),
                c_temporary_channel_id,
                c_funding_script_pubkey,
                ptr::null_mut(),
                ptr::null_mut(),
            );
        },
        GeneratorState::Complete(Err(err)) => {
            let err = err_to_c_string(err);
            callback(
                userdata.as_ptr(),
                ptr::null_mut(),
                ptr::null_mut(),
                err,
                ptr::null_mut(),
            );
        },
        GeneratorState::Yielded(err) => {
            let err = err_to_c_string(err);
            callback(
                userdata.as_ptr(),
                ptr::null_mut(),
                ptr::null_mut(),
                ptr::null_mut(),
                err,
            );
        },
    };
    let ret = unsafe { next_generator_state(geelightning, node_open_channel_generator, map) };
    Box::into_raw(Box::new(NodeOpenChannelResumeFuture(ret)))
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_open_channel_resume_finish(
    geelightning: *mut Geelightning,
    node_open_channel_resume_future: *mut NodeOpenChannelResumeFuture,
) {
    unsafe { drop_handle(geelightning, node_open_channel_resume_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_open_channel_free(
    geelightning: *mut Geelightning,
    node_open_channel_generator: *mut NodeOpenChannelGenerator,
) {
    let node_open_channel_generator = SmuggleMut(node_open_channel_generator);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node_open_channel_generator) = node_open_channel_generator;
        let _node_open_channel_generator = unsafe { Box::from_raw(node_open_channel_generator) };
    });
}

/*
pub type NodeFundChannelGeneratorInner =
    impl AsyncGenerator<
        Yield = ErrorString,
        Return = Result<ChannelId, ErrorString>,
    >
    + FusedAsyncGenerator
    + Send
    + 'static;
pub type NodeFundChannelGenerator = Pin<Box<NodeFundChannelGeneratorInner>>;
*/
pub type NodeFundChannelGenerator = Pin<Box<
    dyn FusedAsyncGenerator<
        Yield = ErrorString,
        Return = Result<ChannelId, ErrorString>,
    >
    + Send
    + 'static
>>;

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_fund_channel(
    _geelightning: *mut Geelightning,
    node: *mut Node,
    peer_id: *const u8,
    temporary_channel_id: *const u8,
    funding_outpoint: COutPoint,
) -> *mut NodeFundChannelGenerator {
    let node = SmuggleMut(node);

    let peer_id_res: Result<_, ErrorString> = try_!({
        let peer_id = unsafe { ptr::read(peer_id as *const [u8; PEER_ID_BYTES_LEN]) };
        PeerId::try_from_bytes(peer_id)?
    });

    let temporary_channel_id = {
        let bytes = unsafe { ptr::read(temporary_channel_id as *const [u8; CHANNEL_ID_BYTES_LEN]) };
        TemporaryChannelId::from_bytes(bytes)
    };

    let funding_outpoint = funding_outpoint.into_rust();

    let future = async move {
        let peer_id = peer_id_res?;
        let node = unsafe { node.as_mut() };
        let fund_channel_generator = node
            .fund_channel(peer_id, temporary_channel_id, funding_outpoint)
            .map_err(|e| e.into())
            .map_yield(|e| e.into());
        Ok(fund_channel_generator)
    };

    let generator = future.try_flatten_async_generator().fuse();
    Box::into_raw(Box::new(Box::pin(generator)))
}

//pub type NodeFundChannelResumeFuture = impl Handle;

#[repr(transparent)]
pub struct NodeFundChannelResumeFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_fund_channel_resume(
    geelightning: *mut Geelightning,
    node_fund_channel_generator: *mut NodeFundChannelGenerator,
    callback: extern "C" fn(*const c_void, *mut u8, *mut c_char, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeFundChannelResumeFuture {
    let userdata = Smuggle(userdata);
    let map = move |state: GeneratorState<_, Result<ChannelId, _>>| match state {
        GeneratorState::Complete(Ok(channel_id)) => {
            let (_, c_channel_id) = alloc_slice(channel_id.as_bytes());
            callback(
                userdata.as_ptr(),
                c_channel_id,
                ptr::null_mut(),
                ptr::null_mut(),
            )
        },
        GeneratorState::Complete(Err(err)) => {
            let err = err_to_c_string(err);
            callback(userdata.as_ptr(), ptr::null_mut(), err, ptr::null_mut());
        },
        GeneratorState::Yielded(err) => {
            let err = err_to_c_string(err);
            callback(userdata.as_ptr(), ptr::null_mut(), ptr::null_mut(), err);
        },
    };
    let ret = unsafe { next_generator_state(geelightning, node_fund_channel_generator, map) };
    Box::into_raw(Box::new(NodeFundChannelResumeFuture(ret)))
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_fund_channel_resume_finish(
    geelightning: *mut Geelightning,
    node_fund_channel_resume_future: *mut NodeFundChannelResumeFuture,
) {
    unsafe { drop_handle(geelightning, node_fund_channel_resume_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_fund_channel_free(
    geelightning: *mut Geelightning,
    node_fund_channel_generator: *mut NodeFundChannelGenerator,
) {
    let node_fund_channel_generator = SmuggleMut(node_fund_channel_generator);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node_fund_channel_generator) = node_fund_channel_generator;
        let _node_fund_channel_generator = unsafe { Box::from_raw(node_fund_channel_generator) };
    });
}

/*
type NodeChannelsStreamInner = impl Stream<Item = Result<HashMap<ChannelId, ChannelInfo>, ErrorString>> + Send + 'static;
type NodeChannelsStream = Pin<Box<NodeChannelsStreamInner>>;
*/
type NodeChannelsStream = Pin<Box<
    dyn Stream<Item = Result<HashMap<ChannelId, ChannelInfo>, ErrorString>> + Send + 'static
>>;

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_channels(
    _geelightning: *mut Geelightning,
    node: *mut Node,
) -> *mut NodeChannelsStream {
    let node = unsafe { &mut *node };
    let channels_res_stream = node.channels().map_err(|err| err.into());
    Box::into_raw(Box::new(Box::pin(channels_res_stream)))
}

//pub type NodeChannelsNextFuture = impl Handle;

#[repr(transparent)]
pub struct NodeChannelsNextFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_channels_next(
    geelightning: *mut Geelightning,
    node_channels_stream: *mut NodeChannelsStream,
    callback: extern "C" fn(*const c_void, usize, *mut CChannelInfo, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeChannelsNextFuture {
    let runtime = unsafe { &mut *geelightning };
    let node_channels_stream = SmuggleMut(node_channels_stream);
    let userdata = Smuggle(userdata);
    let ret = Box::into_raw(Box::new(NodeChannelsNextFuture(run_future(
        runtime,
        async move {
            let node_channels_stream_mut = unsafe { node_channels_stream.as_mut() };
            let channel_infos_res = node_channels_stream_mut.next().await.unwrap();
            channel_infos_res
        },
        move |channel_infos_res: Result<HashMap<ChannelId, ChannelInfo>, ErrorString>| {
            match channel_infos_res {
                Ok(channel_infos) => {
                    let mut cchannel_infos = Vec::with_capacity(channel_infos.len());
                    for (channel_id, channel_info) in channel_infos {
                        cchannel_infos.push(CChannelInfo::from_rust(channel_id, channel_info));
                    }
                    let (cchannel_infos_len, cchannel_infos) = alloc_array(cchannel_infos);
                    callback(
                        userdata.as_ptr(),
                        cchannel_infos_len,
                        cchannel_infos,
                        ptr::null_mut(),
                    );
                },
                Err(err) => {
                    let err = err_to_c_string(err);
                    callback(userdata.as_ptr(), 0, ptr::null_mut(), err);
                },
            }
        },
    ))));
    ret
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_channels_next_finish(
    geelightning: *mut Geelightning,
    node_channels_next_future: *mut NodeChannelsNextFuture,
) {
    unsafe { drop_handle(geelightning, node_channels_next_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_channels_free(
    geelightning: *mut Geelightning,
    node_channels_stream: *mut NodeChannelsStream,
) {
    let node_channels_stream = SmuggleMut(node_channels_stream);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node_channels_stream) = node_channels_stream;
        let _node_channels_stream = unsafe { Box::from_raw(node_channels_stream) };
    });
}

/*
type NodeBalanceStreamInner = impl Stream<Item = Result<Balance, ErrorString>> + Send + 'static;
type NodeBalanceStream = Pin<Box<NodeBalanceStreamInner>>;
*/
type NodeBalanceStream = Pin<Box<
    dyn Stream<Item = Result<Balance, ErrorString>> + Send + 'static
>>;

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_balance(
    _geelightning: *mut Geelightning,
    node: *mut Node,
) -> *mut NodeBalanceStream {
    let node = unsafe { &mut *node };
    let balance_res_stream = node.balance().map_err(|err| err.into());
    Box::into_raw(Box::new(Box::pin(balance_res_stream)))
}

//pub type NodeBalanceNextFuture = impl Handle;

#[repr(transparent)]
pub struct NodeBalanceNextFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_balance_next(
    geelightning: *mut Geelightning,
    node_balance_stream: *mut NodeBalanceStream,
    callback: extern "C" fn(*const c_void, u64, u64, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeBalanceNextFuture {
    let runtime = unsafe { &mut *geelightning };
    let node_balance_stream = SmuggleMut(node_balance_stream);
    let userdata = Smuggle(userdata);
    let ret = Box::into_raw(Box::new(NodeBalanceNextFuture(run_future(
        runtime,
        async move {
            let node_balance_stream_mut = unsafe { node_balance_stream.as_mut() };
            let balance_res = node_balance_stream_mut.next().await.unwrap();
            balance_res
        },
        move |balance_res: Result<Balance, ErrorString>| match balance_res {
            Ok(balance) => {
                callback(
                    userdata.as_ptr(),
                    balance.unconfirmed.to_u64(),
                    balance.confirmed.to_u64(),
                    ptr::null_mut(),
                );
            },
            Err(err) => {
                let err = err_to_c_string(err);
                callback(userdata.as_ptr(), 0, 0, err);
            },
        },
    ))));
    ret
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_balance_next_finish(
    geelightning: *mut Geelightning,
    node_balance_next_future: *mut NodeBalanceNextFuture,
) {
    unsafe { drop_handle(geelightning, node_balance_next_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_balance_free(
    geelightning: *mut Geelightning,
    node_balance_stream: *mut NodeBalanceStream,
) {
    let node_balance_stream = SmuggleMut(node_balance_stream);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node_balance_stream) = node_balance_stream;
        let _node_balance_stream = unsafe { Box::from_raw(node_balance_stream) };
    });
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_get_listen_endpoint(
    geelightning: *mut Geelightning,
    node: *mut Node,
    peer_id_out: *mut u8,
    addr_out: *mut libc::sockaddr,
) {
    let runtime = unsafe { &mut *geelightning };
    let node = SmuggleMut(node);
    let endpoint = runtime.block_on(async move {
        let node = unsafe { node.as_mut() };
        node.get_listen_endpoint().await
    });
    let peer_id_out = unsafe { slice::from_raw_parts_mut(peer_id_out, PEER_ID_BYTES_LEN) };
    peer_id_out.clone_from_slice(&endpoint.peer_id.to_bytes()[..]);
    let addr_out = unsafe { &mut *addr_out };
    endpoint.addr.to_libc(addr_out)
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_offer_endpoint(
    geelightning: *mut Geelightning,
    node: *mut Node,
    peer_id: *const u8,
    addr: *const libc::sockaddr,
    error_out: *mut *mut c_char,
) {
    let error_out = unsafe { &mut *error_out };
    *error_out = ptr::null_mut();

    let peer_id = {
        let peer_id = unsafe { ptr::read(peer_id as *const [u8; PEER_ID_BYTES_LEN]) };
        match PeerId::try_from_bytes(peer_id) {
            Ok(peer_id) => peer_id,
            Err(err) => {
                *error_out = err_to_c_string(err);
                return;
            },
        }
    };
    let addr = {
        let addr = unsafe { &*addr };
        match SocketAddr::try_from_libc(addr) {
            Ok(addr) => addr,
            Err(err) => {
                *error_out = err_to_c_string(err);
                return;
            },
        }
    };
    let endpoint = Endpoint { peer_id, addr };
    let node = SmuggleMut(node);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let node = unsafe { node.as_mut() };
        node.offer_endpoint(endpoint).await;
    })
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_cancel_channel(
    geelightning: *mut Geelightning,
    node: *mut Node,
    channel_id: *const u8,
    error_out: *mut *mut c_char,
) {
    let error_out = unsafe { &mut *error_out };
    *error_out = ptr::null_mut();

    let channel_id = {
        let channel_id = unsafe { ptr::read(channel_id as *const [u8; CHANNEL_ID_BYTES_LEN]) };
        ChannelId::from_bytes(channel_id)
    };
    let node = SmuggleMut(node);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let node = unsafe { node.as_mut() };
        match node.cancel_channel(channel_id).await {
            Ok(()) => (),
            Err(err) => {
                *error_out = err_to_c_string(err);
            },
        }
    })
}

/*
pub type NodeLocalConfirmChannelGeneratorInner =
    impl AsyncGenerator<
        Yield = ErrorString,
        Return = Result<(), ErrorString>,
    >
    + FusedAsyncGenerator
    + Send
    + 'static;
pub type NodeLocalConfirmChannelGenerator = Pin<Box<NodeLocalConfirmChannelGeneratorInner>>;
*/
pub type NodeLocalConfirmChannelGenerator = Pin<Box<
    dyn FusedAsyncGenerator<
        Yield = ErrorString,
        Return = Result<(), ErrorString>,
    >
    + Send
    + 'static
>>;

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_local_confirm_channel(
    _geelightning: *mut Geelightning,
    node: *mut Node,
    channel_id: *const u8,
) -> *mut NodeLocalConfirmChannelGenerator {
    let node = SmuggleMut(node);

    let channel_id = {
        let channel_id = unsafe { ptr::read(channel_id as *const [u8; CHANNEL_ID_BYTES_LEN]) };
        ChannelId::from_bytes(channel_id)
    };

    let future = async move {
        let node = unsafe { node.as_mut() };
        let local_confirm_channel_generator = node.local_confirm_channel(channel_id);
        Ok(local_confirm_channel_generator
            .map_err(|e| e.into())
            .map_yield(|e| e.into()))
    };
    let generator = future.try_flatten_async_generator().fuse();
    Box::into_raw(Box::new(Box::pin(generator)))
}

//pub type NodeLocalConfirmChannelResumeFuture = impl Handle;

#[repr(transparent)]
pub struct NodeLocalConfirmChannelResumeFuture(HandleImpl);

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_local_confirm_channel_resume(
    geelightning: *mut Geelightning,
    node_local_confirm_channel_generator: *mut NodeLocalConfirmChannelGenerator,
    callback: extern "C" fn(*const c_void, *mut c_char, *mut c_char),
    userdata: *const c_void,
) -> *mut NodeLocalConfirmChannelResumeFuture {
    let userdata = Smuggle(userdata);
    let map = move |state| match state {
        GeneratorState::Complete(Ok(())) => {
            callback(userdata.as_ptr(), ptr::null_mut(), ptr::null_mut());
        },
        GeneratorState::Complete(Err(err)) => {
            let err = err_to_c_string(err);
            callback(userdata.as_ptr(), err, ptr::null_mut());
        },
        GeneratorState::Yielded(err) => {
            let err = err_to_c_string(err);
            callback(userdata.as_ptr(), ptr::null_mut(), err);
        },
    };
    let ret =
        unsafe { next_generator_state(geelightning, node_local_confirm_channel_generator, map) };
    Box::into_raw(Box::new(NodeLocalConfirmChannelResumeFuture(ret)))
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_local_confirm_channel_resume_finish(
    geelightning: *mut Geelightning,
    node_local_confirm_channel_resume_future: *mut NodeLocalConfirmChannelResumeFuture,
) {
    unsafe { drop_handle(geelightning, node_local_confirm_channel_resume_future as *mut _) };
}

#[no_mangle]
pub unsafe extern "C" fn GEELIGHTNING_node_local_confirm_channel_free(
    geelightning: *mut Geelightning,
    node_local_confirm_channel_generator: *mut NodeLocalConfirmChannelGenerator,
) {
    let node_local_confirm_channel_generator = SmuggleMut(node_local_confirm_channel_generator);
    let runtime = unsafe { &mut *geelightning };
    runtime.block_on(async move {
        let SmuggleMut(node_local_confirm_channel_generator) = node_local_confirm_channel_generator;
        let _node_local_confirm_channel_generator =
            unsafe { Box::from_raw(node_local_confirm_channel_generator) };
    });
}

#[cfg(test)]
#[test]
fn test() {
    use geelightning::crypto::SecretKey;
    use std::{
        ffi::{CStr, CString},
        sync::mpsc,
        thread,
    };
    use tempdir::TempDir;

    macro_rules! uncallbackify(
        (($($name:ident: $t:ty,)*), $geelightning:expr, $code:expr) => {{
            let (args_send, args_recv) = mpsc::channel::<($($t,)*)>();
            let args_send = Box::into_raw(Box::new(args_send)) as *const c_void;
            extern "C" fn callback(userdata: *const c_void, $($name: $t),*) {
                let args_send: mpsc::Sender<_> = unsafe { *Box::from_raw(userdata as *mut _) };
                args_send.send(($($name,)*)).unwrap();
            }
            let handle = $code(callback, args_send);
            let ret = args_recv.recv().unwrap();
            unsafe { drop_handle($geelightning, handle as *mut _) };
            ret
        }}
    );

    let mut error = ptr::null_mut();
    let geelightning = unsafe { GEELIGHTNING_init(&mut error) };
    if !error.is_null() {
        let error = unsafe { CStr::from_ptr(error) }.to_str().unwrap();
        panic!("GEELIGHTNING_init returned an error: {}", error);
    }

    let temp_dir = TempDir::new("geelightning-ffi-test").unwrap();
    let base_directory = CString::new(temp_dir.path().to_str().unwrap()).unwrap();
    let base_directory = base_directory.as_ptr();
    let peer_secret_key = SecretKey::new().to_bytes();
    let (node, error_stream, error) = uncallbackify!(
        (
            node: *mut Node,
            error_stream: *mut ErrorStream,
            error: *mut c_char,
        ),
        geelightning,
        |callback, userdata| {
            unsafe {
                GEELIGHTNING_node_new(
                    geelightning,
                    base_directory,
                    peer_secret_key.as_ptr(),
                    callback,
                    userdata,
                )
            }
        }
    );
    let smuggle_geelightning = SmuggleMut(geelightning);
    let smuggle_error_stream = SmuggleMut(error_stream);
    let error_stream_thread = thread::spawn(move || {
        let SmuggleMut(geelightning) = smuggle_geelightning;
        let SmuggleMut(error_stream) = smuggle_error_stream;
        loop {
            let (error,) =
                uncallbackify!((error: *mut c_char,), geelightning, |callback, userdata| {
                    unsafe {
                        GEELIGHTNING_error_stream_next(
                            geelightning,
                            error_stream,
                            callback,
                            userdata,
                        )
                    }
                });
            if !error.is_null() {
                let error = unsafe { CStr::from_ptr(error) }.to_str().unwrap();
                println!("GEELIGHTNING_error_stream_next yielded an error: {}", error);
                continue;
            }
            break;
        }
    });
    if !error.is_null() {
        let error = unsafe { CStr::from_ptr(error) }.to_str().unwrap();
        panic!("GEELIGHTNING_node_new returned an error: {}", error);
    }

    let balance_stream = unsafe { GEELIGHTNING_node_balance(geelightning, node) };
    let (unconfirmed_msat, confirmed_msat, error) = uncallbackify!(
        (
            unconfirmed_msat: u64,
            confirmed_msat: u64,
            error: *mut c_char,
        ),
        geelightning,
        |callback, userdata| {
            unsafe {
                GEELIGHTNING_node_balance_next(geelightning, balance_stream, callback, userdata)
            }
        }
    );
    if !error.is_null() {
        let error = unsafe { CStr::from_ptr(error) }.to_str().unwrap();
        panic!(
            "GEELIGHTNING_node_balance_next returned an error: {}",
            error
        );
    }
    assert_eq!(unconfirmed_msat, 0);
    assert_eq!(confirmed_msat, 0);

    unsafe {
        GEELIGHTNING_node_free(geelightning, node);
    }

    error_stream_thread.join().unwrap();
    unsafe {
        GEELIGHTNING_error_stream_free(geelightning, error_stream);
        GEELIGHTNING_quit(geelightning);
    }
}
