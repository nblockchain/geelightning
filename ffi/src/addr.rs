use super::*;

#[derive(Debug, Fail)]
#[fail(display = "invalid address family: {}", family)]
pub struct InvalidAddrFamily {
    family: libc::sa_family_t,
}

pub trait SocketAddrExt {
    fn to_libc(&self, sockaddr_out: &mut libc::sockaddr);
    fn try_from_libc(sockaddr: &libc::sockaddr) -> Result<SocketAddr, InvalidAddrFamily>;
}

impl SocketAddrExt for SocketAddr {
    fn to_libc(&self, sockaddr_out: &mut libc::sockaddr) {
        match self {
            SocketAddr::V4(socket_addr_v4) => {
                let sockaddr_v4_out = sockaddr_out as *mut libc::sockaddr as *mut libc::sockaddr_in;
                let sockaddr_v4_out = unsafe { &mut *sockaddr_v4_out };
                *sockaddr_v4_out = libc::sockaddr_in {
                    sin_family: libc::AF_INET as u16,
                    sin_port: socket_addr_v4.port().to_be(),
                    sin_addr: libc::in_addr {
                        s_addr: u32::from(*socket_addr_v4.ip()).to_be(),
                    },
                    sin_zero: [0u8; 8],
                };
            },
            SocketAddr::V6(socket_addr_v6) => {
                let sockaddr_v6_out =
                    sockaddr_out as *mut libc::sockaddr as *mut libc::sockaddr_in6;
                let sockaddr_v6_out = unsafe { &mut *sockaddr_v6_out };
                *sockaddr_v6_out = libc::sockaddr_in6 {
                    sin6_family: libc::AF_INET6 as u16,
                    sin6_port: socket_addr_v6.port().to_be(),
                    sin6_flowinfo: socket_addr_v6.flowinfo(),
                    sin6_addr: libc::in6_addr {
                        s6_addr: socket_addr_v6.ip().octets(),
                    },
                    sin6_scope_id: socket_addr_v6.scope_id(),
                };
            },
        }
    }

    fn try_from_libc(sockaddr: &libc::sockaddr) -> Result<SocketAddr, InvalidAddrFamily> {
        match i32::from(sockaddr.sa_family) {
            libc::AF_INET => {
                let sockaddr_v4 = sockaddr as *const libc::sockaddr as *const libc::sockaddr_in;
                let sockaddr_v4 = unsafe { &*sockaddr_v4 };
                let ip = Ipv4Addr::from(u32::from_be(sockaddr_v4.sin_addr.s_addr));
                let port = u16::from_be(sockaddr_v4.sin_port);
                Ok(SocketAddr::V4(SocketAddrV4::new(ip, port)))
            },
            libc::AF_INET6 => {
                let sockaddr_v6 = sockaddr as *const libc::sockaddr as *const libc::sockaddr_in6;
                let sockaddr_v6 = unsafe { &*sockaddr_v6 };
                let ip = Ipv6Addr::from(sockaddr_v6.sin6_addr.s6_addr);
                let port = u16::from_be(sockaddr_v6.sin6_port);
                let flowinfo = sockaddr_v6.sin6_flowinfo;
                let scope_id = sockaddr_v6.sin6_scope_id;
                Ok(SocketAddr::V6(SocketAddrV6::new(
                    ip, port, flowinfo, scope_id,
                )))
            },
            _ => Err(InvalidAddrFamily {
                family: sockaddr.sa_family,
            }),
        }
    }
}
