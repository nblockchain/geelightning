use super::*;

// NOTE: See the comment in lib.rs regarding feature(type_alias_impl_trait) for why this is
// commented.
/*
pub trait Handle {
    fn drop_and_complete(self, runtime: &Runtime);
}
*/

pub struct HandleImpl {
    drop_send: DropSender,
    complete_recv: DropReceiver,
}

/*
impl Handle for HandleImpl {
    fn drop_and_complete(self, runtime: &Runtime) {
        let HandleImpl {
            drop_send,
            complete_recv,
        } = self;
        drop(drop_send);
        runtime.block_on(complete_recv);
    }
}
*/

impl HandleImpl {
    fn drop_and_complete(self, runtime: &mut Runtime) {
        let HandleImpl {
            drop_send,
            complete_recv,
        } = self;
        runtime.enter(|| drop(drop_send));
        runtime.block_on(complete_recv);
    }
}

pub unsafe fn drop_handle(geelightning: *mut Geelightning, handle: *mut HandleImpl) {
    let runtime = unsafe { &mut *geelightning };
    let handle = unsafe { Box::from_raw(handle) };
    handle.drop_and_complete(runtime);
}

pub fn run_future<F, C>(runtime: &Runtime, future: F, callback: C) -> HandleImpl
where
    F: Future + Send + 'static,
    C: FnOnce(F::Output) + Send + 'static,
{
    runtime.enter(move || {
        let drop_send = DropSender::new();
        let mut drop_recv = drop_send.receiver();
        let complete_send = DropSender::new();
        let complete_recv = complete_send.receiver();
        tokio::spawn(async move {
            let future = future.fuse();
            pin_mut!(future);
            futures::select! {
                ret = future => callback(ret),
                () = drop_recv => (),
            }
            drop(complete_send);
        });
        HandleImpl {
            drop_send,
            complete_recv,
        }
    })
}

pub unsafe fn next_generator_state<G: ?Sized, C>(
    geelightning: *mut Geelightning,
    async_generator: *mut Pin<Box<G>>,
    callback: C,
) -> HandleImpl
where
    G: FusedAsyncGenerator + Send + 'static,
    C: FnOnce(GeneratorState<G::Yield, G::Return>) + Send + 'static,
{
    let runtime = unsafe { &*geelightning };
    let async_generator = SmuggleMut(async_generator);
    runtime.enter(move || {
        let drop_send = DropSender::new();
        let mut drop_recv = drop_send.receiver();
        let complete_send = DropSender::new();
        let complete_recv = complete_send.receiver();
        tokio::spawn(async move {
            let async_generator = unsafe { async_generator.as_mut() };
            let future = async_generator.next_state();
            pin_mut!(future);
            futures::select! {
                ret = future => callback(ret),
                () = drop_recv => (),
            }
            drop(complete_send);
        });
        HandleImpl {
            drop_send,
            complete_recv,
        }
    })
}
