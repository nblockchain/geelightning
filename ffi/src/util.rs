use super::*;

pub fn err_to_c_string<E: ToString>(err: E) -> *mut c_char {
    let mut err_string = err.to_string();
    err_string.push('\0');

    unsafe { libc::strdup(err_string.as_ptr() as *const c_char) }
}

pub trait Nullable {
    fn null() -> Self;
}

impl<T> Nullable for *mut T {
    fn null() -> *mut T {
        ptr::null_mut()
    }
}

impl Nullable for () {
    fn null() {}
}

#[derive(Debug)]
pub struct ErrorString {
    string: String,
}

impl<T> From<T> for ErrorString
where
    T: fmt::Display,
{
    fn from(val: T) -> ErrorString {
        ErrorString {
            string: format!("{}", val),
        }
    }
}

impl ToString for ErrorString {
    fn to_string(&self) -> String {
        self.string.clone()
    }
}

// Rust's raw pointers don't implement Send or Sync. This makes it impossible to move them into or
// carry them inside a Future unless they're wrapped in something which does. Smuggle is a simple
// wrapper struct for smuggling const pointers into Futures.
pub struct Smuggle<T>(pub *const T);

unsafe impl<T> Send for Smuggle<T> {}
unsafe impl<T> Sync for Smuggle<T> {}
impl<T> Copy for Smuggle<T> {}

impl<T> Clone for Smuggle<T> {
    fn clone(&self) -> Smuggle<T> {
        let Smuggle(ptr) = *self;
        Smuggle(ptr)
    }
}

impl<T> Smuggle<T> {
    pub fn as_ptr(self) -> *const T {
        let Smuggle(ptr) = self;
        ptr
    }
}

// Rust's raw pointers don't implement Send or Sync. This makes it impossible to move them into or
// carry them inside a Future unless they're wrapped in something which does. SmuggleMut is a
// simple wrapper struct for smuggling mut pointers into Futures.
pub struct SmuggleMut<T>(pub *mut T);

unsafe impl<T> Send for SmuggleMut<T> {}
unsafe impl<T> Sync for SmuggleMut<T> {}
impl<T> Copy for SmuggleMut<T> {}

impl<T> Clone for SmuggleMut<T> {
    fn clone(&self) -> SmuggleMut<T> {
        let SmuggleMut(ptr) = *self;
        SmuggleMut(ptr)
    }
}

impl<T> SmuggleMut<T> {
    pub unsafe fn as_mut<'a>(self) -> &'a mut T {
        let SmuggleMut(ptr) = self;
        unsafe {
            &mut *ptr
        }
    }
}
