set -ex

REPO_DIR=$(git rev-parse --git-dir)/..
RUST_VERSION=$(cat $REPO_DIR/rust-toolchain | xargs)

apt-get install gcc -y
if command -v rustup; then
    rustup update $RUST_VERSION
else
    curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain $RUST_VERSION
fi
