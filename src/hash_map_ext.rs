use super::*;

#[ext_trait]
impl<K, V> HashMapExt<K, V> for HashMap<K, V> {
    /// Get a reference to a random key in the `HashMap`. The implementation is inefficient, so
    /// don't use this in a loop on a large `HashMap`.
    fn get_random_key(&self) -> Option<&K> {
        let len = self.len();
        if len == 0 {
            return None;
        }

        let index = rand::thread_rng().gen_range(0, len);
        self.keys().nth(index)
    }
}
