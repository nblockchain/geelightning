use super::*;

pub const PUBLIC_KEY_BYTES_LEN: usize = 33;
pub const SECRET_KEY_BYTES_LEN: usize = 32;
pub const SIGNATURE_COMPACT_BYTES_LEN: usize = 64;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct PublicKey {
    key: secp256k1::PublicKey,
}

#[derive(Clone, PartialEq, Eq)]
pub struct SecretKey {
    // TODO: Protect secret keys better. eg. use zero-on-drop, keep them in protected memory, etc.
    key: Arc<secp256k1::SecretKey>,
}

impl fmt::Debug for SecretKey {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let public = PublicKey::from_secret_key(self);
        formatter
            .debug_struct("SecretKey")
            .field("public", &public)
            .finish()
    }
}

impl Serialize for SecretKey {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let bytes = self.to_bytes();
        bytes.serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for SecretKey {
    fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<SecretKey, D::Error> {
        let bytes = Deserialize::deserialize(deserializer)?;
        let secret_key = SecretKey::try_from_bytes(bytes).map_err(serde::de::Error::custom)?;
        Ok(secret_key)
    }
}

#[allow(clippy::derive_hash_xor_eq)]
impl std::hash::Hash for SecretKey {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.to_bytes().hash(state)
    }
}

pub struct SharedSecret {
    shared_secret: Arc<secp256k1::ecdh::SharedSecret>,
}

#[allow(clippy::derive_hash_xor_eq)]
#[derive(Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Signature {
    signature: secp256k1::Signature,
}

#[allow(clippy::derive_hash_xor_eq)]
impl std::hash::Hash for Signature {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.serialize_compact().hash(state)
    }
}

#[derive(Clone, Debug, Error)]
#[error("error parsing public key: {}", source)]
pub struct PublicKeyParseError {
    source: secp256k1::Error,
}

#[derive(Clone, Debug, Error)]
#[error("error parsing secret key: {}", source)]
pub struct SecretKeyParseError {
    source: secp256k1::Error,
}

#[derive(Debug, Error)]
#[error("error verifying signature: {}", source)]
pub struct SignatureError {
    source: secp256k1::Error,
}

impl PublicKey {
    pub fn to_secp256k1_public_key(&self) -> secp256k1::PublicKey {
        self.key
    }

    pub fn from_secret_key(secret_key: &SecretKey) -> PublicKey {
        PublicKey {
            key: secp256k1::PublicKey::from_secret_key(&SECP256K1, &secret_key.key),
        }
    }

    pub fn try_from_bytes(
        bytes: [u8; PUBLIC_KEY_BYTES_LEN],
    ) -> Result<PublicKey, PublicKeyParseError> {
        Ok(PublicKey {
            key: secp256k1::PublicKey::from_slice(&bytes)
                .map_err(|source| PublicKeyParseError { source })?,
        })
    }

    pub fn to_bytes(&self) -> [u8; PUBLIC_KEY_BYTES_LEN] {
        self.key.serialize()
    }

    pub fn verify_signature(
        &self,
        hash: SigHash,
        signature: &Signature,
    ) -> Result<(), SignatureError> {
        let message = secp256k1::Message::from_slice(hash.as_ref()).unwrap();
        SECP256K1
            .verify(&message, &signature.signature, &self.key)
            .map_err(|source| SignatureError { source })
    }

    pub fn tweak_add_exp(self, secret_key: &SecretKey) -> PublicKey {
        let mut key = self.key;
        key.add_exp_assign(&SECP256K1, &secret_key.key[..]).unwrap();
        PublicKey { key }
    }

    pub fn tweak_add(self, public_key: &PublicKey) -> PublicKey {
        let key = self.key.combine(&public_key.key).unwrap();
        PublicKey { key }
    }

    pub fn tweak_mul(self, secret_key: &SecretKey) -> PublicKey {
        let mut key = self.key;
        key.mul_assign(&SECP256K1, &secret_key.key[..]).unwrap();
        PublicKey { key }
    }
}

impl fmt::Debug for PublicKey {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.to_bytes();
        let bytes = format!("{:?}", HexDebug(&bytes));

        fmt.debug_tuple("public_key!").field(&bytes).finish()
    }
}

impl PartialOrd for PublicKey {
    fn partial_cmp(&self, other: &PublicKey) -> Option<cmp::Ordering> {
        self.to_bytes().partial_cmp(&other.to_bytes())
    }
}

impl SecretKey {
    pub fn new() -> SecretKey {
        SecretKey {
            key: Arc::new(secp256k1::SecretKey::new(&mut rand::thread_rng())),
        }
    }

    pub fn to_bytes(&self) -> [u8; SECRET_KEY_BYTES_LEN] {
        TryFrom::try_from(&self.key[..]).unwrap()
    }

    pub fn try_from_bytes(
        bytes: [u8; SECRET_KEY_BYTES_LEN],
    ) -> Result<SecretKey, SecretKeyParseError> {
        Ok(SecretKey {
            key: Arc::new(
                secp256k1::SecretKey::from_slice(&bytes)
                    .map_err(|source| SecretKeyParseError { source })?,
            ),
        })
    }

    pub fn from_hash(hash: sha256::Hash) -> SecretKey {
        SecretKey {
            key: Arc::new(secp256k1::SecretKey::from_slice(hash.as_ref()).unwrap()),
        }
    }

    pub fn sign_digest(&self, hash: SigHash) -> Signature {
        let message = secp256k1::Message::from_slice(hash.as_ref()).unwrap();
        let signature = SECP256K1.sign(&message, &self.key);
        Signature { signature }
    }

    pub fn tweak_add(&self, other: &SecretKey) -> SecretKey {
        let mut key = *self.key;
        key.add_assign(&other.key[..]).unwrap();
        SecretKey { key: Arc::new(key) }
    }

    pub fn tweak_mul(&self, other: &SecretKey) -> SecretKey {
        let mut key = *self.key;
        key.mul_assign(&other.key[..]).unwrap();
        SecretKey { key: Arc::new(key) }
    }
}

impl SharedSecret {
    pub fn new(public_key: &PublicKey, secret_key: &SecretKey) -> SharedSecret {
        SharedSecret {
            shared_secret: Arc::new(secp256k1::ecdh::SharedSecret::new(
                &public_key.key,
                &secret_key.key,
            )),
        }
    }

    pub fn as_slice(&self) -> &[u8] {
        &self.shared_secret[..]
    }
}

impl Signature {
    pub fn from_compact(
        bytes: [u8; SIGNATURE_COMPACT_BYTES_LEN],
    ) -> Result<Signature, secp256k1::Error> {
        Ok(Signature {
            signature: secp256k1::Signature::from_compact(&bytes)?,
        })
    }

    pub fn deserialize_der(bytes: &[u8]) -> Result<Signature, secp256k1::Error> {
        Ok(Signature {
            signature: secp256k1::Signature::from_der(bytes)?,
        })
    }

    pub fn serialize_der(&self) -> secp256k1::SerializedSignature {
        self.signature.serialize_der()
    }

    pub fn serialize_compact(&self) -> [u8; SIGNATURE_COMPACT_BYTES_LEN] {
        self.signature.serialize_compact()
    }
}

impl fmt::Debug for Signature {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.serialize_compact();
        let bytes = format!("{:?}", HexDebug(&bytes));

        fmt.debug_tuple("signature!").field(&bytes).finish()
    }
}
