use super::*;

mod key;

lazy_static! {
    pub static ref SECP256K1: secp256k1::Secp256k1<secp256k1::All> = secp256k1::Secp256k1::new();
}

pub use self::key::{
    PublicKey, PublicKeyParseError, SecretKey, SecretKeyParseError, SharedSecret, Signature,
    SignatureError, PUBLIC_KEY_BYTES_LEN, SECRET_KEY_BYTES_LEN, SIGNATURE_COMPACT_BYTES_LEN,
};
