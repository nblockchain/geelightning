//! This module provides a 48 bit unsigned integer type, implemented by wrapping a `u64` and
//! enforcing that the most significant 16 bits are always zero.

use super::*;

pub const BIT_MASK: u64 = 0x0000_ffff_ffff_ffff;

#[derive(PartialEq, Eq, Clone, Copy, Serialize, Deserialize, Hash)]
pub struct U48 {
    val: u64,
}

#[macro_export]
macro_rules! u48 {
    ($v:expr) => {{
        const VAL: u64 = $v;
        // compile-time assert that the value fits in 48 bits
        const _: [(); (VAL & !$crate::u48::BIT_MASK) as usize] = [];
        U48::from_be_bytes([
            ((VAL >> (5 * 8)) & 0xff) as u8,
            ((VAL >> (4 * 8)) & 0xff) as u8,
            ((VAL >> (3 * 8)) & 0xff) as u8,
            ((VAL >> (2 * 8)) & 0xff) as u8,
            ((VAL >> (1 * 8)) & 0xff) as u8,
            (VAL & 0xff) as u8,
        ])
    }};
}

impl U48 {
    pub const MAX: U48 = U48 { val: BIT_MASK };

    pub fn to_be_bytes(self) -> [u8; 6] {
        let bytes = self.val.to_be_bytes();
        [bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]]
    }

    pub fn from_be_bytes(bytes: [u8; 6]) -> U48 {
        U48 {
            val: u64::from_be_bytes([
                0x00, 0x00, bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5],
            ]),
        }
    }
}

#[allow(clippy::suspicious_arithmetic_impl)]
impl ops::Add<U48> for U48 {
    type Output = U48;

    fn add(self, other: U48) -> U48 {
        let val = self.val + other.val;
        let val = if cfg!(debug_assertions) {
            assert!(val <= BIT_MASK);
            val
        } else {
            val & BIT_MASK
        };
        U48 { val }
    }
}

impl ops::Sub<U48> for U48 {
    type Output = U48;

    fn sub(self, other: U48) -> U48 {
        U48 {
            val: (self.val - other.val) & BIT_MASK,
        }
    }
}

impl ops::Shr<usize> for U48 {
    type Output = U48;

    fn shr(self, other: usize) -> U48 {
        U48 {
            val: self.val >> other,
        }
    }
}

impl ops::BitAnd<U48> for U48 {
    type Output = U48;

    fn bitand(self, other: U48) -> U48 {
        U48 {
            val: self.val & other.val,
        }
    }
}

impl TryFrom<U48> for u32 {
    type Error = TryFromIntError;

    fn try_from(val: U48) -> Result<u32, TryFromIntError> {
        u32::try_from(val.val)
    }
}

macro_rules! fmt_impl (
    ($t:path) => {
        impl $t for U48 {
            fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
                <u64 as $t>::fmt(&self.val, fmt)
            }
        }
    }
);

fmt_impl!(fmt::Binary);
fmt_impl!(fmt::Debug);
fmt_impl!(fmt::Display);
fmt_impl!(fmt::LowerHex);
fmt_impl!(fmt::Octal);
fmt_impl!(fmt::UpperHex);
