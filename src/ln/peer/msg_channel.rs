use super::*;

/// A MsgChannel is used to send/receive messages to a remote peer. Only one MsgChannel to a peer
/// exists at a time, but it can be used through a shared reference by many tasks at once. When
/// there is an error on the channel (eg. if the underlying TCP connection goes down) the error is
/// returned via a side-channel (the MsgChannelDriver) and any tasks trying to send or receive on
/// the channel will block indefinitely. These tasks then need to be destroyed in order to release
/// the MsgChannel so that it can be destroyed and re-created by the `MsgChannelManager` (see the
/// `msg_channel_manager` module).

pub struct MsgChannel {
    pub msg_channel_send: MsgChannelSend,
    pub msg_subscribers: MsgSubscribers,
}

pub struct MsgSubscribers {
    pub error_msg_subscriber: Subscriber<ChannelId, ErrorMsg>,
    pub accept_channel_msg_subscriber: Subscriber<TemporaryChannelId, AcceptChannelMsg>,
    pub funding_created_msg_subscriber: Subscriber<TemporaryChannelId, FundingCreatedMsg>,
    pub funding_signed_msg_subscriber: Subscriber<ChannelId, FundingSignedMsg>,
}

pub struct MsgReceivers {
    pub ping_msg_recv: mpsc::Receiver<PingMsg>,
    pub pong_msg_recv: mpsc::Receiver<PongMsg>,
    pub open_channel_msg_recv: mpsc::Receiver<OpenChannelMsg>,
    pub funding_locked_msg_recv: mpsc::Receiver<FundingLockedMsg>,
    pub error_msg_recv: mpsc::Receiver<ErrorMsg>,
}

struct MsgPublishers {
    error_msg_publisher: Publisher<ChannelId, ErrorMsg>,
    accept_channel_msg_publisher: Publisher<TemporaryChannelId, AcceptChannelMsg>,
    funding_created_msg_publisher: Publisher<TemporaryChannelId, FundingCreatedMsg>,
    funding_signed_msg_publisher: Publisher<ChannelId, FundingSignedMsg>,
}

pub struct MsgSenders {
    pub ping_msg_send: mpsc::Sender<PingMsg>,
    pub pong_msg_send: mpsc::Sender<PongMsg>,
    pub open_channel_msg_send: mpsc::Sender<OpenChannelMsg>,
    pub funding_locked_msg_send: mpsc::Sender<FundingLockedMsg>,
    pub error_msg_send: mpsc::Sender<ErrorMsg>,
}

pub struct MsgChannelSend {
    msg_send: mpsc::Sender<(Msg, oneshot::Sender<()>)>,
}

fn msg_channels() -> (MsgSenders, MsgReceivers) {
    let (ping_msg_send, ping_msg_recv) = mpsc_channel!();
    let (pong_msg_send, pong_msg_recv) = mpsc_channel!();
    let (open_channel_msg_send, open_channel_msg_recv) = mpsc_channel!();
    let (funding_locked_msg_send, funding_locked_msg_recv) = mpsc_channel!();
    let (error_msg_send, error_msg_recv) = mpsc_channel!();
    let msg_senders = MsgSenders {
        ping_msg_send,
        pong_msg_send,
        open_channel_msg_send,
        funding_locked_msg_send,
        error_msg_send,
    };
    let msg_receivers = MsgReceivers {
        ping_msg_recv,
        pong_msg_recv,
        open_channel_msg_recv,
        funding_locked_msg_recv,
        error_msg_recv,
    };
    (msg_senders, msg_receivers)
}

impl MsgChannelSend {
    pub async fn send_msg(&self, msg: Msg) {
        let (reply_send, reply_recv) = oneshot_channel!();
        let mut msg_send = self.msg_send.clone();
        msg_send.send((msg, reply_send)).await.unwrap();
        reply_recv.await.unwrap()
    }
}

#[derive(Debug, Error)]
pub enum MsgChannelBrokeError {
    #[error("io error on encrypted transport stream: {}", source)]
    Io { source: io::Error },
    #[error("protocol violation by remote peer: {}", source)]
    ProtocolViolation { source: ProtocolViolationError },
    #[error("remote peer disconnected")]
    PeerDisconnected,
}

#[derive(Debug, Error)]
pub enum ProtocolViolationError {
    #[error("error decrypting and authenticating received message")]
    Crypto,
    #[error("error deserializing received message: {}", source)]
    Deserialize { source: MsgDeserializeError },
}

#[derive(Debug, Error)]
pub enum InitiateError {
    #[error("encrypted transport message send failed: {}", source)]
    TransportSend { source: io::Error },
    #[error("encrypted transport message receive failed: {}", source)]
    TransportRecv { source: handshake::RecvMsgError },
    #[error("remote peer disconnected before sending init msg")]
    PeerDisconnected,
    #[error("error deserializing received init msg: {}", source)]
    InvalidInitMsg { source: MsgDeserializeError },
    #[error("expected an init msg got msg type {}", msg_type)]
    ExpectedInitMsg { msg_type: MsgType },
    #[error("invalid global features: {}", source)]
    InvalidGlobalFeatures { source: ValidateGlobalFeaturesError },
    #[error("invalid local features: {}", source)]
    InvalidLocalFeatures { source: ValidateLocalFeaturesError },
}

impl InitiateError {
    pub fn is_protocol_violation(&self) -> bool {
        match self {
            InitiateError::TransportSend { .. } | InitiateError::PeerDisconnected => false,
            InitiateError::TransportRecv { source } => source.is_protocol_violation(),
            InitiateError::InvalidInitMsg { .. }
            | InitiateError::ExpectedInitMsg { .. }
            | InitiateError::InvalidGlobalFeatures { .. }
            | InitiateError::InvalidLocalFeatures { .. } => true,
        }
    }
}

//#[rustfmt::skip]
//pub type MsgChannelDriver = impl Future<Output = MsgChannelBrokeError> + Unpin;
// TODO: once impl trait type aliases are stable the dyn can be removed.
pub type MsgChannelDriver = Pin<Box<dyn Future<Output = MsgChannelBrokeError> + Send>>;

impl MsgChannel {
    // Swap init messages on the provided transport. This is required by the lightning protocol
    // before any other messages can be exchanged.
    async fn initiate_channel(transport: &mut Transport) -> Result<(), InitiateError> {
        let our_global_features = GlobalFeatures {};
        let our_local_features = LocalFeatures {
            option_data_loss_protect: FeatureFlag::Required,
            initial_routing_sync: OptionalFeatureFlag::No,
            option_upfront_shutdown_script: FeatureFlag::No,
            gossip_queries: FeatureFlag::No,
        };
        let init_msg = InitMsg {
            global_features: our_global_features.to_unvalidated(),
            local_features: our_local_features.to_unvalidated(),
        };
        let msg_bytes = Msg::Init(init_msg).to_bytes();
        transport
            .transport_send
            .send_msg(msg_bytes)
            .await
            .map_err(|source| InitiateError::TransportSend { source })?;

        let msg_bytes_opt = {
            transport
                .transport_recv
                .recv_msg()
                .await
                .map_err(|source| InitiateError::TransportRecv { source })?
        };
        let msg_bytes = match msg_bytes_opt {
            Some(msg_bytes) => msg_bytes,
            None => return Err(InitiateError::PeerDisconnected),
        };
        let msg = Msg::from_bytes(&msg_bytes)
            .map_err(|source| InitiateError::InvalidInitMsg { source })?;
        let init_msg = match msg {
            Msg::Init(init_msg) => init_msg,
            msg => {
                return Err(InitiateError::ExpectedInitMsg {
                    msg_type: msg.msg_type(),
                })
            }
        };
        let _global_features = {
            init_msg
                .global_features
                .validate(our_global_features)
                .map_err(|source| InitiateError::InvalidGlobalFeatures { source })?
        };
        let _local_features = {
            init_msg
                .local_features
                .validate(our_local_features)
                .map_err(|source| InitiateError::InvalidLocalFeatures { source })?
        };
        Ok(())
    }

    pub async fn from_transport(
        mut transport: Transport,
    ) -> Result<(MsgChannel, MsgChannelDriver, MsgReceivers), InitiateError> {
        MsgChannel::initiate_channel(&mut transport).await?;

        let (msg_send, msg_recv) = mpsc_channel!();
        let (msg_subscribers, msg_publishers) = {
            let (error_msg_subscriber, error_msg_publisher) = subscribe_publish();
            let (accept_channel_msg_subscriber, accept_channel_msg_publisher) = subscribe_publish();
            let (funding_created_msg_subscriber, funding_created_msg_publisher) =
                subscribe_publish();
            let (funding_signed_msg_subscriber, funding_signed_msg_publisher) = subscribe_publish();
            let msg_subscribers = MsgSubscribers {
                error_msg_subscriber,
                accept_channel_msg_subscriber,
                funding_created_msg_subscriber,
                funding_signed_msg_subscriber,
            };
            let msg_publishers = MsgPublishers {
                error_msg_publisher,
                accept_channel_msg_publisher,
                funding_created_msg_publisher,
                funding_signed_msg_publisher,
            };
            (msg_subscribers, msg_publishers)
        };
        let (msg_senders, msg_receivers) = msg_channels();
        let msg_channel_send = MsgChannelSend { msg_send };
        let msg_channel = MsgChannel {
            msg_channel_send,
            msg_subscribers,
        };
        let msg_channel_driver = Box::pin(msg_channel_task(
            transport,
            msg_recv,
            msg_publishers,
            msg_senders,
        ));
        Ok((msg_channel, msg_channel_driver, msg_receivers))
    }
}

async fn msg_channel_task(
    transport: Transport,
    mut msg_recv: mpsc::Receiver<(Msg, oneshot::Sender<()>)>,
    mut msg_publishers: MsgPublishers,
    mut msg_senders: MsgSenders,
) -> MsgChannelBrokeError {
    let Transport {
        mut transport_send,
        mut transport_recv,
    } = transport;

    let sender = async {
        loop {
            let (msg, reply_send) = match msg_recv.next().await {
                Some((msg, reply_send)) => (msg, reply_send),
                None => future::pending().await,
            };
            let msg_bytes = msg.to_bytes();
            let send_res = transport_send.send_msg(msg_bytes).await;
            match send_res {
                Ok(()) => {
                    reply_send.send(()).await.ignore_err();
                }
                Err(source) => {
                    break MsgChannelBrokeError::Io { source };
                }
            }
        }
    };

    let receiver = async {
        loop {
            let msg_bytes_opt_res = transport_recv.recv_msg().await;
            match msg_bytes_opt_res {
                Ok(msg_bytes_opt) => {
                    let msg_bytes = match msg_bytes_opt {
                        Some(msg_bytes) => msg_bytes,
                        None => {
                            break MsgChannelBrokeError::PeerDisconnected;
                        }
                    };
                    let msg = match Msg::from_bytes(&msg_bytes) {
                        Ok(msg) => msg,
                        Err(err) => {
                            if let MsgDeserializeError::UnknownMsgType { source } = &err {
                                if source.can_ignore() {
                                    continue;
                                }
                                // TODO: this should be a hard error.
                                // But while geelightning is still incomplete we need to ignore some
                                // messages for testing against other clients.
                                println!("ignoring unknown msg error: {}", source);
                            }
                            continue;
                            /*
                            break MsgChannelBrokeError::ProtocolViolation(
                                ProtocolViolationError::Deserialize(err),
                            );
                            */
                        }
                    };
                    match msg {
                        Msg::Ping(ping_msg) => {
                            msg_senders.ping_msg_send.send(ping_msg).await.ignore_err();
                        }
                        Msg::Pong(pong_msg) => {
                            msg_senders.pong_msg_send.send(pong_msg).await.ignore_err();
                        }
                        Msg::Error(error_msg) => match error_msg.channel_id {
                            Some(channel_id) => {
                                msg_publishers
                                    .error_msg_publisher
                                    .publish(channel_id, error_msg)
                                    .await;
                            }
                            None => {
                                msg_senders
                                    .error_msg_send
                                    .send(error_msg)
                                    .await
                                    .ignore_err();
                            }
                        },
                        Msg::OpenChannel(open_channel_msg) => {
                            msg_senders
                                .open_channel_msg_send
                                .send(open_channel_msg)
                                .await
                                .ignore_err();
                        }
                        Msg::AcceptChannel(accept_channel_msg) => {
                            msg_publishers
                                .accept_channel_msg_publisher
                                .publish(
                                    accept_channel_msg.temporary_channel_id,
                                    accept_channel_msg,
                                )
                                .await;
                        }
                        Msg::FundingCreated(funding_created_msg) => {
                            msg_publishers
                                .funding_created_msg_publisher
                                .publish(
                                    funding_created_msg.temporary_channel_id,
                                    funding_created_msg,
                                )
                                .await;
                        }
                        Msg::FundingSigned(funding_signed_msg) => {
                            msg_publishers
                                .funding_signed_msg_publisher
                                .publish(funding_signed_msg.channel_id, funding_signed_msg)
                                .await;
                        }
                        Msg::FundingLocked(funding_locked_msg) => msg_senders
                            .funding_locked_msg_send
                            .send(funding_locked_msg)
                            .await
                            .ignore_err(),
                        _ => (),
                    };
                }
                Err(source) => match source {
                    handshake::RecvMsgError::Io { source } => {
                        break MsgChannelBrokeError::Io { source };
                    }
                    handshake::RecvMsgError::InvalidMsg => {
                        break MsgChannelBrokeError::ProtocolViolation {
                            source: ProtocolViolationError::Crypto,
                        };
                    }
                },
            }
        }
    };

    pin_mut!(sender);
    pin_mut!(receiver);

    futures::select! {
        err = sender.fuse() => err,
        err = receiver.fuse() => err,
    }
}
