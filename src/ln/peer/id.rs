use super::*;
use bech32::{FromBase32, ToBase32};

pub const PEER_ID_BYTES_LEN: usize = PUBLIC_KEY_BYTES_LEN;
pub const PEER_SECRET_KEY_BYTES_LEN: usize = SECRET_KEY_BYTES_LEN;
pub const BECH32_HRP: &str = "ln";

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct PeerId {
    public_key: PublicKey,
}

impl PeerId {
    pub fn from_peer_secret_key(peer_secret_key: &PeerSecretKey) -> PeerId {
        PeerId {
            public_key: PublicKey::from_secret_key(peer_secret_key.as_secret_key()),
        }
    }

    pub fn from_public_key(public_key: PublicKey) -> PeerId {
        PeerId { public_key }
    }

    pub fn try_from_bytes(bytes: [u8; PEER_ID_BYTES_LEN]) -> Result<PeerId, PublicKeyParseError> {
        Ok(PeerId {
            public_key: PublicKey::try_from_bytes(bytes)?,
        })
    }

    pub fn to_bytes(&self) -> [u8; PEER_ID_BYTES_LEN] {
        self.public_key.to_bytes()
    }

    pub fn as_public_key(&self) -> &PublicKey {
        &self.public_key
    }

    pub fn as_bech32(&self) -> String {
        let bytes = self.to_bytes();
        bech32::encode(BECH32_HRP, bytes.to_base32()).unwrap()
    }

    pub fn from_bech32(s: &str) -> Result<PeerId, PeerIdFromBech32Error> {
        let (hrp, symbols) =
            bech32::decode(s).map_err(|source| PeerIdFromBech32Error::DecodeBech32 { source })?;
        if hrp != BECH32_HRP {
            return Err(PeerIdFromBech32Error::NotALightningNetworkPeerId { hrp });
        }
        let bytes = Vec::from_base32(&symbols)
            .map_err(|source| PeerIdFromBech32Error::InvalidBytes { source })?;
        let bytes = bytes
            .try_into()
            .ok()
            .ok_or(PeerIdFromBech32Error::InvalidKeyLength)?;
        let public_key = PublicKey::try_from_bytes(bytes)
            .map_err(|source| PeerIdFromBech32Error::InvalidPublicKey { source })?;
        Ok(PeerId { public_key })
    }
}

impl fmt::Display for PeerId {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", HexDebug(&self.public_key.to_bytes()))
    }
}

#[derive(Debug, Error)]
pub enum PeerIdFromBech32Error {
    #[error("failed to decode bech32 string: {}", source)]
    DecodeBech32 { source: bech32::Error },
    #[error(
        "the bech32-encoded peer id does not actually represent a peer id. bech32 \
                   human-readable-part is {:?}, expected \"ln\".",
        hrp
    )]
    NotALightningNetworkPeerId { hrp: String },
    #[error("bech32 string cannot be decoded as bytes: {}", source)]
    InvalidBytes { source: bech32::Error },
    #[error("bech32 encoded peer id has invalid length")]
    InvalidKeyLength,
    #[error("bech32 encoded peer id is not a valid public key: {}", source)]
    InvalidPublicKey { source: PublicKeyParseError },
}

#[derive(Clone, Debug)]
pub struct PeerSecretKey {
    secret_key: SecretKey,
}

impl PeerSecretKey {
    pub fn new() -> PeerSecretKey {
        PeerSecretKey {
            secret_key: SecretKey::new(),
        }
    }

    pub fn from_secret_key(secret_key: SecretKey) -> PeerSecretKey {
        PeerSecretKey { secret_key }
    }

    pub fn try_from_bytes(
        bytes: [u8; PEER_SECRET_KEY_BYTES_LEN],
    ) -> Result<PeerSecretKey, SecretKeyParseError> {
        Ok(PeerSecretKey {
            secret_key: SecretKey::try_from_bytes(bytes)?,
        })
    }

    pub fn as_secret_key(&self) -> &SecretKey {
        &self.secret_key
    }
}
