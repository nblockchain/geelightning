//! This module implements the `MsgChannelManager` type. Its job is to maintain a single, unique
//! MsgChannel to a remote peer and execute operations on that channel. If the `MsgChannel` dies
//! for any reason the `MsgChannelManager` will destroy and re-create it, and restart any
//! operations that were using it.

use super::*;

pub const INITIAL_RECONNECT_BACKOFF: Duration = Duration::from_millis(1);
pub const RECONNECT_BACKOFF_FACTOR: u32 = 2;

pub struct MsgChannelManager {
    operation_send: mpsc::Sender<Operation>,
}

#[derive(Debug, Error)]
pub enum MsgChannelError {
    #[error("error connecting to peer: {}", source)]
    Connect { source: transport::ConnectError },
    #[error("error initiating connection with peer: {}", source)]
    Initiate { source: InitiateError },
    #[error("broken channel: {}", source)]
    ChannelBroke { source: MsgChannelBrokeError },
    #[error("error querying peer address from dns server: {}", source)]
    DnsLookup {
        source: bootstrap::LookupPeerAddressError,
    },
}

impl MsgChannelError {
    pub fn is_protocol_violation(&self) -> bool {
        match self {
            MsgChannelError::Connect { source } => source.is_protocol_violation(),
            MsgChannelError::Initiate { source } => source.is_protocol_violation(),
            MsgChannelError::ChannelBroke { .. } => false,
            MsgChannelError::DnsLookup { .. } => false,
        }
    }
}

type OperationFunc = Box<
    dyn for<'r> FnMut(&'r MsgChannel) -> Pin<Box<dyn Future<Output = ()> + Send + 'r>>
        + Sync
        + Send
        + 'static,
>;

impl MsgChannelManager {
    pub fn new(
        transport_connector: TransportConnector,
        peer_id: PeerId,
        accepted_transport_recv: mpsc::Receiver<(Transport, SocketAddr)>,
        addr_recv: mpsc::Receiver<SocketAddr>,
        channel_responder: impl for<'r> FnMut(
                &'r MsgChannel,
                MsgReceivers,
            ) -> Pin<Box<dyn Future<Output = ()> + Send + 'r>>
            + Send
            + 'static,
    ) -> MsgChannelManager {
        let (operation_send, operation_recv) = mpsc_channel!();
        spawn!(msg_channel_manager_task(
            transport_connector,
            peer_id,
            operation_recv,
            accepted_transport_recv,
            addr_recv,
            channel_responder,
        ));
        let msg_channel_manager = MsgChannelManager { operation_send };
        msg_channel_manager
    }

    // Perform an operation on the message channel.
    //
    // If the channel dies while the operation is in progress, the operation will be restarted
    // after the channel is reestablished. Whenever the channel breaks, or the channel manager has
    // a problem reconnecting, an error will be reported on the returned reciever. The
    // oneshot::Receiver will resolve once the operation is complete, it can be dropped at any time
    // to cancel the operation.
    pub fn with_msg_channel<T, F>(
        &self,
        mut func: F,
    ) -> impl AsyncGenerator<Yield = Arc<MsgChannelError>, Return = T>
    where
        T: Unpin + Sync + Send + 'static,
        F: for<'r> FnMut(&'r MsgChannel) -> Pin<Box<dyn Future<Output = T> + Send + 'r>>
            + Sync
            + Send
            + 'static,
    {
        let mut operation_send = self.operation_send.clone();
        let future = async move {
            let (reply_send, reply_recv) = mpsc_channel!();
            let func: OperationFunc = Box::new(move |msg_channel| {
                let mut reply_send = reply_send.clone();
                let fut = func(msg_channel);
                Box::pin(async move {
                    if reply_send.is_closed() {
                        return;
                    }
                    let reply = fut.await;
                    reply_send.send(reply).await.ignore_err();
                })
            });
            let drop_send = DropSender::new();
            let drop_recv = drop_send.receiver();
            let reply_recv = reply_recv.into_future().map(move |(reply_opt, _rest)| {
                drop(drop_send);
                reply_opt.unwrap()
            });
            let (msg_channel_error_send, msg_channel_error_recv) = mpsc_channel!();
            let operation = Operation {
                func,
                msg_channel_error_send,
                drop_recv,
            };
            operation_send.send(operation).await.unwrap();
            async_generator::stream_then_future(msg_channel_error_recv, reply_recv)
        };
        future.flatten_async_generator()
    }
}

// Stores a pending operation to perform on the channel once it exists. Implements `Future` so that
// the message channel task can see when the operation has been aborted by its creator.
struct Operation {
    func: OperationFunc,
    msg_channel_error_send: mpsc::Sender<Arc<MsgChannelError>>,
    drop_recv: DropReceiver,
}

impl Operation {
    pub fn run(mut self, msg_channel: &MsgChannel) -> RunningOperation {
        let future = (self.func)(msg_channel);
        RunningOperation {
            inner_opt: Some(RunningOperationInner {
                operation: self,
                future,
            }),
        }
    }
}

impl Future for Operation {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        Pin::new(&mut self.get_mut().drop_recv).poll(cx)
    }
}

// Stores an operation currently being performed on the channel. Implements `Future` and resolves
// once the operation is complete or the initiator of the operation has dropped the handle at their
// end to cancel the operation.
struct RunningOperation<'r> {
    inner_opt: Option<RunningOperationInner<'r>>,
}

struct RunningOperationInner<'r> {
    operation: Operation,
    future: Pin<Box<dyn Future<Output = ()> + Send + 'r>>,
}

impl<'r> RunningOperation<'r> {
    // Convert the running operation back into a pending operation. Called when the message channel
    // dies and the operation hasn't been completed yet so the operation can be put back in the
    // operation queue.
    pub fn stop(self) -> Operation {
        self.inner_opt.unwrap().operation
    }
}

impl<'r> Future for RunningOperation<'r> {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        let self_mut = self.get_mut();
        let mut inner = self_mut.inner_opt.take().unwrap();

        if let Poll::Ready(()) = Pin::new(&mut inner.operation.drop_recv).poll(cx) {
            return Poll::Ready(());
        }

        if let Poll::Ready(()) = Pin::new(&mut inner.future).poll(cx) {
            return Poll::Ready(());
        }

        self_mut.inner_opt = Some(inner);
        Poll::Pending
    }
}

// Sucks futures out of `stream` and enques them in `futures_unordered`. The returned future
// returns `()` when the stream has terminated and there are no more futures left in
// `futures_unordered`.
//
// This function should really just be a simple `async fn`, however it needs to return a future
// that implements `Unpin + !Drop` so that we can use it in a select! loop. Unfortunately, `async
// fn`s are `!Unpin` and implementing the function using future combinators results in a future
// which implements `Drop`, so implementing this directly in terms of `impl Future for ...` seems
// to be the only way to write it.
fn pump_futures<'a, S: 'a, F>(
    stream: S,
    futures_unordered: &'a mut FuturesUnorderedWithIntoIter<F>,
) -> PumpFutures<'a, S, F>
where
    S: Stream<Item = F> + Unpin,
    F: Future<Output = ()> + Unpin,
{
    PumpFutures {
        stream,
        futures_unordered,
    }
}

struct PumpFutures<'a, S: 'a, F> {
    stream: S,
    futures_unordered: &'a mut FuturesUnorderedWithIntoIter<F>,
}

impl<'a, S: 'a, F> Future for PumpFutures<'a, S, F>
where
    S: Stream<Item = F> + FusedStream + Unpin,
    F: Future<Output = ()> + Unpin,
{
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        let self_mut = self.get_mut();
        let stream_terminated = self_mut.stream.is_terminated()
            || loop {
                match Pin::new(&mut self_mut.stream).poll_next(cx) {
                    Poll::Ready(Some(future)) => {
                        self_mut.futures_unordered.push(future);
                    }
                    Poll::Ready(None) => break true,
                    Poll::Pending => break false,
                }
            };

        let futures_unordered_terminated = self_mut.futures_unordered.is_terminated()
            || loop {
                match Pin::new(&mut self_mut.futures_unordered).poll_next(cx) {
                    Poll::Ready(Some(())) => (),
                    Poll::Ready(None) => break true,
                    Poll::Pending => break false,
                }
            };

        if stream_terminated && futures_unordered_terminated {
            Poll::Ready(())
        } else {
            Poll::Pending
        }
    }
}

impl<'a, S: Stream<Item = F> + FusedStream + Unpin + 'a, F: Future<Output = ()> + Unpin> FusedFuture
    for PumpFutures<'a, S, F>
{
    fn is_terminated(&self) -> bool {
        self.stream.is_terminated() && self.futures_unordered.is_terminated()
    }
}

// Takes accepted transport streams and returns them as message channels. Returns when there are no
// more incoming transport streams.
async fn accept_msg_channel_task(
    mut accepted_transport_recv: impl Stream<Item = (Transport, SocketAddr)>
        + FusedStream
        + Unpin
        + 'static,
    mut accepted_msg_channel_send: mpsc::Sender<
        Result<(MsgChannel, MsgChannelDriver, MsgReceivers, SocketAddr), InitiateError>,
    >,
) {
    'task: while let Some((mut transport, mut addr)) = accepted_transport_recv.next().await {
        'create_channel: loop {
            let msg_channel_future = async move {
                match MsgChannel::from_transport(transport).await {
                    Ok((msg_channel, msg_channel_driver, msg_receivers)) => {
                        Ok((msg_channel, msg_channel_driver, msg_receivers, addr))
                    }
                    Err(err) => Err(err),
                }
            }
            .fuse();
            pin_mut!(msg_channel_future);
            futures::select! {
                msg_channel_res = msg_channel_future => {
                    accepted_msg_channel_send.send(msg_channel_res).await.unwrap();
                    break 'create_channel;
                },
                transport_addr_opt = accepted_transport_recv.next() => {
                    match transport_addr_opt {
                        // If we accept another transport from the peer while the old one is still
                        // being initiated, just drop the old one and use this one.
                        Some((accepted_transport, accepted_addr)) => {
                            transport = accepted_transport;
                            addr = accepted_addr;
                        },

                        // The stream of accepted transports has terminated. Finish channel
                        // creation which is in progress and return it before exiting.
                        None => {
                            let msg_channel_res = msg_channel_future.await;
                            accepted_msg_channel_send.send(msg_channel_res).await.unwrap();
                            break 'task;
                        },
                    }
                },
            }
        }
    }
}

// Sends an error on all operations.
async fn send_error(
    operation_buffer: &mut FuturesUnorderedWithIntoIter<Operation>,
    err: MsgChannelError,
) {
    let err = Arc::new(err);

    // Moving out of `operation_buffer` and re-queing all the operations is inefficient but
    // unfortunately `FuturesUnordered::iter_mut` can't be called across an await boundary, so
    // there's no other way to do it. I've filed an issue for this: https://github.com/rust-lang-nursery/futures-rs/issues/1801
    let old_operation_buffer = mem::replace(operation_buffer, FuturesUnorderedWithIntoIter::new());
    for mut operation in old_operation_buffer.into_iter() {
        operation
            .msg_channel_error_send
            .send(err.clone())
            .await
            .ignore_err();
        operation_buffer.push(operation);
    }
}

/*
TODO:
DNS peer lookups aren't working. It seems that niether of the hard-coded lightning DNS seeds in
bootstrap.rs actually implement SRV lookups for individual peers as specified in BOLT #10. It needs
to be investigate why this is, whether they plan to implement it or something else, and if there's
another way to get the port number of a peer via DNS.

fn dns_addr_lookup(
    peer_id: PeerId,
) -> impl Stream<Item = Result<SocketAddr, bootstrap::LookupPeerAddressError>> {
    struct State<S> {
        backoff_duration: Duration,
        reconnect_instant: Instant,
        addr_stream: S,
        any_ok: bool,
    }
    let state = State {
        backoff_duration: INITIAL_RECONNECT_BACKOFF,
        reconnect_instant: Instant::now(),
        addr_stream: bootstrap::lookup_peer_address(peer_id),
        any_ok: false,
    };
    stream::unfold(state, move |mut state| {
        async move {
            loop {
                if let Some(addr_res) = state.addr_stream.next().await {
                    return match addr_res {
                        Ok(addr) => {
                            state.any_ok = true;
                            Some((Ok(addr), state))
                        },
                        Err(err) => Some((Err(err), state)),
                    };
                }
                if state.any_ok {
                    state.backoff_duration = INITIAL_RECONNECT_BACKOFF;
                    state.reconnect_instant = Instant::now();
                } else {
                    state.reconnect_instant += state.backoff_duration;
                    state.backoff_duration *= RECONNECT_BACKOFF_FACTOR;
                    delay(state.reconnect_instant).await;
                }
                state.addr_stream = bootstrap::lookup_peer_address(peer_id);
                state.any_ok = false;
            }
        }
    })
}
*/

async fn msg_channel_manager_task(
    transport_connector: TransportConnector,
    peer_id: PeerId,
    mut operation_recv: mpsc::Receiver<Operation>,
    accepted_transport_recv: impl Stream<Item = (Transport, SocketAddr)>
        + FusedStream
        + Unpin
        + Send
        + 'static,
    mut addr_recv: impl Stream<Item = SocketAddr> + FusedStream + Unpin + Send + 'static,
    mut channel_responder: impl for<'r> FnMut(&'r MsgChannel, MsgReceivers) -> Pin<Box<dyn Future<Output = ()> + Send + 'r>>
        + Send
        + 'static,
) {
    let (accepted_msg_channel_send, mut accepted_msg_channel_recv) = mpsc_channel!();
    spawn!(accept_msg_channel_task(
        accepted_transport_recv,
        accepted_msg_channel_send,
    ));

    // NOTE: see commented-out definition of dns_addr_lookup above
    //let dns_addr_lookup_stream = dns_addr_lookup(peer_id).fuse();
    //pin_mut!(dns_addr_lookup_stream);
    let mut ready_addrs: HashMap<SocketAddr, Duration> = HashMap::new();
    let mut backoff_addrs = FuturesUnordered::new();

    let mut operation_buffer = FuturesUnorderedWithIntoIter::new();
    'task: loop {
        // Connecting phase. Queue incoming operations and break from the 'msg_channel loop when we
        // have a message channel.
        let mut msg_channel_stuff = 'msg_channel: loop {
            while let Some(&addr) = ready_addrs.get_random_key() {
                let msg_channel_connect = async {
                    let endpoint = Endpoint { peer_id, addr };
                    let transport = {
                        transport_connector
                            .connect(&endpoint)
                            .await
                            .map_err(|source| MsgChannelError::Connect { source })?
                    };
                    let msg_channel_stuff = {
                        MsgChannel::from_transport(transport)
                            .await
                            .map_err(|source| MsgChannelError::Initiate { source })
                    }?;
                    Ok(msg_channel_stuff)
                }
                .fuse();
                pin_mut!(msg_channel_connect);
                'connecting: loop {
                    futures::select! {
                        msg_channel_res = accepted_msg_channel_recv.select_next_some() => {
                            match msg_channel_res {
                                Ok((msg_channel, msg_channel_driver, msg_receivers, accepted_addr)) => {
                                    // We received an incoming connection while we already have an
                                    // outgoing connection in-progress. Even though at our end we
                                    // saw the incoming connection complete before the outgoing
                                    // one, it's possible that the remote peer saw events happen in
                                    // reverse order so we can't make any assumptions about which
                                    // connection the remote peer will choose to keep. As such, we
                                    // flip a coin to decide whether to use the new incoming
                                    // connection or to discard it and keep waiting for the
                                    // outgoing connection. If we choose incorrectly, we'll end up
                                    // trying to re-connect and the process will repeat.
                                    ready_addrs.insert(accepted_addr, INITIAL_RECONNECT_BACKOFF);
                                    if rand::random() {
                                        break 'msg_channel (msg_channel, msg_channel_driver, msg_receivers);
                                    }
                                },
                                Err(source) => {
                                    let err = MsgChannelError::Initiate { source };
                                    send_error(&mut operation_buffer, err).await;
                                },
                            }
                        },
                        msg_channel_and_driver_res = msg_channel_connect => {
                            match msg_channel_and_driver_res {
                                Ok(msg_channel_stuff) => {
                                    ready_addrs.insert(addr, INITIAL_RECONNECT_BACKOFF);
                                    break 'msg_channel msg_channel_stuff;
                                },
                                Err(err) => {
                                    let backoff = ready_addrs.remove(&addr).unwrap();
                                    if !MsgChannelError::is_protocol_violation(&err) {
                                        backoff_addrs.push(async move {
                                            sleep(backoff).await;
                                            let next_backoff = backoff * RECONNECT_BACKOFF_FACTOR;
                                            (addr, next_backoff)
                                        });
                                    }
                                    send_error(&mut operation_buffer, err).await;
                                    break 'connecting;
                                },
                            }
                        },
                        addr = addr_recv.select_next_some() => {
                            let _ = ready_addrs.insert(addr, INITIAL_RECONNECT_BACKOFF);
                        },
                        (addr, backoff) = backoff_addrs.select_next_some() => {
                            let _ = ready_addrs.insert(addr, backoff);
                        },
                        () = pump_futures(&mut operation_recv, &mut operation_buffer) => break 'task,
                    }
                }
            }

            // There were errors making outgoing connections for all the peer addresses we know of.
            // Wait for any addresses we know of to exit backoff status before attempting to
            // reconnect on them. While we're waiting we will immediately accept any incoming
            // connection (rather than randomly dropping them). We use exponential backoff to
            // prolong the time between reconnects, which resets when we make a successful
            // connection.
            'backoff: loop {
                futures::select! {
                    msg_channel_res = accepted_msg_channel_recv.select_next_some() => {
                        match msg_channel_res {
                            Ok((msg_channel, msg_channel_driver, msg_receivers, accepted_addr)) => {
                                ready_addrs.insert(accepted_addr, INITIAL_RECONNECT_BACKOFF);
                                break 'msg_channel (msg_channel, msg_channel_driver, msg_receivers);
                            },
                            Err(source) => {
                                let err = MsgChannelError::Initiate { source };
                                send_error(&mut operation_buffer, err).await;
                            },
                        }
                    },
                    /*
                    addr_res = dns_addr_lookup_stream.select_next_some() => {
                        match addr_res {
                            Ok(addr) => {
                                let _ = ready_addrs.insert(addr, INITIAL_RECONNECT_BACKOFF);
                                break 'backoff;
                            },
                            Err(err) => {
                                let err = MsgChannelError::DnsLookup(err);
                                send_error(&mut operation_buffer, err).await;
                            },
                        }
                    },
                    */
                    addr = addr_recv.select_next_some() => {
                        let _ = ready_addrs.insert(addr, INITIAL_RECONNECT_BACKOFF);
                        break 'backoff;
                    },
                    (addr, backoff) = backoff_addrs.select_next_some() => {
                        let _ = ready_addrs.insert(addr, backoff);
                        break 'backoff;
                    },
                    () = pump_futures(&mut operation_recv, &mut operation_buffer) => break 'task,
                }
            }
        };

        // We have a message channel! Start running it and performing the queued operations. If
        // the channel breaks we exit this loop and go back to re-connecting.
        'connected: loop {
            let (msg_channel, msg_channel_driver, msg_receivers) = msg_channel_stuff;
            let msg_channel_driver = msg_channel_driver.fuse();
            pin_mut!(msg_channel_driver);

            let channel_responder_task = channel_responder(&msg_channel, msg_receivers).fuse();
            pin_mut!(channel_responder_task);

            let mut running_operation_buffer = {
                operation_buffer
                    .into_iter()
                    .map(|operation| operation.run(&msg_channel))
                    .collect()
            };

            msg_channel_stuff = 'accept_msg_channel: loop {
                futures::select! {
                    msg_channel_res = accepted_msg_channel_recv.select_next_some() => {
                        match msg_channel_res {
                            Ok((msg_channel, msg_channel_driver, msg_receivers, accepted_addr)) => {
                                ready_addrs.insert(accepted_addr, INITIAL_RECONNECT_BACKOFF);
                                // See use of `random()` above.
                                if rand::random() {
                                    operation_buffer = {
                                        running_operation_buffer
                                        .into_iter()
                                        .map(|running_operation| running_operation.stop())
                                        .collect()
                                    };
                                    break 'accept_msg_channel (msg_channel, msg_channel_driver, msg_receivers);
                                }
                            },
                            Err(source) => {
                                let err = Arc::new(MsgChannelError::Initiate { source });
                                let old_running_operation_buffer = mem::replace(&mut running_operation_buffer, FuturesUnorderedWithIntoIter::new());
                                for mut running_operation in old_running_operation_buffer.into_iter() {
                                    let inner = running_operation.inner_opt.as_mut().unwrap();
                                    inner.operation.msg_channel_error_send.send(err.clone()).await.ignore_err();
                                    running_operation_buffer.push(running_operation);
                                }
                            },
                        }
                    },
                    addr = addr_recv.select_next_some() => {
                        let _ = ready_addrs.insert(addr, INITIAL_RECONNECT_BACKOFF);
                    },
                    () = pump_futures(
                        (&mut operation_recv).map(|operation| operation.run(&msg_channel)),
                        &mut running_operation_buffer,
                    ) => break 'task,
                    () = channel_responder_task => (),
                    err = msg_channel_driver => {
                        operation_buffer = {
                            running_operation_buffer
                            .into_iter()
                            .map(|running_operation| running_operation.stop())
                            .collect()
                        };
                        let err = MsgChannelError::ChannelBroke { source: err };
                        send_error(&mut operation_buffer, err).await;
                        break 'connected;
                    },
                }
            }
        }
    }
}
