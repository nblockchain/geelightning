use super::*;
use msg_channel_manager::{INITIAL_RECONNECT_BACKOFF, RECONNECT_BACKOFF_FACTOR};
use statrs::function::factorial::binomial;

#[tokio::test]
async fn big_msg_channel_test() {
    static FIRST_MESSAGE: &[u8] = b"first message";
    static FIRST_MESSAGE_REPLY: &[u8] = b"first message reply";
    static SECOND_MESSAGE: &[u8] = b"second message";

    let secret_key_0 = SecretKey::new();
    let peer_secret_key_1 = PeerSecretKey::from_secret_key(SecretKey::new());
    let peer_id_1 = PeerId::from_peer_secret_key(&peer_secret_key_1);

    let local_endpoint_0 = LocalEndpoint {
        addr: addr!("0.0.0.0:0"),
        peer_secret_key: PeerSecretKey::from_secret_key(secret_key_0),
    };
    let (transport_connector_0, mut transport_listener_0, listen_endpoint_0) =
        TransportListener::bind(&local_endpoint_0).await.unwrap();
    let (mut accepted_transport_send, accepted_transport_recv) = mpsc_channel!();
    let (mut addr_send, addr_recv) = mpsc_channel!();

    let forward_transport = async move {
        let _void: Void = loop {
            let (transport, endpoint) = transport_listener_0.accept().await.unwrap();
            accepted_transport_send
                .send((transport, endpoint.addr))
                .await
                .unwrap()
        };
        #[allow(unreachable_code)]
        _void
    };
    pin_mut!(forward_transport);

    let server = async move {
        let (finished_send, mut finished_recv) = mpsc_channel!();
        let _msg_channel_manager = MsgChannelManager::new(
            transport_connector_0,
            peer_id_1,
            accepted_transport_recv,
            addr_recv,
            move |msg_channel, msg_receivers| {
                let mut finished_send = finished_send.clone();
                Box::pin(async move {
                    let msg_channel_send = &msg_channel.msg_channel_send;
                    let error_msg_recv = msg_receivers.error_msg_recv;
                    pin_mut!(error_msg_recv);
                    msg_channel_send
                        .send_msg(Msg::Error(ErrorMsg {
                            channel_id: None,
                            data: Vec::from(FIRST_MESSAGE),
                        }))
                        .await;
                    let error_msg = error_msg_recv.next().await.unwrap();
                    assert_eq!(&error_msg.data, FIRST_MESSAGE_REPLY);
                    msg_channel_send
                        .send_msg(Msg::Error(ErrorMsg {
                            channel_id: None,
                            data: Vec::from(SECOND_MESSAGE),
                        }))
                        .await;
                    finished_send.send(()).await.unwrap();
                })
            },
        );
        finished_recv.next().await.unwrap();
    };

    #[allow(clippy::cognitive_complexity)]
    let client = async {
        let local_endpoint_1 = LocalEndpoint {
            addr: addr!("0.0.0.0:0"),
            peer_secret_key: peer_secret_key_1,
        };
        let (transport_connector_1, mut transport_listener_1, _listen_endpoint_1) =
            TransportListener::bind(&local_endpoint_1).await.unwrap();
        let transport = transport_connector_1
            .connect(&listen_endpoint_0)
            .await
            .unwrap();
        let Transport {
            mut transport_send,
            mut transport_recv,
        } = transport;

        // Check that the MsgChannelManager sends an init message on connect.

        let msg_bytes_opt = transport_recv.recv_msg().await.unwrap();
        let msg_bytes = msg_bytes_opt.unwrap();
        match Msg::from_bytes(&msg_bytes).unwrap() {
            Msg::Init(..) => (),
            msg => panic!("unexpected msg type: {}", msg.msg_type()),
        }

        // Check that the MsgChannelManager doesn't run its responder until it receives an init.

        {
            let next_msg_bytes_opt_res_future = transport_recv.recv_msg().fuse();
            pin_mut!(next_msg_bytes_opt_res_future);
            futures::select! {
                _ = next_msg_bytes_opt_res_future => {
                    panic!("msg channel sent a msg before receiving init")
                },
                () = sleep(Duration::from_secs(1)).fuse() => (),
            };
            let msg = Msg::Init(InitMsg {
                global_features: GlobalFeatures {}.to_unvalidated(),
                local_features: LocalFeatures {
                    option_data_loss_protect: FeatureFlag::No,
                    initial_routing_sync: OptionalFeatureFlag::No,
                    option_upfront_shutdown_script: FeatureFlag::No,
                    gossip_queries: FeatureFlag::No,
                }
                .to_unvalidated(),
            });
            let msg_bytes = msg.to_bytes();
            transport_send.send_msg(msg_bytes).await.unwrap();

            // Check that the MsgChannelManager does run its responder after it receives an init.

            let next_msg_bytes = next_msg_bytes_opt_res_future.await.unwrap().unwrap();
            match Msg::from_bytes(&next_msg_bytes).unwrap() {
                Msg::Error(ErrorMsg { data, .. }) => assert_eq!(&data, FIRST_MESSAGE),
                msg => panic!("unexpected msg type: {}", msg.msg_type()),
            }
        }

        // Check that the MsgChannelManager reconnects promptly if it loses a connection.

        drop(transport_send);
        drop(transport_recv);

        let endpoint = {
            let transport_accept = transport_listener_1.accept().fuse();
            pin_mut!(transport_accept);
            let (transport, endpoint) = futures::select! {
                accepted_transport_res = transport_accept => accepted_transport_res.unwrap(),
                () = sleep(Duration::from_millis(200)).fuse() => {
                    panic!("MsgChannelManager did not immediately try to reconnect");
                },
            };
            drop(transport);
            endpoint
        };

        // Check that the MsgChannelManager is connecting from the same port that it listens on.

        assert_eq!(listen_endpoint_0.addr.port(), endpoint.addr.port());

        drop(transport_listener_1);
        drop(transport_connector_1);

        // Check that the MsgChannelManager uses suggested addresses we send it but does not re-use
        // addresses after those addresses send it non-LN data.

        let futures_unordered = FuturesUnordered::new();
        for _ in 0..5 {
            let mut addr_send = addr_send.clone();
            let future = async move {
                let listener = TcpListener::bind(&addr!("0.0.0.0:0")).await.unwrap();
                let addr = listener.local_addr().unwrap();
                let mut incoming = TcpListenerStream::new(listener).fuse();
                addr_send.send(addr).await.unwrap();
                let mut tcp_stream = futures::select! {
                    tcp_stream_res = incoming.select_next_some() => tcp_stream_res.unwrap(),
                    () = sleep(Duration::from_secs(10)).fuse() => {
                        panic!("MsgChannelManager did not try to connect to address we gave it");
                    },
                };
                tcp_stream.write_all(b"nonsense").await.unwrap();
                drop(tcp_stream);
                futures::select! {
                    _tcp_stream_res = incoming.select_next_some() => {
                        panic!("MsgChannelManager tried to re-use address after it was sent non-LN data");
                    },
                    () = sleep(Duration::from_secs(1)).fuse() => (),
                };
            };
            futures_unordered.push(future);
        }
        futures_unordered.for_each(|()| async {}).await;

        // Check that the MsgChannelManager keeps attempting to reconnect if connecting fails due
        // to a TCP error, and that it is using exponential backoff.

        let listener = TcpListener::bind(&addr!("0.0.0.0:0")).await.unwrap();
        let addr = listener.local_addr().unwrap();
        let mut incoming = TcpListenerStream::new(listener).fuse();
        addr_send.send(addr).await.unwrap();
        let tcp_stream = incoming.next().await.unwrap().unwrap();
        tcp_stream.set_linger(Some(Duration::new(0, 0))).unwrap();
        drop(tcp_stream);
        let mut reconnect_instant = Instant::now();
        let mut reconnect_delay = INITIAL_RECONNECT_BACKOFF;

        for _ in 0..10u32 {
            let leniency = reconnect_delay.div_f32(10.0) + Duration::from_millis(100);
            let min_reconnect_delay = reconnect_delay
                .checked_sub(leniency)
                .unwrap_or_else(|| Duration::new(0, 0));
            let max_reconnect_delay = reconnect_delay + leniency;
            futures::select! {
                tcp_stream_res = incoming.select_next_some() => {
                    let tcp_stream = tcp_stream_res.unwrap();
                    tcp_stream.set_linger(Some(Duration::new(0, 0))).unwrap();
                    drop(tcp_stream);
                    let next_reconnect_instant = Instant::now();
                    let actual_reconnect_delay = next_reconnect_instant - reconnect_instant;
                    reconnect_instant = next_reconnect_instant;
                    assert!(actual_reconnect_delay > min_reconnect_delay);
                    reconnect_delay *= RECONNECT_BACKOFF_FACTOR;
                },
                () = sleep(max_reconnect_delay).fuse() => {
                    panic!("MsgChannelManager took too long to reconnect");
                },
            }
        }
        drop(incoming);

        // Give the MsgChannel a bunch of accepted transports while it is already connected. Check
        // that it switches over to the new transport about half the time.

        let (transport_connector_1, transport_listener_1, _listen_endpoint_1) =
            { TransportListener::bind(&local_endpoint_1).await.unwrap() };

        let transport = transport_connector_1
            .connect(&listen_endpoint_0)
            .await
            .unwrap();
        let (mut msg_channel, msg_channel_driver, msg_receivers) =
            MsgChannel::from_transport(transport).await.unwrap();
        drop(msg_receivers);
        let mut msg_channel_driver = Box::pin(msg_channel_driver).fuse();
        let mut num_switches = 0u64;
        let mut num_keeps = 0u64;
        let min_of_each = 2u64;
        let mut num_tests = 0u64;

        loop {
            let (next_transport_connector, _transport_listener_1, _listen_endpoint_1) =
                { TransportListener::bind(&local_endpoint_1).await.unwrap() };

            let next_transport = next_transport_connector
                .connect(&listen_endpoint_0)
                .await
                .unwrap();
            let (next_msg_channel, next_msg_channel_driver, msg_receivers) =
                MsgChannel::from_transport(next_transport).await.unwrap();
            let mut next_msg_channel_driver = Box::pin(next_msg_channel_driver).fuse();
            drop(msg_receivers);

            futures::select! {
                _err = msg_channel_driver => {
                    num_switches += 1;
                    msg_channel = next_msg_channel;
                    msg_channel_driver = next_msg_channel_driver;
                },
                _err = next_msg_channel_driver => {
                    num_keeps += 1;
                },
            }
            num_tests += 1;

            let mut num_possible_fail_outcomes = 0.0f64;
            for i in 0..min_of_each {
                let num_exactly_i_keeps_outcomes = binomial(num_tests, i);
                let num_exactly_i_switches_outcomes = num_exactly_i_keeps_outcomes;
                num_possible_fail_outcomes += num_exactly_i_keeps_outcomes;
                num_possible_fail_outcomes += num_exactly_i_switches_outcomes;
            }
            let num_possible_outcomes_total = 2.0f64.powi(num_tests as i32);
            let probability_of_false_negative =
                { num_possible_fail_outcomes / num_possible_outcomes_total };
            if probability_of_false_negative < 1.0 / 1_000_000.0 {
                assert!(num_switches >= min_of_each);
                assert!(num_keeps >= min_of_each);
                break;
            }
        }
        drop(transport_listener_1);
        drop(msg_channel);
        drop(msg_channel_driver);

        // Check that, despite all of the above, the MsgChannelManager runs its responder from
        // start until completion when given a stable connection.

        let mut num_tests = 0;
        loop {
            let (transport_connector_1, _transport_listener_1, _listen_endpoint_1) =
                { TransportListener::bind(&local_endpoint_1).await.unwrap() };

            let transport = transport_connector_1
                .connect(&listen_endpoint_0)
                .await
                .unwrap();
            let (msg_channel, msg_channel_driver, msg_receivers) =
                MsgChannel::from_transport(transport).await.unwrap();
            let drive_then_wait = async move {
                let err = msg_channel_driver.await;
                sleep(Duration::from_secs(1)).await;
                err
            }
            .fuse();
            pin_mut!(drive_then_wait);
            let swap_messages = async move {
                let msg_channel_send = msg_channel.msg_channel_send;
                let error_msg_recv = msg_receivers.error_msg_recv;
                pin_mut!(error_msg_recv);
                let error_msg = match error_msg_recv.next().await {
                    Some(error_msg) => error_msg,
                    None => future::pending().await,
                };
                assert_eq!(&error_msg.data, FIRST_MESSAGE);
                msg_channel_send
                    .send_msg(Msg::Error(ErrorMsg {
                        channel_id: None,
                        data: Vec::from(FIRST_MESSAGE_REPLY),
                    }))
                    .await;
                let error_msg = match error_msg_recv.next().await {
                    Some(error_msg) => error_msg,
                    None => future::pending().await,
                };
                assert_eq!(&error_msg.data, SECOND_MESSAGE);
            }
            .fuse();
            pin_mut!(swap_messages);
            futures::select! {
                () = swap_messages => break,
                _err = drive_then_wait => {
                    num_tests += 1;
                    let probability_of_random_failure = 0.5f64.powi(num_tests);
                    if probability_of_random_failure < 1.0 / 1_000_000.0 {
                        panic!("MsgChannelManager is not using the transports we're giving it");
                    }
                },
            }
        }
    };

    pin_mut!(client);
    pin_mut!(server);
    futures::select! {
        () = client.fuse() => (),
        () = server.fuse() => (),
        never = forward_transport.fuse() => match never {},
    }
}
