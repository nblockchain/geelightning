use super::*;

// A RemotePeer represents a connection to a remote peer and can be used to perform actions on that
// peer (such as opening a channel). Only one RemotePeer objects exists for a given peer at any
// given time.

pub struct RemotePeer {
    peer_id: PeerId,
    msg_channel_manager: MsgChannelManager,
    accepted_transport_send: mpsc::Sender<(Transport, SocketAddr)>,
    addr_send: mpsc::Sender<SocketAddr>,
    drop_send: DropSender,
}

#[derive(Debug, Error)]
pub enum OpenChannelError {
    #[error("invalid outgoing channel params: {}", source)]
    InvalidOutgoingParams {
        source: OutgoingOpenChannelParamsError,
    },
    #[error("invalid channel params requested by remote peer: {}", source)]
    InvalidIncomingParams {
        source: IncomingAcceptChannelParamsError,
    },
    #[error("remote peer responded with an error message: {}", error_msg)]
    ErrorResponse { error_msg: String },
}

#[derive(Debug, Error)]
pub enum FundChannelError {
    #[error("the remote peer sent an invalid commitment signature: {}", source)]
    InvalidCommitmentSignature { source: SignatureError },
    #[error("remote peer responded with an error message: {}", error_msg)]
    ErrorResponse { error_msg: String },
}

#[derive(Debug, Error)]
pub enum FundingLockedError {
    #[error("dummy variant")]
    Dummy,
}

impl RemotePeer {
    pub async fn new(
        transport_connector: TransportConnector,
        peer_id: PeerId,
    ) -> (
        RemotePeer,
        mpsc::Receiver<Result<Channel, AcceptChannelError>>,
        mpsc::Receiver<(ChannelId, PublicKey)>,
    ) {
        let (accepted_transport_send, accepted_transport_recv) = mpsc_channel!();
        let (addr_send, addr_recv) = mpsc_channel!();
        let (accepted_channel_send, accepted_channel_recv) = mpsc_channel!();
        let (remote_funding_locked_send, remote_funding_locked_recv) = mpsc_channel!();
        let msg_channel_manager = MsgChannelManager::new(
            transport_connector,
            peer_id,
            accepted_transport_recv,
            addr_recv,
            move |msg_channel, msg_receivers| {
                let accepted_channel_send = accepted_channel_send.clone();
                let remote_funding_locked_send = remote_funding_locked_send.clone();
                Box::pin(async move {
                    channel_respond(
                        peer_id,
                        msg_channel,
                        msg_receivers,
                        accepted_channel_send,
                        remote_funding_locked_send,
                    )
                    .await
                })
            },
        );
        let drop_send = DropSender::new();
        let remote_peer = RemotePeer {
            peer_id,
            msg_channel_manager,
            accepted_transport_send,
            addr_send,
            drop_send,
        };
        (
            remote_peer,
            accepted_channel_recv,
            remote_funding_locked_recv,
        )
    }

    pub fn peer_id(&self) -> PeerId {
        self.peer_id
    }

    pub async fn offer_transport(&self, transport: Transport, addr: SocketAddr) {
        let mut accepted_transport_send = self.accepted_transport_send.clone();
        accepted_transport_send
            .send((transport, addr))
            .await
            .ignore_err();
    }

    pub async fn offer_addr(&self, addr: SocketAddr) {
        let mut addr_send = self.addr_send.clone();
        addr_send.send(addr).await.ignore_err();
    }

    pub fn on_disconnect(&self) -> DropReceiver {
        self.drop_send.receiver()
    }

    pub fn open_channel(
        &self,
        temporary_channel_id: TemporaryChannelId,
        local_params: LocalChannelParams,
        funding: Sat64,
        push: Msat64,
        feerate: SatPerKw32,
        network: Network,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<UnfundedChannel, OpenChannelError>,
    > {
        let remote_peer_id = self.peer_id;
        self.msg_channel_manager.with_msg_channel(move |msg_channel: &MsgChannel| {
            let local_params = local_params.clone();
            Box::pin(async move {
                let msg_channel_send = &msg_channel.msg_channel_send;
                let msg_subscribers = &msg_channel.msg_subscribers;
                let mut error_msg_subscriber = {
                    msg_subscribers.error_msg_subscriber.clone()
                };
                let mut accept_channel_msg_subscriber = {
                    msg_subscribers.accept_channel_msg_subscriber.clone()
                };

                let accept_channel_msg_recv = {
                    accept_channel_msg_subscriber
                    .subscribe(temporary_channel_id)
                    .await
                    .fuse()
                };
                let error_msg_recv = {
                    error_msg_subscriber
                    .subscribe(temporary_channel_id.pretend_real())
                    .await
                    .fuse()
                };
                pin_mut!(accept_channel_msg_recv);
                pin_mut!(error_msg_recv);

                let open_channel_msg = {
                    let local_params = local_params.clone();
                    local_params.prepare_outgoing_open_channel_msg(
                        temporary_channel_id, funding, push, feerate, network
                    )
                    .map_err(|source| OpenChannelError::InvalidOutgoingParams { source })?
                };
                let msg = Msg::OpenChannel(open_channel_msg);
                msg_channel_send.send_msg(msg).await;
                let accept_channel_msg = futures::select! {
                    accept_channel_msg = accept_channel_msg_recv.select_next_some() => accept_channel_msg,
                    error_msg = error_msg_recv.select_next_some() => {
                        return Err(OpenChannelError::ErrorResponse { error_msg: error_msg.into_string() });
                    },
                };
                let remote_first_per_commitment_point = accept_channel_msg.first_per_commitment_point;
                let minimum_depth = accept_channel_msg.minimum_depth;
                let remote_params_res = RemoteChannelParams::validate_incoming_accept_channel_msg(
                    accept_channel_msg,
                    &local_params.params,
                );
                let remote_params = match remote_params_res {
                    Ok(remote_params) => remote_params,
                    Err(source) => {
                        let reply_error_msg = Msg::Error(
                            ErrorMsg::new(Some(temporary_channel_id.pretend_real()), &source)
                        );

                        msg_channel_send.send_msg(reply_error_msg).await;
                        return Err(OpenChannelError::InvalidIncomingParams { source });
                    },
                };
                let unfunded_channel = UnfundedChannel {
                    remote_peer_id,
                    local_params,
                    remote_params,
                    minimum_depth,
                    feerate,
                    remote_first_per_commitment_point,
                    funding,
                };
                Ok(unfunded_channel)
            })
        })
    }

    pub fn fund_channel(
        &self,
        temporary_channel_id: TemporaryChannelId,
        unfunded_channel: UnfundedChannel,
        funding_outpoint: OutPoint,
    ) -> impl AsyncGenerator<Yield = Arc<MsgChannelError>, Return = Result<Channel, FundChannelError>>
    {
        self.msg_channel_manager.with_msg_channel(move |msg_channel: &MsgChannel| {
            let unfunded_channel = unfunded_channel.clone();
            Box::pin(async move {
                let msg_channel_send = &msg_channel.msg_channel_send;
                let msg_subscribers = &msg_channel.msg_subscribers;
                let mut error_msg_subscriber = {
                    msg_subscribers.error_msg_subscriber.clone()
                };
                let mut funding_signed_msg_subscriber = {
                    msg_subscribers.funding_signed_msg_subscriber.clone()
                };
                let UnfundedChannel {
                    remote_peer_id,
                    local_params,
                    remote_params,
                    minimum_depth,
                    feerate,
                    remote_first_per_commitment_point,
                    funding,
                } = unfunded_channel;
                let channel_config = ChannelConfig {
                    polarity: ChannelPolarity::Funder,
                    remote_peer_id,
                    local_params,
                    remote_params,
                    funding_outpoint,
                    capacity: funding,
                };
                let initial_channel_state = channel_config.initial_state(
                    remote_first_per_commitment_point,
                    feerate,
                );
                let signature = channel_config.commitment_signature(
                    ChannelPolarity::Fundee,
                    &initial_channel_state,
                );
                let channel_id = ChannelId::from_funding_outpoint(funding_outpoint);

                let funding_created_msg = FundingCreatedMsg {
                    temporary_channel_id,
                    funding_txid: funding_outpoint.txid,
                    funding_output_index: funding_outpoint.vout as u16,
                    signature,
                };
                let msg = Msg::FundingCreated(funding_created_msg);
                let funding_signed_msg_recv = {
                    funding_signed_msg_subscriber
                    .subscribe(channel_id)
                    .await
                    .fuse()
                };
                pin_mut!(funding_signed_msg_recv);
                let error_msg_recv = {
                    error_msg_subscriber
                    .subscribe(channel_id)
                    .await
                    .fuse()
                };
                pin_mut!(error_msg_recv);
                msg_channel_send.send_msg(msg).await;
                let funding_signed_msg = futures::select! {
                    funding_signed_msg = funding_signed_msg_recv.select_next_some() => funding_signed_msg,
                    error_msg = error_msg_recv.select_next_some() => {
                        return Err(FundChannelError::ErrorResponse { error_msg: error_msg.into_string() });
                    },
                };
                let channel = Channel::new_unconfirmed(
                    channel_config,
                    initial_channel_state,
                    funding_signed_msg.signature,
                    minimum_depth,
                ).map_err(|source| FundChannelError::InvalidCommitmentSignature { source })?;

                Ok(channel)
            })
        })
    }

    pub fn funding_locked(
        &self,
        channel: Channel,
    ) -> impl AsyncGenerator<Yield = Arc<MsgChannelError>, Return = Result<(), FundingLockedError>>
    {
        self.msg_channel_manager
            .with_msg_channel(move |msg_channel| {
                let channel = channel.clone();
                Box::pin(async move {
                    let msg_channel_send = &msg_channel.msg_channel_send;

                    let channel_id = channel.channel_id();
                    let next_per_commitment_point = channel.next_commitment_point();
                    let funding_locked_msg = FundingLockedMsg {
                        channel_id,
                        next_per_commitment_point,
                    };
                    let msg = Msg::FundingLocked(funding_locked_msg);
                    msg_channel_send.send_msg(msg).await;
                    Ok(())
                })
            })
    }
}

async fn channel_respond(
    remote_peer_id: PeerId,
    msg_channel: &MsgChannel,
    msg_receivers: MsgReceivers,
    mut accepted_channel_send: mpsc::Sender<Result<Channel, AcceptChannelError>>,
    mut remote_funding_locked_send: mpsc::Sender<(ChannelId, PublicKey)>,
) {
    let MsgReceivers {
        mut ping_msg_recv,
        mut pong_msg_recv,
        mut open_channel_msg_recv,
        mut funding_locked_msg_recv,
        error_msg_recv,
    } = msg_receivers;
    let mut accept_channel_futures = FuturesUnordered::new();
    let _ = error_msg_recv; // TODO: handle errors
    loop {
        futures::select! {
            ping_msg = ping_msg_recv.select_next_some() => {
                let pong_msg = PongMsg {
                    bytes_len: ping_msg.bytes_len,
                };
                let msg = Msg::Pong(pong_msg);
                msg_channel.msg_channel_send.send_msg(msg).await;
            },
            pong_msg = pong_msg_recv.select_next_some() => {
                let _pong_msg = pong_msg;
            },
            open_channel_msg = open_channel_msg_recv.select_next_some() => {
                let accept_channel_future = accept_channel(
                    remote_peer_id,
                    msg_channel,
                    open_channel_msg,
                );
                accept_channel_futures.push(accept_channel_future);
            },
            funding_locked_msg = funding_locked_msg_recv.select_next_some() => {
                let FundingLockedMsg {
                    channel_id,
                    next_per_commitment_point,
                } = funding_locked_msg;
                remote_funding_locked_send.send((channel_id, next_per_commitment_point)).await.ignore_err();
            },
            channel_res = accept_channel_futures.select_next_some() => {
                let channel_res = match channel_res {
                    Ok(channel) => Ok(channel),
                    Err((channel_id, err)) => {
                        let error_msg = Msg::Error(
                            ErrorMsg::new(Some(channel_id), &err)
                        );
                        msg_channel.msg_channel_send.send_msg(error_msg).await;
                        Err(err)
                    },
                };
                accepted_channel_send.send(channel_res).await.ignore_err();
            },
        }
    }
}

async fn accept_channel(
    remote_peer_id: PeerId,
    msg_channel: &MsgChannel,
    open_channel_msg: OpenChannelMsg,
) -> Result<Channel, (ChannelId, AcceptChannelError)> {
    let temporary_channel_id = open_channel_msg.temporary_channel_id;
    let funding = open_channel_msg.funding_satoshis;
    let feerate = open_channel_msg.feerate_per_kw;
    let remote_first_per_commitment_point = open_channel_msg.first_per_commitment_point;
    let remote_params = RemoteChannelParams::validate_incoming_open_channel_msg(open_channel_msg)
        .map_err(|source| AcceptChannelError::InvalidIncomingParams { source })
        .map_err(|err| (temporary_channel_id.pretend_real(), err))?;
    let local_params = LocalChannelParams::new_default_params(funding);
    let accept_channel_msg = {
        local_params
            .clone()
            .prepare_outgoing_accept_channel_msg(remote_params.params.clone(), temporary_channel_id)
            .map_err(|source| AcceptChannelError::InvalidOutgoingParams { source })
            .map_err(move |err| (temporary_channel_id.pretend_real(), err))?
    };
    let minimum_depth = accept_channel_msg.minimum_depth;
    let msg = Msg::AcceptChannel(accept_channel_msg);
    let mut funding_created_msg_subscriber = {
        msg_channel
            .msg_subscribers
            .funding_created_msg_subscriber
            .clone()
    };
    let mut error_msg_subscriber = { msg_channel.msg_subscribers.error_msg_subscriber.clone() };
    let funding_created_msg_recv = {
        funding_created_msg_subscriber
            .subscribe(temporary_channel_id)
            .await
            .fuse()
    };
    let error_msg_recv = {
        error_msg_subscriber
            .subscribe(temporary_channel_id.pretend_real())
            .await
            .fuse()
    };
    pin_mut!(funding_created_msg_recv);
    pin_mut!(error_msg_recv);
    msg_channel.msg_channel_send.send_msg(msg).await;
    let funding_created_msg = futures::select! {
        funding_created_msg = funding_created_msg_recv.select_next_some() => funding_created_msg,
        error_msg = error_msg_recv.select_next_some() => {
            return Err((temporary_channel_id.pretend_real(), AcceptChannelError::ErrorResponse { error_msg: error_msg.into_string() }));
        },
    };
    let funding_outpoint = OutPoint {
        txid: funding_created_msg.funding_txid,
        vout: u32::from(funding_created_msg.funding_output_index),
    };
    let channel_id = ChannelId::from_funding_outpoint(funding_outpoint);
    let their_signature = funding_created_msg.signature;
    let channel_config = ChannelConfig {
        polarity: ChannelPolarity::Fundee,
        remote_peer_id,
        local_params,
        remote_params,
        funding_outpoint,
        capacity: funding,
    };
    let initial_channel_state =
        channel_config.initial_state(remote_first_per_commitment_point, feerate);
    let signature =
        channel_config.commitment_signature(ChannelPolarity::Funder, &initial_channel_state);
    let channel = Channel::new_unconfirmed(
        channel_config,
        initial_channel_state,
        their_signature,
        minimum_depth,
    )
    .map_err(|source| AcceptChannelError::InvalidCommitmentSignature { source })
    .map_err(|err| (channel_id, err))?;
    let funding_signed_msg = FundingSignedMsg {
        channel_id,
        signature,
    };
    let msg = Msg::FundingSigned(funding_signed_msg);
    msg_channel.msg_channel_send.send_msg(msg).await;
    Ok(channel)
}

#[derive(Debug, Error)]
pub enum AcceptChannelError {
    #[error("invalid incoming parameters: {}", source)]
    InvalidIncomingParams {
        source: IncomingOpenChannelParamsError,
    },
    #[error("invalid outgoing parameters: {}", source)]
    InvalidOutgoingParams {
        source: OutgoingAcceptChannelParamsError,
    },
    #[error("invalid commitment signature: {}", source)]
    InvalidCommitmentSignature { source: SignatureError },
    #[error("peer responded with an error: {}", error_msg)]
    ErrorResponse { error_msg: String },
}
