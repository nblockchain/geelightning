use super::*;

const ACCEPTED_PEER_KEEP_ALIVE_TIME: Duration = Duration::from_secs(60);

// A PeerManager manages a set of RemotePeers. It binds a local TCP port and creates a
// RemotePeer object for each incoming connection. It can be used to get a shared handle to any
// RemotePeer it's connected to and will initiate connections to new peers on-demand.
//
// PeerManager is intended to be used as a singleton type. eg. there is only one PeerManager object
// per instance of this library and all network actions are performed via it.

pub struct PeerManager {
    get_peer_send: mpsc::Sender<(PeerId, oneshot::Sender<Arc<RemotePeer>>)>,
    listen_endpoint: Endpoint,
    _drop_send: DropSender,
}

#[derive(Debug, Error)]
pub enum PeerManagerNewError {
    #[error("error binding to local address: {}", source)]
    Bind { source: io::Error },
}

impl PeerManager {
    pub async fn new(
        local_endpoint: &LocalEndpoint,
    ) -> Result<
        (
            PeerManager,
            impl Stream<Item = transport::AcceptError>,
            mpsc::Receiver<Result<(Arc<RemotePeer>, Channel), AcceptChannelError>>,
            mpsc::Receiver<(PeerId, ChannelId, PublicKey)>,
        ),
        PeerManagerNewError,
    > {
        let local_endpoint = local_endpoint.clone();
        let (transport_connector, transport_listener, listen_endpoint) = {
            TransportListener::bind(&local_endpoint)
                .await
                .map_err(|source| PeerManagerNewError::Bind { source })?
        };
        let (get_peer_send, get_peer_recv) = mpsc_channel!();
        let (accept_err_send, accept_err_recv) = mpsc_channel!();
        let (accepted_channel_send, accepted_channel_recv) = mpsc_channel!();
        let (remote_funding_locked_send, remote_funding_locked_recv) = mpsc_channel!();
        spawn!(peer_map_task(
            transport_connector,
            get_peer_recv,
            accepted_channel_send,
            remote_funding_locked_send,
        ));
        let drop_send = DropSender::new();
        let drop_recv = drop_send.receiver();
        spawn!({
            let get_peer_send = get_peer_send.clone();
            accept_peer_task(
                transport_listener,
                accept_err_send,
                get_peer_send,
                drop_recv,
            )
        });
        Ok((
            PeerManager {
                get_peer_send,
                listen_endpoint,
                _drop_send: drop_send,
            },
            accept_err_recv,
            accepted_channel_recv,
            remote_funding_locked_recv,
        ))
    }

    pub fn get_listen_endpoint(&self) -> Endpoint {
        self.listen_endpoint.clone()
    }

    pub fn get_peer(&self, peer_id: PeerId) -> impl Future<Output = Arc<RemotePeer>> {
        let mut get_peer_send = self.get_peer_send.clone();
        async move {
            let (peer_send, peer_recv) = oneshot_channel!();
            get_peer_send.send((peer_id, peer_send)).await.unwrap();
            peer_recv.await.unwrap()
        }
    }

    pub async fn offer_endpoint(&self, endpoint: &Endpoint) {
        let peer_id = endpoint.peer_id;
        let remote_peer = self.get_peer(peer_id).await;
        remote_peer.offer_addr(endpoint.addr).await;
        // TODO: Remove this once peer address caching is implemented.
        spawn!(async move {
            sleep(ACCEPTED_PEER_KEEP_ALIVE_TIME).await;
            drop(remote_peer);
        });
    }
}

async fn accept_peer_task(
    mut transport_listener: TransportListener,
    mut accept_err_send: mpsc::Sender<transport::AcceptError>,
    mut get_peer_send: mpsc::Sender<(PeerId, oneshot::Sender<Arc<RemotePeer>>)>,
    mut drop_recv: DropReceiver,
) {
    'task: loop {
        let accept = transport_listener.accept().fuse();
        pin_mut!(accept);
        futures::select! {
            accepted_res = accept => {
                let (transport, remote_endpoint) = match accepted_res {
                    Ok(accepted) => accepted,
                    Err(accept_err) => {
                        accept_err_send.send(accept_err).await.ignore_err();
                        continue;
                    },
                };
                let peer_id = remote_endpoint.peer_id;
                let (peer_send, peer_recv) = oneshot_channel!();
                get_peer_send.send((peer_id, peer_send)).await.unwrap();
                let remote_peer = peer_recv.await.unwrap();
                remote_peer.offer_transport(transport, remote_endpoint.addr).await;
                spawn!(async move {
                    sleep(ACCEPTED_PEER_KEEP_ALIVE_TIME).await;
                    drop(remote_peer);
                });
            },
            () = drop_recv => break 'task,
        }
    }
}

async fn peer_map_task(
    transport_connector: TransportConnector,
    mut get_peer_recv: mpsc::Receiver<(PeerId, oneshot::Sender<Arc<RemotePeer>>)>,
    mut accepted_channel_send: mpsc::Sender<Result<(Arc<RemotePeer>, Channel), AcceptChannelError>>,
    mut remote_funding_locked_send: mpsc::Sender<(PeerId, ChannelId, PublicKey)>,
) {
    let mut remote_peers: HashMap<PeerId, std::sync::Weak<RemotePeer>> = HashMap::new();
    let mut peer_drops = FuturesUnordered::new();
    let mut channel_acceptors = stream::SelectAll::new();
    let mut remote_funding_locked_recvs = stream::SelectAll::new();
    'task: loop {
        futures::select! {
            get_peer_opt = get_peer_recv.next() => {
                let (peer_id, peer_send) = match get_peer_opt {
                    Some(get_peer) => get_peer,
                    None => break 'task,
                };
                let remote_peer = loop {
                    match remote_peers.entry(peer_id) {
                        hash_map::Entry::Occupied(oe) => {
                            match oe.get().upgrade() {
                                Some(remote_peer) => {
                                    break remote_peer;
                                },
                                None => {
                                    let _ = oe.remove();
                                },
                            }
                        },
                        hash_map::Entry::Vacant(ve) => {
                            let (remote_peer, accepted_channel_recv, remote_funding_locked_recv) = RemotePeer::new(
                                transport_connector.clone(), peer_id,
                            ).await;
                            let remote_peer = Arc::new(remote_peer);
                            channel_acceptors.push({
                                let remote_peer = Arc::downgrade(&remote_peer);
                                accepted_channel_recv
                                .map_ok(move |channel| {
                                    let remote_peer = remote_peer.upgrade()?;
                                    Some((remote_peer, channel))
                                })
                                .try_filter_map(|opt| async { Ok(opt) })
                                .boxed()
                            });
                            remote_funding_locked_recvs.push({
                                remote_funding_locked_recv
                                .map(move |(channel_id, next_per_commitment_point)| {
                                    (peer_id, channel_id, next_per_commitment_point)
                                })
                            });
                            let drop_recv = remote_peer.on_disconnect();
                            peer_drops.push(drop_recv.map(move |()| peer_id));
                            ve.insert(Arc::downgrade(&remote_peer));
                            break remote_peer;
                        },
                    }
                };
                peer_send.send(remote_peer).await.ignore_err();
            },
            channel_res = channel_acceptors.select_next_some() => {
                accepted_channel_send.send(channel_res).await.ignore_err();
            },
            funding_locked = remote_funding_locked_recvs.select_next_some() => {
                remote_funding_locked_send.send(funding_locked).await.ignore_err();
            },
            peer_id = peer_drops.select_next_some() => {
                if let hash_map::Entry::Occupied(oe) = remote_peers.entry(peer_id) {
                    match oe.get().upgrade() {
                        Some(_remote_peer) => (),
                        None => {
                            let _ = oe.remove();
                        },
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn connect_two_peer_managers() {
        let secret_key_0 = PeerSecretKey::from_secret_key(SecretKey::new());
        let local_endpoint_0 = LocalEndpoint {
            peer_secret_key: secret_key_0,
            addr: addr!("0.0.0.0:0"),
        };
        let (peer_manager_0, accept_err_0, accepted_channels_0, remote_funding_locked_recv_0) =
            PeerManager::new(&local_endpoint_0).await.unwrap();
        drop(accept_err_0);
        drop(accepted_channels_0);
        drop(remote_funding_locked_recv_0);

        let secret_key_1 = PeerSecretKey::from_secret_key(SecretKey::new());
        let local_endpoint_1 = LocalEndpoint {
            peer_secret_key: secret_key_1,
            addr: addr!("0.0.0.0:0"),
        };
        let (peer_manager_1, accept_err_1, accepted_channels_1, remote_funding_locked_recv_1) =
            PeerManager::new(&local_endpoint_1).await.unwrap();
        drop(accept_err_1);
        drop(accepted_channels_1);
        drop(remote_funding_locked_recv_1);
        let endpoint_1 = peer_manager_1.get_listen_endpoint();
        let peer_id_1 = endpoint_1.peer_id;

        let remote_peer_0 = peer_manager_0.get_peer(peer_id_1).await;
        peer_manager_0.offer_endpoint(&endpoint_1).await;

        let temporary_channel_id = TemporaryChannelId::new_random();
        let funding = Sat64::from_u64(1000);
        let push = Msat64::from_u64(1000);
        let feerate = SatPerKw32::from_u32(1000);
        let local_params = LocalChannelParams::new_default_params(funding);
        let open_channel_res_future = remote_peer_0
            .open_channel(
                temporary_channel_id,
                local_params,
                funding,
                push,
                feerate,
                Network::Bitcoin,
            )
            .for_each(|_msg_channel_err| async {});
        let _unfunded_channel = open_channel_res_future.await.unwrap();
    }
}
