use super::*;

pub struct Node {
    channel_manager: ChannelManager,
}

#[derive(Debug, Error)]
pub enum NodeNewError {
    #[error("error creating channel manager: {}", source)]
    ChannelManagerNew { source: ChannelManagerNewError },
}

#[derive(Debug, Error)]
pub enum NodeError {
    #[error("error accepting connecting peer: {}", source)]
    Accept { source: transport::AcceptError },
    #[error("channel manager error: {}", source)]
    ChannelManager {
        source: channel::ChannelManagerError,
    },
}

impl Node {
    /// This is the main entry point to the library.
    /// `base_directory` is the directory where the wallet file lives.
    /// `local_endpoint` contains the node's secret key and the socket address that we want to bind
    /// to.
    pub async fn new<P: AsRef<Path>>(
        base_directory: P,
        local_endpoint: &LocalEndpoint,
    ) -> Result<(Node, impl Stream<Item = NodeError>), NodeNewError> {
        let base_directory = PathBuf::from(base_directory.as_ref());
        let (channel_manager, accept_errors) = ChannelManager::new(base_directory, local_endpoint)
            .await
            .map_err(|source| NodeNewError::ChannelManagerNew { source })?;
        let node = Node { channel_manager };
        let node_errors = accept_errors.map(|source| NodeError::ChannelManager { source });
        Ok((node, node_errors))
    }

    /// Get a `Stream` of balances which updates every time the wallet balance updates. The stream
    /// will immediately return the current balance of the wallet upon creation.
    pub fn balance(&self) -> impl Stream<Item = Result<Balance, Arc<ReadFromDiskError>>> {
        let channels_res_stream = self.channel_manager.channels();
        channels_res_stream.map_ok(|channels| channels.values().map(Channel::balance).sum())
    }

    /// Start the channel opening process. The returned generator yields any network errors that
    /// occur during the process and returns the temporary channel id and funding script pubkey of
    /// the channel. The caller then needs to create a funding transaction which pays to this
    /// script pubkey and call `Node::fund_channel` to continue the channel opening process.
    pub fn open_channel<'a>(
        &'a self,
        peer_id: PeerId,
        funding: Sat64,
        feerate: SatPerKw32,
        network: Network,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<(TemporaryChannelId, Script), OpenChannelError>,
    > + 'a {
        self.channel_manager
            .open_channel(peer_id, funding, feerate, network)
    }

    /// After calling `Node::open_channel`, getting the funding script pubkey, and creating the
    /// funding transaction, call this method to continue the channel opening process. The returned
    /// generator will yield any network errors that occur during the process and will return with
    /// the channel id of the new channel. It is important not to broadcast the funding transaction
    /// until after this function returns success. Once this function returns success you broadcast
    /// the funding transaction, wait for it to reach the required confirmation depth in the block
    /// chain, then call `Node::local_confirm_channel`. To get the required depth, you can use the
    /// `Node::channels` method (though we should probably just return the minimum depth from this
    /// function (TODO)).
    pub fn fund_channel<'a>(
        &'a self,
        peer_id: PeerId,
        temporary_channel_id: TemporaryChannelId,
        funding_outpoint: OutPoint,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<ChannelId, FundChannelError>,
    > + 'a {
        self.channel_manager
            .fund_channel(peer_id, temporary_channel_id, funding_outpoint)
    }

    /// Get the TCP IP:port that the node is listening for incoming connections on.
    pub async fn get_listen_endpoint(&self) -> Endpoint {
        self.channel_manager.get_listen_endpoint().await
    }

    /// Tell the node about an endpoint for a peer. An `Endpoint` contains a peer ID and a
    /// `SocketAddr`. If you try to open a channel with a peer without calling this method the
    /// channel opening process will hang, waiting to learn the peer's IP:port. Eventually, we
    /// should support looking up the peer's IP:port using the lightning network DNS seeds, though
    /// this is not fully implemented.
    pub async fn offer_endpoint(&self, endpoint: Endpoint) {
        self.channel_manager.offer_endpoint(endpoint).await
    }

    /// Gets a `Stream` that returns the set of channels in the wallet. The stream will yield with
    /// the new set of channels every time the wallet is updated. The stream will immediately yield
    /// once with the current set of channels upon creation.
    pub fn channels(
        &self,
    ) -> impl Stream<Item = Result<HashMap<ChannelId, ChannelInfo>, Arc<ReadFromDiskError>>> {
        self.channel_manager.channels().map_ok(|channels| {
            let mut channel_infos = HashMap::with_capacity(channels.len());
            for (channel_id, channel) in channels {
                channel_infos.insert(channel_id, ChannelInfo::from_channel(channel));
            }
            channel_infos
        })
    }

    /// Tells the node that the funding transaction for a channel has been confirmed on the
    /// blockchain and the channel can now be considered usable.
    pub fn local_confirm_channel<'a>(
        &'a self,
        channel_id: ChannelId,
        /* TODO:
         * This function should take these arguments as proof that the funding transaction has been
         * confirmed, rather than just trusting the caller to call it at the correct time.
         * See: https://gitlab.com/nblockchain/geelightning/issues/3
         *
         * funding_tx_merkle_tree: &PartialMerkleTree,
         * confirmation_block_headers: &[BlockHeader],
         */
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<(), LocalConfirmChannelError>,
    > + 'a {
        self.channel_manager.local_confirm_channel(channel_id)
    }

    /// Deletes a channel from the wallet, without closing it. This is only for use when
    /// `Node::open_channel` has been called but you then decide to abort creating the channel and
    /// the funding transaction has not yet been broadcast. If you call the method after the
    /// funding transaction has been broadcast you will lose funds.
    pub async fn cancel_channel(&self, channel_id: ChannelId) -> Result<(), CancelChannelError> {
        self.channel_manager.cancel_channel(channel_id).await
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use tempdir::TempDir;

    #[tokio::test]
    async fn connect_two_nodes_and_open_channel() {
        let peer_secret_key_0 = PeerSecretKey::new();
        let local_endpoint_0 = LocalEndpoint {
            peer_secret_key: peer_secret_key_0.clone(),
            addr: addr!("0.0.0.0:0"),
        };
        let base_directory_0 = TempDir::new("geelightning-test").unwrap();
        let (node_0, node_errors_0) = Node::new(base_directory_0.path(), &local_endpoint_0)
            .await
            .unwrap();
        let node_errors_task_0 = async move {
            pin_mut!(node_errors_0);
            while let Some(node_error) = node_errors_0.next().await {
                println!("node_0 error: {}", node_error);
            }
        };

        let peer_secret_key_1 = PeerSecretKey::new();
        let local_endpoint_1 = LocalEndpoint {
            peer_secret_key: peer_secret_key_1.clone(),
            addr: addr!("0.0.0.0:0"),
        };
        let base_directory_1 = TempDir::new("geelightning-test").unwrap();
        let (node_1, node_errors_1) = Node::new(base_directory_1.path(), &local_endpoint_1)
            .await
            .unwrap();
        let node_errors_task_1 = async move {
            pin_mut!(node_errors_1);
            while let Some(node_error) = node_errors_1.next().await {
                println!("node_1 error: {}", node_error);
            }
        };

        let actual_endpoint_1 = node_1.get_listen_endpoint().await;
        node_0.offer_endpoint(actual_endpoint_1).await;

        let peer_id = PeerId::from_peer_secret_key(&peer_secret_key_1);
        const FUNDING_AMOUNT: Sat64 = Sat64::from_u64(100);
        let open_channel_res_future = node_0
            .open_channel(
                peer_id,
                Sat64::from_u64(100),
                SatPerKw32::from_u32(1),
                Network::Bitcoin,
            )
            .for_each(|msg_channel_error| async move {
                println!(
                    "msg channel error during open_channel: {}",
                    msg_channel_error
                );
            });
        let (temporary_channel_id, _funding_txout) = open_channel_res_future.await.unwrap();

        let funding_outpoint = OutPoint::random();
        let fund_channel_res_future = node_0
            .fund_channel(peer_id, temporary_channel_id, funding_outpoint)
            .for_each(|msg_channel_error| async move {
                println!(
                    "msg channel error during fund_channel: {}",
                    msg_channel_error
                );
            });
        let channel_id = fund_channel_res_future.await.unwrap();

        let channels_0 = Box::pin(node_0.channels()).next().await.unwrap().unwrap();
        let mut channels_0: Vec<_> = channels_0.into_iter().collect();
        assert_eq!(channels_0.len(), 1);
        let (stored_channel_id, stored_channel) = channels_0.pop().unwrap();
        assert_eq!(channel_id, stored_channel_id);
        assert_eq!(stored_channel.confirmed_balance, Msat64::from_u64(0));
        assert_eq!(
            stored_channel.unconfirmed_balance,
            FUNDING_AMOUNT.try_to_msat64().unwrap()
        );

        let channels_1 = Box::pin(node_1.channels()).next().await.unwrap().unwrap();
        let mut channels_1: Vec<_> = channels_1.into_iter().collect();
        assert_eq!(channels_1.len(), 1);
        let (stored_channel_id, stored_channel) = channels_1.pop().unwrap();
        assert_eq!(channel_id, stored_channel_id);
        assert_eq!(stored_channel.confirmed_balance, Msat64::from_u64(0));
        assert_eq!(stored_channel.unconfirmed_balance, Msat64::from_u64(0));

        let () = node_0
            .local_confirm_channel(channel_id)
            .for_each(|local_confirm_error| async move {
                println!("local confirm error: {}", local_confirm_error);
            })
            .await
            .unwrap();
        let () = node_1
            .local_confirm_channel(channel_id)
            .for_each(|local_confirm_error| async move {
                println!("local confirm error: {}", local_confirm_error);
            })
            .await
            .unwrap();

        let channels_stream_0 = node_0.channels().fuse();
        pin_mut!(channels_stream_0);
        let timeout = sleep(Duration::from_secs(5)).fuse();
        pin_mut!(timeout);
        loop {
            let channels_0 = futures::select! {
                channels_0_res_opt = channels_stream_0.next() => {
                    channels_0_res_opt.unwrap().unwrap()
                },
                () = timeout => {
                    panic!("timed out waiting for channel confirmation to take effect");
                },
            };
            let mut channels_0: Vec<_> = channels_0.into_iter().collect();
            assert_eq!(channels_0.len(), 1);
            let (stored_channel_id, stored_channel) = channels_0.pop().unwrap();
            assert_eq!(channel_id, stored_channel_id);
            assert_eq!(
                stored_channel.unconfirmed_balance,
                FUNDING_AMOUNT.try_to_msat64().unwrap()
            );
            if stored_channel.confirmed_balance == FUNDING_AMOUNT.try_to_msat64().unwrap() {
                break;
            } else if stored_channel.confirmed_balance == Msat64::from_u64(0) {
                continue;
            } else {
                panic!("unexpected balance: {}", stored_channel.confirmed_balance);
            }
        }

        let channels_1 = Box::pin(node_1.channels()).next().await.unwrap().unwrap();
        let mut channels_1: Vec<_> = channels_1.into_iter().collect();
        assert_eq!(channels_1.len(), 1);
        let (stored_channel_id, stored_channel) = channels_1.pop().unwrap();
        assert_eq!(channel_id, stored_channel_id);
        assert_eq!(stored_channel.confirmed_balance, Msat64::from_u64(0));
        assert_eq!(stored_channel.unconfirmed_balance, Msat64::from_u64(0));

        drop(node_0);
        drop(node_1);
        let ((), ()) = futures::join!(node_errors_task_0, node_errors_task_1);
    }
}
