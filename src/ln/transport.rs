//! This module provides the `TransportConnector` type which binds to a port (wrapping a
//! `TcpListener`) accepts incoming connections and allows making outgoing connections from that
//! port. It also wraps the `handshake` module to provide encryption on all TCP streams.

use super::*;

#[derive(Debug)]
pub struct Transport {
    pub transport_send: TransportSend,
    pub transport_recv: TransportRecv,
}

#[derive(Debug, Clone)]
pub struct TransportSend {
    msg_send: mpsc::Sender<(Bytes, oneshot::Sender<io::Result<()>>)>,
}

#[derive(Debug)]
pub struct TransportRecv {
    read_stream: tokio::net::tcp::OwnedReadHalf,
    recv_key: HandshakeKey,
}

impl Transport {
    fn from_handshaken_tcp_stream(
        tcp_stream: TcpStream,
        send_key: HandshakeKey,
        recv_key: HandshakeKey,
    ) -> Transport {
        let (read_stream, mut write_stream) = tcp_stream.into_split();
        let (msg_send, mut msg_recv) = mpsc_channel!();
        let transport_send = TransportSend { msg_send };
        let transport_recv = TransportRecv {
            read_stream,
            recv_key,
        };
        spawn!(async move {
            let mut send_key = send_key;
            while let Some((msg, result_send)) = msg_recv.next().await {
                let result = handshake::send_msg(&mut write_stream, &mut send_key, &msg).await;
                result_send.send(result).await.ignore_err();
            }

            future::poll_fn(|cx| {
                let write_stream = &mut write_stream;
                pin_mut!(write_stream);
                AsyncWrite::poll_shutdown(write_stream, cx)
            })
            .await
            .ignore_err();
        });
        Transport {
            transport_send,
            transport_recv,
        }
    }
}

#[derive(Debug, Error)]
pub enum ConnectError {
    #[error("tcp connect error: {}", source)]
    Connect { source: io::Error },
    #[error("handshake failed: {}", source)]
    Handshake { source: handshake::HandshakeError },
}

impl ConnectError {
    pub fn is_protocol_violation(&self) -> bool {
        match self {
            ConnectError::Connect { .. } => false,
            ConnectError::Handshake { source } => source.is_protocol_violation(),
        }
    }
}

impl TransportSend {
    pub async fn send_msg(&mut self, msg: Bytes) -> io::Result<()> {
        let (result_send, result_recv) = oneshot_channel!();
        self.msg_send.send((msg, result_send)).await.unwrap();
        result_recv.await.unwrap()
    }
}

impl TransportRecv {
    pub async fn recv_msg(&mut self) -> Result<Option<Bytes>, handshake::RecvMsgError> {
        handshake::recv_msg(&mut self.read_stream, &mut self.recv_key).await
    }
}

#[derive(Clone)]
pub struct TransportConnector {
    local_endpoint: LocalEndpoint,
}

impl TransportConnector {
    pub async fn connect(&self, endpoint: &Endpoint) -> Result<Transport, ConnectError> {
        let mut stream = {
            TcpStream::connect_reusable(self.local_endpoint.addr, endpoint.addr)
                .map_err(|source| ConnectError::Connect { source })
                .await?
        };
        let (send_key, recv_key) = {
            handshake::initiate_handshake(
                &mut stream,
                &self.local_endpoint.peer_secret_key,
                &endpoint.peer_id,
            )
            .map_err(|source| ConnectError::Handshake { source })
            .await?
        };
        Ok(Transport::from_handshaken_tcp_stream(
            stream, send_key, recv_key,
        ))
    }
}

pub struct TransportListener {
    stream_recv: mpsc::Receiver<Result<(Transport, Endpoint), AcceptError>>,
    _drop_send: DropSender,
}

impl TransportListener {
    pub async fn bind(
        local_endpoint: &LocalEndpoint,
    ) -> io::Result<(TransportConnector, TransportListener, Endpoint)> {
        let peer_secret_key = local_endpoint.peer_secret_key.clone();
        let peer_id = PeerId::from_peer_secret_key(&peer_secret_key);
        let mut listener = TcpListener::bind_reusable(local_endpoint.addr)?;
        let addr = listener.local_addr()?;
        let (mut stream_send, stream_recv) = mpsc_channel!();
        let drop_send = DropSender::new();
        let drop_recv = drop_send.receiver();

        let local_endpoint = LocalEndpoint {
            peer_secret_key: peer_secret_key.clone(),
            addr,
        };
        let listen_endpoint = Endpoint { peer_id, addr };
        let transport_connector = TransportConnector { local_endpoint };
        let transport_listener = TransportListener {
            _drop_send: drop_send,
            stream_recv,
        };
        spawn!(async move {
            let mut drop_recv = drop_recv.fuse();
            loop {
                let accept_stream = accept_stream(&mut listener, &peer_secret_key).fuse();
                pin_mut!(accept_stream);
                futures::select! {
                    res = accept_stream => {
                        match stream_send.send(res).await {
                            Ok(()) => (),
                            Err(_err) => break,
                        }
                    },
                    () = drop_recv => break,
                }
            }
        });

        Ok((transport_connector, transport_listener, listen_endpoint))
    }

    pub async fn accept(&mut self) -> Result<(Transport, Endpoint), AcceptError> {
        self.stream_recv.next().await.unwrap()
    }
}

async fn accept_stream(
    listener: &mut TcpListener,
    peer_secret_key: &PeerSecretKey,
) -> Result<(Transport, Endpoint), AcceptError> {
    let (mut tcp_stream, addr) = {
        listener
            .accept()
            .await
            .map_err(|source| AcceptError::TcpAccept { source })?
    };

    let (remote_peer_id, recv_key, send_key) = {
        handshake::accept_handshake(&mut tcp_stream, peer_secret_key)
            .map_err(|source| AcceptError::Handshake { source })
            .await?
    };

    let transport = Transport::from_handshaken_tcp_stream(tcp_stream, send_key, recv_key);
    let endpoint = Endpoint {
        peer_id: remote_peer_id,
        addr,
    };
    Ok((transport, endpoint))
}

#[derive(Debug, Error)]
pub enum AcceptError {
    #[error("error accepting incoming tcp connection: {}", source)]
    TcpAccept { source: io::Error },
    #[error("handshake failed: {}", source)]
    Handshake { source: handshake::HandshakeError },
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn connect_accept_send_recv() {
        let secret_key_0 = SecretKey::new();
        let local_endpoint_0 = LocalEndpoint {
            peer_secret_key: PeerSecretKey::from_secret_key(secret_key_0),
            addr: addr!("0.0.0.0:0"),
        };
        let (_transport_connector, mut transport_listener, endpoint_0) =
            { TransportListener::bind(&local_endpoint_0).await.unwrap() };

        let secret_key_1 = SecretKey::new();
        let local_endpoint_1 = LocalEndpoint {
            peer_secret_key: PeerSecretKey::from_secret_key(secret_key_1),
            addr: addr!("0.0.0.0:0"),
        };
        let (transport_connector, _transport_listener, _endpoint_1) =
            { TransportListener::bind(&local_endpoint_1).await.unwrap() };

        let connect = {
            transport_connector
                .connect(&endpoint_0)
                .unwrap_or_else(|err| panic!("connect failed: {}", err))
        };
        let accept = {
            transport_listener
                .accept()
                .unwrap_or_else(|err| panic!("accept failed: {}", err))
        };
        let (transport_0, (transport_1, _endpoint_1)) = futures::join!(connect, accept);
        let mut transport_send_0 = transport_0.transport_send;
        let mut transport_recv_0 = transport_0.transport_recv;
        let mut transport_send_1 = transport_1.transport_send;
        let mut transport_recv_1 = transport_1.transport_recv;

        const NUM_MSGS: usize = 2000;
        let mut msgs_0 = Vec::with_capacity(NUM_MSGS);
        for len in 0..=1000 {
            msgs_0.push(Bytes::from(random_vec(len)));
        }
        for len in (u16::MAX as usize - 1000)..=(u16::MAX as usize) {
            msgs_0.push(Bytes::from(random_vec(len)));
        }

        let msgs_1 = msgs_0.clone();
        let send = async move {
            for msg in msgs_0 {
                transport_send_0.send_msg(msg.clone()).await.unwrap();
            }
        };
        let bounce = async move {
            while let Some(msg) = transport_recv_1.recv_msg().await.unwrap() {
                transport_send_1.send_msg(msg).await.unwrap();
            }
        };
        let recv = async move {
            let mut msgs_2 = Vec::with_capacity(NUM_MSGS);
            while let Some(msg) = transport_recv_0.recv_msg().await.unwrap() {
                msgs_2.push(msg);
            }
            msgs_2
        };
        let ((), (), msgs_2) = futures::join!(send, bounce, recv);
        assert_eq!(msgs_1, msgs_2);
    }
}
