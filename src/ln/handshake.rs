//! This module is an implementation of BOLT #8
//! https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md

#![allow(clippy::many_single_char_names)]
use super::*;
use bitcoin::hashes::HashEngine;

#[derive(Debug)]
pub struct HandshakeKey {
    ck: [u8; 32],
    k: [u8; 32],
    n: u64,
}

/// Implements BOLT #8: Encrypting and Sending Messages
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#encrypting-and-sending-messages
pub async fn send_msg<W: AsyncWrite + Unpin>(
    stream: &mut W,
    handshake_key: &mut HandshakeKey,
    msg: &[u8],
) -> io::Result<()> {
    let l = (msg.len() as u16).to_be_bytes();
    let lc = encrypt_with_ad(&handshake_key.k, handshake_key.n, &[], &l);
    stream.write_all(&lc).await?;
    rotate_key(handshake_key);

    let c = encrypt_with_ad(&handshake_key.k, handshake_key.n, &[], &msg);
    stream.write_all(&c).await?;
    rotate_key(handshake_key);

    Ok(())
}

/// Implements BOLT #8: Receiving and Decrypting Messages
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#receiving-and-decrypting-messages
pub async fn recv_msg<R: AsyncRead + Unpin>(
    stream: &mut R,
    handshake_key: &mut HandshakeKey,
) -> Result<Option<Bytes>, RecvMsgError> {
    let mut lc = [0u8; 18];
    let bytes_read = match stream.read(&mut lc).await {
        Ok(n) => n,
        Err(source) => return Err(RecvMsgError::Io { source }),
    };
    // Check if the first read returns 0 bytes so that we can handle disconnection
    // gracefully.
    if bytes_read == 0 {
        return Ok(None);
    }
    if bytes_read < lc.len() {
        stream
            .read_exact(&mut lc[bytes_read..])
            .map_err(|source| RecvMsgError::Io { source })
            .await?;
    }

    let l = {
        let l = {
            decrypt_with_ad(&handshake_key.k, handshake_key.n, &[], &lc)
                .ok_or(RecvMsgError::InvalidMsg)
        }?;
        let l = l.try_into().unwrap();
        u16::from_be_bytes(l)
    };

    rotate_key(handshake_key);

    let mut c = vec![0; l as usize + 16];
    stream
        .read_exact(&mut c)
        .map_err(|source| RecvMsgError::Io { source })
        .await?;

    let msg = {
        decrypt_with_ad(&handshake_key.k, handshake_key.n, &[], &c)
            .ok_or(RecvMsgError::InvalidMsg)?
    };

    rotate_key(handshake_key);

    Ok(Some(Bytes::from(msg)))
}

#[derive(Debug, Error)]
pub enum RecvMsgError {
    #[error("error reading from socket: {}", source)]
    Io { source: io::Error },
    #[error("invalid message received from peer")]
    InvalidMsg,
}

impl RecvMsgError {
    pub fn is_protocol_violation(&self) -> bool {
        match self {
            RecvMsgError::Io { .. } => false,
            RecvMsgError::InvalidMsg => true,
        }
    }
}

pub async fn initiate_handshake(
    stream: &mut TcpStream,
    ls_peer_id: &PeerSecretKey,
    rs_peer_id: &PeerId,
) -> Result<(HandshakeKey, HandshakeKey), HandshakeError> {
    let ls_secret_key = ls_peer_id.as_secret_key();
    let ls_public_key = PublicKey::from_secret_key(ls_secret_key);
    let rs = rs_peer_id.as_public_key();
    let e_secret_key = SecretKey::new();
    let e_public_key = PublicKey::from_secret_key(&e_secret_key);
    initiate_handshake_with_keys(
        stream,
        ls_secret_key,
        &ls_public_key,
        rs,
        &e_secret_key,
        &e_public_key,
    )
    .await
}

/// This function (and accept_handshake_with_keys) implement BOLT #8: Handshake Exchange
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#handshake-exchange
///
/// The only difference is we pass the ephemeral keys in as arguments rather than generate them in
/// the function. This is for ease of testing.
async fn initiate_handshake_with_keys(
    stream: &mut TcpStream,
    ls_secret_key: &SecretKey,
    ls_public_key: &PublicKey,
    rs: &PublicKey,
    e_secret_key: &SecretKey,
    e_public_key: &PublicKey,
) -> Result<(HandshakeKey, HandshakeKey), HandshakeError> {
    let (h, ck) = init_handshake_state();

    // BOLT #8: Act One (Sender Actions)

    let h = sha256_multi(&[&h, &rs.to_bytes()]);

    let h = sha256_multi(&[&h, &e_public_key.to_bytes()]);
    let es = SharedSecret::new(&rs, &e_secret_key);
    let (ck, temp_k1) = hkdf(&ck, es.as_slice());

    let c = encrypt_with_ad(&temp_k1, 0, &h, &[]);

    let h = sha256_multi(&[&h, &c]);
    let mut output = Vec::with_capacity(50);
    output.push(0);
    output.extend(&e_public_key.to_bytes());
    output.extend(&c);

    stream
        .write_all(&output)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    // BOLT #8: Act Two (Receiver Actions)

    let mut version = [0; 1];
    stream
        .read_exact(&mut version)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let [version] = version;

    if version != 0 {
        return Err(HandshakeError::NonZeroVersion(version));
    }

    let mut re_bytes = [0; 33];
    stream
        .read_exact(&mut re_bytes)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let re =
        { PublicKey::try_from_bytes(re_bytes).map_err(|_err| HandshakeError::InvalidResponse)? };

    let mut c = [0; 16];
    stream
        .read_exact(&mut c)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    let h = sha256_multi(&[&h, &re_bytes]);
    let ee = SharedSecret::new(&re, &e_secret_key);
    let (ck, temp_k2) = hkdf(&ck, ee.as_slice());
    match decrypt_with_ad(&temp_k2, 0, &h, &c) {
        Some(..) => (),
        None => return Err(HandshakeError::FailedMacDecryption),
    }

    let h = sha256_multi(&[&h, &c]);

    // BOLT #8: Act Three (Sender Actions)

    let c = encrypt_with_ad(&temp_k2, 1, &h, &ls_public_key.to_bytes());
    let h = sha256_multi(&[&h, &c]);
    let se = SharedSecret::new(&re, &ls_secret_key);
    let (ck, temp_k3) = hkdf(&ck, se.as_slice());
    let t = encrypt_with_ad(&temp_k3, 0, &h, &[]);
    let (sk, rk) = hkdf(&ck, &[]);

    let mut output = Vec::with_capacity(66);
    output.push(0);
    output.extend(&c);
    output.extend(&t);

    stream
        .write_all(&output)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    let s = HandshakeKey { ck, k: sk, n: 0 };
    let r = HandshakeKey { ck, k: rk, n: 0 };

    Ok((s, r))
}

pub async fn accept_handshake(
    stream: &mut TcpStream,
    ls_peer_id: &PeerSecretKey,
) -> Result<(PeerId, HandshakeKey, HandshakeKey), HandshakeError> {
    let ls_secret_key = ls_peer_id.as_secret_key();
    let ls_public_key = PublicKey::from_secret_key(ls_secret_key);
    let e_secret_key = SecretKey::new();
    let e_public_key = PublicKey::from_secret_key(&e_secret_key);
    accept_handshake_with_keys(
        stream,
        ls_secret_key,
        &ls_public_key,
        &e_secret_key,
        &e_public_key,
    )
    .await
}

/// This function (and initiate_handshake_with_keys) implement BOLT #8: Handshake Exchange
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#handshake-exchange
///
/// The only difference is we pass the ephemeral keys in as arguments rather than generate them in
/// the function. This is for ease of testing.
pub(crate) async fn accept_handshake_with_keys(
    stream: &mut TcpStream,
    ls_secret_key: &SecretKey,
    ls_public_key: &PublicKey,
    e_secret_key: &SecretKey,
    e_public_key: &PublicKey,
) -> Result<(PeerId, HandshakeKey, HandshakeKey), HandshakeError> {
    let (h, ck) = init_handshake_state();

    // BOLT #8: Act One (Receiver Actions)

    let h = sha256_multi(&[&h, &ls_public_key.to_bytes()]);

    let mut version = [0; 1];
    stream
        .read_exact(&mut version)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let [version] = version;
    if version != 0 {
        return Err(HandshakeError::NonZeroVersion(version));
    }

    let mut re_bytes = [0; 33];
    stream
        .read_exact(&mut re_bytes)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let re =
        { PublicKey::try_from_bytes(re_bytes).map_err(|_err| HandshakeError::InvalidResponse)? };

    let mut c = [0; 16];
    stream
        .read_exact(&mut c)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    let h = sha256_multi(&[&h, &re_bytes]);
    let es = SharedSecret::new(&re, &ls_secret_key);
    let (ck, temp_k1) = hkdf(&ck, es.as_slice());
    match decrypt_with_ad(&temp_k1, 0, &h, &c) {
        Some(..) => (),
        None => return Err(HandshakeError::FailedMacDecryption),
    };
    let h = sha256_multi(&[&h, &c]);

    // BOLT #8: Act Two (Sender Actions)

    let h = sha256_multi(&[&h, &e_public_key.to_bytes()]);
    let ee = SharedSecret::new(&re, &e_secret_key);

    let (ck, temp_k2) = hkdf(&ck, ee.as_slice());
    let c = encrypt_with_ad(&temp_k2, 0, &h, &[]);
    let h = sha256_multi(&[&h, &c]);

    let mut output = Vec::with_capacity(50);
    output.push(0);
    output.extend(&e_public_key.to_bytes());
    output.extend(c);
    stream
        .write_all(&output)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    // BOLT #8: Act Three (Receiver Actions)

    let mut version = [0; 1];
    stream
        .read_exact(&mut version)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let [version] = version;
    if version != 0 {
        return Err(HandshakeError::NonZeroVersion(version));
    }

    let mut c = [0; 49];
    stream
        .read_exact(&mut c)
        .map_err(|source| HandshakeError::Io { source })
        .await?;
    let mut t = [0; 16];
    stream
        .read_exact(&mut t)
        .map_err(|source| HandshakeError::Io { source })
        .await?;

    let rs = match decrypt_with_ad(&temp_k2, 1, &h, &c) {
        Some(rs) => rs,
        None => return Err(HandshakeError::FailedMacDecryption),
    };
    let rs: [u8; PUBLIC_KEY_BYTES_LEN] =
        rs.try_into().ok().ok_or(HandshakeError::InvalidResponse)?;
    let rs = PublicKey::try_from_bytes(rs).map_err(|_err| HandshakeError::InvalidResponse)?;
    let h = sha256_multi(&[&h, &c]);
    let se = SharedSecret::new(&rs, &e_secret_key);
    let (ck, temp_k3) = hkdf(&ck, se.as_slice());
    match decrypt_with_ad(&temp_k3, 0, &h, &t) {
        Some(..) => (),
        None => return Err(HandshakeError::FailedMacDecryption),
    };
    let (rk, sk) = hkdf(&ck, &[]);

    let r = HandshakeKey { ck, k: rk, n: 0 };
    let s = HandshakeKey { ck, k: sk, n: 0 };
    let rs = PeerId::from_public_key(rs);

    Ok((rs, r, s))
}

/// Implements BOLT #8: Lightning Message Key Rotation
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#lightning-message-key-rotation
fn rotate_key(handshake_key: &mut HandshakeKey) {
    handshake_key.n += 1;
    if handshake_key.n == 1000 {
        let (ck, k) = hkdf(&handshake_key.ck, &handshake_key.k);
        let n = 0;
        *handshake_key = HandshakeKey { ck, k, n };
    }
}

#[derive(Debug, Error)]
pub enum HandshakeError {
    #[error("io error on socket: {}", source)]
    Io { source: io::Error },
    #[error("remote peer sent an invalid response")]
    InvalidResponse,
    #[error("remote peer took too long to respond")]
    TimedOut,
    #[error("version changed unexpectedly. Expected 0, found {}", _0)]
    NonZeroVersion(u8),
    #[error("failed to decrypt the MAC (Message Authentication Code)")]
    FailedMacDecryption,
}

impl HandshakeError {
    pub fn is_protocol_violation(&self) -> bool {
        match self {
            HandshakeError::Io { .. } => false,
            HandshakeError::TimedOut => false,
            HandshakeError::InvalidResponse => true,
            HandshakeError::NonZeroVersion(..) => true,
            HandshakeError::FailedMacDecryption => true,
        }
    }
}

/// Implements BOLT #8: Handshake State Initialization
/// https://github.com/lightningnetwork/lightning-rfc/blob/master/08-transport.md#handshake-state-initialization
fn init_handshake_state() -> ([u8; 32], [u8; 32]) {
    let h = sha256_multi(&[b"Noise_XK_secp256k1_ChaChaPoly_SHA256"]);
    let ck = h;
    let h = sha256_multi(&[&h, b"lightning"]);
    (h, ck)
}

fn hkdf(salt: &[u8], ikm: &[u8]) -> ([u8; 32], [u8; 32]) {
    let (_, hkdf) = Hkdf::<Sha256>::extract(Some(salt), ikm);
    let mut expanded = [0u8; 64];
    hkdf.expand(&[], &mut expanded).unwrap();
    let r0 = expanded[..32].try_into().unwrap();
    let r1 = expanded[32..].try_into().unwrap();
    (r0, r1)
}

fn encode_nonce(n: u64) -> [u8; 12] {
    concat_arrays!([0u8; 4], n.to_le_bytes())
}

fn encrypt_with_ad(k: &[u8], n: u64, ad: &[u8], plaintext: &[u8]) -> Vec<u8> {
    let nonce = encode_nonce(n);

    let mut v = Vec::new();
    let c = chacha20_poly1305_aead::encrypt(k, &nonce, ad, plaintext, &mut v).unwrap();
    v.extend(&c);
    v
}

fn decrypt_with_ad(k: &[u8], n: u64, ad: &[u8], ciphertext: &[u8]) -> Option<Vec<u8>> {
    let nonce = encode_nonce(n);
    let len = ciphertext.len();

    let mut v = Vec::new();
    match chacha20_poly1305_aead::decrypt(
        &k,
        &nonce,
        ad,
        &ciphertext[..(len - 16)],
        &ciphertext[(len - 16)..],
        &mut v,
    ) {
        Ok(()) => Some(v),
        Err(..) => None,
    }
}

fn sha256_multi(blocks: &[&[u8]]) -> [u8; 32] {
    let mut engine = sha256::HashEngine::default();
    for block in blocks {
        engine.input(block)
    }
    sha256::Hash::from_engine(engine).into_inner()
}

#[cfg(test)]
mod test {
    use super::*;

    const INITIATOR_WRITE_TO_RESPONDER_1: [u8; 50] = hex!(
        "0002466d7fcae563e5cb09a0d1870bb580344804617879a149
        49cf22285f1bae3f276e2470b93aac583c9ef6eafca3f730ae"
    );
    const RESPONDER_WRITE_TO_INITIATOR_1: [u8; 50] = hex!(
        "00036360e856310ce5d294e8be33fc807077dc56ac80d95d9c
        d4ddbd21325eff73f70df6086551151f58b8afe6c195782c6a"
    );
    const RESPONDER_WRITE_TO_INITIATOR_2: [u8; 66] = hex!(
        "00b9e3a702e93e3a9948c2ed6e5fd7590a6e1c3a0344cfc9d5b57357049aa22355
        361aa02e55a8fc28fef5bd6d71ad0c38228dc68b1c466263b47fdf31e560e139ba"
    );

    lazy_static::lazy_static! {
        static ref SERVER_PUBLIC_KEY: PublicKey = PublicKey::try_from_bytes(
            hex!("028d7500dd4c12685d1f568b4c2b5048e8534b873319f3a8daa612b469132ec7f7")
        ).unwrap();
        static ref SERVER_SECRET_KEY: SecretKey = SecretKey::try_from_bytes(
            hex!("2121212121212121212121212121212121212121212121212121212121212121")
        ).unwrap();
        static ref SERVER_EPHEMERAL_SECRET_KEY: SecretKey =
            SecretKey::try_from_bytes(hex!(
                "2222222222222222222222222222222222222222222222222222222222222222"
            ))
            .unwrap();
        static ref SERVER_EPHEMERAL_PUBLIC_KEY: PublicKey =
            PublicKey::try_from_bytes(hex!(
                "02466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f27"
            ))
            .unwrap();
    }

    async fn default_accept_handshake(
        stream: &mut TcpStream,
    ) -> Result<(PeerId, HandshakeKey, HandshakeKey), HandshakeError> {
        let server_public_key = PublicKey::try_from_bytes(hex!(
            "028d7500dd4c12685d1f568b4c2b5048e8534b873319f3a8daa612b469132ec7f7"
        ))
        .unwrap();
        let server_e_secret_key = SecretKey::try_from_bytes(hex!(
            "2222222222222222222222222222222222222222222222222222222222222222"
        ))
        .unwrap();
        let server_e_public_key = PublicKey::try_from_bytes(hex!(
            "02466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f27"
        ))
        .unwrap();
        accept_handshake_with_keys(
            stream,
            &SERVER_SECRET_KEY,
            &server_public_key,
            &server_e_secret_key,
            &server_e_public_key,
        )
        .await
    }

    async fn default_initiator_handshake(
        stream: &mut TcpStream,
    ) -> Result<(HandshakeKey, HandshakeKey), HandshakeError> {
        let client_e_secret_key = SecretKey::try_from_bytes(hex!(
            "1212121212121212121212121212121212121212121212121212121212121212"
        ))
        .unwrap();
        let client_e_public_key = PublicKey::try_from_bytes(hex!(
            "036360e856310ce5d294e8be33fc807077dc56ac80d95d9cd4ddbd21325eff73f7"
        ))
        .unwrap();
        let client_secret_key = SecretKey::try_from_bytes(hex!(
            "1111111111111111111111111111111111111111111111111111111111111111"
        ))
        .unwrap();
        let client_public_key = PublicKey::try_from_bytes(hex!(
            "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa"
        ))
        .unwrap();
        initiate_handshake_with_keys(
            stream,
            &client_secret_key,
            &client_public_key,
            &SERVER_PUBLIC_KEY,
            &client_e_secret_key,
            &client_e_public_key,
        )
        .await
    }

    #[tokio::test]
    async fn bolt8_act_one_responder_must_abort_if_version_is_nonzero() {
        let mut bytes = RESPONDER_WRITE_TO_INITIATOR_1;
        bytes[0] = 1;
        let conn_rslt = test_utils::simulate_initiator_for_responder_handshake(
            |mut stream| async move { default_accept_handshake(&mut stream).await },
            || &bytes,
            |_| {},
            || &RESPONDER_WRITE_TO_INITIATOR_2,
        )
        .await;
        assert_matches!(conn_rslt, Err(HandshakeError::NonZeroVersion(1)));
    }

    #[tokio::test]
    async fn bolt8_act_one_responder_must_abort_if_mac_check_fails() {
        let mut bytes = RESPONDER_WRITE_TO_INITIATOR_1;
        bytes[14] = 0;
        let rslt = test_utils::simulate_initiator_for_responder_handshake(
            |mut stream| async move { default_accept_handshake(&mut stream).await },
            || &bytes,
            |_| {},
            || &RESPONDER_WRITE_TO_INITIATOR_2,
        )
        .await;
        assert_matches!(rslt, Err(HandshakeError::FailedMacDecryption));
    }

    #[tokio::test]
    async fn bolt8_act_two_initiator_must_abort_if_version_is_nonzero() {
        let mut bytes = INITIATOR_WRITE_TO_RESPONDER_1;
        bytes[0] = 1;
        let rslt = test_utils::simulate_responder_for_initiator_handshake(
            |mut stream| async move { default_initiator_handshake(&mut stream).await },
            |_| {},
            || &bytes,
            |_| {},
        )
        .await;
        assert_matches!(rslt, Err(HandshakeError::NonZeroVersion(1)));
    }

    #[tokio::test]
    async fn bolt8_act_two_initiator_must_abort_if_mac_check_fails() {
        let mut bytes = INITIATOR_WRITE_TO_RESPONDER_1;
        bytes[7] = 0;
        let rslt = test_utils::simulate_responder_for_initiator_handshake(
            |mut stream| async move { default_initiator_handshake(&mut stream).await },
            |_| {},
            || &bytes,
            |_| {},
        )
        .await;
        assert_matches!(rslt, Err(HandshakeError::FailedMacDecryption));
    }

    #[tokio::test]
    async fn bolt8_act_three_responder_must_abort_if_version_is_nonzero() {
        let mut bytes = RESPONDER_WRITE_TO_INITIATOR_2;
        bytes[0] = 1;
        let conn_rslt = test_utils::simulate_initiator_for_responder_handshake(
            |mut stream| async move { default_accept_handshake(&mut stream).await },
            || &RESPONDER_WRITE_TO_INITIATOR_1,
            |_| {},
            || &bytes,
        )
        .await;
        assert_matches!(conn_rslt, Err(HandshakeError::NonZeroVersion(1)));
    }

    #[tokio::test]
    async fn bolt8_act_three_responder_must_abort_if_mac_check_fails() {
        let mut bytes = RESPONDER_WRITE_TO_INITIATOR_2;
        bytes[7] = 0;
        let rslt = test_utils::simulate_initiator_for_responder_handshake(
            |mut stream| async move { default_accept_handshake(&mut stream).await },
            || &RESPONDER_WRITE_TO_INITIATOR_1,
            |_| {},
            || &bytes,
        )
        .await;
        assert_matches!(rslt, Err(HandshakeError::FailedMacDecryption));
    }

    #[tokio::test]
    async fn handshake_accept() {
        let initial_bytes_to_write = &hex!(
            "00036360e856310ce5d294e8be33fc807077dc56ac80d95d9c
            d4ddbd21325eff73f70df6086551151f58b8afe6c195782c6a"
        );
        let final_bytes_to_write = &hex!(
            "00b9e3a702e93e3a9948c2ed6e5fd7590a6e1c3a0344cfc9d5b57357049aa22355
            361aa02e55a8fc28fef5bd6d71ad0c38228dc68b1c466263b47fdf31e560e139ba"
        );

        let (h, ck) = init_handshake_state();
        let h = sha256_multi(&[&h, &SERVER_PUBLIC_KEY.to_bytes()]);

        let re: [u8; 33] = initial_bytes_to_write[1..34].try_into().unwrap();
        let c: [u8; 16] = initial_bytes_to_write[34..].try_into().unwrap();
        let h = sha256_multi(&[&h, &re]);
        assert_eq!(
            &re,
            &hex!(
                "036360e856310ce5d294e8be33fc80707
            7dc56ac80d95d9cd4ddbd21325eff73f7"
            )
        );
        assert_eq!(
            h,
            hex!("9e0e7de8bb75554f21db034633de04be41a2b8a18da7a319a03c803bf02b396c")
        );
        let re = PublicKey::try_from_bytes(re).unwrap();
        let ss = SharedSecret::new(&re, &SERVER_SECRET_KEY);
        assert_eq!(
            ss.as_slice(),
            &hex!(
                "1e2fb3c8fe8fb9f262f649f64d26ecf0
            f2c0a805a767cf02dc2d77a6ef1fdcc3"
            )
        );
        let (ck, temp_k1) = hkdf(&ck, ss.as_slice());
        assert_eq!(
            ck,
            hex!("b61ec1191326fa240decc9564369dbb3ae2b34341d1e11ad64ed89f89180582f")
        );
        assert_eq!(
            temp_k1,
            hex!("e68f69b7f096d7917245f5e5cf8ae1595febe4d4644333c99f9c4a1282031c9f")
        );
        decrypt_with_ad(&temp_k1, 0, &h, &c).unwrap();
        let h = sha256_multi(&[&h, &c]);
        assert_eq!(
            h,
            hex!("9d1ffbb639e7e20021d9259491dc7b160aab270fb1339ef135053f6f2cebe9ce")
        );
        let h = sha256_multi(&[&h, &SERVER_EPHEMERAL_PUBLIC_KEY.to_bytes()]);
        assert_eq!(
            h,
            hex!("38122f669819f906000621a14071802f93f2ef97df100097bcac3ae76c6dc0bf")
        );

        let ee = SharedSecret::new(&re, &SERVER_EPHEMERAL_SECRET_KEY);
        assert_eq!(
            ee.as_slice(),
            &hex!("c06363d6cc549bcb7913dbb9ac1c33fc1158680c89e972000ecd06b36c472e47")
        );

        let (ck, temp_k2) = hkdf(&ck, ee.as_slice());
        assert_eq!(
            ck,
            hex!("e89d31033a1b6bf68c07d22e08ea4d7884646c4b60a9528598ccb4ee2c8f56ba")
        );
        assert_eq!(
            temp_k2,
            hex!("908b166535c01a935cf1e130a5fe895ab4e6f3ef8855d87e9b7581c4ab663ddc")
        );

        let c = encrypt_with_ad(&temp_k2, 0, &h, &[]);
        assert_eq!(c, hex!("6e2470b93aac583c9ef6eafca3f730ae"));
        let h = sha256_multi(&[&h, &c]);
        assert_eq!(
            h,
            hex!("90578e247e98674e661013da3c5c1ca6a8c8f48c90b485c0dfa1494e23d56d72")
        );

        let rslt = test_utils::simulate_initiator_for_responder_handshake(
            |mut stream| async move { default_accept_handshake(&mut stream).await },
            || initial_bytes_to_write,
            |read_bytes| {
                assert_eq!(
                    &read_bytes,
                    &hex!(
                        "0002466d7fcae563e5cb09a0d1870bb580344804617879a149
                    49cf22285f1bae3f276e2470b93aac583c9ef6eafca3f730ae"
                    ),
                );
            },
            || final_bytes_to_write,
        )
        .await;

        let c: [u8; 49] = final_bytes_to_write[1..50].try_into().unwrap();
        let t: [u8; 16] = final_bytes_to_write[50..].try_into().unwrap();
        let rs = decrypt_with_ad(&temp_k2, 1, &h, &c).unwrap();
        assert_eq!(
            &rs,
            &hex!(
                "034f355bdcb7cc0af728ef3cceb9615d
            90684bb5b2ca5f859ab0f0b704075871aa
        "
            )
        );
        let rs: [u8; PUBLIC_KEY_BYTES_LEN] = rs.try_into().unwrap();
        let rs = PublicKey::try_from_bytes(rs).unwrap();
        let h = sha256_multi(&[&h, &c]);
        assert_eq!(
            h,
            hex!("5dcb5ea9b4ccc755e0e3456af3990641276e1d5dc9afd82f974d90a47c918660")
        );
        let se = SharedSecret::new(&rs, &SERVER_EPHEMERAL_SECRET_KEY);
        assert_eq!(
            se.as_slice(),
            &hex!(
                "b36b6d195982c5be874d6d542dc2682
            34379e1ae4ff1709402135b7de5cf0766
        "
            )
        );
        let (ck, temp_k3) = hkdf(&ck, se.as_slice());
        assert_eq!(
            ck,
            hex!("919219dbb2920afa8db80f9a51787a840bcf111ed8d588caf9ab4be716e42b01")
        );
        assert_eq!(
            temp_k3,
            hex!(
                "981a46c820fb7a241bc8184ba4bb1f0
            1bcdfafb00dde80098cb8c38db9141520"
            )
        );
        decrypt_with_ad(&temp_k3, 0, &h, &t).unwrap();
        let (rk, sk) = hkdf(&ck, &[]);
        assert_eq!(
            rk,
            hex!("969ab31b4d288cedf6218839b27a3e2140827047f2c0f01bf5c04435d43511a9")
        );
        assert_eq!(
            sk,
            hex!("bb9020b8965f4df047e07f955f3c4b88418984aadc5cdb35096b9ea8fa5c3442")
        );

        let (_, rk, sk) = rslt.unwrap();
        assert_eq!(
            rk.k,
            hex!("969ab31b4d288cedf6218839b27a3e2140827047f2c0f01bf5c04435d43511a9")
        );
        assert_eq!(
            sk.k,
            hex!("bb9020b8965f4df047e07f955f3c4b88418984aadc5cdb35096b9ea8fa5c3442")
        );
    }

    #[tokio::test]
    async fn handshake_initiate() {
        let rslt = test_utils::simulate_responder_for_initiator_handshake(
            |mut stream| async move { default_initiator_handshake(&mut stream).await },
            |read_bytes| {
                assert_eq!(&read_bytes, &hex!("00036360e856310ce5d294e8be33fc807077dc56ac80d95d9cd4ddbd21325eff73f70df6086551151f58b8afe6c195782c6a"));

                let (h, ck) = init_handshake_state();
                let h = sha256_multi(&[&h, &SERVER_PUBLIC_KEY.to_bytes()]);

                let v = read_bytes[0];
                let re: [u8; 33] = read_bytes[1..34].try_into().unwrap();
                let c: [u8; 16] = read_bytes[34..].try_into().unwrap();
                let re = PublicKey::try_from_bytes(re).unwrap();
                assert_eq!(v, 0);
                assert_eq!(&re.to_bytes(), &hex!("036360e856310ce5d294e8be33fc807077dc56ac80d95d9cd4ddbd21325eff73f7"));

                let h = sha256_multi(&[&h, &re.to_bytes()]);
                assert_eq!(h, hex!("9e0e7de8bb75554f21db034633de04be41a2b8a18da7a319a03c803bf02b396c"));

                let ss = SharedSecret::new(&re, &SERVER_SECRET_KEY);
                assert_eq!(ss.as_slice(), &hex!("1e2fb3c8fe8fb9f262f649f64d26ecf0f2c0a805a767cf02dc2d77a6ef1fdcc3"));

                let (ck, temp_k1) = hkdf(&ck, ss.as_slice());
                assert_eq!(ck, hex!("b61ec1191326fa240decc9564369dbb3ae2b34341d1e11ad64ed89f89180582f"));
                assert_eq!(temp_k1, hex!("e68f69b7f096d7917245f5e5cf8ae1595febe4d4644333c99f9c4a1282031c9f"));

                decrypt_with_ad(&temp_k1, 0, &h, &c).unwrap();
                let h = sha256_multi(&[&h, &c]);
                assert_eq!(h, hex!("9d1ffbb639e7e20021d9259491dc7b160aab270fb1339ef135053f6f2cebe9ce"));

                let server_e_secret_key = SecretKey::try_from_bytes(hex!("2222222222222222222222222222222222222222222222222222222222222222")).unwrap();
                let server_e_public_key = PublicKey::try_from_bytes(hex!("02466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f27")).unwrap();

                let h = sha256_multi(&[&h, &server_e_public_key.to_bytes()]);
                assert_eq!(h, hex!("38122f669819f906000621a14071802f93f2ef97df100097bcac3ae76c6dc0bf"));
                let ss = SharedSecret::new(&re, &server_e_secret_key);
                assert_eq!(ss.as_slice(), hex!("c06363d6cc549bcb7913dbb9ac1c33fc1158680c89e972000ecd06b36c472e47"));
                let (ck, temp_k2) = hkdf(&ck, ss.as_slice());
                assert_eq!(ck, hex!("e89d31033a1b6bf68c07d22e08ea4d7884646c4b60a9528598ccb4ee2c8f56ba"));
                assert_eq!(temp_k2, hex!("908b166535c01a935cf1e130a5fe895ab4e6f3ef8855d87e9b7581c4ab663ddc"));

                let c = encrypt_with_ad(&temp_k2, 0, &h, &[]);
                assert_eq!(c, hex!("6e2470b93aac583c9ef6eafca3f730ae"));
                let h = sha256_multi(&[&h, &c]);
                assert_eq!(h, hex!("90578e247e98674e661013da3c5c1ca6a8c8f48c90b485c0dfa1494e23d56d72"));

                let mut output = Vec::with_capacity(50);
                output.push(0);
                output.extend(&server_e_public_key.to_bytes());
                output.extend(&c);
                assert_eq!(&output, &hex!("0002466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f276e2470b93aac583c9ef6eafca3f730ae"));
            },
            || &hex!("0002466d7fcae563e5cb09a0d1870bb580344804617879a14949cf22285f1bae3f276e2470b93aac583c9ef6eafca3f730ae"),
            |read_bytes| {
                assert_eq!(read_bytes, hex!("00b9e3a702e93e3a9948c2ed6e5fd7590a6e1c3a0344cfc9d5b57357049aa22355361aa02e55a8fc28fef5bd6d71ad0c38228dc68b1c466263b47fdf31e560e139ba"));
            }
        ).await;
        let (sk, rk) = rslt.unwrap();
        assert_eq!(
            sk.k,
            hex!("969ab31b4d288cedf6218839b27a3e2140827047f2c0f01bf5c04435d43511a9")
        );
        assert_eq!(
            rk.k,
            hex!("bb9020b8965f4df047e07f955f3c4b88418984aadc5cdb35096b9ea8fa5c3442")
        );
    }
}
