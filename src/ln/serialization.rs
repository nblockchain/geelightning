use super::*;

/// Implements (de)serialization for lightning network types.

#[derive(Debug)]
pub struct LnDeserializer<'a> {
    pos: usize,
    bytes: &'a [u8],
    finished: bool,
}

impl<'a> LnDeserializer<'a> {
    pub fn new(bytes: &'a [u8]) -> LnDeserializer<'a> {
        LnDeserializer {
            pos: 0,
            bytes,
            finished: false,
        }
    }

    pub fn abort(&mut self) {
        self.finished = true;
    }

    pub fn finish(&mut self) -> Result<(), MsgTooLongError> {
        self.finished = true;
        if !self.finished() {
            return Err(MsgTooLongError);
        }
        Ok(())
    }

    pub fn read_to_end(&mut self) -> &'a [u8] {
        let ret = &self.bytes[self.pos..];
        self.pos = self.bytes.len();
        self.finished = true;
        ret
    }

    pub fn read_slice(&mut self, len: usize) -> Result<&'a [u8], MsgTooShortError> {
        if self.pos + len > self.bytes.len() {
            self.finished = true;
            return Err(MsgTooShortError);
        }
        let ret = &self.bytes[self.pos..][..len];
        self.pos += len;
        Ok(ret)
    }

    pub fn read_array<const LEN: usize>(&mut self) -> Result<[u8; LEN], MsgTooShortError> {
        Ok(self.read_slice(LEN)?.try_into().unwrap())
    }

    pub fn read_array_rev<const LEN: usize>(&mut self) -> Result<[u8; LEN], MsgTooShortError> {
        let rev = self.read_array::<LEN>()?;
        let mut ret = [0u8; LEN];
        for i in 0..LEN {
            ret[LEN - i - 1] = rev[i];
        }
        Ok(ret)
    }

    pub fn read_u8(&mut self) -> Result<u8, MsgTooShortError> {
        let [ret] = self.read_array::<1>()?;
        Ok(ret)
    }

    pub fn read_u16(&mut self) -> Result<u16, MsgTooShortError> {
        let ret = self.read_array::<2>()?;
        Ok(u16::from_be_bytes(ret))
    }

    pub fn read_u32(&mut self) -> Result<u32, MsgTooShortError> {
        let ret = self.read_array::<4>()?;
        Ok(u32::from_be_bytes(ret))
    }

    pub fn read_u64(&mut self) -> Result<u64, MsgTooShortError> {
        let ret = self.read_array::<8>()?;
        Ok(u64::from_be_bytes(ret))
    }

    pub fn finished(&self) -> bool {
        self.pos == self.bytes.len()
    }
}

impl<'a> Drop for LnDeserializer<'a> {
    fn drop(&mut self) {
        // It's a bug to not read a message to completion and accidently discard data. Any code
        // that uses a LnDeserializer must call abort, finish, or read_to_end to ensure that we've
        // finished with the message.
        //
        // Since panicking while panicking causes the process to abort, and since the fact that
        // the thread is already panicking could explain why we didn't finish reading the message,
        // only panic if we're not already panicking.
        if !thread::panicking() && !self.finished {
            panic!("LnDeserializer not read to completion");
        }
    }
}

#[derive(Default)]
pub struct LnSerializer {
    bytes: BytesMut,
}

impl LnSerializer {
    pub fn new() -> LnSerializer {
        LnSerializer {
            bytes: BytesMut::new(),
        }
    }

    pub fn into_bytes(self) -> Bytes {
        self.bytes.freeze()
    }

    pub fn write_slice(&mut self, slice: &[u8]) {
        self.bytes.extend_from_slice(slice);
    }

    pub fn write_slice_rev(&mut self, slice: &[u8]) {
        for byte in slice.iter().rev() {
            self.write_u8(*byte);
        }
    }

    pub fn write_u8(&mut self, val: u8) {
        self.write_slice(&[val])
    }

    pub fn write_u16(&mut self, val: u16) {
        let val = u16::to_be_bytes(val);
        self.write_slice(&val);
    }

    pub fn write_u32(&mut self, val: u32) {
        let val = u32::to_be_bytes(val);
        self.write_slice(&val);
    }

    pub fn write_u64(&mut self, val: u64) {
        let val = u64::to_be_bytes(val);
        self.write_slice(&val);
    }

    pub fn write_zeros(&mut self, len: usize) {
        self.bytes.resize(self.bytes.len() + len, 0)
    }
}

#[derive(Clone, Debug, Error)]
#[error("message too short")]
pub struct MsgTooShortError;

#[derive(Clone, Debug, Error)]
#[error("message too long")]
pub struct MsgTooLongError;

impl PublicKey {
    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(&self.to_bytes());
    }

    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<PublicKey, PublicKeyDeserializeError> {
        let serialized = deserializer
            .read_array::<PUBLIC_KEY_BYTES_LEN>()
            .map_err(|source| PublicKeyDeserializeError::MsgTooShort { source })?;
        let public_key = {
            PublicKey::try_from_bytes(serialized)
                .map_err(|source| PublicKeyDeserializeError::InvalidPublicKey { source })?
        };
        Ok(public_key)
    }
}

#[derive(Clone, Debug, Error)]
pub enum PublicKeyDeserializeError {
    #[error("unexpected end of message when deserializing public key: {}", source)]
    MsgTooShort { source: MsgTooShortError },
    #[error("invalid public key: {}", source)]
    InvalidPublicKey { source: PublicKeyParseError },
}

impl SecretKey {
    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(&self.to_bytes());
    }

    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<SecretKey, SecretKeyDeserializeError> {
        let serialized = deserializer
            .read_array::<SECRET_KEY_BYTES_LEN>()
            .map_err(|source| SecretKeyDeserializeError::MsgTooShort { source })?;
        let secret_key = {
            SecretKey::try_from_bytes(serialized)
                .map_err(|source| SecretKeyDeserializeError::InvalidSecretKey { source })?
        };
        Ok(secret_key)
    }
}

#[derive(Clone, Debug, Error)]
pub enum SecretKeyDeserializeError {
    #[error("unexpected end of message when deserializing secret key: {}", source)]
    MsgTooShort { source: MsgTooShortError },
    #[error("invalid secret key: {}", source)]
    InvalidSecretKey { source: SecretKeyParseError },
}

const SIGNATURE_SERIALIZED_BYTES_LEN: usize = 64;

impl Signature {
    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(&self.serialize_compact());
    }

    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<Signature, SignatureDeserializeError> {
        let serialized = deserializer
            .read_array::<SIGNATURE_SERIALIZED_BYTES_LEN>()
            .map_err(|source| SignatureDeserializeError::MsgTooShort { source })?;
        let signature = {
            Signature::from_compact(serialized)
                .map_err(|source| SignatureDeserializeError::MalformattedSignature { source })?
        };
        Ok(signature)
    }
}

#[derive(Clone, Debug, Error)]
pub enum SignatureDeserializeError {
    #[error("unexpected end of message when deserializing public key: {}", source)]
    MsgTooShort { source: MsgTooShortError },
    #[error("malformatted signature: {}", source)]
    MalformattedSignature { source: secp256k1::Error },
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_serialization_deserialization_primitives() {
        let mut serializer = LnSerializer::new();

        let some_u8 = 0x01u8;
        let some_u16 = 0x0123_u16;
        let some_u32 = 0x0123_4567_u32;
        let some_u64 = 0x0123_4567_89ab_cdef_u64;
        let some_slice = b"abcdefghijklmnopqrstuvwxyz";

        serializer.write_u8(some_u8);
        serializer.write_u16(some_u16);
        serializer.write_u32(some_u32);
        serializer.write_u64(some_u64);
        serializer.write_slice(some_slice);

        let bytes = serializer.into_bytes();

        let mut deserializer = LnDeserializer::new(&bytes);
        assert_eq!(deserializer.read_u8().unwrap(), some_u8);
        assert_eq!(deserializer.read_u16().unwrap(), some_u16);
        assert_eq!(deserializer.read_u32().unwrap(), some_u32);
        assert_eq!(deserializer.read_u64().unwrap(), some_u64);
        assert_eq!(
            deserializer.read_slice(some_slice.len()).unwrap(),
            some_slice
        );
        deserializer.finish().unwrap();
    }
}
