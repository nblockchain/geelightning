use super::*;

pub struct ErrorMsg {
    pub channel_id: Option<ChannelId>,
    pub data: Vec<u8>,
}

impl fmt::Debug for ErrorMsg {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_struct("ErrorMsg")
            .field("channel_id", &self.channel_id)
            .field("data", &HexDebug(&self.data))
            .field("data_as_str", &String::from_utf8_lossy(&self.data))
            .finish()
    }
}

#[derive(Clone, Debug, Error)]
pub enum ErrorMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
}

impl From<MsgTooShortError> for ErrorMsgDeserializeError {
    fn from(source: MsgTooShortError) -> ErrorMsgDeserializeError {
        ErrorMsgDeserializeError::PayloadTooShort { source }
    }
}

impl ErrorMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<ErrorMsg, ErrorMsgDeserializeError> {
        let channel_id_maybe = ChannelId::ln_deserialize(deserializer)?;
        let channel_id = if channel_id_maybe.as_bytes() == &[0u8; CHANNEL_ID_BYTES_LEN] {
            None
        } else {
            Some(channel_id_maybe)
        };
        let len = deserializer.read_u16()?;
        let data = Vec::from(deserializer.read_slice(len as usize)?);
        deserializer
            .finish()
            .map_err(|source| ErrorMsgDeserializeError::PayloadTooLong { source })?;
        Ok(ErrorMsg { channel_id, data })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        match self.channel_id {
            None => serializer.write_slice(&[0u8; CHANNEL_ID_BYTES_LEN]),
            Some(ref channel_id) => channel_id.ln_serialize(serializer),
        }
        serializer.write_u16(self.data.len() as u16);
        serializer.write_slice(&self.data);
    }

    pub fn into_string(self) -> String {
        String::from_utf8_lossy(&self.data).to_string()
    }

    pub fn new<S: ToString>(channel_id: Option<ChannelId>, msg: S) -> ErrorMsg {
        let data = msg.to_string().into();
        ErrorMsg { channel_id, data }
    }
}
