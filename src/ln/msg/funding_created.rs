use super::*;

#[derive(Debug)]
pub struct FundingCreatedMsg {
    pub temporary_channel_id: TemporaryChannelId,
    pub funding_txid: Txid,
    pub funding_output_index: u16,
    pub signature: Signature,
}

impl From<MsgTooShortError> for FundingCreatedMsgDeserializeError {
    fn from(source: MsgTooShortError) -> FundingCreatedMsgDeserializeError {
        FundingCreatedMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum FundingCreatedMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("error deserializing signature: {}", source)]
    MalformattedSignature { source: SignatureDeserializeError },
}

impl FundingCreatedMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<FundingCreatedMsg, FundingCreatedMsgDeserializeError> {
        let temporary_channel_id = TemporaryChannelId::ln_deserialize(deserializer)?;
        let funding_txid = Txid::ln_deserialize(deserializer)?;
        let funding_output_index = deserializer.read_u16()?;
        let signature = {
            Signature::ln_deserialize(deserializer).map_err(|source| {
                FundingCreatedMsgDeserializeError::MalformattedSignature { source }
            })?
        };
        deserializer
            .finish()
            .map_err(|source| FundingCreatedMsgDeserializeError::PayloadTooLong { source })?;

        Ok(FundingCreatedMsg {
            temporary_channel_id,
            funding_txid,
            funding_output_index,
            signature,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.temporary_channel_id.ln_serialize(serializer);
        self.funding_txid.ln_serialize(serializer);
        serializer.write_u16(self.funding_output_index);
        self.signature.ln_serialize(serializer);
    }
}
