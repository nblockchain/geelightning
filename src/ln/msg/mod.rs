pub use super::*;

mod accept_channel;
mod channel_reestablish;
mod error;
mod funding_created;
mod funding_locked;
mod funding_signed;
mod init;
mod open_channel;
mod ping;
mod pong;

pub use accept_channel::*;
pub use channel_reestablish::*;
pub use error::*;
pub use funding_created::*;
pub use funding_locked::*;
pub use funding_signed::*;
pub use init::*;
pub use open_channel::*;
pub use ping::*;
pub use pong::*;

// Note: Ideally this module shouldn't be needed since we should be able to derive the conversions
// between MsgType and u16 instead.
//
// Keep an eye on this issue:
// https://github.com/rust-lang/rfcs/issues/2783
mod msg_type {
    pub const INIT: u16 = 16;
    pub const ERROR: u16 = 17;
    pub const PING: u16 = 18;
    pub const PONG: u16 = 19;
    pub const OPEN_CHANNEL: u16 = 32;
    pub const ACCEPT_CHANNEL: u16 = 33;
    pub const FUNDING_CREATED: u16 = 34;
    pub const FUNDING_SIGNED: u16 = 35;
    pub const FUNDING_LOCKED: u16 = 36;
    pub const CHANNEL_REESTABLISH: u16 = 136;
}

#[derive(Debug, Clone, Copy)]
#[repr(u16)]
pub enum MsgType {
    Init = msg_type::INIT,
    Error = msg_type::ERROR,
    Ping = msg_type::PING,
    Pong = msg_type::PONG,
    OpenChannel = msg_type::OPEN_CHANNEL,
    AcceptChannel = msg_type::ACCEPT_CHANNEL,
    FundingCreated = msg_type::FUNDING_CREATED,
    FundingSigned = msg_type::FUNDING_SIGNED,
    FundingLocked = msg_type::FUNDING_LOCKED,
    ChannelReestablish = msg_type::CHANNEL_REESTABLISH,
}

impl MsgType {
    pub fn from_type_tag(type_tag: u16) -> Result<MsgType, UnknownMsgType> {
        match type_tag {
            msg_type::INIT => Ok(MsgType::Init),
            msg_type::ERROR => Ok(MsgType::Error),
            msg_type::PING => Ok(MsgType::Ping),
            msg_type::PONG => Ok(MsgType::Pong),
            msg_type::OPEN_CHANNEL => Ok(MsgType::OpenChannel),
            msg_type::ACCEPT_CHANNEL => Ok(MsgType::AcceptChannel),
            msg_type::FUNDING_CREATED => Ok(MsgType::FundingCreated),
            msg_type::FUNDING_SIGNED => Ok(MsgType::FundingSigned),
            msg_type::FUNDING_LOCKED => Ok(MsgType::FundingLocked),
            msg_type::CHANNEL_REESTABLISH => Ok(MsgType::ChannelReestablish),
            _ => Err(UnknownMsgType { type_tag }),
        }
    }
}

impl fmt::Display for MsgType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MsgType::Init => write!(f, "init"),
            MsgType::Error => write!(f, "error"),
            MsgType::Ping => write!(f, "ping"),
            MsgType::Pong => write!(f, "pong"),
            MsgType::OpenChannel => write!(f, "open_channel"),
            MsgType::AcceptChannel => write!(f, "accept_channel"),
            MsgType::FundingCreated => write!(f, "funding_created"),
            MsgType::FundingSigned => write!(f, "funding_signed"),
            MsgType::FundingLocked => write!(f, "funding_locked"),
            MsgType::ChannelReestablish => write!(f, "channel_reestablish"),
        }
    }
}

#[derive(Clone, Copy, Debug, Error)]
#[error("unknown message type ({})", type_tag)]
pub struct UnknownMsgType {
    type_tag: u16,
}

impl UnknownMsgType {
    pub fn can_ignore(self) -> bool {
        self.type_tag % 2 == 1
    }
}

#[derive(Debug)]
#[allow(clippy::large_enum_variant)]
pub enum Msg {
    Init(InitMsg),
    Error(ErrorMsg),
    Ping(PingMsg),
    Pong(PongMsg),
    OpenChannel(OpenChannelMsg),
    AcceptChannel(AcceptChannelMsg),
    FundingCreated(FundingCreatedMsg),
    FundingSigned(FundingSignedMsg),
    FundingLocked(FundingLockedMsg),
    ChannelReestablish(ChannelReestablishMsg),
}

#[derive(Clone, Debug, Error)]
pub enum MsgDeserializeError {
    #[error("{}", source)]
    UnknownMsgType { source: UnknownMsgType },
    #[error("{}", source)]
    MsgTooShort { source: MsgTooShortError },
    #[error("failed to deserialize init msg: {}", source)]
    Init { source: InitMsgDeserializeError },
    #[error("failed to deserialize error msg: {}", source)]
    Error { source: ErrorMsgDeserializeError },
    #[error("failed to deserialize ping msg: {}", source)]
    Ping { source: PingMsgDeserializeError },
    #[error("failed to deserialize pong msg: {}", source)]
    Pong { source: PongMsgDeserializeError },
    #[error("failed to deserialize open channel msg: {}", source)]
    OpenChannel {
        source: OpenChannelMsgDeserializeError,
    },
    #[error("failed to deserialize accept channel msg: {}", source)]
    AcceptChannel {
        source: AcceptChannelMsgDeserializeError,
    },
    #[error("failed to deserailize funding created msg: {}", source)]
    FundingCreated {
        source: FundingCreatedMsgDeserializeError,
    },
    #[error("failed to deserailize funding signed msg: {}", source)]
    FundingSigned {
        source: FundingSignedMsgDeserializeError,
    },
    #[error("failed to deserialize funding locked msg: {}", source)]
    FundingLocked {
        source: FundingLockedMsgDeserializeError,
    },
    #[error("failed to deserialize channel reestablish msg: {}", source)]
    ChannelReestablish {
        source: ChannelReestablishMsgDeserializeError,
    },
}

impl Msg {
    pub fn msg_type(&self) -> MsgType {
        match self {
            Msg::Init(..) => MsgType::Init,
            Msg::Error(..) => MsgType::Error,
            Msg::Ping(..) => MsgType::Ping,
            Msg::Pong(..) => MsgType::Pong,
            Msg::OpenChannel(..) => MsgType::OpenChannel,
            Msg::AcceptChannel(..) => MsgType::AcceptChannel,
            Msg::FundingCreated(..) => MsgType::FundingCreated,
            Msg::FundingSigned(..) => MsgType::FundingSigned,
            Msg::FundingLocked(..) => MsgType::FundingLocked,
            Msg::ChannelReestablish(..) => MsgType::ChannelReestablish,
        }
    }

    pub fn to_bytes(&self) -> Bytes {
        let mut serializer = LnSerializer::new();
        serializer.write_u16(self.msg_type() as u16);
        match self {
            Msg::Init(init_msg) => {
                init_msg.ln_serialize(&mut serializer);
            }
            Msg::Error(error_msg) => {
                error_msg.ln_serialize(&mut serializer);
            }
            Msg::Ping(ping_msg) => {
                ping_msg.ln_serialize(&mut serializer);
            }
            Msg::Pong(pong_msg) => {
                pong_msg.ln_serialize(&mut serializer);
            }
            Msg::OpenChannel(open_channel_msg) => {
                open_channel_msg.ln_serialize(&mut serializer);
            }
            Msg::AcceptChannel(accept_channel_msg) => {
                accept_channel_msg.ln_serialize(&mut serializer);
            }
            Msg::FundingCreated(funding_created_msg) => {
                funding_created_msg.ln_serialize(&mut serializer);
            }
            Msg::FundingSigned(funding_signed_msg) => {
                funding_signed_msg.ln_serialize(&mut serializer);
            }
            Msg::FundingLocked(funding_locked_msg) => {
                funding_locked_msg.ln_serialize(&mut serializer);
            }
            Msg::ChannelReestablish(channel_reestablish_msg) => {
                channel_reestablish_msg.ln_serialize(&mut serializer);
            }
        }
        serializer.into_bytes()
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Msg, MsgDeserializeError> {
        let mut deserializer = LnDeserializer::new(bytes);
        let type_tag = deserializer
            .read_u16()
            .map_err(|source| MsgDeserializeError::MsgTooShort { source })?;
        let msg_type = MsgType::from_type_tag(type_tag).map_err(|source| {
            deserializer.abort();
            MsgDeserializeError::UnknownMsgType { source }
        })?;
        let msg = match msg_type {
            MsgType::Init => {
                let init_msg = InitMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::Init { source })?;
                Msg::Init(init_msg)
            }
            MsgType::Error => {
                let error_msg = ErrorMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::Error { source })?;
                Msg::Error(error_msg)
            }
            MsgType::Ping => {
                let ping_msg = PingMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::Ping { source })?;
                Msg::Ping(ping_msg)
            }
            MsgType::Pong => {
                let pong_msg = PongMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::Pong { source })?;
                Msg::Pong(pong_msg)
            }
            MsgType::OpenChannel => {
                let open_channel_msg = OpenChannelMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::OpenChannel { source })?;
                Msg::OpenChannel(open_channel_msg)
            }
            MsgType::AcceptChannel => {
                let accept_channel_msg = AcceptChannelMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::AcceptChannel { source })?;
                Msg::AcceptChannel(accept_channel_msg)
            }
            MsgType::FundingCreated => {
                let funding_created_msg = FundingCreatedMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::FundingCreated { source })?;
                Msg::FundingCreated(funding_created_msg)
            }
            MsgType::FundingSigned => {
                let funding_signed_msg = FundingSignedMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::FundingSigned { source })?;
                Msg::FundingSigned(funding_signed_msg)
            }
            MsgType::FundingLocked => {
                let funding_locked_msg = FundingLockedMsg::ln_deserialize(&mut deserializer)
                    .map_err(|source| MsgDeserializeError::FundingLocked { source })?;
                Msg::FundingLocked(funding_locked_msg)
            }
            MsgType::ChannelReestablish => {
                let channel_reestablish_msg =
                    ChannelReestablishMsg::ln_deserialize(&mut deserializer)
                        .map_err(|source| MsgDeserializeError::ChannelReestablish { source })?;
                Msg::ChannelReestablish(channel_reestablish_msg)
            }
        };
        Ok(msg)
    }
}
