use super::*;

#[derive(Debug, Clone)]
pub struct PingMsg {
    pub num_pong_bytes: u16,
    pub bytes_len: u16,
}

impl From<MsgTooShortError> for PingMsgDeserializeError {
    fn from(source: MsgTooShortError) -> PingMsgDeserializeError {
        PingMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum PingMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
}

impl PingMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<PingMsg, PingMsgDeserializeError> {
        let num_pong_bytes = deserializer.read_u16()?;
        let bytes_len = deserializer.read_u16()?;
        let _ignored = deserializer.read_slice(bytes_len as usize)?;

        deserializer
            .finish()
            .map_err(|source| PingMsgDeserializeError::PayloadTooLong { source })?;

        Ok(PingMsg {
            num_pong_bytes,
            bytes_len,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_u16(self.num_pong_bytes);
        serializer.write_u16(self.bytes_len);
        serializer.write_zeros(self.bytes_len as usize);
    }
}
