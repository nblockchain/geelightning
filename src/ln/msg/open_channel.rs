use super::*;

#[derive(Debug, Clone)]
pub struct OpenChannelMsg {
    pub chain_hash: BlockHash,
    pub temporary_channel_id: TemporaryChannelId,
    pub funding_satoshis: Sat64,
    pub push_msat: Msat64,
    pub dust_limit_satoshis: Sat64,
    pub max_htlc_value_in_flight_msat: Msat64,
    pub channel_reserve_satoshis: Sat64,
    pub htlc_minimum_msat: Msat64,
    pub feerate_per_kw: SatPerKw32,
    pub to_self_delay: u16,
    pub max_accepted_htlcs: u16,
    pub funding_pubkey: PublicKey,
    pub revocation_basepoint: PublicKey,
    pub payment_basepoint: PublicKey,
    pub delayed_payment_basepoint: PublicKey,
    pub htlc_basepoint: PublicKey,
    pub first_per_commitment_point: PublicKey,
    pub channel_flags: ChannelFlags,
    pub shutdown_scriptpubkey: Option<Script>,
}

impl From<MsgTooShortError> for OpenChannelMsgDeserializeError {
    fn from(source: MsgTooShortError) -> OpenChannelMsgDeserializeError {
        OpenChannelMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum OpenChannelMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("invalid funding pubkey: {}", source)]
    InvalidFundingPubkey { source: PublicKeyDeserializeError },
    #[error("invalid revocation basepoint: {}", source)]
    InvalidRevocationBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid payment basepoint: {}", source)]
    InvalidPaymentBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid delayed payment basepoint: {}", source)]
    InvalidDelayedPaymentBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid htlc basepoint: {}", source)]
    InvalidHtlcBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid first per commitment point: {}", source)]
    InvalidFirstPerCommitmentPoint { source: PublicKeyDeserializeError },
}

impl OpenChannelMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<OpenChannelMsg, OpenChannelMsgDeserializeError> {
        let chain_hash = BlockHash::ln_deserialize(deserializer)?;
        let temporary_channel_id = TemporaryChannelId::ln_deserialize(deserializer)?;
        let funding_satoshis = {
            let funding_satoshis = deserializer.read_u64()?;
            Sat64::from_u64(funding_satoshis)
        };
        let push_msat = {
            let push_msat = deserializer.read_u64()?;
            Msat64::from_u64(push_msat)
        };
        let dust_limit_satoshis = {
            let dust_limit_satoshis = deserializer.read_u64()?;
            Sat64::from_u64(dust_limit_satoshis)
        };
        let max_htlc_value_in_flight_msat = {
            let max_htlc_value_in_flight_msat = deserializer.read_u64()?;
            Msat64::from_u64(max_htlc_value_in_flight_msat)
        };
        let channel_reserve_satoshis = {
            let channel_reserve_satoshis = deserializer.read_u64()?;
            Sat64::from_u64(channel_reserve_satoshis)
        };
        let htlc_minimum_msat = {
            let htlc_minimum_msat = deserializer.read_u64()?;
            Msat64::from_u64(htlc_minimum_msat)
        };
        let feerate_per_kw = {
            let feerate_per_kw = deserializer.read_u32()?;
            SatPerKw32::from_u32(feerate_per_kw)
        };
        let to_self_delay = deserializer.read_u16()?;
        let max_accepted_htlcs = deserializer.read_u16()?;
        let funding_pubkey = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidFundingPubkey { source }
            })?
        };
        let revocation_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidRevocationBasepoint { source }
            })?
        };
        let payment_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidPaymentBasepoint { source }
            })?
        };
        let delayed_payment_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidDelayedPaymentBasepoint { source }
            })?
        };
        let htlc_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidHtlcBasepoint { source }
            })?
        };
        let first_per_commitment_point = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                OpenChannelMsgDeserializeError::InvalidFirstPerCommitmentPoint { source }
            })?
        };
        let channel_flags = {
            let channel_flags = deserializer.read_u8()?;
            let announce_channel = channel_flags & 0x01 == 0x01;
            ChannelFlags { announce_channel }
        };
        let shutdown_scriptpubkey = if deserializer.finished() {
            None
        } else {
            let len = deserializer.read_u16()?;
            let script = deserializer.read_slice(len as usize)?;
            let script = Script::from_bytes(script);
            Some(script)
        };

        deserializer
            .finish()
            .map_err(|source| OpenChannelMsgDeserializeError::PayloadTooLong { source })?;

        Ok(OpenChannelMsg {
            chain_hash,
            temporary_channel_id,
            funding_satoshis,
            push_msat,
            dust_limit_satoshis,
            max_htlc_value_in_flight_msat,
            channel_reserve_satoshis,
            htlc_minimum_msat,
            feerate_per_kw,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            first_per_commitment_point,
            channel_flags,
            shutdown_scriptpubkey,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.chain_hash.ln_serialize(serializer);
        self.temporary_channel_id.ln_serialize(serializer);
        serializer.write_u64(self.funding_satoshis.to_u64());
        serializer.write_u64(self.push_msat.to_u64());
        serializer.write_u64(self.dust_limit_satoshis.to_u64());
        serializer.write_u64(self.max_htlc_value_in_flight_msat.to_u64());
        serializer.write_u64(self.channel_reserve_satoshis.to_u64());
        serializer.write_u64(self.htlc_minimum_msat.to_u64());
        serializer.write_u32(self.feerate_per_kw.to_u32());
        serializer.write_u16(self.to_self_delay);
        serializer.write_u16(self.max_accepted_htlcs);
        serializer.write_slice(&self.funding_pubkey.to_bytes());
        serializer.write_slice(&self.revocation_basepoint.to_bytes());
        serializer.write_slice(&self.payment_basepoint.to_bytes());
        serializer.write_slice(&self.delayed_payment_basepoint.to_bytes());
        serializer.write_slice(&self.htlc_basepoint.to_bytes());
        serializer.write_slice(&self.first_per_commitment_point.to_bytes());
        let channel_flags = if self.channel_flags.announce_channel {
            0x01
        } else {
            0x00
        };
        serializer.write_u8(channel_flags);
        if let Some(shutdown_scriptpubkey) = &self.shutdown_scriptpubkey {
            let bytes = shutdown_scriptpubkey.as_bytes();
            serializer.write_u16(bytes.len() as u16);
            serializer.write_slice(bytes);
        }
    }
}
