use super::*;

#[derive(Debug, Clone)]
pub struct ChannelReestablishMsg {
    pub channel_id: ChannelId,
    pub next_commitment_number: u64,
    pub next_revocation_number: u64,
    pub your_last_per_commitment_secret: Option<SecretKey>,
    pub my_current_per_commitment_point: PublicKey,
}

impl From<MsgTooShortError> for ChannelReestablishMsgDeserializeError {
    fn from(source: MsgTooShortError) -> ChannelReestablishMsgDeserializeError {
        ChannelReestablishMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum ChannelReestablishMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("error parsing your_last_per_commitment_secret field: {}", source)]
    InvalidYourLastPerCommitmentSecret { source: SecretKeyParseError },
    #[error("error parsing my_current_per_commitment_point field: {}", source)]
    InvalidMyCurrentPerCommitmentPoint { source: PublicKeyDeserializeError },
}

impl ChannelReestablishMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<ChannelReestablishMsg, ChannelReestablishMsgDeserializeError> {
        let channel_id = ChannelId::ln_deserialize(deserializer)?;
        let next_commitment_number = deserializer.read_u64()?;
        let next_revocation_number = deserializer.read_u64()?;
        let your_last_per_commitment_secret = {
            let serialized =
                deserializer
                    .read_array::<SECRET_KEY_BYTES_LEN>()
                    .map_err(
                        |source| ChannelReestablishMsgDeserializeError::PayloadTooShort { source },
                    )?;
            if serialized == [0u8; SECRET_KEY_BYTES_LEN] {
                None
            } else {
                let secret_key = SecretKey::try_from_bytes(serialized).map_err(|source| {
                    deserializer.abort();
                    ChannelReestablishMsgDeserializeError::InvalidYourLastPerCommitmentSecret {
                        source,
                    }
                })?;
                Some(secret_key)
            }
        };
        let my_current_per_commitment_point = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                ChannelReestablishMsgDeserializeError::InvalidMyCurrentPerCommitmentPoint { source }
            })?
        };

        deserializer
            .finish()
            .map_err(|source| ChannelReestablishMsgDeserializeError::PayloadTooLong { source })?;

        Ok(ChannelReestablishMsg {
            channel_id,
            next_commitment_number,
            next_revocation_number,
            your_last_per_commitment_secret,
            my_current_per_commitment_point,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.channel_id.ln_serialize(serializer);
        serializer.write_u64(self.next_commitment_number);
        serializer.write_u64(self.next_revocation_number);
        match &self.your_last_per_commitment_secret {
            Some(secret_key) => {
                secret_key.ln_serialize(serializer);
            }
            None => {
                serializer.write_slice(&[0u8; SECRET_KEY_BYTES_LEN]);
            }
        }
        self.my_current_per_commitment_point
            .ln_serialize(serializer);
    }
}
