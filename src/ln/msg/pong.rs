use super::*;

#[derive(Debug, Clone)]
pub struct PongMsg {
    pub bytes_len: u16,
}

impl From<MsgTooShortError> for PongMsgDeserializeError {
    fn from(source: MsgTooShortError) -> PongMsgDeserializeError {
        PongMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum PongMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
}

impl PongMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<PongMsg, PongMsgDeserializeError> {
        let bytes_len = deserializer.read_u16()?;
        let _ignored = deserializer.read_slice(bytes_len as usize)?;

        deserializer
            .finish()
            .map_err(|source| PongMsgDeserializeError::PayloadTooLong { source })?;

        Ok(PongMsg { bytes_len })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_u16(self.bytes_len);
        serializer.write_zeros(self.bytes_len as usize);
    }
}
