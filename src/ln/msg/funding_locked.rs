use super::*;

#[derive(Debug, Clone)]
pub struct FundingLockedMsg {
    pub channel_id: ChannelId,
    pub next_per_commitment_point: PublicKey,
}

impl From<MsgTooShortError> for FundingLockedMsgDeserializeError {
    fn from(source: MsgTooShortError) -> FundingLockedMsgDeserializeError {
        FundingLockedMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum FundingLockedMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("invalid next per commitment point: {}", source)]
    InvalidNextPerCommitmentPoint { source: PublicKeyDeserializeError },
}

impl FundingLockedMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<FundingLockedMsg, FundingLockedMsgDeserializeError> {
        let channel_id = ChannelId::ln_deserialize(deserializer)?;
        let next_per_commitment_point = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                FundingLockedMsgDeserializeError::InvalidNextPerCommitmentPoint { source }
            })?
        };

        deserializer
            .finish()
            .map_err(|source| FundingLockedMsgDeserializeError::PayloadTooLong { source })?;

        Ok(FundingLockedMsg {
            channel_id,
            next_per_commitment_point,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.channel_id.ln_serialize(serializer);
        serializer.write_slice(&self.next_per_commitment_point.to_bytes());
    }
}
