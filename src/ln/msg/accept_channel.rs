use super::*;

#[derive(Debug)]
pub struct AcceptChannelMsg {
    pub temporary_channel_id: TemporaryChannelId,
    pub dust_limit_satoshis: Sat64,
    pub max_htlc_value_in_flight_msat: Msat64,
    pub channel_reserve_satoshis: Sat64,
    pub htlc_minimum_msat: Msat64,
    pub minimum_depth: u32,
    pub to_self_delay: u16,
    pub max_accepted_htlcs: u16,
    pub funding_pubkey: PublicKey,
    pub revocation_basepoint: PublicKey,
    pub payment_basepoint: PublicKey,
    pub delayed_payment_basepoint: PublicKey,
    pub htlc_basepoint: PublicKey,
    pub first_per_commitment_point: PublicKey,
    pub shutdown_scriptpubkey: Option<Script>,
}

impl From<MsgTooShortError> for AcceptChannelMsgDeserializeError {
    fn from(source: MsgTooShortError) -> AcceptChannelMsgDeserializeError {
        AcceptChannelMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum AcceptChannelMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("invalid funding pubkey: {}", source)]
    InvalidFundingPubkey { source: PublicKeyDeserializeError },
    #[error("invalid revocation basepoint: {}", source)]
    InvalidRevocationBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid payment basepoint: {}", source)]
    InvalidPaymentBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid delayed payment basepoint: {}", source)]
    InvalidDelayedPaymentBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid htlc basepoint: {}", source)]
    InvalidHtlcBasepoint { source: PublicKeyDeserializeError },
    #[error("invalid first per commitment point: {}", source)]
    InvalidFirstPerCommitmentPoint { source: PublicKeyDeserializeError },
}

impl AcceptChannelMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<AcceptChannelMsg, AcceptChannelMsgDeserializeError> {
        let temporary_channel_id = TemporaryChannelId::ln_deserialize(deserializer)?;
        let dust_limit_satoshis = {
            let dust_limit_satoshis = deserializer.read_u64()?;
            Sat64::from_u64(dust_limit_satoshis)
        };
        let max_htlc_value_in_flight_msat = {
            let max_htlc_value_in_flight_msat = deserializer.read_u64()?;
            Msat64::from_u64(max_htlc_value_in_flight_msat)
        };
        let channel_reserve_satoshis = {
            let channel_reserve_satoshis = deserializer.read_u64()?;
            Sat64::from_u64(channel_reserve_satoshis)
        };
        let htlc_minimum_msat = {
            let htlc_minimum_msat = deserializer.read_u64()?;
            Msat64::from_u64(htlc_minimum_msat)
        };
        let minimum_depth = deserializer.read_u32()?;
        let to_self_delay = deserializer.read_u16()?;
        let max_accepted_htlcs = deserializer.read_u16()?;
        let funding_pubkey = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidFundingPubkey { source }
            })?
        };
        let revocation_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidRevocationBasepoint { source }
            })?
        };
        let payment_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidPaymentBasepoint { source }
            })?
        };
        let delayed_payment_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidDelayedPaymentBasepoint { source }
            })?
        };
        let htlc_basepoint = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidHtlcBasepoint { source }
            })?
        };
        let first_per_commitment_point = {
            PublicKey::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                AcceptChannelMsgDeserializeError::InvalidFirstPerCommitmentPoint { source }
            })?
        };
        let shutdown_scriptpubkey = if deserializer.finished() {
            None
        } else {
            let len = deserializer.read_u16()?;
            let script = deserializer.read_slice(len as usize)?;
            let script = Script::from_bytes(script);
            Some(script)
        };

        deserializer
            .finish()
            .map_err(|source| AcceptChannelMsgDeserializeError::PayloadTooLong { source })?;

        Ok(AcceptChannelMsg {
            temporary_channel_id,
            dust_limit_satoshis,
            max_htlc_value_in_flight_msat,
            channel_reserve_satoshis,
            htlc_minimum_msat,
            minimum_depth,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            first_per_commitment_point,
            shutdown_scriptpubkey,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.temporary_channel_id.ln_serialize(serializer);
        serializer.write_u64(self.dust_limit_satoshis.to_u64());
        serializer.write_u64(self.max_htlc_value_in_flight_msat.to_u64());
        serializer.write_u64(self.channel_reserve_satoshis.to_u64());
        serializer.write_u64(self.htlc_minimum_msat.to_u64());
        serializer.write_u32(self.minimum_depth);
        serializer.write_u16(self.to_self_delay);
        serializer.write_u16(self.max_accepted_htlcs);
        serializer.write_slice(&self.funding_pubkey.to_bytes());
        serializer.write_slice(&self.revocation_basepoint.to_bytes());
        serializer.write_slice(&self.payment_basepoint.to_bytes());
        serializer.write_slice(&self.delayed_payment_basepoint.to_bytes());
        serializer.write_slice(&self.htlc_basepoint.to_bytes());
        serializer.write_slice(&self.first_per_commitment_point.to_bytes());
        if let Some(shutdown_scriptpubkey) = &self.shutdown_scriptpubkey {
            let bytes = shutdown_scriptpubkey.as_bytes();
            serializer.write_u16(bytes.len() as u16);
            serializer.write_slice(bytes);
        }
    }
}
