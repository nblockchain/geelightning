use super::*;

#[derive(Debug)]
pub struct FundingSignedMsg {
    pub channel_id: ChannelId,
    pub signature: Signature,
}

impl From<MsgTooShortError> for FundingSignedMsgDeserializeError {
    fn from(source: MsgTooShortError) -> FundingSignedMsgDeserializeError {
        FundingSignedMsgDeserializeError::PayloadTooShort { source }
    }
}

#[derive(Clone, Debug, Error)]
pub enum FundingSignedMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("error deserializing signature: {}", source)]
    MalformattedSignature { source: SignatureDeserializeError },
}

impl FundingSignedMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<FundingSignedMsg, FundingSignedMsgDeserializeError> {
        let channel_id = ChannelId::ln_deserialize(deserializer)?;
        let signature = {
            Signature::ln_deserialize(deserializer).map_err(|source| {
                FundingSignedMsgDeserializeError::MalformattedSignature { source }
            })?
        };
        deserializer
            .finish()
            .map_err(|source| FundingSignedMsgDeserializeError::PayloadTooLong { source })?;

        Ok(FundingSignedMsg {
            channel_id,
            signature,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.channel_id.ln_serialize(serializer);
        self.signature.ln_serialize(serializer);
    }
}
