use super::*;

#[derive(Debug)]
pub struct InitMsg {
    pub global_features: UnvalidatedGlobalFeatures,
    pub local_features: UnvalidatedLocalFeatures,
}

#[derive(Clone, Debug, Error)]
pub enum InitMsgDeserializeError {
    #[error("{}", source)]
    PayloadTooShort { source: MsgTooShortError },
    #[error("{}", source)]
    PayloadTooLong { source: MsgTooLongError },
    #[error("failed to deserialize global features: {}", source)]
    ParseGlobalFeatures { source: DeserializeFeaturesError },
    #[error("failed to deserialize local features: {}", source)]
    ParseLocalFeatures { source: DeserializeFeaturesError },
}

impl From<MsgTooShortError> for InitMsgDeserializeError {
    fn from(source: MsgTooShortError) -> InitMsgDeserializeError {
        InitMsgDeserializeError::PayloadTooShort { source }
    }
}

impl InitMsg {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<InitMsg, InitMsgDeserializeError> {
        let global_features = {
            UnvalidatedGlobalFeatures::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                InitMsgDeserializeError::ParseGlobalFeatures { source }
            })?
        };
        let local_features = {
            UnvalidatedLocalFeatures::ln_deserialize(deserializer).map_err(|source| {
                deserializer.abort();
                InitMsgDeserializeError::ParseLocalFeatures { source }
            })?
        };
        deserializer
            .finish()
            .map_err(|source| InitMsgDeserializeError::PayloadTooLong { source })?;
        Ok(InitMsg {
            global_features,
            local_features,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.global_features.ln_serialize(serializer);
        self.local_features.ln_serialize(serializer);
    }
}
