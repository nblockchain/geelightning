use super::*;

#[derive(Clone, Debug)]
pub struct Endpoint {
    pub peer_id: PeerId,
    pub addr: SocketAddr,
}

#[derive(Clone, Debug)]
pub struct LocalEndpoint {
    pub peer_secret_key: PeerSecretKey,
    pub addr: SocketAddr,
}
