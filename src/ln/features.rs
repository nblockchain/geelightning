//! This module is an implementation of BOLT #9: Assigned Feature Flags
//!
//! The types `GlobalFeatures` and `LocalFeatures` represent sets of feature flags. There are also
//! the rawer `UnvalidatedGlobalFeautures` and `UnvalidatedLocalFeatures` types which are allowed
//! to contain flags which we don't recognize or don't support. The unvalidated types support
//! (de)serialization directly to/from the wire and can be converted to the validated types via the
//! validate() methods.

use super::*;

const NUM_KNOWN_FEATURES: usize = 4;
const BITS_PER_FEATURE: usize = 2;
const FEATURES_PER_BYTE: usize = 8 / BITS_PER_FEATURE;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct GlobalFeatures {}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct LocalFeatures {
    pub option_data_loss_protect: FeatureFlag,
    pub initial_routing_sync: OptionalFeatureFlag,
    pub option_upfront_shutdown_script: FeatureFlag,
    pub gossip_queries: FeatureFlag,
}

#[derive(Debug, Error)]
pub enum ValidateFeaturesError {
    #[error("unknown required feature (bit index {})", index)]
    UnknownRequiredFeature { index: u16 },
    #[error("feature must not be required (bit index {})", index)]
    FeatureMustNotBeRequired { index: u16 },
}

#[derive(Debug, Error)]
#[error("error validating global features: {}", source)]
pub struct ValidateGlobalFeaturesError {
    #[from]
    source: ValidateFeaturesError,
}

#[derive(Debug, Error)]
pub enum ValidateLocalFeaturesError {
    #[error("error validating local features: {}", source)]
    Invalid {
        #[from]
        source: ValidateFeaturesError,
    },
    #[error("peer requires data loss protection support which is not supported")]
    DataLossProtectionNotSupported,
    #[error("peer requires upfront shutdown script support which is not supported")]
    UpfrontShutdownScriptsNotSupported,
    #[error("peer requires gossip query support which is not supported")]
    GossipQueriesNotSupported,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum FeatureFlag {
    No,
    Optional,
    Required,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum OptionalFeatureFlag {
    No,
    Optional,
}

#[derive(Clone, Debug, Error)]
#[error("malformed feature flag")]
pub struct MalformedFeatureFlagError;

impl FeatureFlag {
    fn validate(self, against: FeatureFlag) -> bool {
        !(self == FeatureFlag::Required && against == FeatureFlag::No)
    }

    fn from_index(bits: &[u8], index: u16) -> Result<FeatureFlag, MalformedFeatureFlagError> {
        let bits_len = bits.len();
        if index >= bits_len as u16 {
            return Ok(FeatureFlag::No);
        }
        let flags = (bits[bits_len - (1 + (index as usize) / 8)] >> (index % 8)) & 0x03;
        match flags {
            0x00 => Ok(FeatureFlag::No),
            0x01 => Ok(FeatureFlag::Required),
            0x02 => Ok(FeatureFlag::Optional),
            _ => Err(MalformedFeatureFlagError),
        }
    }

    fn to_index(self, bits: &mut [u8], index: u16) {
        let bits_len = bits.len();
        let flags = match self {
            FeatureFlag::No => 0x00,
            FeatureFlag::Required => 0x01,
            FeatureFlag::Optional => 0x02,
        };
        bits[bits_len - (1 + (index as usize) / 8)] |= flags << (index % 8);
    }

    fn try_to_optional(self) -> Option<OptionalFeatureFlag> {
        match self {
            FeatureFlag::No => Some(OptionalFeatureFlag::No),
            FeatureFlag::Required => None,
            FeatureFlag::Optional => Some(OptionalFeatureFlag::Optional),
        }
    }
}

impl OptionalFeatureFlag {
    pub fn to_feature_flag(self) -> FeatureFlag {
        match self {
            OptionalFeatureFlag::No => FeatureFlag::No,
            OptionalFeatureFlag::Optional => FeatureFlag::Optional,
        }
    }
}

impl GlobalFeatures {
    pub fn to_unvalidated(self) -> UnvalidatedGlobalFeatures {
        UnvalidatedGlobalFeatures {
            features: UnvalidatedFeatures {
                feature_flags: SmallVec::new(),
            },
        }
    }
}

impl LocalFeatures {
    pub fn to_unvalidated(self) -> UnvalidatedLocalFeatures {
        UnvalidatedLocalFeatures {
            features: UnvalidatedFeatures {
                feature_flags: smallvec![
                    self.option_data_loss_protect,
                    self.initial_routing_sync.to_feature_flag(),
                    self.option_upfront_shutdown_script,
                    self.gossip_queries,
                ],
            },
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct UnvalidatedGlobalFeatures {
    features: UnvalidatedFeatures,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct UnvalidatedLocalFeatures {
    features: UnvalidatedFeatures,
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct UnvalidatedFeatures {
    feature_flags: SmallVec<[FeatureFlag; NUM_KNOWN_FEATURES]>,
}

#[derive(Clone, Debug, Error)]
pub enum DeserializeFeaturesError {
    #[error(
        "unexpected end of message when deserializing feature flags: {}",
        source
    )]
    MsgTooShort { source: MsgTooShortError },
    #[error("malformed feature flag: {}", source)]
    MalformedFeatureFlag { source: MalformedFeatureFlagError },
}

impl UnvalidatedFeatures {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<UnvalidatedFeatures, DeserializeFeaturesError> {
        let len = deserializer
            .read_u16()
            .map_err(|source| DeserializeFeaturesError::MsgTooShort { source })?
            as usize;
        let mut feature_flags = SmallVec::with_capacity(4 * len);
        let mut index = 0;
        let bytes = deserializer
            .read_slice(len)
            .map_err(|source| DeserializeFeaturesError::MsgTooShort { source })?;
        while index < bytes.len() {
            let feature_flag = {
                FeatureFlag::from_index(bytes, index as u16)
                    .map_err(|source| DeserializeFeaturesError::MalformedFeatureFlag { source })?
            };
            feature_flags.push(feature_flag);
            index += 2;
        }
        Ok(UnvalidatedFeatures { feature_flags })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        let num_bytes = usize_ceil_div(self.feature_flags.len(), FEATURES_PER_BYTE);
        serializer.write_u16(num_bytes as u16);
        let mut flags: SmallVec<[u8; usize_ceil_div(NUM_KNOWN_FEATURES, FEATURES_PER_BYTE)]> =
            smallvec![0u8; num_bytes];
        for (half_index, feature_flag) in self.feature_flags.iter().enumerate() {
            feature_flag.to_index(&mut flags, half_index as u16 * 2);
        }
        serializer.write_slice(&flags);
    }

    pub fn get_index(&self, index: u16) -> FeatureFlag {
        assert_eq!(index % 2, 0);
        let half_index = index / 2;
        match self.feature_flags.get(half_index as usize) {
            Some(feature_flag) => *feature_flag,
            None => FeatureFlag::No,
        }
    }

    pub fn get_index_optional(
        &self,
        index: u16,
    ) -> Result<OptionalFeatureFlag, ValidateFeaturesError> {
        match self.get_index(index).try_to_optional() {
            Some(flag) => Ok(flag),
            None => Err(ValidateFeaturesError::FeatureMustNotBeRequired { index }),
        }
    }
}

impl UnvalidatedGlobalFeatures {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<UnvalidatedGlobalFeatures, DeserializeFeaturesError> {
        Ok(UnvalidatedGlobalFeatures {
            features: UnvalidatedFeatures::ln_deserialize(deserializer)?,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.features.ln_serialize(serializer);
    }

    pub fn validate(
        &self,
        _against: GlobalFeatures,
    ) -> Result<GlobalFeatures, ValidateGlobalFeaturesError> {
        for (half_index, feature_flag) in self.features.feature_flags.iter().enumerate() {
            if *feature_flag == FeatureFlag::Required {
                return Err(ValidateFeaturesError::UnknownRequiredFeature {
                    index: half_index as u16 * 2,
                }
                .into());
            }
        }
        Ok(GlobalFeatures {})
    }
}

impl UnvalidatedLocalFeatures {
    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<UnvalidatedLocalFeatures, DeserializeFeaturesError> {
        Ok(UnvalidatedLocalFeatures {
            features: UnvalidatedFeatures::ln_deserialize(deserializer)?,
        })
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        self.features.ln_serialize(serializer);
    }

    pub fn validate(
        &self,
        against: LocalFeatures,
    ) -> Result<LocalFeatures, ValidateLocalFeaturesError> {
        let option_data_loss_protect = self.features.get_index(0);
        let initial_routing_sync = self.features.get_index_optional(2)?;
        let option_upfront_shutdown_script = self.features.get_index(4);
        let gossip_queries = self.features.get_index(6);

        if !option_data_loss_protect.validate(against.option_data_loss_protect) {
            return Err(ValidateLocalFeaturesError::DataLossProtectionNotSupported);
        }
        if !option_upfront_shutdown_script.validate(against.option_upfront_shutdown_script) {
            return Err(ValidateLocalFeaturesError::UpfrontShutdownScriptsNotSupported);
        }
        if !gossip_queries.validate(against.gossip_queries) {
            return Err(ValidateLocalFeaturesError::GossipQueriesNotSupported);
        }

        for (half_index, feature_flag) in self.features.feature_flags.iter().enumerate().skip(4) {
            if *feature_flag == FeatureFlag::Required {
                return Err(ValidateFeaturesError::UnknownRequiredFeature {
                    index: half_index as u16 * 2,
                }
                .into());
            }
        }
        Ok(LocalFeatures {
            option_data_loss_protect,
            initial_routing_sync,
            option_upfront_shutdown_script,
            gossip_queries,
        })
    }
}
