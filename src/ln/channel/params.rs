use super::*;

#[derive(Debug, Clone, Copy)]
pub struct ChannelFlags {
    pub announce_channel: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct ChannelParams {
    pub dust_limit: Sat64,
    pub max_htlc_value_in_flight: Msat64,
    pub channel_reserve: Sat64,
    pub htlc_minimum: Msat64,
    pub to_self_delay: u16,
    pub max_accepted_htlcs: u16,
    pub shutdown_scriptpubkey: Option<Script>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct RemoteChannelParams {
    pub params: ChannelParams,
    pub channel_public_key: ChannelPublicKey,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct LocalChannelParams {
    pub params: ChannelParams,
    pub channel_secret_key: ChannelSecretKey,
}

impl ChannelParams {
    const MAX_MAX_ACCEPTED_HTLCS: u16 = 483;

    // TODO: how to determine a reasonable value for this? I'm using two weeks for no particular
    // reason. (Note that lnd uses 10,000 blocks which seems pretty large and also comes with the
    // comment:
    //     // TODO(halseth): find a more scientific choice of value.
    const MAX_TO_SELF_DELAY: u16 = 2016;

    pub fn dust_limit(&self) -> Sat64 {
        self.dust_limit
    }

    pub fn max_htlc_value_in_flight(&self) -> Msat64 {
        self.max_htlc_value_in_flight
    }

    pub fn channel_reserve(&self) -> Sat64 {
        self.channel_reserve
    }

    pub fn htlc_minimum(&self) -> Msat64 {
        self.htlc_minimum
    }

    pub fn to_self_delay(&self) -> u16 {
        self.to_self_delay
    }

    pub fn max_accepted_htlcs(&self) -> u16 {
        self.max_accepted_htlcs
    }

    pub fn shutdown_scriptpubkey(&self) -> &Option<Script> {
        &self.shutdown_scriptpubkey
    }

    pub fn default_params(funding: Sat64) -> ChannelParams {
        // From BOLT #2: 1% of the channel total is suggested.
        // We round up here to make sure the reserve is at least 1%
        let channel_reserve = {
            let (channel_reserve, remainder) = funding.div_mod_u64(100).unwrap();
            if remainder > Sat64::from_u64(0) {
                channel_reserve.checked_add(Sat64::from_u64(1)).unwrap()
            } else {
                channel_reserve
            }
        };
        ChannelParams {
            // TODO: what's a good value for this?
            // The dust limit cannot be less than the channel reserve. I can't see a reason to set
            // it higher.
            dust_limit: channel_reserve,

            // TODO: what's good value for this?
            max_htlc_value_in_flight: funding.try_to_msat64().unwrap(),

            channel_reserve,

            // TODO: what's good value for this?
            htlc_minimum: channel_reserve.try_to_msat64().unwrap(),

            // TODO: what's a good value for this?
            // Using 6 blocks seems reasonable since that's the traditional number of blocks before
            // a transaction is considered irreversible. Might be worth checking what value other
            // implementations use though.
            to_self_delay: 6,

            // TODO: what's a good value for this?
            max_accepted_htlcs: ChannelParams::MAX_MAX_ACCEPTED_HTLCS / 2,
            shutdown_scriptpubkey: None,
        }
    }
}

impl RemoteChannelParams {
    pub fn dust_limit(&self) -> Sat64 {
        self.params.dust_limit
    }

    pub fn max_htlc_value_in_flight(&self) -> Msat64 {
        self.params.max_htlc_value_in_flight
    }

    pub fn channel_reserve(&self) -> Sat64 {
        self.params.channel_reserve
    }

    pub fn htlc_minimum(&self) -> Msat64 {
        self.params.htlc_minimum
    }

    pub fn to_self_delay(&self) -> u16 {
        self.params.to_self_delay
    }

    pub fn max_accepted_htlcs(&self) -> u16 {
        self.params.max_accepted_htlcs
    }

    pub fn shutdown_scriptpubkey(&self) -> &Option<Script> {
        &self.params.shutdown_scriptpubkey
    }

    pub fn funding_pubkey(&self) -> PublicKey {
        self.channel_public_key.funding_pubkey
    }

    pub fn revocation_basepoint(&self) -> PublicKey {
        self.channel_public_key.revocation_basepoint
    }

    pub fn payment_basepoint(&self) -> PublicKey {
        self.channel_public_key.payment_basepoint
    }

    pub fn delayed_payment_basepoint(&self) -> PublicKey {
        self.channel_public_key.delayed_payment_basepoint
    }

    pub fn htlc_basepoint(&self) -> PublicKey {
        self.channel_public_key.htlc_basepoint
    }

    pub fn validate_incoming_open_channel_msg(
        open_channel_msg: OpenChannelMsg,
    ) -> Result<RemoteChannelParams, IncomingOpenChannelParamsError> {
        let OpenChannelMsg {
            funding_satoshis: funding,
            push_msat: push,
            dust_limit_satoshis: dust_limit,
            max_htlc_value_in_flight_msat: max_htlc_value_in_flight,
            channel_reserve_satoshis: channel_reserve,
            htlc_minimum_msat: htlc_minimum,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            shutdown_scriptpubkey,
            ..
        } = open_channel_msg;

        if push > funding {
            // From BOLT #2
            // > The receiving node MUST fail the channel if [..] push_msat is greater than
            // > funding_satoshis * 1000.
            // (Unit conversion is handled by the `>` operator impl)
            return Err(IncomingOpenChannelParamsError::PushedValueTooGreat { push, funding });
        }

        if to_self_delay > ChannelParams::MAX_TO_SELF_DELAY {
            return Err(IncomingOpenChannelParamsError::ToSelfDelayTooLarge { to_self_delay });
        }

        if max_accepted_htlcs > ChannelParams::MAX_MAX_ACCEPTED_HTLCS {
            // From BOLT #2
            // > The receiving node MUST fail the channel if [..] max_accepted_htlcs is greater
            // > than 483.
            return Err(IncomingOpenChannelParamsError::MaxAcceptedHtlcsTooLarge {
                max_accepted_htlcs,
            });
        }

        if channel_reserve < dust_limit {
            // From BOLT #2
            // > The receiving node MUST fail the channel if [..] dust_limit_satoshis is greater
            // > than channel_reserve_satoshis.
            return Err(IncomingOpenChannelParamsError::ChannelReserveTooSmall {
                channel_reserve,
                dust_limit,
            });
        }

        // TODO: decide whether feerate is acceptable

        Ok(RemoteChannelParams {
            params: ChannelParams {
                dust_limit,
                max_htlc_value_in_flight,
                channel_reserve,
                htlc_minimum,
                to_self_delay,
                max_accepted_htlcs,
                shutdown_scriptpubkey,
            },
            channel_public_key: ChannelPublicKey {
                funding_pubkey,
                revocation_basepoint,
                payment_basepoint,
                delayed_payment_basepoint,
                htlc_basepoint,
            },
        })
    }

    pub fn validate_incoming_accept_channel_msg(
        accept_channel_msg: AcceptChannelMsg,
        our_channel_params: &ChannelParams,
    ) -> Result<RemoteChannelParams, IncomingAcceptChannelParamsError> {
        let AcceptChannelMsg {
            dust_limit_satoshis: dust_limit,
            max_htlc_value_in_flight_msat: max_htlc_value_in_flight,
            channel_reserve_satoshis: channel_reserve,
            htlc_minimum_msat: htlc_minimum,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            shutdown_scriptpubkey,
            minimum_depth,
            ..
        } = accept_channel_msg;

        // TODO: What minimum depth should be considered unreasonably large?
        // Since 6 blocks is traditionally when transactions are considered confirmed I've just
        // doubled that.
        if minimum_depth > 12 {
            return Err(IncomingAcceptChannelParamsError::MinimumDepthTooLarge { minimum_depth });
        }

        let smaller_dust_limit = cmp::min(dust_limit, our_channel_params.dust_limit);
        if channel_reserve < smaller_dust_limit {
            // From BOLT #2
            // > The receiver [..] if channel_reserve_satoshis is less than dust_limit_satoshis
            // > within the open_channel message: MUST reject the channel.
            return Err(IncomingAcceptChannelParamsError::ChannelReserveTooSmall {
                channel_reserve,
                smaller_dust_limit,
            });
        }

        if our_channel_params.channel_reserve < dust_limit {
            // From BOLT #2
            // > The receiver [..] if channel_reserve_satoshis from the open_channel message is
            // > less than dust_limit_satoshis: MUST reject the channel.
            return Err(IncomingAcceptChannelParamsError::DustLimitTooLarge {
                channel_reserve: our_channel_params.channel_reserve,
                dust_limit,
            });
        }

        if to_self_delay > ChannelParams::MAX_TO_SELF_DELAY {
            return Err(IncomingAcceptChannelParamsError::ToSelfDelayTooLarge { to_self_delay });
        }

        if max_accepted_htlcs > ChannelParams::MAX_MAX_ACCEPTED_HTLCS {
            // From BOLT #2
            // > The receiving node MUST fail the channel if [..] max_accepted_htlcs is greater
            // > than 483.
            // > [..]
            // > Other fields have the same requirements as their counterparts in open_channel.
            return Err(IncomingAcceptChannelParamsError::MaxAcceptedHtlcsTooLarge {
                max_accepted_htlcs,
            });
        }

        Ok(RemoteChannelParams {
            params: ChannelParams {
                dust_limit,
                max_htlc_value_in_flight,
                channel_reserve,
                htlc_minimum,
                to_self_delay,
                max_accepted_htlcs,
                shutdown_scriptpubkey,
            },
            channel_public_key: ChannelPublicKey {
                funding_pubkey,
                revocation_basepoint,
                payment_basepoint,
                delayed_payment_basepoint,
                htlc_basepoint,
            },
        })
    }
}

impl LocalChannelParams {
    pub fn to_remote_params(&self) -> RemoteChannelParams {
        RemoteChannelParams {
            params: self.params.clone(),
            channel_public_key: ChannelPublicKey::from_secret_key(&self.channel_secret_key),
        }
    }

    pub fn dust_limit(&self) -> Sat64 {
        self.params.dust_limit
    }

    pub fn max_htlc_value_in_flight(&self) -> Msat64 {
        self.params.max_htlc_value_in_flight
    }

    pub fn channel_reserve(&self) -> Sat64 {
        self.params.channel_reserve
    }

    pub fn htlc_minimum(&self) -> Msat64 {
        self.params.htlc_minimum
    }

    pub fn to_self_delay(&self) -> u16 {
        self.params.to_self_delay
    }

    pub fn max_accepted_htlcs(&self) -> u16 {
        self.params.max_accepted_htlcs
    }

    pub fn shutdown_scriptpubkey(&self) -> &Option<Script> {
        &self.params.shutdown_scriptpubkey
    }

    pub fn funding_pubkey(&self) -> PublicKey {
        self.channel_secret_key.funding_pubkey()
    }

    pub fn funding_seckey(&self) -> &SecretKey {
        &self.channel_secret_key.funding_seckey
    }

    pub fn revocation_basepoint(&self) -> PublicKey {
        self.channel_secret_key.revocation_basepoint()
    }

    pub fn payment_basepoint(&self) -> PublicKey {
        self.channel_secret_key.payment_basepoint()
    }

    pub fn delayed_payment_basepoint(&self) -> PublicKey {
        self.channel_secret_key.delayed_payment_basepoint()
    }

    pub fn htlc_basepoint(&self) -> PublicKey {
        self.channel_secret_key.htlc_basepoint()
    }

    pub fn new_default_params(funding: Sat64) -> LocalChannelParams {
        let params = ChannelParams::default_params(funding);
        let channel_secret_key = ChannelSecretKey::new();
        LocalChannelParams {
            params,
            channel_secret_key,
        }
    }

    pub fn prepare_outgoing_open_channel_msg(
        self,
        temporary_channel_id: TemporaryChannelId,
        funding: Sat64,
        push: Msat64,
        feerate: SatPerKw32,
        network: Network,
    ) -> Result<OpenChannelMsg, OutgoingOpenChannelParamsError> {
        // From BOLT #2
        // > The sending node [..] MUST set funding_satoshis to less than 2^24 satoshi.
        const MAX_FUNDING: Sat64 = Sat64::from_u64(1 << 24);

        let LocalChannelParams {
            params:
                ChannelParams {
                    dust_limit,
                    max_htlc_value_in_flight,
                    channel_reserve,
                    htlc_minimum,
                    to_self_delay,
                    max_accepted_htlcs,
                    shutdown_scriptpubkey,
                },
            channel_secret_key,
        } = self;
        let ChannelPublicKey {
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
        } = ChannelPublicKey::from_secret_key(&channel_secret_key);

        let chain_hash = network.chain_hash();
        let channel_flags = ChannelFlags {
            announce_channel: true,
        };

        if funding >= MAX_FUNDING {
            return Err(OutgoingOpenChannelParamsError::FundingTooGreat { funding });
        }

        if push > funding {
            // From BOLT #2
            // > The sending node [..] MUST set push_msat to equal or less than
            // > 1000 * funding_satoshis.
            // (Unit conversion is handled by the `>` operator impl)
            return Err(OutgoingOpenChannelParamsError::PushedValueTooGreat { push, funding });
        }

        if channel_reserve < dust_limit {
            // From BOLT #2
            // > The sending node [..] MUST set channel_reserve_satoshis greater than or equal to
            // dust_limit_satoshis.
            return Err(OutgoingOpenChannelParamsError::ChannelReserveTooSmall {
                channel_reserve,
                dust_limit,
            });
        }

        let first_per_commitment_point =
            PublicKey::from_secret_key(&channel_secret_key.per_commitment_secret(u48!(0)));

        Ok(OpenChannelMsg {
            chain_hash,
            temporary_channel_id,
            funding_satoshis: funding,
            push_msat: push,
            dust_limit_satoshis: dust_limit,
            max_htlc_value_in_flight_msat: max_htlc_value_in_flight,
            channel_reserve_satoshis: channel_reserve,
            htlc_minimum_msat: htlc_minimum,
            feerate_per_kw: feerate,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            first_per_commitment_point,
            channel_flags,
            shutdown_scriptpubkey,
        })
    }

    pub fn prepare_outgoing_accept_channel_msg(
        self,
        their_channel_params: ChannelParams,
        temporary_channel_id: TemporaryChannelId,
    ) -> Result<AcceptChannelMsg, OutgoingAcceptChannelParamsError> {
        let LocalChannelParams {
            params:
                ChannelParams {
                    dust_limit,
                    max_htlc_value_in_flight,
                    channel_reserve,
                    htlc_minimum,
                    to_self_delay,
                    max_accepted_htlcs,
                    shutdown_scriptpubkey,
                },
            channel_secret_key,
        } = self;
        let ChannelPublicKey {
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
        } = ChannelPublicKey::from_secret_key(&channel_secret_key);

        if channel_reserve < their_channel_params.dust_limit {
            // From BOLT #2
            // > The sender [..] MUST set channel_reserve_satoshis greater than or equal to
            // > dust_limit_satoshis from the open_channel message.
            return Err(OutgoingAcceptChannelParamsError::ChannelReserveTooSmall {
                channel_reserve,
                dust_limit: their_channel_params.dust_limit,
            });
        }

        if their_channel_params.channel_reserve < dust_limit {
            // From BOLT #2
            // > The sender [..] MUST set dust_limit_satoshis less than or equal to
            // > channel_reserve_satoshis from the open_channel message.
            return Err(OutgoingAcceptChannelParamsError::DustLimitTooSmall {
                channel_reserve: their_channel_params.channel_reserve,
                dust_limit,
            });
        }

        let minimum_depth = 6;

        let first_per_commitment_point =
            PublicKey::from_secret_key(&channel_secret_key.per_commitment_secret(u48!(0)));

        Ok(AcceptChannelMsg {
            temporary_channel_id,
            dust_limit_satoshis: dust_limit,
            max_htlc_value_in_flight_msat: max_htlc_value_in_flight,
            channel_reserve_satoshis: channel_reserve,
            htlc_minimum_msat: htlc_minimum,
            minimum_depth,
            to_self_delay,
            max_accepted_htlcs,
            funding_pubkey,
            revocation_basepoint,
            payment_basepoint,
            delayed_payment_basepoint,
            htlc_basepoint,
            first_per_commitment_point,
            shutdown_scriptpubkey,
        })
    }
}

#[derive(Clone, Debug, Error)]
pub enum OutgoingOpenChannelParamsError {
    #[error("funding too great ({})", funding)]
    FundingTooGreat { funding: Sat64 },
    #[error("push amount greater than channel funding ({} > {})", push, funding)]
    PushedValueTooGreat { push: Msat64, funding: Sat64 },
    #[error(
        "channel reserve smaller than the dust limit ({} < {})",
        channel_reserve,
        dust_limit
    )]
    ChannelReserveTooSmall {
        channel_reserve: Sat64,
        dust_limit: Sat64,
    },
}

#[derive(Clone, Debug, Error)]
pub enum IncomingOpenChannelParamsError {
    #[error("push amount greater than channel funding ({} > {})", push, funding)]
    PushedValueTooGreat { push: Msat64, funding: Sat64 },
    #[error("to self delay too large ({} blocks)", to_self_delay)]
    ToSelfDelayTooLarge { to_self_delay: u16 },
    #[error("max_accepted_htlcs too large ({})", max_accepted_htlcs)]
    MaxAcceptedHtlcsTooLarge { max_accepted_htlcs: u16 },
    #[error(
        "channel reserve smaller than the dust limit ({} < {})",
        channel_reserve,
        dust_limit
    )]
    ChannelReserveTooSmall {
        channel_reserve: Sat64,
        dust_limit: Sat64,
    },
}

#[derive(Clone, Debug, Error)]
pub enum OutgoingAcceptChannelParamsError {
    #[error(
        "channel reserve smaller than the funder's dust limit ({} < {})",
        channel_reserve,
        dust_limit
    )]
    ChannelReserveTooSmall {
        channel_reserve: Sat64,
        dust_limit: Sat64,
    },
    #[error(
        "dust limit smaller than the funder's channel reserve ({} < {})",
        dust_limit,
        channel_reserve
    )]
    DustLimitTooSmall {
        channel_reserve: Sat64,
        dust_limit: Sat64,
    },
}

#[derive(Clone, Debug, Error)]
pub enum IncomingAcceptChannelParamsError {
    #[error(
        "requested minimum depth of funding transaction too large ({})",
        minimum_depth
    )]
    MinimumDepthTooLarge { minimum_depth: u32 },
    #[error("to self delay too large ({} blocks)", to_self_delay)]
    ToSelfDelayTooLarge { to_self_delay: u16 },
    #[error("max_accepted_htlcs too large ({})", max_accepted_htlcs)]
    MaxAcceptedHtlcsTooLarge { max_accepted_htlcs: u16 },
    #[error(
        "requested channel reserve smaller than the smaller dust limit ({} < {})",
        channel_reserve,
        smaller_dust_limit
    )]
    ChannelReserveTooSmall {
        channel_reserve: Sat64,
        smaller_dust_limit: Sat64,
    },
    #[error(
        "requested dust limit greater than our channel reserve ({} > {})",
        dust_limit,
        channel_reserve
    )]
    DustLimitTooLarge {
        channel_reserve: Sat64,
        dust_limit: Sat64,
    },
}
