use super::*;

#[derive(Clone)]
pub struct ChannelStore {
    inner: OnDisk<ChannelStoreInner>,
}

#[derive(Debug, Serialize, Deserialize, Default, PartialEq)]
pub struct ChannelStoreInner {
    pub unfunded_channels: HashMap<(PeerId, TemporaryChannelId), UnfundedChannel>,
    pub channels: HashMap<ChannelId, Channel>,
}

// TODO: once type_alias_impl_trait is stable this can be unboxed/undyned
pub type ChannelsStream =
    Pin<Box<dyn Stream<Item = Result<HashMap<ChannelId, Channel>, Arc<ReadFromDiskError>>> + Send>>;

impl ChannelStore {
    pub async fn open(
        base_directory: PathBuf,
        peer_secret_key: &PeerSecretKey,
    ) -> Result<ChannelStore, OnDiskNewError> {
        let mut path = base_directory;
        path.push("channel_store");
        let inner = OnDisk::new_data(peer_secret_key.as_secret_key(), path).await?;
        Ok(ChannelStore { inner })
    }

    pub fn channels(&self) -> ChannelsStream {
        let watch = self.inner.watch();
        watch.map_ok(|inner| inner.channels.clone()).boxed()
    }

    pub async fn read(&self) -> Result<ChannelStoreInner, ReadFromDiskError> {
        self.inner.read().await
    }

    pub async fn mutate<R>(
        &self,
        f: impl FnOnce(&mut ChannelStoreInner) -> R,
    ) -> Result<R, OnDiskMutateError> {
        self.inner.mutate(f).await
    }
}
