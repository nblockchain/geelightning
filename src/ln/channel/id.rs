use super::*;

pub const CHANNEL_ID_BYTES_LEN: usize = 32;

#[derive(Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct ChannelId {
    id: [u8; CHANNEL_ID_BYTES_LEN],
}

impl fmt::Debug for ChannelId {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.as_bytes();
        let bytes = format!("{:?}", HexDebug(bytes));

        fmt.debug_tuple("channel_id!").field(&bytes).finish()
    }
}

impl fmt::Display for ChannelId {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.as_bytes();
        write!(fmt, "{:?}", HexDebug(bytes))
    }
}

impl ChannelId {
    pub fn from_funding_outpoint(funding_outpoint: OutPoint) -> ChannelId {
        let mut id = funding_outpoint.txid.into_inner();
        let output_index_bytes = (funding_outpoint.vout as u16).to_be_bytes();
        id[id.len() - 2] ^= output_index_bytes[0];
        id[id.len() - 1] ^= output_index_bytes[1];
        ChannelId { id }
    }

    pub fn from_bytes(bytes: [u8; CHANNEL_ID_BYTES_LEN]) -> ChannelId {
        ChannelId { id: bytes }
    }

    pub fn as_bytes(&self) -> &[u8; CHANNEL_ID_BYTES_LEN] {
        &self.id
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(&self.id)
    }

    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<ChannelId, MsgTooShortError> {
        let id = deserializer.read_array::<CHANNEL_ID_BYTES_LEN>()?;
        Ok(ChannelId { id })
    }
}

#[derive(Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct TemporaryChannelId {
    id: [u8; CHANNEL_ID_BYTES_LEN],
}

impl fmt::Debug for TemporaryChannelId {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let bytes = self.as_bytes();
        let bytes = format!("{:?}", HexDebug(bytes));

        fmt.debug_tuple("temporary_channel_id!")
            .field(&bytes)
            .finish()
    }
}

impl TemporaryChannelId {
    pub fn new_random() -> TemporaryChannelId {
        TemporaryChannelId { id: rand::random() }
    }

    pub fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(&self.id);
    }

    pub fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<TemporaryChannelId, MsgTooShortError> {
        let id = deserializer.read_array::<CHANNEL_ID_BYTES_LEN>()?;
        Ok(TemporaryChannelId { id })
    }

    // This partly defeats the purpose of having separate ChannelId and TemporaryChannelId types,
    // however it's necessary because the wire protocol's error messages don't distinguish between
    // the two channel id types.
    pub fn pretend_real(self) -> ChannelId {
        ChannelId { id: self.id }
    }

    pub fn from_bytes(bytes: [u8; CHANNEL_ID_BYTES_LEN]) -> TemporaryChannelId {
        TemporaryChannelId { id: bytes }
    }

    pub fn as_bytes(&self) -> &[u8; CHANNEL_ID_BYTES_LEN] {
        &self.id
    }
}
