use super::*;

#[derive(Debug, Error)]
pub enum ChannelManagerNewError {
    #[error("error starting the peer manager: {}", source)]
    PeerManagerNew { source: PeerManagerNewError },
    #[error("error opening wallet: {}", source)]
    OpenWallet { source: OnDiskNewError },
}

#[derive(Debug, Error)]
pub enum OpenChannelError {
    #[error("error opening channel: {}", source)]
    OpenChannel { source: peer::OpenChannelError },
    #[error("error saving channel to wallet: {}", source)]
    StoreChannel { source: OnDiskMutateError },
}

#[derive(Debug, Error)]
pub enum FundChannelError {
    #[error("error funding channel: {}", source)]
    FundChannel { source: peer::FundChannelError },
    #[error("error saving channel to wallet: {}", source)]
    StoreChannel { source: OnDiskMutateError },
    #[error("error reading channel state from wallet: {}", source)]
    ReadWalletFile { source: ReadFromDiskError },
    #[error("no channel with the specified id exists")]
    NoSuchChannel,
}

#[derive(Debug, Error)]
pub enum ChannelManagerError {
    #[error("error accepting peer: {}", source)]
    TransportAccept { source: transport::AcceptError },
    #[error("error accepting channel: {}", source)]
    AcceptChannel { source: AcceptChannelError },
    #[error("error saving accepted channel to disk: {}", source)]
    StoreAcceptedChannel { source: OnDiskMutateError },
    #[error("error reading wallet file: {}", source)]
    ReadWalletFile { source: Arc<ReadFromDiskError> },
    #[error(
        "error updating wallet file to record that funding has been locked: {}",
        source
    )]
    RecordRemoteFundingLocked { source: OnDiskMutateError },
}

#[derive(Debug, Error)]
pub enum LocalConfirmChannelError {
    #[error("error reading wallet file: {}", source)]
    ReadWalletFile { source: ReadFromDiskError },
    #[error("error updating wallet file: {}", source)]
    UpdateWalletFile { source: OnDiskMutateError },
    #[error("no channel with id {}", channel_id)]
    NoSuchChannel { channel_id: ChannelId },
    #[error("error sending funding locked message: {}", source)]
    SendFundingLocked { source: peer::FundingLockedError },
}

#[derive(Debug, Error)]
pub enum CancelChannelError {
    #[error("funding already locked")]
    FundingAlreadyLocked,
    #[error("error updating wallet file: {}", source)]
    UpdateWalletFile { source: OnDiskMutateError },
}

pub struct ChannelManager {
    channel_manager_cmd_send: mpsc::Sender<ChannelManagerCmd>,
}

#[allow(clippy::large_enum_variant)]
enum ChannelManagerCmd {
    OpenChannel {
        peer_id: PeerId,
        funding: Sat64,
        feerate: SatPerKw32,
        reply_send: async_generator::Sender<
            Arc<MsgChannelError>,
            Result<(TemporaryChannelId, Script), OpenChannelError>,
        >,
        network: Network,
    },
    FundChannel {
        peer_id: PeerId,
        temporary_channel_id: TemporaryChannelId,
        funding_outpoint: OutPoint,
        reply_send:
            async_generator::Sender<Arc<MsgChannelError>, Result<ChannelId, FundChannelError>>,
    },
    OfferEndpoint {
        endpoint: Endpoint,
        reply_send: oneshot::Sender<()>,
    },
    Channels {
        reply_send: oneshot::Sender<ChannelsStream>,
    },
    LocalConfirmChannel {
        channel_id: ChannelId,
        reply_send:
            async_generator::Sender<Arc<MsgChannelError>, Result<(), LocalConfirmChannelError>>,
    },
    CancelChannel {
        channel_id: ChannelId,
        reply_send: oneshot::Sender<Result<(), CancelChannelError>>,
    },
    GetListenEndpoint {
        reply_send: oneshot::Sender<Endpoint>,
    },
}

impl ChannelManager {
    pub async fn new(
        base_directory: PathBuf,
        local_endpoint: &LocalEndpoint,
    ) -> Result<(ChannelManager, mpsc::Receiver<ChannelManagerError>), ChannelManagerNewError> {
        let (peer_manager, accept_errors, accepted_channel_recv, remote_funding_locked_recv) = {
            PeerManager::new(local_endpoint)
                .await
                .map_err(|source| ChannelManagerNewError::PeerManagerNew { source })?
        };
        let channel_store = ChannelStore::open(base_directory, &local_endpoint.peer_secret_key)
            .await
            .map_err(|source| ChannelManagerNewError::OpenWallet { source })?;
        let (channel_manager_cmd_send, channel_manager_cmd_recv) = mpsc_channel!();
        let (channel_manager_error_send, channel_manager_error_recv) = mpsc_channel!();
        let channel_manager = ChannelManager {
            channel_manager_cmd_send,
        };
        spawn!(channel_manager_task(
            peer_manager,
            accept_errors,
            accepted_channel_recv,
            remote_funding_locked_recv,
            channel_store,
            channel_manager_cmd_recv,
            channel_manager_error_send,
        ));
        Ok((channel_manager, channel_manager_error_recv))
    }

    pub fn open_channel<'a>(
        &'a self,
        peer_id: PeerId,
        funding: Sat64,
        feerate: SatPerKw32,
        network: Network,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<(TemporaryChannelId, Script), OpenChannelError>,
    > + 'a {
        let (reply_send, reply_recv) = async_generator::channel();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        async move {
            channel_manager_cmd_send
                .send(ChannelManagerCmd::OpenChannel {
                    peer_id,
                    funding,
                    feerate,
                    reply_send,
                    network,
                })
                .await
                .unwrap();
            reply_recv.map(|r_opt| r_opt.unwrap())
        }
        .flatten_async_generator()
    }

    pub fn fund_channel<'a>(
        &'a self,
        peer_id: PeerId,
        temporary_channel_id: TemporaryChannelId,
        funding_outpoint: OutPoint,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<ChannelId, FundChannelError>,
    > + 'a {
        let (reply_send, reply_recv) = async_generator::channel();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        async move {
            channel_manager_cmd_send
                .send(ChannelManagerCmd::FundChannel {
                    peer_id,
                    temporary_channel_id,
                    funding_outpoint,
                    reply_send,
                })
                .await
                .unwrap();

            reply_recv.map(|r_opt| r_opt.unwrap())
        }
        .flatten_async_generator()
    }

    pub fn channels(
        &self,
    ) -> impl Stream<Item = Result<HashMap<ChannelId, Channel>, Arc<ReadFromDiskError>>> {
        let (reply_send, reply_recv) = oneshot_channel!();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        async move {
            channel_manager_cmd_send
                .send(ChannelManagerCmd::Channels { reply_send })
                .await
                .unwrap();

            reply_recv.await.unwrap()
        }
        .flatten_stream()
    }

    pub fn local_confirm_channel<'a>(
        &'a self,
        channel_id: ChannelId,
    ) -> impl AsyncGenerator<
        Yield = Arc<MsgChannelError>,
        Return = Result<(), LocalConfirmChannelError>,
    > + 'a {
        let (reply_send, reply_recv) = async_generator::channel();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        async move {
            channel_manager_cmd_send
                .send(ChannelManagerCmd::LocalConfirmChannel {
                    channel_id,
                    reply_send,
                })
                .await
                .unwrap();

            reply_recv.map(|r_opt| r_opt.unwrap())
        }
        .flatten_async_generator()
    }

    pub async fn cancel_channel(&self, channel_id: ChannelId) -> Result<(), CancelChannelError> {
        let (reply_send, reply_recv) = oneshot_channel!();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        channel_manager_cmd_send
            .send(ChannelManagerCmd::CancelChannel {
                channel_id,
                reply_send,
            })
            .await
            .unwrap();

        reply_recv.await.unwrap()
    }

    pub async fn offer_endpoint(&self, endpoint: Endpoint) {
        let (reply_send, reply_recv) = oneshot_channel!();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        channel_manager_cmd_send
            .send(ChannelManagerCmd::OfferEndpoint {
                endpoint,
                reply_send,
            })
            .await
            .unwrap();

        reply_recv.await.unwrap()
    }

    pub async fn get_listen_endpoint(&self) -> Endpoint {
        let (reply_send, reply_recv) = oneshot_channel!();
        let mut channel_manager_cmd_send = self.channel_manager_cmd_send.clone();
        channel_manager_cmd_send
            .send(ChannelManagerCmd::GetListenEndpoint { reply_send })
            .await
            .unwrap();

        reply_recv.await.unwrap()
    }
}

#[allow(clippy::cognitive_complexity)]
async fn channel_manager_task(
    peer_manager: PeerManager,
    accept_error_recv: impl Stream<Item = transport::AcceptError>,
    mut accepted_channel_recv: mpsc::Receiver<
        Result<(Arc<RemotePeer>, Channel), AcceptChannelError>,
    >,
    mut remote_funding_locked_recv: mpsc::Receiver<(PeerId, ChannelId, PublicKey)>,
    channel_store: ChannelStore,
    mut channel_manager_cmd_recv: mpsc::Receiver<ChannelManagerCmd>,
    mut channel_manager_error_send: mpsc::Sender<ChannelManagerError>,
) {
    let accept_error_recv = accept_error_recv.fuse();
    pin_mut!(accept_error_recv);
    let mut open_channel_futures: FuturesUnordered<_> = FuturesUnordered::new();
    let mut fund_channel_futures: FuturesUnordered<_> = FuturesUnordered::new();
    let mut funding_locked_futures: FuturesUnordered<_> = FuturesUnordered::new();
    let mut channel_store_updates = channel_store.channels().fuse();
    let mut channel_peers: HashMap<ChannelId, Arc<RemotePeer>> = HashMap::new();
    let mut unfunded_channel_peers: HashMap<(PeerId, TemporaryChannelId), Arc<RemotePeer>> =
        HashMap::new();
    loop {
        let channel_manager_cmd = futures::select! {
            channel_manager_cmd_opt = channel_manager_cmd_recv.next() => {
                match channel_manager_cmd_opt {
                    Some(channel_manager_cmd) => channel_manager_cmd,
                    None => break,
                }
            },
            res_opt = open_channel_futures.select_next_some() => {
                let (reply_send, open_channel_res) = match res_opt {
                    Some(res) => res,
                    None => continue,
                };
                let reply_send: async_generator::Sender<
                    Arc<MsgChannelError>,
                    Result<(TemporaryChannelId, Script), OpenChannelError>,
                > = reply_send;
                let open_channel_res: Result<
                    (TemporaryChannelId, UnfundedChannel, Arc<RemotePeer>),
                    _,
                > = open_channel_res;
                let reply = try_async!({
                    let (temporary_channel_id, unfunded_channel, remote_peer) = open_channel_res?;
                    let peer_id = remote_peer.peer_id();
                    let funding_script_pubkey = unfunded_channel.funding_script_pubkey();
                    channel_store
                        .mutate(|store| {
                            let _ = store.unfunded_channels.insert(
                                (peer_id, temporary_channel_id),
                                unfunded_channel,
                            );
                        })
                        .await
                        .map_err(|source| OpenChannelError::StoreChannel { source })?;
                    unfunded_channel_peers.insert((peer_id, temporary_channel_id), remote_peer);
                    (temporary_channel_id, funding_script_pubkey)
                });
                reply_send.send_return(reply).await.ignore_err();
                continue;
            },
            res_opt = fund_channel_futures.select_next_some() => {
                let (reply_send, fund_channel_res) = match res_opt {
                    Some(res) => res,
                    None => continue,
                };
                let reply_send: async_generator::Sender<Arc<MsgChannelError>, Result<ChannelId, FundChannelError>> = reply_send;
                let fund_channel_res: Result<
                    (TemporaryChannelId, Channel),
                    _,
                > = fund_channel_res;
                let reply = try_async!({
                    let (temporary_channel_id, channel) = fund_channel_res?;
                    let channel_id = channel.channel_id();
                    let peer_id = channel.remote_peer_id();
                    channel_store
                        .mutate(move |store| {
                            let _ = store.unfunded_channels.remove(&(peer_id, temporary_channel_id));
                            store.channels.insert(channel_id, channel);
                        })
                        .await
                        .map_err(|source| FundChannelError::StoreChannel { source })?;
                    let remote_peer = unfunded_channel_peers.remove(&(peer_id, temporary_channel_id)).unwrap();
                    channel_peers.insert(channel_id, remote_peer);
                    channel_id
                });
                reply_send.send_return(reply).await.ignore_err();
                continue;
            },
            res_opt = funding_locked_futures.select_next_some() => {
                let (reply_send, funding_locked_res) = match res_opt {
                    Some(res) => res,
                    None => continue,
                };
                let reply_send: async_generator::Sender<Arc<MsgChannelError>, Result<(), LocalConfirmChannelError>> = reply_send;
                let reply = try_async!({
                    let channel_id: ChannelId = funding_locked_res?;
                    channel_store
                        .mutate(move |store| {
                            match store.channels.get_mut(&channel_id) {
                                Some(channel) => {
                                    channel.local_confirm();
                                    Ok(())
                                },
                                None => Err(LocalConfirmChannelError::NoSuchChannel { channel_id }),
                            }
                        })
                        .await
                        .unwrap_or_else(|source| Err(LocalConfirmChannelError::UpdateWalletFile { source }))?;
                });
                reply_send.send_return(reply).await.ignore_err();
                continue;
            },
            accepted_channel_res = accepted_channel_recv.select_next_some() => {
                let res = try_async!({
                    let (remote_peer, channel) =
                        accepted_channel_res
                        .map_err(|source| ChannelManagerError::AcceptChannel { source })?;
                    let channel_id = channel.channel_id();
                    channel_store
                        .mutate(|store| {
                            store.channels.insert(channel_id, channel);
                        })
                        .await
                        .map_err(|source| ChannelManagerError::StoreAcceptedChannel { source })?;
                    channel_peers.insert(channel_id, remote_peer);
                });
                match res {
                    Ok(()) => (),
                    Err(err) => channel_manager_error_send.send(err).await.ignore_err(),
                }
                continue;
            },
            (peer_id, channel_id, next_per_commitment_point) = remote_funding_locked_recv.select_next_some() => {
                let res = channel_store
                    .mutate(move |store| {
                        match store.channels.get_mut(&channel_id) {
                            Some(channel) => {
                                let actual_peer_id = channel.remote_peer_id();
                                if actual_peer_id == peer_id {
                                    channel.remote_confirm(next_per_commitment_point);
                                } else {
                                    // TODO: peer sent a message for a channel not owned by them.
                                    // we should probably drop the connection to that peer.
                                    println!("peer {} sent a message for channel not owned by them", peer_id);
                                    println!("we should probably drop the connection to that peer");
                                }
                            },
                            None => {
                                // TODO: no such channel.
                                // How best to handle this?
                                println!("received a funding_locked message for channel that doesn't exist");
                                println!("channel_id == {}, from peer {}", channel_id, peer_id);
                            },
                        }
                    })
                    .await
                    .map_err(|source| ChannelManagerError::RecordRemoteFundingLocked { source });
                match res {
                    Ok(()) => (),
                    Err(err) => channel_manager_error_send.send(err).await.ignore_err(),
                }
                continue;
            },
            err = accept_error_recv.select_next_some() => {
                channel_manager_error_send.send(ChannelManagerError::TransportAccept { source: err }).await.ignore_err();
                continue;
            },
            channels_res = channel_store_updates.select_next_some() => {
                match channels_res {
                    Ok(channels) => {
                        let mut new_channel_peers = HashMap::new();
                        for (channel_id, channel) in channels {
                            let peer_id = channel.remote_peer_id();
                            let remote_peer = peer_manager.get_peer(peer_id).await;
                            new_channel_peers.insert(channel_id, remote_peer);
                        }
                        let _ = mem::replace(&mut channel_peers, new_channel_peers);
                    },
                    Err(err) => {
                        channel_manager_error_send.send(ChannelManagerError::ReadWalletFile { source: err }).await.ignore_err();
                    },
                }
                continue;
            },
        };
        match channel_manager_cmd {
            ChannelManagerCmd::OpenChannel {
                peer_id,
                funding,
                feerate,
                network,
                mut reply_send,
            } => {
                let temporary_channel_id = TemporaryChannelId::new_random();
                let local_params = LocalChannelParams::new_default_params(funding);
                let push = Msat64::from_u64(0);
                let remote_peer_future = peer_manager.get_peer(peer_id);
                let open_channel_future = async move {
                    let remote_peer = remote_peer_future.await;
                    let open_channel_generator = remote_peer
                        .open_channel(
                            temporary_channel_id,
                            local_params,
                            funding,
                            push,
                            feerate,
                            network,
                        )
                        .map_ok(move |unfunded_channel| {
                            (temporary_channel_id, unfunded_channel, remote_peer)
                        })
                        .map_err(|source| OpenChannelError::OpenChannel { source })
                        .fuse();
                    pin_mut!(open_channel_generator);
                    loop {
                        futures::select! {
                            state = open_channel_generator.next_state() => {
                                match state {
                                    GeneratorState::Yielded(y) => {
                                        match reply_send.send_yield(y).await {
                                            Ok(()) => (),
                                            Err(..) => break None,
                                        }
                                    },
                                    GeneratorState::Complete(r) => {
                                        break Some((reply_send, r));
                                    },
                                }
                            },
                            () = reply_send.on_drop() => break None,
                        }
                    }
                };
                open_channel_futures.push(open_channel_future);
            }
            ChannelManagerCmd::FundChannel {
                peer_id,
                temporary_channel_id,
                funding_outpoint,
                mut reply_send,
            } => {
                let stuff_res = try_async!({
                    let remote_peer = unfunded_channel_peers
                        .get(&(peer_id, temporary_channel_id))
                        .cloned()
                        .ok_or(FundChannelError::NoSuchChannel)?;
                    let store = channel_store
                        .read()
                        .await
                        .map_err(|source| FundChannelError::ReadWalletFile { source })?;
                    let unfunded_channel = store
                        .unfunded_channels
                        .get(&(peer_id, temporary_channel_id))
                        .cloned()
                        .ok_or(FundChannelError::NoSuchChannel)?;
                    (remote_peer, unfunded_channel)
                });
                let (remote_peer, unfunded_channel) = match stuff_res {
                    Ok(stuff) => stuff,
                    Err(err) => {
                        reply_send.send_return(Err(err)).await.ignore_err();
                        continue;
                    }
                };
                let fund_channel_future = async move {
                    let fund_channel_generator = remote_peer
                        .fund_channel(temporary_channel_id, unfunded_channel, funding_outpoint)
                        .map_ok(move |channel| (temporary_channel_id, channel))
                        .map_err(|source| FundChannelError::FundChannel { source })
                        .fuse();
                    pin_mut!(fund_channel_generator);
                    loop {
                        futures::select! {
                            state = fund_channel_generator.next_state() => {
                                match state {
                                    GeneratorState::Yielded(y) => {
                                        match reply_send.send_yield(y).await {
                                            Ok(()) => (),
                                            Err(..) => break None,
                                        }
                                    },
                                    GeneratorState::Complete(r) => {
                                        break Some((reply_send, r));
                                    },
                                }
                            },
                            () = reply_send.on_drop() => break None,
                        }
                    }
                };
                fund_channel_futures.push(fund_channel_future);
            }
            ChannelManagerCmd::Channels { reply_send } => {
                let reply = channel_store.channels();
                reply_send.send(reply).await.ok().unwrap();
            }
            ChannelManagerCmd::LocalConfirmChannel {
                channel_id,
                mut reply_send,
            } => {
                let channel_res = try_async!({
                    let store = channel_store
                        .read()
                        .await
                        .map_err(|source| LocalConfirmChannelError::ReadWalletFile { source })?;
                    store
                        .channels
                        .get(&channel_id)
                        .cloned()
                        .ok_or(LocalConfirmChannelError::NoSuchChannel { channel_id })?
                });
                let channel = match channel_res {
                    Ok(channel) => channel,
                    Err(err) => {
                        reply_send.send_return(Err(err)).await.ignore_err();
                        continue;
                    }
                };
                let peer_id = channel.remote_peer_id();
                let remote_peer = peer_manager.get_peer(peer_id).await;
                let funding_locked_future = async move {
                    let funding_locked_generator = remote_peer
                        .funding_locked(channel)
                        .map_err(|source| LocalConfirmChannelError::SendFundingLocked { source })
                        .map_ok(move |()| channel_id)
                        .fuse();
                    pin_mut!(funding_locked_generator);
                    loop {
                        futures::select! {
                            state = funding_locked_generator.next_state() => {
                                match state {
                                    GeneratorState::Yielded(y) => {
                                        match reply_send.send_yield(y).await {
                                            Ok(()) => (),
                                            Err(..) => break None,
                                        }
                                    },
                                    GeneratorState::Complete(r) => {
                                        break Some((reply_send, r));
                                    },
                                }
                            },
                            () = reply_send.on_drop() => break None,
                        }
                    }
                };
                funding_locked_futures.push(funding_locked_future);
            }
            ChannelManagerCmd::CancelChannel {
                channel_id,
                reply_send,
            } => {
                let reply = channel_store
                    .mutate(move |store| {
                        if let hash_map::Entry::Occupied(oe) = store.channels.entry(channel_id) {
                            if !oe.get().cancelable() {
                                return Err(CancelChannelError::FundingAlreadyLocked);
                            }
                            oe.remove();
                        }
                        Ok(())
                    })
                    .await
                    .unwrap_or_else(|source| Err(CancelChannelError::UpdateWalletFile { source }));
                reply_send.send(reply).await.ignore_err();
            }
            ChannelManagerCmd::OfferEndpoint {
                endpoint,
                reply_send,
            } => {
                peer_manager.offer_endpoint(&endpoint).await;
                reply_send.send(()).await.ignore_err();
            }
            ChannelManagerCmd::GetListenEndpoint { reply_send } => {
                let endpoint = peer_manager.get_listen_endpoint();
                reply_send.send(endpoint).await.ignore_err();
            }
        }
    }
}
