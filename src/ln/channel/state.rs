use super::*;

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub enum ChannelPolarity {
    Funder,
    Fundee,
}

impl ops::Not for ChannelPolarity {
    type Output = ChannelPolarity;

    fn not(self) -> ChannelPolarity {
        match self {
            ChannelPolarity::Funder => ChannelPolarity::Fundee,
            ChannelPolarity::Fundee => ChannelPolarity::Funder,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct Channel {
    config: ChannelConfig,
    remote_commitment_signature: Signature,
    phase: ChannelPhase,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct UnfundedChannel {
    pub remote_peer_id: PeerId,
    pub local_params: LocalChannelParams,
    pub remote_params: RemoteChannelParams,
    pub minimum_depth: u32,
    pub feerate: SatPerKw32,
    pub remote_first_per_commitment_point: PublicKey,
    pub funding: Sat64,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq)]
pub enum ChannelPhase {
    Unconfirmed {
        initial_state: ChannelState,
        minimum_depth: u32,
    },
    RemoteConfirmed {
        initial_state: ChannelState,
        minimum_depth: u32,
        next_per_commitment_point: PublicKey,
    },
    LocalConfirmed {
        initial_state: ChannelState,
    },
    Active {
        current_state: ChannelState,
        commited_state: ChannelState,
    },
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct ChannelConfig {
    pub polarity: ChannelPolarity,
    pub remote_peer_id: PeerId,
    pub local_params: LocalChannelParams,
    pub remote_params: RemoteChannelParams,
    pub funding_outpoint: OutPoint,
    pub capacity: Sat64,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct ChannelState {
    pub remote_per_commitment_point: PublicKey,
    pub feerate: SatPerKw32,
    pub balance: Msat64,
    pub commitment_number: U48,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Balance {
    pub confirmed: Msat64,
    pub unconfirmed: Msat64,
}

impl UnfundedChannel {
    pub fn funding_script_pubkey(&self) -> Script {
        let mut funding_pubkey_0 = self.local_params.funding_pubkey();
        let mut funding_pubkey_1 = self.remote_params.funding_pubkey();
        if funding_pubkey_1 < funding_pubkey_0 {
            mem::swap(&mut funding_pubkey_0, &mut funding_pubkey_1);
        }
        scripts::two_of_two_multisig(funding_pubkey_0, funding_pubkey_1).to_v0_p2wsh()
    }
}

impl ChannelConfig {
    fn commitment_transaction(
        &self,
        to_local_polarity: ChannelPolarity,
        state: &ChannelState,
    ) -> Transaction {
        let self_params = &self.local_params.to_remote_params();
        let peer_params = &self.remote_params;

        let funder_params = match self.polarity {
            ChannelPolarity::Funder => self_params,
            ChannelPolarity::Fundee => peer_params,
        };
        let fundee_params = match self.polarity {
            ChannelPolarity::Funder => peer_params,
            ChannelPolarity::Fundee => self_params,
        };

        let local_params = match to_local_polarity {
            ChannelPolarity::Funder => funder_params,
            ChannelPolarity::Fundee => fundee_params,
        };
        let remote_params = match to_local_polarity {
            ChannelPolarity::Funder => fundee_params,
            ChannelPolarity::Fundee => funder_params,
        };

        let commitment_number = state.commitment_number;
        let commitment_public_key = {
            let per_commitment_point = if self.polarity == to_local_polarity {
                PublicKey::from_secret_key(
                    &self
                        .local_params
                        .channel_secret_key
                        .per_commitment_secret(commitment_number),
                )
            } else {
                state.remote_per_commitment_point
            };
            CommitmentPublicKey::from_channel_keys(
                &local_params.channel_public_key,
                &remote_params.channel_public_key,
                &per_commitment_point,
            )
        };

        let self_balance = state.balance;
        let peer_balance = {
            let capacity = self.capacity.try_to_msat64().unwrap();
            capacity.checked_sub(state.balance).unwrap()
        };

        let funder_balance = match self.polarity {
            ChannelPolarity::Funder => self_balance,
            ChannelPolarity::Fundee => peer_balance,
        };
        let fundee_balance = match self.polarity {
            ChannelPolarity::Funder => peer_balance,
            ChannelPolarity::Fundee => self_balance,
        };

        commitment_transaction_from_commitment_public_key(
            commitment_public_key,
            self.funding_outpoint,
            funder_balance,
            fundee_balance,
            state.feerate,
            commitment_number,
            funder_params,
            fundee_params,
            to_local_polarity,
        )
    }

    pub fn commitment_signature(
        &self,
        polarity: ChannelPolarity,
        state: &ChannelState,
    ) -> Signature {
        let commitment_transaction = self.commitment_transaction(polarity, state);
        commitment_signature_from_commitment_transaction(
            commitment_transaction,
            self.local_params.funding_seckey(),
            self.remote_params.funding_pubkey(),
            self.capacity,
        )
    }

    pub fn verify_commitment_signature(
        &self,
        polarity: ChannelPolarity,
        state: &ChannelState,
        signature: &Signature,
    ) -> Result<(), SignatureError> {
        let unsigned_transaction = self.commitment_transaction(polarity, state);

        let mut pubkey_0 = self.local_params.funding_pubkey();
        let mut pubkey_1 = self.remote_params.funding_pubkey();
        if pubkey_1 < pubkey_0 {
            mem::swap(&mut pubkey_0, &mut pubkey_1);
        }
        let script = scripts::two_of_two_multisig(pubkey_0, pubkey_1);
        unsigned_transaction.verify_p2wsh(
            0,
            self.capacity,
            &script,
            &self.remote_params.funding_pubkey(),
            signature,
        )
    }

    pub fn initial_state(
        &self,
        remote_per_commitment_point: PublicKey,
        feerate: SatPerKw32,
    ) -> ChannelState {
        let balance = match self.polarity {
            ChannelPolarity::Funder => self.capacity.try_to_msat64().unwrap(),
            ChannelPolarity::Fundee => Msat64::from_u64(0),
        };
        ChannelState {
            remote_per_commitment_point,
            feerate,
            balance,
            commitment_number: u48!(0),
        }
    }
}

impl ChannelState {
    pub fn advance(&self, next_per_commitment_point: PublicKey) -> ChannelState {
        ChannelState {
            remote_per_commitment_point: next_per_commitment_point,
            feerate: self.feerate,
            balance: self.balance,
            commitment_number: self.commitment_number + u48!(1),
        }
    }
}

// From BOLT #3:
// > The 48-bit commitment number is obscured by XOR with the lower 48 bits of:
// > SHA256(payment_basepoint from open_channel || payment_basepoint from accept_channel)
fn obscured_commitment_number(
    local_payment_basepoint: PublicKey,
    remote_payment_basepoint: PublicKey,
    commitment_number: U48,
) -> U48 {
    let hash = {
        let mut bytes = [0; 2 * PUBLIC_KEY_BYTES_LEN];
        bytes[..PUBLIC_KEY_BYTES_LEN].clone_from_slice(&local_payment_basepoint.to_bytes());
        bytes[PUBLIC_KEY_BYTES_LEN..].clone_from_slice(&remote_payment_basepoint.to_bytes());
        sha256::Hash::hash(&bytes).into_inner()
    };
    let mut commitment_number_bytes = commitment_number.to_be_bytes();
    let hash_bytes = &hash[(hash.len() - commitment_number_bytes.len())..];
    for i in 0..commitment_number_bytes.len() {
        commitment_number_bytes[i] ^= hash_bytes[i];
    }
    U48::from_be_bytes(commitment_number_bytes)
}

fn commitment_transaction_fee(local_feerate: SatPerKw32) -> Sat64 {
    // As specified in BOLT #3:
    // > * Start with weight = 724.
    // > * For each committed HTLC, if that output is not trimmed as specified in Trimmed
    // >   Outputs, add 172 to weight.
    const MIN_COMMITMENT_WEIGHT: u64 = 724;

    let weight = {
        // TODO include htlc weights
        MIN_COMMITMENT_WEIGHT
    };
    local_feerate.checked_mul_weight(weight).unwrap()
}

#[allow(clippy::too_many_arguments)]
fn commitment_transaction_from_commitment_public_key(
    commitment_public_key: CommitmentPublicKey,
    funding_out_point: OutPoint,
    funder_balance: Msat64,
    fundee_balance: Msat64,
    feerate: SatPerKw32,
    commitment_number: U48,
    funder_params: &RemoteChannelParams,
    fundee_params: &RemoteChannelParams,
    to_local_polarity: ChannelPolarity,
) -> Transaction {
    let fee = commitment_transaction_fee(feerate);

    let (local_balance, _remainder) = match to_local_polarity {
        ChannelPolarity::Funder => funder_balance.to_sat64_remainder(),
        ChannelPolarity::Fundee => fundee_balance.to_sat64_remainder(),
    };
    let (remote_balance, _remainder) = match to_local_polarity {
        ChannelPolarity::Funder => fundee_balance.to_sat64_remainder(),
        ChannelPolarity::Fundee => funder_balance.to_sat64_remainder(),
    };
    let to_local_amount_opt = match to_local_polarity {
        ChannelPolarity::Funder => local_balance.checked_sub(fee),
        ChannelPolarity::Fundee => Some(local_balance),
    };
    let to_remote_amount_opt = match to_local_polarity {
        ChannelPolarity::Funder => Some(remote_balance),
        ChannelPolarity::Fundee => remote_balance.checked_sub(fee),
    };

    let obscured_commitment_number = obscured_commitment_number(
        funder_params.payment_basepoint(),
        fundee_params.payment_basepoint(),
        commitment_number,
    );

    let CommitmentPublicKey {
        remotepubkey,
        local_delayedpubkey,
        revocation_pubkey,
        ..
    } = commitment_public_key;

    let dust_limit = match to_local_polarity {
        ChannelPolarity::Funder => funder_params.dust_limit(),
        ChannelPolarity::Fundee => fundee_params.dust_limit(),
    };
    let delay = match to_local_polarity {
        ChannelPolarity::Funder => fundee_params.to_self_delay(),
        ChannelPolarity::Fundee => funder_params.to_self_delay(),
    };

    let txouts = {
        let mut txouts = TxOuts::empty();
        if let Some(to_local_amount) = to_local_amount_opt {
            if dust_limit < to_local_amount {
                let script =
                    scripts::commitment_to_local(&revocation_pubkey, &local_delayedpubkey, delay);
                let to_local_output = TxOut {
                    value: to_local_amount.to_u64(),
                    script_pubkey: Script::from_script_p2wsh(&script),
                };
                txouts.insert(to_local_output);
            }
        }
        if let Some(to_remote_amount) = to_remote_amount_opt {
            if dust_limit < to_remote_amount {
                let to_remote_output = TxOut {
                    value: to_remote_amount.to_u64(),
                    script_pubkey: Script::from_pubkey_p2wpkh(&remotepubkey),
                };
                txouts.insert(to_remote_output);
            }
        }
        txouts
    };

    let unsigned_txins = {
        let unsigned_txin = {
            // From BOLT #3:
            // > sequence: upper 8 bits are 0x80, lower 24 bits are upper 24 bits of the obscured commitment number
            let sequence = {
                let upper = 0x80 << 24;
                let lower = u32::try_from(obscured_commitment_number >> 24).unwrap();
                upper | lower
            };
            TxIn {
                previous_output: funding_out_point,
                sequence,
                script_sig: Script::new(),
                witness: Vec::new(),
            }
        };
        let mut unsigned_txins = TxIns::empty();
        let _ = unsigned_txins.insert(unsigned_txin);
        unsigned_txins
    };

    // From BOLT #3:
    // > locktime: upper 8 bits are 0x20, lower 24 bits are the lower 24 bits of the obscured commitment number
    let lock_time = {
        let upper = 0x20 << 24;
        let lower = u32::try_from(obscured_commitment_number & u48!((1 << 24) - 1)).unwrap();
        upper | lower
    };
    Transaction {
        version: 2,
        input: unsigned_txins.into_vec(),
        output: txouts.into_vec(),
        lock_time,
    }
}

fn commitment_signature_from_commitment_transaction(
    commitment_transaction: Transaction,
    local_funding_secret_key: &SecretKey,
    remote_funding_public_key: PublicKey,
    capacity: Sat64,
) -> Signature {
    let local_funding_public_key = PublicKey::from_secret_key(&local_funding_secret_key);
    let mut pubkey_0 = local_funding_public_key;
    let mut pubkey_1 = remote_funding_public_key;
    if pubkey_1 < pubkey_0 {
        mem::swap(&mut pubkey_0, &mut pubkey_1);
    }
    let script = scripts::two_of_two_multisig(pubkey_0, pubkey_1);
    commitment_transaction.sign_p2wsh(0, capacity, &script, &local_funding_secret_key)
}

impl Channel {
    pub fn new_unconfirmed(
        config: ChannelConfig,
        initial_state: ChannelState,
        remote_commitment_signature: Signature,
        minimum_depth: u32,
    ) -> Result<Channel, SignatureError> {
        config.verify_commitment_signature(
            config.polarity,
            &initial_state,
            &remote_commitment_signature,
        )?;
        let channel = Channel {
            config,
            remote_commitment_signature,
            phase: ChannelPhase::Unconfirmed {
                initial_state,
                minimum_depth,
            },
        };
        Ok(channel)
    }

    pub fn balance(&self) -> Balance {
        match &self.phase {
            ChannelPhase::Unconfirmed { initial_state, .. }
            | ChannelPhase::RemoteConfirmed { initial_state, .. }
            | ChannelPhase::LocalConfirmed { initial_state } => Balance {
                unconfirmed: initial_state.balance,
                confirmed: Msat64::from_u64(0),
            },
            ChannelPhase::Active {
                current_state,
                commited_state,
            } => Balance {
                unconfirmed: cmp::max(current_state.balance, commited_state.balance),
                confirmed: cmp::min(current_state.balance, commited_state.balance),
            },
        }
    }

    pub fn channel_id(&self) -> ChannelId {
        ChannelId::from_funding_outpoint(self.config.funding_outpoint)
    }

    pub fn remote_peer_id(&self) -> PeerId {
        self.config.remote_peer_id
    }

    pub fn local_confirm(&mut self) {
        // TODO: we should provide proof that the funding transaction has been confirmed in the
        // form of a merkle branch to the transaction and a list of `minimum_depth` subsequent
        // block headers.
        // See: https://gitlab.com/nblockchain/geelightning/issues/3
        match &self.phase {
            ChannelPhase::Unconfirmed {
                initial_state,
                minimum_depth,
            } => {
                let _minimum_depth = minimum_depth;
                self.phase = ChannelPhase::LocalConfirmed {
                    initial_state: initial_state.clone(),
                };
            }
            ChannelPhase::RemoteConfirmed {
                initial_state,
                minimum_depth,
                next_per_commitment_point,
            } => {
                let _minimum_depth = minimum_depth;
                self.phase = ChannelPhase::Active {
                    current_state: initial_state.advance(*next_per_commitment_point),
                    commited_state: initial_state.clone(),
                };
            }
            ChannelPhase::LocalConfirmed { .. } => (),
            ChannelPhase::Active { .. } => (),
        }
    }

    pub fn remote_confirm(&mut self, next_per_commitment_point: PublicKey) {
        match &self.phase {
            ChannelPhase::Unconfirmed {
                initial_state,
                minimum_depth,
            } => {
                self.phase = ChannelPhase::RemoteConfirmed {
                    initial_state: initial_state.clone(),
                    minimum_depth: *minimum_depth,
                    next_per_commitment_point,
                };
            }
            ChannelPhase::RemoteConfirmed { .. } => (),
            ChannelPhase::LocalConfirmed { initial_state } => {
                self.phase = ChannelPhase::Active {
                    current_state: initial_state.advance(next_per_commitment_point),
                    commited_state: initial_state.clone(),
                };
            }
            ChannelPhase::Active { .. } => (),
        }
    }

    pub fn cancelable(&self) -> bool {
        matches!(&self.phase, ChannelPhase::Unconfirmed { .. })
    }

    pub fn next_commitment_point(&self) -> PublicKey {
        let state = match &self.phase {
            ChannelPhase::Unconfirmed { initial_state, .. }
            | ChannelPhase::RemoteConfirmed { initial_state, .. }
            | ChannelPhase::LocalConfirmed { initial_state, .. } => initial_state,

            // NOTE: not currently used, in case there's a need for this to do something different
            // in the future.
            ChannelPhase::Active { commited_state, .. } => commited_state,
        };
        let commitment_number = state.commitment_number;
        let channel_secret_key = &self.config.local_params.channel_secret_key;
        PublicKey::from_secret_key(&channel_secret_key.per_commitment_secret(commitment_number))
    }
}

impl ops::AddAssign<Balance> for Balance {
    fn add_assign(&mut self, other: Balance) {
        self.unconfirmed = self.unconfirmed.checked_add(other.unconfirmed).unwrap();
        self.confirmed = self.confirmed.checked_add(other.confirmed).unwrap();
    }
}

impl iter::Sum<Balance> for Balance {
    fn sum<I>(iter: I) -> Balance
    where
        I: Iterator<Item = Balance>,
    {
        let mut total = Balance {
            unconfirmed: Msat64::from_u64(0),
            confirmed: Msat64::from_u64(0),
        };
        for balance in iter {
            total += balance;
        }
        total
    }
}

#[derive(Debug)]
pub struct ChannelInfo {
    pub capacity: Sat64,
    pub confirmed_balance: Msat64,
    pub unconfirmed_balance: Msat64,
    pub funding_outpoint: OutPoint,
    pub phase: ChannelPhaseInfo,
}

#[derive(Debug)]
pub enum ChannelPhaseInfo {
    Unconfirmed { minimum_depth: u32 },
    LocalConfirmed,
    Active,
}

impl ChannelInfo {
    pub fn from_channel(channel: Channel) -> ChannelInfo {
        let balance = channel.balance();
        ChannelInfo {
            capacity: channel.config.capacity,
            confirmed_balance: balance.confirmed,
            unconfirmed_balance: balance.unconfirmed,
            funding_outpoint: channel.config.funding_outpoint,
            phase: ChannelPhaseInfo::from_channel_phase(channel.phase),
        }
    }
}

impl ChannelPhaseInfo {
    pub fn from_channel_phase(channel_phase: ChannelPhase) -> ChannelPhaseInfo {
        match channel_phase {
            ChannelPhase::Unconfirmed { minimum_depth, .. }
            | ChannelPhase::RemoteConfirmed { minimum_depth, .. } => {
                ChannelPhaseInfo::Unconfirmed { minimum_depth }
            }
            ChannelPhase::LocalConfirmed { .. } => ChannelPhaseInfo::LocalConfirmed,
            ChannelPhase::Active { .. } => ChannelPhaseInfo::Active,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_simple_commitment_tx_with_no_htlcs() {
        let funding_out_point = OutPoint {
            txid: Txid::from(sha256d::Hash::from_inner(hex!(
                "bef67e4e2fb9ddeeb3461973cd4c62abb35050b1add772995b820b584a488489"
            ))),
            vout: 0,
        };
        let commitment_number = u48!(42);
        let local_delay = 144;
        let local_funding_secret_key = SecretKey::try_from_bytes(hex!(
            "30ff4956bbdd3222d44cc5e8a1261dab1e07957bdac5ae88fe3261ef321f3749"
        ))
        .unwrap();
        let remote_funding_public_key = PublicKey::try_from_bytes(hex!(
            "030e9f7b623d2ccc7c9bd44d66d5ce21ce504c0acf6385a132cec6d3c39fa711c1"
        ))
        .unwrap();
        let localpubkey = PublicKey::try_from_bytes(hex!(
            "030d417a46946384f88d5f3337267c5e579765875dc4daca813e21734b140639e7"
        ))
        .unwrap();
        let remotepubkey = PublicKey::try_from_bytes(hex!(
            "0394854aa6eab5b2a8122cc726e9dded053a2184d88256816826d6231c068d4a5b"
        ))
        .unwrap();
        let local_delayedpubkey = PublicKey::try_from_bytes(hex!(
            "03fd5960528dc152014952efdb702a88f71e3c1653b2314431701ec77e57fde83c"
        ))
        .unwrap();
        let revocation_pubkey = PublicKey::try_from_bytes(hex!(
            "0212a140cd0c6539d07cd08dfe09984dec3251ea808b892efeac3ede9402bf2b19"
        ))
        .unwrap();
        let capacity = Sat64::from_u64(10_000_000);
        let feerate = SatPerKw32::from_u32(15000);
        let funder_balance = Msat64::from_u64(7_000_000_000);
        let fundee_balance = Msat64::from_u64(3_000_000_000);
        let to_local_wscript =
            scripts::commitment_to_local(&revocation_pubkey, &local_delayedpubkey, local_delay);
        let expected_to_local_wscript = Script::from(Vec::from(&hex!(
            "63210212a140cd0c6539d07cd08dfe09984dec3251ea808b892efeac3ede9402bf2b1967029000b2752103fd5960528dc152014952efdb702a88f71e3c1653b2314431701ec77e57fde83c68ac"
        )[..]));
        assert_eq!(to_local_wscript, expected_to_local_wscript);
        let commitment_public_key = CommitmentPublicKey {
            localpubkey,
            remotepubkey,
            local_htlcpubkey: PublicKey::from_secret_key(&SecretKey::new()),
            remote_htlcpubkey: PublicKey::from_secret_key(&SecretKey::new()),
            local_delayedpubkey,
            remote_delayedpubkey: PublicKey::from_secret_key(&SecretKey::new()),
            revocation_pubkey,
        };
        let funder_params = RemoteChannelParams {
            params: ChannelParams {
                dust_limit: Sat64::from_u64(546),
                max_htlc_value_in_flight: Msat64::from_u64(0),
                channel_reserve: Sat64::from_u64(0),
                htlc_minimum: Msat64::from_u64(0),
                to_self_delay: 0,
                max_accepted_htlcs: 0,
                shutdown_scriptpubkey: None,
            },
            channel_public_key: ChannelPublicKey {
                funding_pubkey: PublicKey::try_from_bytes(hex!(
                    "023da092f6980e58d2c037173180e9a465476026ee50f96695963e8efe436f54eb"
                ))
                .unwrap(),
                revocation_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
                payment_basepoint: PublicKey::try_from_bytes(hex!(
                    "034f355bdcb7cc0af728ef3cceb9615d90684bb5b2ca5f859ab0f0b704075871aa"
                ))
                .unwrap(),
                delayed_payment_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
                htlc_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
            },
        };
        let fundee_params = RemoteChannelParams {
            params: ChannelParams {
                dust_limit: Sat64::from_u64(0),
                max_htlc_value_in_flight: Msat64::from_u64(0),
                channel_reserve: Sat64::from_u64(0),
                htlc_minimum: Msat64::from_u64(0),
                to_self_delay: local_delay,
                max_accepted_htlcs: 0,
                shutdown_scriptpubkey: None,
            },
            channel_public_key: ChannelPublicKey {
                funding_pubkey: PublicKey::try_from_bytes(hex!(
                    "030e9f7b623d2ccc7c9bd44d66d5ce21ce504c0acf6385a132cec6d3c39fa711c1"
                ))
                .unwrap(),
                revocation_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
                payment_basepoint: PublicKey::try_from_bytes(hex!(
                    "032c0b7cf95324a07d05398b240174dc0c2be444d96b159aa6c7f7b1e668680991"
                ))
                .unwrap(),
                delayed_payment_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
                htlc_basepoint: PublicKey::from_secret_key(&SecretKey::new()),
            },
        };
        let transaction = commitment_transaction_from_commitment_public_key(
            commitment_public_key,
            funding_out_point,
            funder_balance,
            fundee_balance,
            feerate,
            commitment_number,
            &funder_params,
            &fundee_params,
            ChannelPolarity::Funder,
        );
        let local_signature = commitment_signature_from_commitment_transaction(
            transaction,
            &local_funding_secret_key,
            remote_funding_public_key,
            capacity,
        );
        let expected_signature = Signature::deserialize_der(&hex!(
            "3044022051b75c73198c6deee1a875871c3961832909acd297c6b908d59e3319e5185a46022055c419379c5051a78d00dbbce11b5b664a0c22815fbcc6fcef6b1937c3836939"
        )).unwrap();
        assert_eq!(local_signature, expected_signature);
    }
}
