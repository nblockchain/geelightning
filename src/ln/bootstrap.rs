//! This module implements BOLT #10: DNS bootstrapping.
//!
//! The functions herein can be used to bootstrap to the network via DNS seeds. This is needed
//! in order to reconnect to peers who have migrated to a new IP address, or to connect to
//! arbitrary peers in order to obtain general information about the network.

use super::*;
use trust_dns_resolver::{lookup::SrvLookup, Name};

lazy_static! {
    pub static ref DNS_SEEDS: [Name; 2] = [
        Name::from_ascii("lseed.bitcoinstats.com").unwrap(),
        Name::from_ascii("nodes.lightning.directory").unwrap(),
    ];
}

const MAX_PEER_LOOKUP_ERRORS_PER_DNS_SEED_LOOKUP: usize = 10;
const MAX_SEED_LOOKUP_ERRORS: usize = 10;
const DNS_ERROR_INITIAL_BACKOFF: Duration = Duration::from_millis(100);

async fn collate_errors<K, T, E>(
    error_buffer_size: usize,
    stream: impl Stream<Item = (K, Result<T, E>)> + Unpin,
) -> Result<impl Stream<Item = T>, HashMap<K, E>>
where
    K: Eq + std::hash::Hash,
{
    let mut stream = stream;
    let mut errors = VecDeque::new();
    while let Some((key, result)) = stream.next().await {
        match result {
            Err(err) => {
                errors.push_back((key, err));
                if errors.len() > error_buffer_size {
                    let _drop = errors.pop_front();
                }
            }
            Ok(first) => {
                let stream = async_stream::stream! {
                    yield first;

                    for await (_key, result) in stream {
                        match result {
                            Ok(value) => yield value,
                            Err(_) => (),
                        }
                    }
                };
                return Ok(stream);
            }
        }
    }
    let errors = errors.into_iter().collect();
    Err(errors)
}

#[derive(Debug, Error)]
pub enum ParsePeerDomainNameError {
    #[error("empty name")]
    EmptyName,
    #[error("non utf8 domain name: {}", source)]
    InvalidUtf8 { source: Utf8Error },
    #[error("malformed bech32 peer id: {}", source)]
    InvalidBech32PeerId { source: PeerIdFromBech32Error },
}

fn parse_peer_domain_name(name: &Name) -> Result<PeerId, ParsePeerDomainNameError> {
    let peer_id_bech32_bytes = match name.iter().next() {
        Some(peer_id_bech32_bytes) => peer_id_bech32_bytes,
        None => return Err(ParsePeerDomainNameError::EmptyName),
    };
    let peer_id_bech32_str = {
        str::from_utf8(peer_id_bech32_bytes)
            .map_err(|source| ParsePeerDomainNameError::InvalidUtf8 { source })?
    };
    let peer_id = {
        PeerId::from_bech32(peer_id_bech32_str)
            .map_err(|source| ParsePeerDomainNameError::InvalidBech32PeerId { source })?
    };
    Ok(peer_id)
}

#[derive(Debug, Error)]
pub enum LookupPeerError {
    #[error("malformed peer domain name: {}", source)]
    MalformedPeerDomainName { source: ParsePeerDomainNameError },
    #[error("ip look failed: {}", source)]
    IpLookup { source: Box<ResolveError> },
}

async fn lookup_peer(
    resolver: &TokioAsyncResolver,
    name: Name,
) -> Result<impl Stream<Item = (PeerId, IpAddr)>, LookupPeerError> {
    let peer_id = {
        parse_peer_domain_name(&name)
            .map_err(|source| LookupPeerError::MalformedPeerDomainName { source })?
    };
    let ip_addrs = {
        resolver
            .lookup_ip(name)
            .await
            .map_err(|source| LookupPeerError::IpLookup {
                source: Box::new(source),
            })?
    };
    let stream = async_stream::stream! {
        for ip_addr in ip_addrs {
            yield (peer_id, ip_addr);
        }
    };
    Ok(stream)
}

fn order_srv_results(srv_lookup: SrvLookup) -> Vec<(Name, u16)> {
    let mut rng = rand::thread_rng();
    let mut results = BTreeMap::new();
    let mut num_results = 0;
    for result in srv_lookup.iter() {
        num_results += 1;
        let result_vec: &mut Vec<_> = results.entry(result.priority()).or_default();
        result_vec.push(result);
    }

    let mut ordered_results = Vec::with_capacity(num_results);
    for (_, mut result_vec) in results {
        result_vec.sort_unstable_by_key(|result| result.weight());

        while !result_vec.is_empty() {
            let total_weight: u64 = result_vec
                .iter()
                .map(|result| u64::from(result.weight()))
                .sum();
            let target_weight = rng.gen_range(0, total_weight + 1);
            let mut sum_weight = 0;
            for i in 0..result_vec.len() {
                sum_weight += u64::from(result_vec[i].weight());
                if target_weight <= sum_weight {
                    let result = result_vec.remove(i);
                    ordered_results.push((result.target().clone(), result.port()));
                    break;
                }
            }
        }
    }
    ordered_results
}

#[derive(Debug, Error)]
pub enum LookupDnsSeedError {
    #[error("srv lookup failed: {}", source)]
    SrvLookup { source: ResolveError },
    #[error("all peer lookups failed: {}", ErrorMap(&sources))]
    AllPeerLookupsFailed {
        sources: HashMap<Name, LookupPeerError>,
    },
}

async fn lookup_dns_seed<'r>(
    resolver: &'r TokioAsyncResolver,
    seed: &Name,
) -> Result<impl Stream<Item = Endpoint> + 'r, LookupDnsSeedError> {
    let srv_lookup = {
        resolver
            .srv_lookup(seed.clone())
            .await
            .map_err(|source| LookupDnsSeedError::SrvLookup { source })?
    };
    let ordered_results = order_srv_results(srv_lookup);
    let result_stream = async_stream::stream! {
        for (name, port) in ordered_results {
            let peer_addrs = match lookup_peer(resolver, name.clone()).await {
                Ok(peer_addrs) => peer_addrs,
                Err(err) => {
                    yield (name, Err(err));
                    continue;
                },
            };
            for await (peer_id, ip_addr) in peer_addrs {
                let endpoint = Endpoint {
                    peer_id,
                    addr: SocketAddr::new(ip_addr, port),
                };
                yield (name.clone(), Ok(endpoint));
            }
        }
    };
    let stream = {
        collate_errors(
            MAX_PEER_LOOKUP_ERRORS_PER_DNS_SEED_LOOKUP,
            Box::pin(result_stream),
        )
        .await
        .map_err(|sources| LookupDnsSeedError::AllPeerLookupsFailed { sources })?
    };
    Ok(stream)
}

#[derive(Debug, Error)]
#[error("all dns seed lookups failed: {}", ErrorMap(sources))]
pub struct LookupDnsSeedsError {
    sources: HashMap<Name, LookupDnsSeedError>,
}

async fn lookup_dns_seeds<'r, 's>(
    resolver: &'r TokioAsyncResolver,
    seeds: &'r [Name],
) -> Result<impl Stream<Item = Endpoint> + 'r, LookupDnsSeedsError> {
    let result_stream = async_stream::stream! {
        for seed in seeds {
            let endpoints = match lookup_dns_seed(resolver, seed).await {
                Ok(endpoints) => endpoints,
                Err(err) => {
                    yield (seed.clone(), Err(err));
                    continue;
                },
            };
            for await endpoint in endpoints {
                yield (seed.clone(), Ok(endpoint));
            }
        }
    };
    let stream = {
        collate_errors(MAX_SEED_LOOKUP_ERRORS, Box::pin(result_stream))
            .await
            .map_err(|sources| LookupDnsSeedsError { sources })?
    };
    Ok(stream)
}

pub async fn repeat_with_backoff<T, E, M, F>(
    initial_backoff: Duration,
    future: M,
) -> impl Stream<Item = Result<T, E>>
where
    M: FnMut() -> F,
    F: Future<Output = Result<T, E>>,
{
    async_stream::stream! {
        let mut future = future;
        let mut backoff = initial_backoff;
        loop {
            match future().await {
                Ok(value) => {
                    yield Ok(value);
                    backoff = initial_backoff;
                },
                Err(err) => {
                    yield Err(err);
                    sleep(backoff).await;
                    backoff *= 2;
                },
            }
        }
    }
}

pub async fn lookup_all_dns_seeds_forever<'r>(
    resolver: &'r TokioAsyncResolver,
) -> impl Stream<Item = Result<Endpoint, LookupDnsSeedsError>> + 'r {
    async_stream::stream! {
        let results = repeat_with_backoff(
            DNS_ERROR_INITIAL_BACKOFF,
            || lookup_dns_seeds(resolver, &*DNS_SEEDS)
        ).await;
        for await result in results {
            match result {
                Err(err) => {
                    yield Err(err);
                },
                Ok(endpoints) => {
                    for await endpoint in endpoints {
                        yield Ok(endpoint);
                    }
                },
            }
        }
    }
}

// NOTE: This code is not currently being used and it looked like a chore to update when updating
// rust. So I've left it commented out for now. If we decide to use it later we can un-comment it
// and update it.
/*
/// Bootstrap to the network.
///
/// Given a `TransportConnector` to make connections from, this function will bootstrap to the
/// lightning network via DNS seeds, returning a `Stream` of connections to arbitrary peers on the
/// network. These peers can then be used to gather information about the network, such as the
/// current network addresses of peers with which we have channels, or general information about
/// the network topology.
pub fn bootstrap(
    transport_connector: TransportConnector,
) -> impl Stream<Item = Result<Transport, DnsBootstrapError>> {
    let endpoints = Box::pin(bootstrap_lookup().fuse());
    stream::unfold(
        (endpoints, FuturesUnordered::new(), Instant::now()),
        move |(mut endpoints, mut pending_transports, mut next_connect_time)| {
            let transport_connector = transport_connector.clone();
            async move {
                const BOOTSTRAP_DELAY: Duration = Duration::from_secs(1);
                loop {
                    let next_connect_future = delay_until(next_connect_time).fuse();
                    pin_mut!(next_connect_future);
                    futures::select! {
                        transport_opt = pending_transports.select_next_some() => {
                            let num_pending_transports = pending_transports.len() as u32;
                            let delay = BOOTSTRAP_DELAY * num_pending_transports;
                            next_connect_time = Instant::now() + delay;
                            if let Some(transport) = transport_opt {
                                return Some((Ok(transport), (endpoints, pending_transports, next_connect_time)));
                            }
                        },
                        () = next_connect_future => {
                            let endpoint_res = endpoints.next().await.unwrap();
                            let endpoint = match endpoint_res {
                                Ok(endpoint) => endpoint,
                                Err(err) => {
                                    let num_pending_transports = pending_transports.len() as u32;
                                    let delay = BOOTSTRAP_DELAY * num_pending_transports;
                                    next_connect_time = Instant::now() + delay;
                                    return Some((Err(err), (endpoints, pending_transports, next_connect_time)));
                                },
                            };
                            let transport_connector = transport_connector.clone();
                            pending_transports.push(async move {
                                transport_connector.connect(&endpoint).await.ok()
                            });
                            let num_pending_transports = pending_transports.len() as u32;
                            let delay = BOOTSTRAP_DELAY * num_pending_transports;
                            next_connect_time = Instant::now() + delay;
                        },
                    }
                }
            }
        },
    )
}

#[derive(Debug, Fail)]
pub enum DnsBootstrapError {
    //#[fail(display = "error initiating DNS resolver: {:?}", _0)]
    InitiateResolver(Mutex<ResolveError>),
    //#[fail(display = "all DNS seed lookups failed with errors: {:?}", _0)]
    AllSeedLookupsFailed(Mutex<HashMap<String, ResolveError>>),
    InvalidResponse {
        seed: Name,
        response: Name,
    },
}

impl fmt::Display for DnsBootstrapError {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DnsBootstrapError::InitiateResolver(err) => {
                let err = err.lock().unwrap();
                write!(fmt, "failed to initiate resolver: {}", err)
            },
            DnsBootstrapError::AllSeedLookupsFailed(err) => {
                write!(fmt, "all seed lookups failed:")?;
                let err_map = err.lock().unwrap();
                for (seed, err) in err_map.iter() {
                    write!(fmt, "\n    {}: {};", seed, err)?;
                }
                Ok(())
            },
        }
    }
}

/// Lookup a peer's address via DNS bootstrap seeds.
///
/// This function takes a `PeerId` and returns a `Stream` of network addresses that may be usable
/// to contact the peer. This is necessary, for instance, if we need to reconnect to a peer who has
/// changed their IP address.
pub fn lookup_peer_address(
    peer_id: PeerId,
) -> impl Stream<Item = Result<SocketAddr, LookupPeerAddressError>> {
    let (resolver, resolver_task) = TokioAsyncResolver::tokio(Default::default(), Default::default());
    spawn!(resolver_task);
    let resolver_arc = Arc::new(resolver);

    stream::iter(DNS_SEEDS.iter().cloned())
        .map({
            let resolver_arc = resolver_arc.clone();
            move |seed| {
                let name = format!("{}.{}", peer_id.as_bech32(), seed);
                let name = Name::from_str(&name).unwrap();
                resolver_arc
                    .lookup_srv(name.clone())
                    .map_err(|err| LookupPeerAddressError::ResolveDnsSeed { name, err })
            }
        })
        .buffer_unordered(DNS_SEEDS.len())
        .map_ok(|srv_lookup| {
            let ordered_results = order_srv_results(srv_lookup);
            stream::iter(ordered_results).map(Ok)
        })
        .try_flatten()
        .map_ok(move |(name, port)| {
            resolver_arc
                .lookup_ip(name.clone())
                .map_err(|err| LookupPeerAddressError::ResolvePeerAddress { name, err })
                .map_ok(|ip_lookup| stream::iter(ip_lookup.iter().collect::<Vec<_>>()).map(Ok))
                .try_flatten_stream()
                .map_ok(move |ip| SocketAddr::new(ip, port))
        })
        .try_flatten()
}
*/

#[derive(Debug, Error)]
pub enum LookupPeerAddressError {
    #[error("failed to lookup peer using DNS seed {}: {}", name, source)]
    ResolveDnsSeed {
        name: Name,
        source: Box<ResolveError>,
    },
    #[error("failed to lookup peer address {}: {}", name, source)]
    ResolvePeerAddress {
        name: Name,
        source: Box<ResolveError>,
    },
}

/*
#[cfg(test)]
mod test {
    use super::*;
    #[tokio::test]
    async fn bootstrap_to_network() {
        let (transport_connector, _transport_listener, _endpoint) =
            TransportListener::bind(&LocalEndpoint {
                peer_secret_key: PeerSecretKey::from_secret_key(SecretKey::new()),
                addr: addr!("0.0.0.0:0"),
            })
            .await
        .unwrap();

        let transports = bootstrap(transport_connector);
        pin_mut!(transports);
        let transport_res = transports.next().await.unwrap();
        let transport = transport_res.unwrap();
        println!("got peer: {:?}", transport);
    }
}
*/
