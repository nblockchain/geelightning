use super::*;

pub async fn simulate_initiator_for_responder_handshake<'a, F, F1, F2, F3, F4>(
    accept_handshake: F1,
    write_to_responder1: F2,
    read_from_responder1: F3,
    write_to_responder2: F4,
) -> Result<(PeerId, HandshakeKey, HandshakeKey), HandshakeError>
where
    F: Future<Output = Result<(PeerId, HandshakeKey, HandshakeKey), HandshakeError>>,
    F1: Fn(TcpStream) -> F,
    F2: Fn() -> &'a [u8],
    F3: Fn(&[u8]),
    F4: Fn() -> &'a [u8],
{
    let listener = TcpListener::bind(&addr!("0.0.0.0:0")).await.unwrap();
    let listener_addr = listener.local_addr().unwrap();

    let initiator_closure = async {
        let mut stream = TcpStream::connect(&listener_addr).await.unwrap();
        if stream.write_all(write_to_responder1()).await.is_err() {
            return;
        }
        let mut buffer = [0; 50];
        if stream.read_exact(&mut buffer).await.is_err() {
            return;
        }
        read_from_responder1(&buffer);
        let _ignore_error = stream.write_all(write_to_responder2()).await;
    };

    let responder_closure = async {
        let mut incoming = TcpListenerStream::new(listener);
        let stream_opt = incoming.next().await.unwrap();
        let stream = stream_opt.unwrap();
        accept_handshake(stream).await
    };

    let (_, responder) = futures::join!(initiator_closure, responder_closure);
    responder
}

pub async fn simulate_responder_for_initiator_handshake<'a, F, F1, F2, F3, F4>(
    initiate_handshake: F1,
    read_from_initiator1: F2,
    write_to_initiator1: F3,
    read_from_initiator2: F4,
) -> Result<(HandshakeKey, HandshakeKey), HandshakeError>
where
    F: Future<Output = Result<(HandshakeKey, HandshakeKey), HandshakeError>>,
    F1: Fn(TcpStream) -> F,
    F2: Fn(&[u8]),
    F3: Fn() -> &'a [u8],
    F4: Fn(&[u8]),
{
    let listener = TcpListener::bind(&addr!("0.0.0.0:0")).await.unwrap();
    let listener_addr = listener.local_addr().unwrap();

    let initiator_closure = async {
        let stream = TcpStream::connect(&listener_addr).await.unwrap();
        initiate_handshake(stream).await
    };

    let responder_closure = async {
        let mut incoming = TcpListenerStream::new(listener);
        let stream_opt = incoming.next().await.unwrap();
        let mut stream = stream_opt.unwrap();

        let mut buffer = [0u8; 50];
        if stream.read_exact(&mut buffer).await.is_err() {
            return;
        }
        read_from_initiator1(&buffer);

        if stream.write_all(write_to_initiator1()).await.is_err() {
            return;
        }

        let mut buffer = [0; 66];
        if stream.read_exact(&mut buffer).await.is_err() {
            return;
        }
        read_from_initiator2(&buffer);
    };

    let (initiator, _) = futures::join!(initiator_closure, responder_closure);
    initiator
}
