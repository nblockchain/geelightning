use super::*;
use rand::distributions::Distribution;
use statrs::distribution::{Exponential, Normal};

/// Generate a `Duration` from the exponential distribution with mean `mean`.
pub fn exponentially_random_duration(mean: Duration) -> Duration {
    let lambda = Duration::from_secs(1).div_duration_f64_(mean);
    let secs = Exponential::new(lambda)
        .unwrap()
        .sample(&mut rand::thread_rng());
    Duration::from_secs(1).mul_f64(secs)
}

/// Generate a `Duration` from the normal distribution with mean `mean` and standard deviation
/// `std_dev`. Since `Duration`s are unsigned, the returned bool represents the sign of the
/// returned `Duration`.
pub fn normal_random_duration(mean: Duration, std_dev: Duration) -> (bool, Duration) {
    let mean_secs = mean.div_duration_f64_(Duration::from_secs(1));
    let std_dev_secs = std_dev.div_duration_f64_(Duration::from_secs(1));
    let secs = Normal::new(mean_secs, std_dev_secs)
        .unwrap()
        .sample(&mut rand::thread_rng());
    if secs < 0.0 {
        (true, Duration::from_secs(1).mul_f64(-secs))
    } else {
        (false, Duration::from_secs(1).mul_f64(secs))
    }
}
