use super::*;

#[macro_export]
macro_rules! assert_matches {
    ($a:expr, $b:pat) => {
        assert!(matches!($a, $b))
    };
}

pub fn random_vec(len: usize) -> Vec<u8> {
    let mut rng = rand::rngs::SmallRng::from_rng(&mut rand::thread_rng()).unwrap();
    let mut ret = vec![0; len];
    rng.fill(&mut ret[..]);
    ret
}
