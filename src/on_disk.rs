use super::*;
use notify::{EventKind, Watcher};

const MAGIC_BYTES_LEN: usize = 2;
const MAGIC_BYTES: [u8; MAGIC_BYTES_LEN] = *b"OD";
const FORMAT_VERSION: u16 = 1;
const FORMAT_VERSION_LEN: usize = mem::size_of::<u16>();
const NONCE_LEN: usize = 12;
const TAG_LEN: usize = 16;

pub struct OnDisk<T> {
    inner: Arc<AsyncMutex<OnDiskInner<T>>>,
}

impl<T> Clone for OnDisk<T> {
    fn clone(&self) -> OnDisk<T> {
        let inner = self.inner.clone();
        OnDisk { inner }
    }
}

pub type OnDiskUpdate<T> = Result<Arc<T>, Arc<ReadFromDiskError>>;

struct OnDiskInner<T> {
    path: PathBuf,
    secret_key: SecretKey,
    value_send_send: mpsc::Sender<(mpsc::Sender<OnDiskUpdate<T>>, oneshot::Sender<()>)>,
    watcher: notify::RecommendedWatcher,
}

#[derive(Debug, Error)]
pub enum OnDiskNewError {
    #[error("error creating file system watcher: {}", source)]
    CreateNotifier { source: notify::Error },
    #[error("error watching file: {}", source)]
    WatchFile { source: notify::Error },
    #[error("error reading current state: {}", source)]
    ReadFromDisk { source: ReadFromDiskError },
    #[error("error writing initial contents: {}", source)]
    WriteToDisk { source: WriteToDiskError },
}

#[derive(Debug, Error)]
pub enum ReadFromDiskError {
    #[error("error reading file from disk: {}", source)]
    ReadFile { source: io::Error },
    #[error("file on disk is corrupt: {}", source)]
    FileCorrupt { source: CorruptFileError },
    #[error("unknown file version ({})", version)]
    UnknownFileVersion { version: u16 },
}

#[derive(Debug, Error)]
pub enum CorruptFileError {
    #[error("invalid file length ({})", length)]
    InvalidLength { length: usize },
    #[error("invalid magic bytes")]
    InvalidMagicBytes,
    #[error("decryption error: {}", source)]
    Decrypt {
        source: chacha20_poly1305_aead::DecryptError,
    },
    #[error("deserialization error: {}", source)]
    Deserialize { source: bincode::Error },
}

#[derive(Debug, Error)]
pub enum WriteToDiskError {
    #[error("error creating directory: {}", source)]
    CreateDirectory { source: io::Error },
    #[error("error writing file to disk: {}", source)]
    WriteFile { source: io::Error },
    #[error("error overwriting file: {}", source)]
    OverwriteFile { source: io::Error },
    #[error("error watching file: {}", source)]
    WatchFile { source: notify::Error },
}

#[derive(Debug, Error)]
pub enum OnDiskMutateError {
    #[error("error reading current state: {}", source)]
    ReadFromDisk { source: ReadFromDiskError },
    #[error("error writing mutated contents: {}", source)]
    WriteToDisk { source: WriteToDiskError },
}

impl<T> OnDisk<T>
where
    T: Serialize + DeserializeOwned + Default + Send + Sync + PartialEq<T> + 'static,
{
    pub async fn new_data<P: AsRef<Path>>(
        secret_key: &SecretKey,
        path: P,
    ) -> Result<OnDisk<T>, OnDiskNewError> {
        let path = PathBuf::from(path.as_ref());
        OnDisk::new(secret_key, path).await
    }

    async fn new(secret_key: &SecretKey, path: PathBuf) -> Result<OnDisk<T>, OnDiskNewError> {
        let (sync_event_send, sync_event_recv): (
            crossbeam_channel::Sender<Result<notify::Event, notify::Error>>,
            _,
        ) = crossbeam_channel::unbounded();
        let mut watcher: notify::RecommendedWatcher =
            Watcher::new_immediate(move |event| sync_event_send.send(event).ignore_err())
                .map_err(|source| OnDiskNewError::CreateNotifier { source })?;
        let value = OnDisk::read_contents(&path, secret_key)
            .await
            .map_err(|source| OnDiskNewError::ReadFromDisk { source })?;
        OnDisk::write_contents(&path, secret_key, &mut watcher, &value)
            .await
            .map_err(|source| OnDiskNewError::WriteToDisk { source })?;

        let secret_key = secret_key.clone();
        let (event_send, event_recv) = tokio::sync::mpsc::unbounded_channel();
        let event_recv = tokio_stream::wrappers::UnboundedReceiverStream::new(event_recv);
        thread::spawn(move || {
            for event_res in sync_event_recv {
                match event_send.send(event_res) {
                    Ok(()) => (),
                    Err(..) => break,
                }
            }
        });
        let (value_send_send, mut value_send_recv) = mpsc_channel!();
        let value_send_send: mpsc::Sender<(mpsc::Sender<OnDiskUpdate<T>>, oneshot::Sender<()>)> =
            value_send_send;
        spawn!({
            let path = path.clone();
            let secret_key = secret_key.clone();
            let mut value = Ok(Arc::new(value));
            async move {
                let mut value_senders = Vec::new();
                let mut event_recv = event_recv.fuse();
                loop {
                    futures::select! {
                        (mut value_send, reply_send) = value_send_recv.select_next_some() => {
                            reply_send.send(()).await.ignore_err();
                            if value_send.send(value.clone()).await.is_ok() {
                                value_senders.push(value_send);
                            }
                            value_senders.retain(|value_sender| {
                                !value_sender.is_closed()
                            });
                        },
                        event_res_opt = event_recv.next() => {
                            let event_res = match event_res_opt {
                                Some(event_res) => event_res,
                                None => break,
                            };
                            let event = match event_res {
                                Ok(event) => event,
                                Err(..) => continue,    // TODO: how to handle this error?
                            };
                            match event.kind {
                                EventKind::Modify(..) | EventKind::Create(..) | EventKind::Remove(..) => (),
                                _ => continue,
                            }
                            if !event.paths.contains(&path) {
                                continue;
                            }
                            let new_value = OnDisk::read_contents(&path, &secret_key).await;
                            match (&value, &new_value) {
                                (Ok(value), Ok(new_value)) if **value == *new_value => continue,
                                (Err(..), Err(..)) => continue,
                                _ => (),
                            }
                            value = match new_value {
                                Ok(value) => Ok(Arc::new(value)),
                                Err(err) => Err(Arc::new(err)),
                            };

                            let mut i = 0;
                            while i < value_senders.len() {
                                if !value_senders[i].send(value.clone()).await.is_ok() {
                                    let _ = value_senders.swap_remove(i);
                                    continue;
                                }
                                i += 1;
                            }
                        },
                    }
                }
            }
        });

        let inner = Arc::new(AsyncMutex::new(OnDiskInner {
            path,
            secret_key,
            value_send_send,
            watcher,
        }));
        Ok(OnDisk { inner })
    }

    pub fn watch(&self) -> impl Stream<Item = OnDiskUpdate<T>> {
        let (value_send, value_recv) = mpsc_channel!();
        let (reply_send, reply_recv) = oneshot_channel!();

        let inner = self.inner.clone();
        async move {
            let mut inner = inner.lock().await;
            inner
                .value_send_send
                .send((value_send, reply_send))
                .await
                .unwrap();
            drop(inner);

            reply_recv.await.unwrap();
            value_recv
        }
        .flatten_stream()
    }

    async fn read_contents(path: &Path, secret_key: &SecretKey) -> Result<T, ReadFromDiskError> {
        let file_contents = match tokio::fs::read(path).await {
            Ok(file_contents) => file_contents,
            Err(err) if err.kind() == io::ErrorKind::NotFound => {
                return Ok(T::default());
            }
            Err(source) => return Err(ReadFromDiskError::ReadFile { source }),
        };
        let metadata_len = MAGIC_BYTES_LEN + FORMAT_VERSION_LEN + NONCE_LEN + TAG_LEN;
        if file_contents.len() < metadata_len {
            return Err(ReadFromDiskError::FileCorrupt {
                source: CorruptFileError::InvalidLength {
                    length: file_contents.len(),
                },
            });
        }
        let encrypted_len = file_contents.len() - metadata_len;

        let magic_bytes: [u8; MAGIC_BYTES_LEN] =
            file_contents[..MAGIC_BYTES_LEN].try_into().unwrap();
        if magic_bytes != MAGIC_BYTES {
            return Err(ReadFromDiskError::FileCorrupt {
                source: CorruptFileError::InvalidMagicBytes,
            });
        }
        let file_contents = &file_contents[MAGIC_BYTES_LEN..];

        let version_bytes = file_contents[..FORMAT_VERSION_LEN].try_into().unwrap();
        let version = u16::from_be_bytes(version_bytes);
        if version != FORMAT_VERSION {
            return Err(ReadFromDiskError::UnknownFileVersion { version });
        }
        let file_contents = &file_contents[FORMAT_VERSION_LEN..];

        let nonce: [u8; NONCE_LEN] = file_contents[..NONCE_LEN].try_into().unwrap();
        let file_contents = &file_contents[NONCE_LEN..];

        let encrypted = &file_contents[..encrypted_len];
        let file_contents = &file_contents[encrypted_len..];

        let tag = file_contents;
        let mut decrypted = Vec::with_capacity(encrypted_len);
        match chacha20_poly1305_aead::decrypt(
            &secret_key.to_bytes(),
            &nonce,
            &[],
            encrypted,
            tag,
            &mut decrypted,
        ) {
            Ok(()) => (),
            Err(source) => {
                return Err(ReadFromDiskError::FileCorrupt {
                    source: CorruptFileError::Decrypt { source },
                })
            }
        };
        let value = match bincode::deserialize(&decrypted) {
            Ok(value) => value,
            Err(source) => {
                return Err(ReadFromDiskError::FileCorrupt {
                    source: CorruptFileError::Deserialize { source },
                })
            }
        };
        Ok(value)
    }

    async fn write_contents(
        path: &Path,
        secret_key: &SecretKey,
        watcher: &mut notify::RecommendedWatcher,
        value: &T,
    ) -> Result<(), WriteToDiskError> {
        let serialized = bincode::serialize(&value).unwrap();
        let encrypted = {
            let metadata_len = MAGIC_BYTES_LEN + FORMAT_VERSION_LEN + NONCE_LEN + TAG_LEN;
            let mut encrypted = Vec::with_capacity(serialized.len() + metadata_len);

            encrypted.extend(&MAGIC_BYTES);
            encrypted.extend(&FORMAT_VERSION.to_be_bytes());
            let nonce: [u8; NONCE_LEN] = rand::random();
            encrypted.extend(&nonce);
            let tag = chacha20_poly1305_aead::encrypt(
                &secret_key.to_bytes(),
                &nonce,
                &[],
                &serialized,
                &mut encrypted,
            )
            .unwrap();
            encrypted.extend(&tag);
            encrypted
        };
        let temp_path = {
            let mut path_str = path.to_owned().into_os_string();
            path_str.push(".new");
            PathBuf::from(path_str)
        };
        tokio::fs::create_dir_all(path.parent().unwrap())
            .await
            .map_err(|source| WriteToDiskError::CreateDirectory { source })?;
        tokio::fs::write(temp_path.clone(), &encrypted)
            .await
            .map_err(|source| WriteToDiskError::WriteFile { source })?;
        tokio::fs::rename(temp_path, &path)
            .await
            .map_err(|source| WriteToDiskError::OverwriteFile { source })?;
        watcher
            .watch(&path, notify::RecursiveMode::NonRecursive)
            .map_err(|source| WriteToDiskError::WatchFile { source })?;
        Ok(())
    }

    pub async fn read(&self) -> Result<T, ReadFromDiskError> {
        let mut inner = self.inner.lock().await;
        let inner = &mut *inner;
        OnDisk::read_contents(&inner.path, &inner.secret_key).await
    }

    pub async fn mutate<R>(&self, f: impl FnOnce(&mut T) -> R) -> Result<R, OnDiskMutateError> {
        let mut inner = self.inner.lock().await;
        let inner = &mut *inner;
        let mut value = OnDisk::read_contents(&inner.path, &inner.secret_key)
            .await
            .map_err(|source| OnDiskMutateError::ReadFromDisk { source })?;
        let ret = f(&mut value);
        OnDisk::write_contents(&inner.path, &inner.secret_key, &mut inner.watcher, &value)
            .map_err(|source| OnDiskMutateError::WriteToDisk { source })
            .await?;
        Ok(ret)
    }
}
