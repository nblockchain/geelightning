use super::*;

#[ext_trait]
impl NetworkExt for Network {
    fn chain_hash(&self) -> BlockHash {
        blockdata::constants::genesis_block(*self)
            .header
            .block_hash()
    }
}

#[ext_trait]
impl Sha256dHashExt for sha256d::Hash {
    const BYTES_LEN: usize = 32;

    fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(self.as_ref());
    }

    fn ln_deserialize(
        deserializer: &mut LnDeserializer,
    ) -> Result<sha256d::Hash, MsgTooShortError> {
        Ok(sha256d::Hash::from_inner(deserializer.read_array()?))
    }
}

#[ext_trait]
impl TransactionExt for Transaction {
    fn sighash_p2wsh(&self, input_index: usize, amount: Sat64, script_code: &Script) -> SigHash {
        let mut sig_hash_cache = SigHashCache::new(self);
        sig_hash_cache.signature_hash(input_index, script_code, amount.to_u64(), SigHashType::All)
    }

    fn sign_p2wsh(
        &self,
        input_index: usize,
        amount: Sat64,
        script_code: &Script,
        secret_key: &SecretKey,
    ) -> Signature {
        let hash = self.sighash_p2wsh(input_index, amount, script_code);
        secret_key.sign_digest(hash)
    }

    fn verify_p2wsh(
        &self,
        input_index: usize,
        amount: Sat64,
        script_code: &Script,
        public_key: &PublicKey,
        signature: &Signature,
    ) -> Result<(), SignatureError> {
        let hash = self.sighash_p2wsh(input_index, amount, script_code);
        public_key.verify_signature(hash, signature)
    }
}

#[ext_trait]
impl ScriptExt for Script {
    fn from_bytes(bytes: &[u8]) -> Script {
        Script::from(Vec::from(bytes))
    }

    fn from_script_p2wsh(script: &Script) -> Script {
        let witness_program = WScriptHash::from(sha256::Hash::hash(script.as_bytes()));
        scripts::p2wsh(witness_program)
    }

    fn from_pubkey_p2wpkh(public_key: &PublicKey) -> Script {
        let witness_program = WPubkeyHash::from(hash160::Hash::hash(&public_key.to_bytes()));
        scripts::p2wpkh(witness_program)
    }
}

#[ext_trait]
impl TxOutExt for TxOut {
    /// Order `TxOut`s lexiographically, as per BIP69
    fn cmp_lexicographic(&self, other: &TxOut) -> cmp::Ordering {
        match self.value.cmp(&other.value) {
            cmp::Ordering::Equal => self
                .script_pubkey
                .as_bytes()
                .cmp(other.script_pubkey.as_bytes()),
            ordering => ordering,
        }
    }
}

#[ext_trait]
impl TxInExt for TxIn {
    /// Order `TxIn`s lexiographically, as per BIP69
    fn cmp_lexicographic(&self, other: &TxIn) -> cmp::Ordering {
        match self.previous_output.txid.cmp(&other.previous_output.txid) {
            cmp::Ordering::Equal => self.previous_output.vout.cmp(&other.previous_output.vout),
            ordering => ordering,
        }
    }
}

#[ext_trait]
impl BlockHashExt for BlockHash {
    fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(self.as_ref());
    }

    fn ln_deserialize(deserializer: &mut LnDeserializer) -> Result<BlockHash, MsgTooShortError> {
        Ok(BlockHash::from(sha256d::Hash::from_inner(
            deserializer.read_array()?,
        )))
    }
}

#[ext_trait]
impl TxidExt for Txid {
    fn ln_serialize(&self, serializer: &mut LnSerializer) {
        serializer.write_slice(self.as_ref());
    }

    fn ln_deserialize(deserializer: &mut LnDeserializer) -> Result<Txid, MsgTooShortError> {
        Ok(Txid::from(sha256d::Hash::from_inner(
            deserializer.read_array()?,
        )))
    }

    #[cfg(test)]
    fn random() -> Txid {
        let bytes: [u8; 32] = rand::random();
        let hash = sha256d::Hash::from_inner(bytes);
        Txid::from(hash)
    }
}

#[ext_trait]
impl OutPointExt for OutPoint {
    #[cfg(test)]
    fn random() -> OutPoint {
        OutPoint {
            txid: Txid::random(),
            vout: {
                let mut vout = 0;
                loop {
                    if rand::random() {
                        vout += 1;
                    } else {
                        break vout;
                    }
                }
            },
        }
    }
}

/// A set of `TxOut`s, ordered lexicographically.
#[derive(Clone, Debug)]
pub struct TxOuts {
    tx_outs: Vec<TxOut>,
}

impl TxOuts {
    pub fn empty() -> TxOuts {
        TxOuts {
            tx_outs: Vec::new(),
        }
    }

    pub fn insert(&mut self, tx_out: TxOut) -> usize {
        let index = match self
            .tx_outs
            .binary_search_by(|other| other.cmp_lexicographic(&tx_out))
        {
            Ok(index) | Err(index) => index,
        };
        self.tx_outs.insert(index, tx_out);
        index
    }

    pub fn into_vec(self) -> Vec<TxOut> {
        self.tx_outs
    }
}

/// A set of `TxIn`s, ordered lexicographically.
#[derive(Clone, Debug)]
pub struct TxIns {
    tx_ins: Vec<TxIn>,
}

impl TxIns {
    pub fn empty() -> TxIns {
        TxIns { tx_ins: Vec::new() }
    }

    pub fn insert(&mut self, tx_in: TxIn) -> usize {
        let index = match self
            .tx_ins
            .binary_search_by(|other| other.cmp_lexicographic(&tx_in))
        {
            Ok(index) | Err(index) => index,
        };
        self.tx_ins.insert(index, tx_in);
        index
    }

    pub fn into_vec(self) -> Vec<TxIn> {
        self.tx_ins
    }
}
