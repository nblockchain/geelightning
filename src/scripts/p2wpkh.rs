use super::*;

pub fn p2wpkh(witness_program: WPubkeyHash) -> Script {
    script! {
        OP_PUSHBYTES_0 #witness_program
    }
}
