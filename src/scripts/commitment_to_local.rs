use super::*;

pub fn commitment_to_local(
    revocation_pubkey: &PublicKey,
    local_delayedpubkey: &PublicKey,
    to_self_delay: u16,
) -> Script {
    script! {
        OP_IF
            #revocation_pubkey
        OP_ELSE
            #to_self_delay
            OP_CSV
            OP_DROP
            #local_delayedpubkey
        OP_ENDIF
        OP_CHECKSIG
    }
}
