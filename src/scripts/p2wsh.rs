use super::*;

pub fn p2wsh(witness_program: WScriptHash) -> Script {
    script! {
        OP_PUSHBYTES_0 #witness_program
    }
}
