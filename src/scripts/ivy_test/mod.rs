use super::*;

use std::{fs, path::PathBuf, process::Command};
use tempdir::TempDir;

#[test]
fn ivy_test() {
    let script_pairs = &[("commitment_to_local.rs", "to_local.ivy")];
    for (rust_script_file, ivy_script_file) in script_pairs {
        let transcoded_rust_script = transcode_rust_script(rust_script_file);
        let compiled_ivy_script = compile_ivy_script(ivy_script_file);
        assert_eq!(transcoded_rust_script, compiled_ivy_script);
    }
}

fn transcode_rust_script(rust_script_file: &str) -> Vec<String> {
    let code = {
        let mut file_name = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_name.push("src");
        file_name.push("scripts");
        file_name.push(rust_script_file);
        fs::read_to_string(file_name).unwrap()
    };
    let bitcoin_script = {
        let marker_start = "script! {";
        let marker_end = "}";
        let start = code.find(marker_start).unwrap() + marker_start.len();
        let end = code[start..].find(marker_end).unwrap();
        &code[start..][..end]
    };
    let mut transcoded = Vec::new();
    for token in bitcoin_script.split_whitespace() {
        if let Some(op) = token.strip_prefix("OP_") {
            let op = match op {
                "CSV" => "CHECKSEQUENCEVERIFY",
                op => op,
            };
            transcoded.push(String::from(op));
        } else if let Some(variable_name) = token.strip_prefix('#') {
            transcoded.push(format!("PUSH({})", variable_name));
        } else {
            panic!("unrecognized token: \"{}\"", token);
        }
    }
    transcoded
}

fn compile_ivy_script(ivy_script_file: &str) -> Vec<String> {
    let file_name = {
        let mut file_name = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_name.push("src");
        file_name.push("scripts");
        file_name.push("ivy_test");
        file_name.push(ivy_script_file);
        file_name
    };
    let temp_dir = TempDir::new("geelightning-ivy-test").unwrap();
    let temp_file_name = {
        let mut temp_file_name = PathBuf::from(temp_dir.path());
        temp_file_name.push(ivy_script_file);
        temp_file_name
    };
    fs::copy(file_name, &temp_file_name).unwrap();

    let exit_status = match Command::new("ivy-compiler").arg(&temp_file_name).status() {
        Ok(exit_status) => exit_status,
        Err(err) => {
            panic!(
                "Failed to run ivy-compiler: {}.\nTo run ivy tests you must install ivy-compiler \
                 from npm. Otherwise skip these tests by disabling the ivy-test feature",
                err
            );
        }
    };
    assert!(exit_status.success());
    let code = {
        let output_file_name = temp_file_name.with_extension("ivy.tpl");
        fs::read_to_string(output_file_name).unwrap()
    };
    code.split_whitespace().map(String::from).collect()
}
