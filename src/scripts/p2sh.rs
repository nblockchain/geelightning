use super::*;

pub fn p2sh(script_hash: ScriptHash) -> Script {
    script! {
        OP_HASH160 #script_hash OP_EQUAL
    }
}
