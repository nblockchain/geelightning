use super::*;

pub fn two_of_two_multisig(funding_pubkey_0: PublicKey, funding_pubkey_1: PublicKey) -> Script {
    script! {
        OP_PUSHNUM_2 #funding_pubkey_0 #funding_pubkey_1
        OP_PUSHNUM_2 OP_CHECKMULTISIG
    }
}
