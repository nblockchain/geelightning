use super::*;
use bitcoin::blockdata::script::Builder;

pub trait SerializeToScript {
    fn push_to_script(&self, builder: Builder) -> Builder;
}

impl<'a, T: SerializeToScript> SerializeToScript for &'a T {
    fn push_to_script(&self, builder: Builder) -> Builder {
        <T as SerializeToScript>::push_to_script(self, builder)
    }
}

impl SerializeToScript for PublicKey {
    fn push_to_script(&self, builder: Builder) -> Builder {
        let public_key = bitcoin::util::key::PublicKey {
            compressed: true,
            key: self.to_secp256k1_public_key(),
        };
        builder.push_key(&public_key)
    }
}

impl SerializeToScript for i64 {
    fn push_to_script(&self, builder: Builder) -> Builder {
        builder.push_int(*self)
    }
}

macro_rules! int_impl (
    ($t:ty) => {
        impl SerializeToScript for $t {
            fn push_to_script(&self, builder: Builder) -> Builder {
                (*self as i64).push_to_script(builder)
            }
        }
    };
);

int_impl!(i32);
int_impl!(i16);
int_impl!(i8);
int_impl!(u32);
int_impl!(u16);
int_impl!(u8);

macro_rules! hash_impl (
    ($t:ty) => {
        impl SerializeToScript for $t {
            fn push_to_script(&self, builder: Builder) -> Builder {
                builder.push_slice(&self.as_ref())
            }
        }
    };
);

hash_impl!(hash160::Hash);
hash_impl!(sha256::Hash);
hash_impl!(ScriptHash);
hash_impl!(WPubkeyHash);
hash_impl!(WScriptHash);
