#![recursion_limit = "1024"]
// This is needed to make large async functions compile since async functions get compiled into
// types. At time of writing the largest async function (which is was bumping against the limit,
// necessitating this attribute) is ln::peer::msg_channel_test::big_msg_channel_test.
#![type_length_limit = "8000000"]
#![allow(clippy::let_and_return)]
#![allow(clippy::let_unit_value)]
#![allow(clippy::new_without_default)]
#![allow(clippy::needless_lifetimes)]
#![allow(clippy::unused_unit)]

// TODO: These macros should probably be moved out of the root module into their own module(s).
// However I had trouble trying to figure out how to export macros from modules.

/// Create an mpsc channel. This is a macro so that, in debug mode, the channel can print file/line
/// information to stdout if it blocks for a long time.
macro_rules! mpsc_channel(() => {
    crate::channel::mpsc::channel_impl(file!(), line!(), column!())
});

/// Create a oneshot channel. This is a macro so that, in debug mode, the channel can print
/// file/line information to stdout if it blocks for a long time.
macro_rules! oneshot_channel(() => {
    crate::channel::oneshot::channel_impl()
});

/// Spawn a task on the tokio runtime. This is a macro so that, during tests, tasks can report
/// file/line information when they're created and when they exit. This is useful for debugging
/// bugs that involve hanging tasks and unclean shutdowns.
macro_rules! spawn (
    ($fut:expr) => {{
        let future = $fut;
        let line = line!();
        let path = module_path!();
        spawn_impl(line, path, future)
    }};
);

#[cfg(test)]
fn spawn_impl(line: u32, path: &'static str, future: impl Future<Output = ()> + Send + 'static) {
    let _join_handle = tokio::spawn(async move {
        println!("entering task at {}:{}", path, line);
        future.await;
        println!("exiting task at {}:{}", path, line);
    });
}

#[cfg(not(test))]
fn spawn_impl(_line: u32, _path: &'static str, future: impl Future<Output = ()> + Send + 'static) {
    let _join_handle = tokio::spawn(future);
}

macro_rules! try_async {
    ($block:block) => {
        (async { Ok({ $block }) }).await
    };
}

// TODO: not all the modules need to be public. Most are just internal implementations details that
// were made public to squash unused code warnings since the modules were written before the code
// that uses them.
//
// Also TODO: Some of these could be split off into independent crates (eg. async_generator and
// on_disk). The `_ext` modules should probably be conglomerated under a single super-module.

pub mod async_generator;
pub mod bitcoin_ext;
pub mod channel;
mod channel_ext;
pub mod crypto;
mod futures_unordered_with_into_iter;
mod hash_map_ext;
pub mod ln;
mod numeric_ext;
mod subscribe_publish;
pub mod units;
#[cfg(test)]
#[macro_use]
mod test_utils;
mod duration_ext;
mod error_util;
pub mod on_disk;
mod result_ext;
pub mod scripts;
#[cfg(debug_assertions)]
mod stats;
mod tcp;
mod u48;

use crate::{
    async_generator::{
        AsyncGenerator, AsyncGeneratorExt, FutureExt as _, GeneratorState, TryAsyncGeneratorExt,
    },
    bitcoin_ext::{BlockHashExt, NetworkExt, ScriptExt, TransactionExt, TxIns, TxOuts, TxidExt},
    channel::{
        drop_channel::{DropReceiver, DropSender},
        mpsc, oneshot,
    },
    crypto::{
        PublicKey, PublicKeyParseError, SecretKey, SecretKeyParseError, SharedSecret, Signature,
        SignatureError, PUBLIC_KEY_BYTES_LEN, SECRET_KEY_BYTES_LEN,
    },
    duration_ext::DurationExt,
    error_util::{ErrorMap, HexDebug},
    futures_unordered_with_into_iter::FuturesUnorderedWithIntoIter,
    hash_map_ext::HashMapExt,
    ln::{LnDeserializer, LnSerializer, MsgTooShortError},
    numeric_ext::usize_ceil_div,
    on_disk::{OnDisk, OnDiskMutateError, OnDiskNewError, ReadFromDiskError},
    result_ext::ResultIgnoreErrExt,
    subscribe_publish::{subscribe_publish, Publisher, Subscriber},
    tcp::{TcpListenerExt, TcpStreamExt},
    u48::U48,
    units::{Msat64, Sat64, SatPerKw32},
};

pub use crate::ln::node::Node;

use async_trait::async_trait;
use bitcoin::{
    blockdata,
    blockdata::transaction::SigHashType,
    hash_types::{BlockHash, ScriptHash, SigHash, Txid, WPubkeyHash, WScriptHash},
    hashes::{hash160, sha256, sha256d, Hash},
    util::bip143::SigHashCache,
    Network, OutPoint, Script, Transaction, TxIn, TxOut,
};
use bytes::{Bytes, BytesMut};
use concat_arrays::concat_arrays;
use futures::{
    future,
    future::FusedFuture,
    lock::Mutex as AsyncMutex,
    stream,
    stream::{FusedStream, FuturesUnordered},
    FutureExt, SinkExt, Stream, StreamExt, TryFuture, TryFutureExt, TryStreamExt,
};
use geelightning_macro::{ext_trait, script};
use hkdf::Hkdf;
use lazy_static::lazy_static;
use pin_utils::pin_mut;
use rand::Rng;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use sha2::Sha256;
use smallvec::{smallvec, SmallVec};
use std::{
    cmp,
    collections::{hash_map, BTreeMap, HashMap, VecDeque},
    convert::{TryFrom, TryInto},
    fmt,
    future::Future,
    i32, io, iter,
    marker::PhantomData,
    mem,
    mem::MaybeUninit,
    net::{IpAddr, SocketAddr},
    num::TryFromIntError,
    ops,
    path::{Path, PathBuf},
    pin::Pin,
    str,
    str::Utf8Error,
    sync::{Arc, Mutex},
    task,
    task::Poll,
    thread,
    time::Duration,
    u16, u32, u64, u8,
};
use thiserror::Error;
use tokio::{
    io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt},
    net::{TcpListener, TcpSocket, TcpStream},
    time::sleep,
};
use trust_dns_resolver::{error::ResolveError, TokioAsyncResolver};

#[cfg(test)]
use {
    crate::{bitcoin_ext::OutPointExt, test_utils::random_vec},
    hex_literal::*,
    net_literals::*,
    rand::SeedableRng,
    std::time::Instant,
    tokio_stream::wrappers::TcpListenerStream,
    void::Void,
};

#[cfg(debug_assertions)]
use {futures::Sink, tokio::time::Sleep};
