/// Divide lhs by rhs, rounding up.
// Note: this would be better as a method on usize defined by an extension trait, but const trait
// fns aren't available yet. lhs and rhs stand for left-hand-side and right-hand-side respectively.
// This is following the naming conventions used by the standard library arithmetic methods (eg.
// https://doc.rust-lang.org/std/primitive.u32.html#method.checked_div)
pub const fn usize_ceil_div(lhs: usize, rhs: usize) -> usize {
    (lhs + rhs - 1) / rhs
}
