//! This module implements a simple subscriber/publisher model.
//!
//! We create a `(Subscriber, Publisher)` pair using `subscribe_publish()`. The `Subscriber` can
//! then be used to subscribe to messages under a given key and the `Publisher` to publish messages
//! under that key so that they are sent to the `Subscriber`. Subscribing returns a `Receiver`
//! which can be dropped to unsubscribe again. Only one `Subscriber` can be subscribed under a key
//! at any given time.

use super::*;

#[derive(Debug)]
pub struct Subscriber<K, V> {
    subscribe_send: mpsc::Sender<(K, DropReceiver, mpsc::Sender<V>, oneshot::Sender<()>)>,
}

impl<K, V> Clone for Subscriber<K, V> {
    fn clone(&self) -> Subscriber<K, V> {
        Subscriber {
            subscribe_send: self.subscribe_send.clone(),
        }
    }
}

#[derive(Debug)]
pub struct Publisher<K, V> {
    publish_send: mpsc::Sender<(K, oneshot::Sender<Option<V>>, V)>,
}

impl<K, V> Subscriber<K, V>
where
    K: Unpin + Copy + std::hash::Hash + Eq + Send + Sync + 'static,
    V: Unpin + Send + 'static,
{
    pub async fn subscribe(&mut self, key: K) -> Receiver<V> {
        let unsubscribe_send = DropSender::new();
        let unsubscribe_recv = unsubscribe_send.receiver();
        let (msg_send, msg_recv) = mpsc_channel!();
        let (subscribed_send, subscribed_recv) = oneshot_channel!();
        self.subscribe_send
            .send((key, unsubscribe_recv, msg_send, subscribed_send))
            .await
            .ignore_err();
        subscribed_recv.await.unwrap();

        Receiver {
            msg_recv,
            _unsubscribe_send: unsubscribe_send,
        }
    }
}

pub struct Receiver<V> {
    msg_recv: crate::channel::mpsc::Receiver<V>,
    _unsubscribe_send: DropSender,
}

impl<V> Stream for Receiver<V>
where
    V: Unpin,
{
    type Item = V;

    fn poll_next(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Option<V>> {
        let this = self.get_mut();
        Pin::new(&mut this.msg_recv).poll_next(cx)
    }
}

impl<K, V> Publisher<K, V>
where
    K: Unpin + Copy + std::hash::Hash + Eq + Send + Sync + 'static,
    V: Unpin + Send + 'static,
{
    pub async fn publish(&mut self, key: K, value: V) -> Option<V> {
        let (finish_send, finish_recv) = oneshot_channel!();
        self.publish_send
            .send((key, finish_send, value))
            .await
            .unwrap();
        finish_recv.await.unwrap()
    }
}

pub fn subscribe_publish<K, V>() -> (Subscriber<K, V>, Publisher<K, V>)
where
    K: Unpin + Copy + std::hash::Hash + Eq + Send + Sync + 'static,
    V: Unpin + Send + 'static,
{
    let (subscribe_send, mut subscribe_recv) = mpsc_channel!();
    let subscriber = Subscriber { subscribe_send };
    let (publish_send, mut publish_recv) = mpsc_channel!();
    let publisher = Publisher { publish_send };
    spawn!(async move {
        let mut subscribers = HashMap::new();
        let mut unsubscribe_recvs = stream::FuturesUnordered::new();

        loop {
            futures::select! {
                // When we receive a subscribe message from the Subscriber, add msg_send to
                // our table of subscribers and create a future in unsubscribe_recvs which
                // waits for the Receiver to be dropped.
                subscribe = subscribe_recv.select_next_some() => {
                    let (key, unsubscribe_recv, msg_send, subscribed_send) = subscribe;
                    unsubscribe_recvs.push(unsubscribe_recv.map(move |()| key));
                    let _prev_msg_send = subscribers.insert(key, msg_send);
                    subscribed_send.send(()).await.ignore_err();
                },

                // Yields keys when Receivers get dropped. Will sometimes yield None when
                // there are no more subscribers but we can safely ignore this.
                key = unsubscribe_recvs.select_next_some() => {
                    let _msg_send = subscribers.remove(&key);
                },

                publish_opt = publish_recv.next() => {
                    match publish_opt {
                        // `Some` indicates the publisher is publishing something.
                        Some((key, finish_send, msg)) => {
                            match subscribers.get_mut(&key) {
                                // We are able to send to the Receiver.
                                Some(msg_send) => {
                                    msg_send.send(msg).await.ignore_err();
                                    finish_send.send(None).await.ignore_err();
                                },
                                // There was no Receiver for that key. Return the msg to
                                // the Publisher.
                                None => {
                                    finish_send.send(Some(msg)).await.ignore_err();
                                },
                            }
                        },

                        // Indicates the Publisher was dropped. Since there aren't going to be any
                        // more messages to send we can exit the task.
                        None => break,
                    }
                },
            }
        }
    });
    (subscriber, publisher)
}
