//! In order to try and trigger race conditions geelightning will, in debug mode, add random delays
//! to messages sent over channels. This is implemented in {mpsc,oneshot}_debug.rs. In release mode
//! the non-debug versions of these modules are used instead. The non-debug versions simply
//! re-export the channel types from the futures crate, except for `oneshot::Sender` which needs a
//! wrapper to make its `send` method async like the debug version.

use super::*;

#[cfg_attr(debug_assertions, path = "mpsc_debug.rs")]
pub mod mpsc;

#[cfg_attr(debug_assertions, path = "oneshot_debug.rs")]
pub mod oneshot;

pub mod drop_channel;
