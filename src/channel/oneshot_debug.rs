//! This module provides wrappers around the futures oneshot channel types and adds random delays
//! when messages are sent in order to try and trigger race conditions in the rest of the code.
//! This module is only used in debug mode. In release mode the file `oneshot.rs` implements this
//! module instead.

use super::*;
use stats::normal_random_duration;

pub use futures::channel::oneshot::Canceled;

#[derive(Debug)]
pub struct Sender<T> {
    sender: futures::channel::oneshot::Sender<(T, Option<Pin<Box<Sleep>>>)>,
}

#[derive(Debug)]
pub struct Receiver<T> {
    receiver: futures::channel::oneshot::Receiver<(T, Option<Pin<Box<Sleep>>>)>,
    waiting: Option<(T, Pin<Box<Sleep>>)>,
}

pub fn channel_impl<T>() -> (Sender<T>, Receiver<T>) {
    let (sender, receiver) = futures::channel::oneshot::channel();
    let sender = Sender { sender };
    let receiver = Receiver {
        receiver,
        waiting: None,
    };
    (sender, receiver)
}

impl<T> Sender<T> {
    pub async fn send(self, value: T) -> Result<(), T> {
        let (is_negative, delay_period) =
            normal_random_duration(Duration::new(0, 0), Duration::from_millis(10));
        let finish_delay = sleep(delay_period);
        let res = if is_negative {
            let res = self.sender.send((value, None));
            finish_delay.await;
            res
        } else {
            self.sender.send((value, Some(Box::pin(finish_delay))))
        };
        match res {
            Ok(()) => Ok(()),
            Err((value, _)) => Err(value),
        }
    }

    pub fn poll_canceled(&mut self, cx: &mut task::Context) -> Poll<()> {
        self.sender.poll_canceled(cx)
    }
}

impl<T: Unpin> Future for Receiver<T> {
    type Output = Result<T, Canceled>;

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Result<T, Canceled>> {
        let self_mut = self.get_mut();
        loop {
            if let Some((value, mut finish_delay)) = self_mut.waiting.take() {
                match Pin::new(&mut finish_delay).poll(cx) {
                    Poll::Ready(()) => {
                        return Poll::Ready(Ok(value));
                    }
                    Poll::Pending => {
                        self_mut.waiting = Some((value, finish_delay));
                        return Poll::Pending;
                    }
                }
            }
            match Pin::new(&mut self_mut.receiver).poll(cx) {
                Poll::Ready(Ok((value, finish_delay_opt))) => match finish_delay_opt {
                    Some(finish_delay) => self_mut.waiting = Some((value, finish_delay)),
                    None => return Poll::Ready(Ok(value)),
                },
                Poll::Ready(Err(Canceled)) => return Poll::Ready(Err(Canceled)),
                Poll::Pending => return Poll::Pending,
            }
        }
    }
}
