use super::*;

pub type Sender<T> = futures::channel::mpsc::Sender<T>;
pub type Receiver<T> = futures::channel::mpsc::Receiver<T>;

pub fn channel_impl<T: Send + 'static>(
    _file: &'static str,
    _line: u32,
    _column: u32,
) -> (Sender<T>, Receiver<T>) {
    futures::channel::mpsc::channel(0)
}
