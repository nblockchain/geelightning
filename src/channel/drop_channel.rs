use super::*;

#[derive(Debug)]
struct Inner {
    next_id: usize,
    wakers: HashMap<usize, task::Waker>,
    dropped: bool,
}

/// Dropping a `DropSender` causes all its associated `DropReceiver` futures to resolve.
pub struct DropSender {
    // NOTE: using a non-async Mutex here is not ideal. This should be replaced when this bug is
    // resolved: https://github.com/rust-lang-nursery/futures-rs/issues/1972
    inner: Arc<Mutex<Inner>>,
}

/// A `Future` which resolves to `()` once its associated `DropSender` is dropped. Created using the
/// `DropSender::receiver` method.
#[derive(Debug)]
pub struct DropReceiver {
    id_inner_opt: Option<(usize, Arc<Mutex<Inner>>)>,
}

impl DropSender {
    pub fn new() -> DropSender {
        let inner = Inner {
            next_id: 0,
            wakers: HashMap::new(),
            dropped: false,
        };
        DropSender {
            inner: Arc::new(Mutex::new(inner)),
        }
    }

    pub fn receiver(&self) -> DropReceiver {
        let id = {
            let mut inner = self.inner.lock().unwrap();
            loop {
                let id = inner.next_id;
                inner.next_id += 1;
                if !inner.wakers.contains_key(&id) {
                    break id;
                }
            }
        };
        DropReceiver {
            id_inner_opt: Some((id, self.inner.clone())),
        }
    }
}

impl Drop for DropSender {
    fn drop(&mut self) {
        let mut inner = self.inner.lock().unwrap();
        inner.dropped = true;
        let wakers = mem::replace(&mut inner.wakers, HashMap::new());
        for (_id, waker) in wakers.into_iter() {
            waker.wake();
        }
    }
}

impl Future for DropReceiver {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        let self_mut = self.get_mut();
        let id_inner_opt = self_mut.id_inner_opt.take();
        if let Some((id, inner)) = id_inner_opt {
            {
                let mut inner = inner.lock().unwrap();
                if inner.dropped {
                    return Poll::Ready(());
                }
                let _ = inner.wakers.insert(id, cx.waker().clone());
            }
            self_mut.id_inner_opt = Some((id, inner));
            return Poll::Pending;
        }
        Poll::Ready(())
    }
}

impl Drop for DropReceiver {
    fn drop(&mut self) {
        let id_inner_opt = self.id_inner_opt.take();
        if let Some((id, inner)) = id_inner_opt {
            let mut inner = inner.lock().unwrap();
            let _ = inner.wakers.remove(&id);
        }
    }
}

impl FusedFuture for DropReceiver {
    fn is_terminated(&self) -> bool {
        self.id_inner_opt.is_none()
    }
}
