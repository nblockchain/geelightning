use super::*;
pub use futures::channel::oneshot::Canceled;

#[derive(Debug)]
pub struct Sender<T> {
    sender: futures::channel::oneshot::Sender<T>,
}

pub type Receiver<T> = futures::channel::oneshot::Receiver<T>;

pub fn channel_impl<T>() -> (Sender<T>, Receiver<T>) {
    let (sender, receiver) = futures::channel::oneshot::channel();
    let sender = Sender { sender };
    (sender, receiver)
}

impl<T> Sender<T> {
    pub async fn send(self, t: T) -> Result<(), T> {
        self.sender.send(t)
    }

    pub fn poll_canceled(&mut self, cx: &mut task::Context) -> Poll<()> {
        self.sender.poll_cancel(cx)
    }
}
