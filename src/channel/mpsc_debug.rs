//! This module provides wrappers around the futures mpsc channel types and adds random delays
//! when messages are sent in order to try and trigger race conditions in the rest of the code.
//! This module is only used in debug mode. In release mode the file `mpsc.rs` implements this
//! module instead and simply re-exports the types from `futures::mpsc`.
//!
//! Every channel gets random mean and variance parameters set upon creation which are used to
//! calculate the random delay every time a message is sent.

use super::*;
use futures::channel::mpsc::SendError;
use stats::{exponentially_random_duration, normal_random_duration};

const DELAY_PERIOD_MEAN_STD_DEV: Duration = Duration::from_millis(10);
const DELAY_PERIOD_STD_DEV: Duration = Duration::from_millis(10);

#[derive(Debug)]
pub struct Sender<T> {
    sender: futures::channel::mpsc::Sender<(T, Option<Pin<Box<Sleep>>>)>,
    delay_period_mean_is_negative: bool,
    delay_period_mean: Duration,
    delay_period_std_dev: Duration,
    waiting: Option<(T, Pin<Box<Sleep>>)>,
    file: &'static str,
    line: u32,
    column: u32,
    next_warn_delay: Option<Pin<Box<Sleep>>>,
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Sender<T> {
        Sender {
            sender: self.sender.clone(),
            delay_period_mean_is_negative: self.delay_period_mean_is_negative,
            delay_period_mean: self.delay_period_mean,
            delay_period_std_dev: self.delay_period_std_dev,
            waiting: None,
            file: self.file,
            line: self.line,
            column: self.column,
            next_warn_delay: None,
        }
    }
}

#[derive(Debug)]
pub struct Receiver<T> {
    receiver: futures::channel::mpsc::Receiver<(T, Option<Pin<Box<Sleep>>>)>,
    waiting: Option<(T, Pin<Box<Sleep>>)>,
}

pub fn channel_impl<T: Send + 'static>(
    file: &'static str,
    line: u32,
    column: u32,
) -> (Sender<T>, Receiver<T>) {
    let (sender, receiver) = futures::channel::mpsc::channel(0);
    let (delay_period_mean_is_negative, delay_period_mean) =
        normal_random_duration(Duration::new(0, 0), DELAY_PERIOD_MEAN_STD_DEV);
    let delay_period_std_dev = exponentially_random_duration(DELAY_PERIOD_STD_DEV);

    let sender = Sender {
        sender,
        delay_period_mean_is_negative,
        delay_period_mean,
        delay_period_std_dev,
        waiting: None,
        file,
        line,
        column,
        next_warn_delay: None,
    };
    let receiver = Receiver {
        receiver,
        waiting: None,
    };
    (sender, receiver)
}

impl<T: Unpin> Sender<T> {
    pub fn is_closed(&self) -> bool {
        self.sender.is_closed()
    }

    pub fn poll_ready(&mut self, cx: &mut task::Context) -> Poll<Result<(), SendError>> {
        Sink::poll_ready(Pin::new(self), cx)
    }
}

impl<T: Unpin> Sink<T> for Sender<T> {
    type Error = SendError;

    fn poll_ready(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Result<(), SendError>> {
        let mut self_mut = self.get_mut();
        match Pin::new(&mut self_mut).poll_flush(cx) {
            Poll::Ready(Ok(())) => (),
            Poll::Ready(Err(err)) => return Poll::Ready(Err(err)),
            Poll::Pending => return Poll::Pending,
        }
        self_mut.sender.poll_ready(cx)
    }

    fn start_send(self: Pin<&mut Self>, value: T) -> Result<(), SendError> {
        let self_mut = self.get_mut();
        let (is_negative, delay_period) =
            normal_random_duration(self_mut.delay_period_mean, self_mut.delay_period_std_dev);
        let finish_delay = sleep(delay_period);
        if is_negative {
            self_mut.waiting = Some((value, Box::pin(finish_delay)));
            Ok(())
        } else {
            self_mut
                .sender
                .start_send((value, Some(Box::pin(finish_delay))))
        }
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Result<(), SendError>> {
        let mut self_mut = self.get_mut();
        if let Some((value, mut finish_delay)) = self_mut.waiting.take() {
            match Pin::new(&mut finish_delay).poll(cx) {
                Poll::Ready(()) => match self_mut.sender.start_send((value, None)) {
                    Ok(()) => {
                        self_mut.next_warn_delay = Some(Box::pin(sleep(Duration::from_secs(5))));
                    }
                    Err(err) => return Poll::Ready(Err(err)),
                },
                Poll::Pending => {
                    self_mut.waiting = Some((value, finish_delay));
                    return Poll::Pending;
                }
            }
        }
        match Pin::new(&mut self_mut.sender).poll_flush(cx) {
            Poll::Ready(res) => {
                self_mut.next_warn_delay = None;
                Poll::Ready(res)
            }
            Poll::Pending => {
                if let Some(mut next_warn_delay) = self_mut.next_warn_delay.as_mut() {
                    if let Poll::Ready(()) = Pin::new(&mut next_warn_delay).poll(cx) {
                        println!(
                            "warning: channel at {}:{}:{} has been blocked for a long time",
                            self_mut.file, self_mut.line, self_mut.column,
                        );
                        next_warn_delay
                            .as_mut()
                            .reset(tokio::time::Instant::now() + Duration::from_secs(5));
                    }
                }
                Poll::Pending
            }
        }
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Result<(), SendError>> {
        let mut self_mut = self.get_mut();
        if self_mut.waiting.is_some() {
            match Pin::new(&mut self_mut).poll_flush(cx) {
                Poll::Ready(Ok(())) => (),
                Poll::Ready(Err(err)) => return Poll::Ready(Err(err)),
                Poll::Pending => return Poll::Pending,
            }
        }
        Pin::new(&mut self_mut.sender).poll_close(cx)
    }
}

impl<T: Unpin> Stream for Receiver<T> {
    type Item = T;

    fn poll_next(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Option<T>> {
        let mut self_mut = self.get_mut();
        loop {
            if let Some((value, mut finish_delay)) = self_mut.waiting.take() {
                match Pin::new(&mut finish_delay).poll(cx) {
                    Poll::Ready(()) => return Poll::Ready(Some(value)),
                    Poll::Pending => {
                        self_mut.waiting = Some((value, finish_delay));
                        return Poll::Pending;
                    }
                }
            }
            match Pin::new(&mut self_mut.receiver).poll_next(cx) {
                Poll::Ready(Some((value, finish_delay_opt))) => match finish_delay_opt {
                    Some(finish_delay) => {
                        self_mut.waiting = Some((value, finish_delay));
                    }
                    None => return Poll::Ready(Some(value)),
                },
                Poll::Ready(None) => return Poll::Ready(None),
                Poll::Pending => return Poll::Pending,
            }
        }
    }
}

impl<T: Unpin> FusedStream for Receiver<T> {
    fn is_terminated(&self) -> bool {
        self.waiting.is_none() && self.receiver.is_terminated()
    }
}
