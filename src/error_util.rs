use super::*;

/// Wraps a slice of bytes that they can be printed in hexadecimal.
pub struct HexDebug<'a>(pub &'a [u8]);
impl<'a> fmt::Debug for HexDebug<'a> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let HexDebug(bytes) = self;
        for byte in *bytes {
            write!(fmt, "{:02x}", byte)?;
        }
        Ok(())
    }
}

/// Wraps a hash map of errors and implements `fmt::Display`.
pub struct ErrorMap<'a, K, V>(pub &'a HashMap<K, V>);

impl<'a, K, V> fmt::Display for ErrorMap<'a, K, V>
where
    K: fmt::Display,
    V: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ErrorMap(map) = self;
        write!(f, "({} failures) [", map.len())?;
        let mut first = true;
        for (key, value) in *map {
            if !first {
                write!(f, ", ")?;
            }
            first = false;
            write!(f, "{}: {}", key, value)?;
        }
        write!(f, "]")?;
        Ok(())
    }
}
