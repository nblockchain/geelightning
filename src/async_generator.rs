//! This module implements the `AsyncGenerator` trait. This is similar to the `Generator` trait on
//! rust nightly, except that it's async.
//!
//! An `AsyncGenerator` is like a `Stream` except that it returns a final value when it is
//! finished. You poll an `AsyncGenerator` by calling `AsyncGenerator::poll_resume` and it will
//! return a `GeneratorState` containing either a yielded value, or the final value of the
//! generator. Once the generator returns `GeneratorState::Complete` it must not be polled again.
//!
//! Some of the uses for generators are:
//!   * They can represent infinite streams (by completing with a `!`). For infinite streams
//!     represented as a `Stream` you still have to handle the impossible case of the stream
//!     ending, eg. by using `unreachable!()`.
//!   * They can represent a stream that distinguishes between fatal and non-fatal errors. Fatal
//!     errors are returned via `Complete` and non-fatal errors are returned via `Yield`.
//!   * They can represent a future which yields diagnostics while running. This is the main use
//!     for them in this crate.
//!
//! The traits in this module mirror the traits of the futures crate that are used for futures and
//! streams. eg. there is the base `AsyncGenerator` trait, an `AsyncGeneratorExt` trait which
//! provides extension methods, a `FusedAsyncGenerator` trait for async generators which are fused,
//! and a `TryAsyncGenerator` trait for async generators which return a `Result`.
//!
//! This module also provides `Sender`/`Receiver` types where the `Receiver` implements
//! `AsyncGenerator` and the `Sender` can be used to either send yielded values to the `Receiver`
//! or to send a final value, consuming the `Sender`.

use super::*;
use futures::channel::mpsc::SendError;

/// This is a reimplementation of std::ops::GeneratorState because that type isn't stable.
/// This type should be replaced with the std type once it is stabilised.
pub enum GeneratorState<Y, R> {
    Yielded(Y),
    Complete(R),
}

pub trait AsyncGenerator {
    type Yield;
    type Return;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>>;
}

impl<P> AsyncGenerator for Pin<P>
where
    P: Unpin + ops::DerefMut,
    <P as ops::Deref>::Target: AsyncGenerator,
{
    type Yield = <<P as ops::Deref>::Target as AsyncGenerator>::Yield;
    type Return = <<P as ops::Deref>::Target as AsyncGenerator>::Return;

    fn poll_resume(
        self: Pin<&mut Pin<P>>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>> {
        AsyncGenerator::poll_resume(self.get_mut().as_mut(), cx)
    }
}

pub trait AsyncGeneratorExt: AsyncGenerator {
    /// Fuse the `AsyncGenerator`, making it safe to call `poll_resume` after the generator has
    /// completed.
    fn fuse(self) -> Fuse<Self>
    where
        Self: Sized,
    {
        Fuse {
            async_generator: self,
            terminated: false,
        }
    }

    /// Asynchronously get the next `GeneratorState` returned by the generator.
    fn next_state(&mut self) -> NextState<Self> {
        NextState {
            async_generator: self,
        }
    }

    /// Chain another `AsyncGenerator` to self, provided both generators yield the same type.
    /// Similar to `FutureExt::then`.
    fn then<F, B>(self, f: F) -> Then<Self, B, F>
    where
        Self: Sized,
        F: FnOnce(Self::Return) -> B,
        B: AsyncGenerator<Yield = Self::Yield>,
    {
        Then {
            inner: ThenInner::First {
                first: self,
                function: MaybeUninit::new(f),
            },
        }
    }

    /// Handle all of the values yielded by the generator and return the final value as a `Future`.
    /// Similar to `Stream::for_each`.
    fn for_each<F, Fut>(self, f: F) -> ForEach<Self, F, Fut>
    where
        Self: Sized,
        F: FnMut(Self::Yield) -> Fut,
        Fut: Future<Output = ()>,
    {
        ForEach {
            async_generator: self,
            function: f,
            future_opt: None,
        }
    }

    /// Map the values yielded by the generator.
    fn map_yield<F, Y>(self, f: F) -> MapYield<Self, F>
    where
        Self: Sized,
        F: FnMut(Self::Yield) -> Y,
    {
        MapYield {
            async_generator: self,
            map: f,
        }
    }

    /// Map the final value returned by the generator.
    fn map<F, R>(self, f: F) -> Map<Self, F>
    where
        Self: Sized,
        F: FnOnce(Self::Return) -> R,
    {
        Map {
            async_generator: self,
            map: Some(f),
        }
    }
}

impl<A: AsyncGenerator> AsyncGeneratorExt for A {}

pub trait TryAsyncGenerator: AsyncGenerator {
    type Ok;
    type Error;

    #[allow(clippy::type_complexity)]
    fn try_poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Result<Self::Ok, Self::Error>>>;
}

impl<A, T, E> TryAsyncGenerator for A
where
    A: AsyncGenerator<Return = Result<T, E>>,
{
    type Ok = T;
    type Error = E;

    #[allow(clippy::type_complexity)]
    fn try_poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Result<Self::Ok, Self::Error>>> {
        AsyncGenerator::poll_resume(self, cx)
    }
}

pub trait TryAsyncGeneratorExt: TryAsyncGenerator {
    fn map_ok<T, F>(self, f: F) -> MapOk<Self, F>
    where
        Self: Sized,
        F: FnOnce(Self::Ok) -> T,
    {
        MapOk {
            async_generator: self,
            map: Some(f),
        }
    }

    fn map_err<E, F>(self, f: F) -> MapErr<Self, F>
    where
        Self: Sized,
        F: FnOnce(Self::Error) -> E,
    {
        MapErr {
            async_generator: self,
            map: Some(f),
        }
    }
}

impl<A: TryAsyncGenerator> TryAsyncGeneratorExt for A {}

pub struct MapOk<A, F> {
    async_generator: A,
    map: Option<F>,
}

impl<A, F, T> AsyncGenerator for MapOk<A, F>
where
    A: TryAsyncGenerator,
    F: FnOnce(<A as TryAsyncGenerator>::Ok) -> T,
{
    type Yield = A::Yield;
    type Return = Result<T, A::Error>;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        let async_generator = unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
        match TryAsyncGenerator::try_poll_resume(async_generator, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(GeneratorState::Yielded(y)) => Poll::Ready(GeneratorState::Yielded(y)),
            Poll::Ready(GeneratorState::Complete(Ok(t))) => {
                let map = self_mut.map.take().unwrap();
                let t = map(t);
                Poll::Ready(GeneratorState::Complete(Ok(t)))
            }
            Poll::Ready(GeneratorState::Complete(Err(err))) => {
                Poll::Ready(GeneratorState::Complete(Err(err)))
            }
        }
    }
}

pub struct MapErr<A, F> {
    async_generator: A,
    map: Option<F>,
}

impl<A, F, E> AsyncGenerator for MapErr<A, F>
where
    A: TryAsyncGenerator,
    F: FnOnce(<A as TryAsyncGenerator>::Error) -> E,
{
    type Yield = A::Yield;
    type Return = Result<A::Ok, E>;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        let async_generator = unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
        match TryAsyncGenerator::try_poll_resume(async_generator, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(GeneratorState::Yielded(y)) => Poll::Ready(GeneratorState::Yielded(y)),
            Poll::Ready(GeneratorState::Complete(Ok(t))) => {
                Poll::Ready(GeneratorState::Complete(Ok(t)))
            }
            Poll::Ready(GeneratorState::Complete(Err(err))) => {
                let map = self_mut.map.take().unwrap();
                let err = map(err);
                Poll::Ready(GeneratorState::Complete(Err(err)))
            }
        }
    }
}

pub trait FusedAsyncGenerator: AsyncGenerator {
    fn is_terminated(&self) -> bool;
}

impl<P> FusedAsyncGenerator for Pin<P>
where
    P: ops::DerefMut + Unpin,
    <P as ops::Deref>::Target: FusedAsyncGenerator,
{
    fn is_terminated(&self) -> bool {
        self.as_ref().is_terminated()
    }
}

pub struct Fuse<A> {
    async_generator: A,
    terminated: bool,
}

impl<A> AsyncGenerator for Fuse<A>
where
    A: AsyncGenerator,
{
    type Yield = A::Yield;
    type Return = A::Return;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<A::Yield, A::Return>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        if self_mut.terminated {
            return Poll::Pending;
        }

        let async_generator = unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
        match AsyncGenerator::poll_resume(async_generator, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(GeneratorState::Yielded(y)) => Poll::Ready(GeneratorState::Yielded(y)),
            Poll::Ready(GeneratorState::Complete(r)) => {
                self_mut.terminated = true;
                Poll::Ready(GeneratorState::Complete(r))
            }
        }
    }
}

impl<A> FusedAsyncGenerator for Fuse<A>
where
    A: AsyncGenerator,
{
    fn is_terminated(&self) -> bool {
        self.terminated
    }
}

pub struct NextState<'a, T: ?Sized> {
    async_generator: &'a mut T,
}

impl<'a, T> Future for NextState<'a, T>
where
    T: AsyncGenerator + Unpin,
{
    type Output = GeneratorState<T::Yield, T::Return>;

    fn poll(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<T::Yield, T::Return>> {
        AsyncGenerator::poll_resume(Pin::new(self.get_mut().async_generator), cx)
    }
}

impl<'a, T> FusedFuture for NextState<'a, T>
where
    T: AsyncGenerator + FusedAsyncGenerator + Unpin,
{
    fn is_terminated(&self) -> bool {
        self.async_generator.is_terminated()
    }
}

pub struct Then<A, Fut, F> {
    inner: ThenInner<A, Fut, F>,
}

enum ThenInner<A, B, F> {
    First { first: A, function: MaybeUninit<F> },
    Second { second: B },
}

impl<A, B, F> AsyncGenerator for Then<A, B, F>
where
    A: AsyncGenerator,
    F: FnOnce(A::Return) -> B,
    B: AsyncGenerator<Yield = A::Yield>,
{
    type Yield = A::Yield;
    type Return = B::Return;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<A::Yield, B::Return>> {
        let inner = unsafe { &mut self.get_unchecked_mut().inner };
        loop {
            match inner {
                ThenInner::First { first, function } => {
                    let first = unsafe { Pin::new_unchecked(first) };
                    match AsyncGenerator::poll_resume(first, cx) {
                        Poll::Pending => return Poll::Pending,
                        Poll::Ready(GeneratorState::Yielded(y)) => {
                            return Poll::Ready(GeneratorState::Yielded(y))
                        }
                        Poll::Ready(GeneratorState::Complete(a)) => {
                            let function = mem::replace(function, MaybeUninit::uninit());
                            let function = unsafe { function.assume_init() };
                            let second = function(a);
                            *inner = ThenInner::Second { second };
                        }
                    }
                }
                ThenInner::Second { second } => {
                    let second = unsafe { Pin::new_unchecked(second) };
                    match AsyncGenerator::poll_resume(second, cx) {
                        Poll::Pending => return Poll::Pending,
                        Poll::Ready(GeneratorState::Yielded(y)) => {
                            return Poll::Ready(GeneratorState::Yielded(y))
                        }
                        Poll::Ready(GeneratorState::Complete(b)) => {
                            return Poll::Ready(GeneratorState::Complete(b))
                        }
                    }
                }
            }
        }
    }
}

pub struct ForEach<A, F, Fut> {
    async_generator: A,
    function: F,
    future_opt: Option<Fut>,
}

impl<A, F, Fut> Future for ForEach<A, F, Fut>
where
    A: AsyncGenerator,
    F: FnMut(A::Yield) -> Fut,
    Fut: Future<Output = ()>,
{
    type Output = A::Return;

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<A::Return> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        loop {
            match &mut self_mut.future_opt {
                Some(future) => {
                    let future = unsafe { Pin::new_unchecked(future) };
                    match Future::poll(future, cx) {
                        Poll::Pending => return Poll::Pending,
                        Poll::Ready(()) => {
                            self_mut.future_opt = None;
                        }
                    }
                }
                None => {
                    let async_generator =
                        unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
                    match AsyncGenerator::poll_resume(async_generator, cx) {
                        Poll::Pending => return Poll::Pending,
                        Poll::Ready(GeneratorState::Yielded(y)) => {
                            self_mut.future_opt = Some((self_mut.function)(y));
                        }
                        Poll::Ready(GeneratorState::Complete(r)) => return Poll::Ready(r),
                    }
                }
            }
        }
    }
}

pub struct MapYield<A, F> {
    async_generator: A,
    map: F,
}

impl<A, F, Y> AsyncGenerator for MapYield<A, F>
where
    A: AsyncGenerator,
    F: FnMut(A::Yield) -> Y,
{
    type Yield = Y;
    type Return = A::Return;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Y, A::Return>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        let async_generator = unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
        match AsyncGenerator::poll_resume(async_generator, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(GeneratorState::Complete(r)) => Poll::Ready(GeneratorState::Complete(r)),
            Poll::Ready(GeneratorState::Yielded(y)) => {
                let y = (self_mut.map)(y);
                Poll::Ready(GeneratorState::Yielded(y))
            }
        }
    }
}

pub struct Map<A, F> {
    async_generator: A,
    map: Option<F>,
}

impl<A, F, R> AsyncGenerator for Map<A, F>
where
    A: AsyncGenerator,
    F: FnOnce(A::Return) -> R,
{
    type Yield = A::Yield;
    type Return = R;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<A::Yield, R>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        let async_generator = unsafe { Pin::new_unchecked(&mut self_mut.async_generator) };
        match AsyncGenerator::poll_resume(async_generator, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(GeneratorState::Yielded(y)) => Poll::Ready(GeneratorState::Yielded(y)),
            Poll::Ready(GeneratorState::Complete(r)) => {
                let map = self_mut.map.take().unwrap();
                let r = map(r);
                Poll::Ready(GeneratorState::Complete(r))
            }
        }
    }
}

pub struct Sender<Y, R> {
    sender: mpsc::Sender<GeneratorState<Y, R>>,
}

pub struct Receiver<Y, R> {
    receiver: mpsc::Receiver<GeneratorState<Y, R>>,
}

impl<Y: Unpin, R: Unpin> Sender<Y, R> {
    pub async fn send_yield(&mut self, yield_msg: Y) -> Result<(), SendError> {
        self.sender.send(GeneratorState::Yielded(yield_msg)).await
    }

    pub async fn send_return(mut self, return_msg: R) -> Result<(), SendError> {
        self.sender.send(GeneratorState::Complete(return_msg)).await
    }

    /// Create a future which borrows the `Sender` and resolves to `()` if the corresponding
    /// `Receiver` is dropped.
    pub fn on_drop<'a>(&'a mut self) -> SenderOnDrop<'a, Y, R> {
        SenderOnDrop {
            sender: self,
            terminated: false,
        }
    }
}

pub struct SenderOnDrop<'a, Y, R> {
    sender: &'a mut Sender<Y, R>,
    terminated: bool,
}

impl<'a, Y, R> Future for SenderOnDrop<'a, Y, R>
where
    Y: Unpin,
    R: Unpin,
{
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        let self_mut = self.get_mut();
        match self_mut.sender.sender.poll_ready(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(Ok(())) => Poll::Pending,
            Poll::Ready(Err(err)) => {
                if err.is_disconnected() {
                    self_mut.terminated = true;
                    Poll::Ready(())
                } else {
                    Poll::Pending
                }
            }
        }
    }
}

impl<'a, Y, R> FusedFuture for SenderOnDrop<'a, Y, R>
where
    Y: Unpin,
    R: Unpin,
{
    fn is_terminated(&self) -> bool {
        self.terminated
    }
}

pub fn channel<Y: Send + 'static, R: Send + 'static>() -> (Sender<Y, R>, Receiver<Y, R>) {
    let (sender, receiver) = mpsc_channel!();
    let sender = Sender { sender };
    let receiver = Receiver { receiver };
    (sender, receiver)
}

impl<Y: Unpin, R: Unpin> AsyncGenerator for Receiver<Y, R> {
    type Yield = Y;
    type Return = Option<R>;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Y, Option<R>>> {
        match Stream::poll_next(Pin::new(&mut self.get_mut().receiver), cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(None) => Poll::Ready(GeneratorState::Complete(None)),
            Poll::Ready(Some(GeneratorState::Complete(r))) => {
                Poll::Ready(GeneratorState::Complete(Some(r)))
            }
            Poll::Ready(Some(GeneratorState::Yielded(y))) => {
                Poll::Ready(GeneratorState::Yielded(y))
            }
        }
    }
}

pub struct StreamThenFuture<S, F> {
    stream_opt: Option<S>,
    future: F,
}

/// Combine a `Stream` and a `Future` into an `AsyncGenerator` which yields values from the
/// `Stream` until the `Future` resolves.
pub fn stream_then_future<S: Stream, F: Future>(stream: S, future: F) -> StreamThenFuture<S, F> {
    let stream_opt = Some(stream);
    StreamThenFuture { stream_opt, future }
}

impl<S: Stream, F: Future> AsyncGenerator for StreamThenFuture<S, F> {
    type Yield = S::Item;
    type Return = F::Output;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<S::Item, F::Output>> {
        let self_mut = unsafe { self.get_unchecked_mut() };
        let future = unsafe { Pin::new_unchecked(&mut self_mut.future) };
        if let Poll::Ready(r) = Future::poll(future, cx) {
            return Poll::Ready(GeneratorState::Complete(r));
        }

        match &mut self_mut.stream_opt {
            Some(stream) => {
                let stream = unsafe { Pin::new_unchecked(stream) };
                match Stream::poll_next(stream, cx) {
                    Poll::Pending => Poll::Pending,
                    Poll::Ready(Some(y)) => Poll::Ready(GeneratorState::Yielded(y)),
                    Poll::Ready(None) => {
                        self_mut.stream_opt = None;
                        Poll::Pending
                    }
                }
            }
            None => Poll::Pending,
        }
    }
}

pub trait FutureExt: Future {
    /// Flatten a future which returns an `AsyncGenerator` into an `AsyncGenerator`. Similar to the
    /// `FutureExt::flatten_stream` method in the futures crate.
    fn flatten_async_generator(self) -> FlattenAsyncGenerator<Self>
    where
        Self: Sized,
        Self::Output: AsyncGenerator,
    {
        FlattenAsyncGenerator {
            inner: FlattenAsyncGeneratorInner::Future(self),
        }
    }
}

impl<T: Future> FutureExt for T {}

pub struct FlattenAsyncGenerator<F: Future> {
    inner: FlattenAsyncGeneratorInner<F>,
}

enum FlattenAsyncGeneratorInner<F: Future> {
    Future(F),
    AsyncGenerator(F::Output),
}

impl<F> AsyncGenerator for FlattenAsyncGenerator<F>
where
    F: Future,
    <F as Future>::Output: AsyncGenerator,
{
    type Yield = <<F as Future>::Output as AsyncGenerator>::Yield;
    type Return = <<F as Future>::Output as AsyncGenerator>::Return;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>> {
        let inner = unsafe { &mut self.get_unchecked_mut().inner };
        loop {
            match inner {
                FlattenAsyncGeneratorInner::Future(future) => {
                    let future = unsafe { Pin::new_unchecked(future) };
                    if let Poll::Ready(async_generator) = Future::poll(future, cx) {
                        *inner = FlattenAsyncGeneratorInner::AsyncGenerator(async_generator);
                        continue;
                    }
                }
                FlattenAsyncGeneratorInner::AsyncGenerator(async_generator) => {
                    let async_generator = unsafe { Pin::new_unchecked(async_generator) };
                    if let Poll::Ready(state) = AsyncGenerator::poll_resume(async_generator, cx) {
                        return Poll::Ready(state);
                    }
                }
            }
            return Poll::Pending;
        }
    }
}

pub trait TryFutureExt: TryFuture {
    /// Flatten a future which returns a `Result` of an `AsyncGenerator` into an `AsyncGenerator`
    /// which returns a `Result`.
    /// Similar to the `TryFutureExt::try_flatten_stream` method in the futures crate.
    fn try_flatten_async_generator(self) -> TryFlattenAsyncGenerator<Self>
    where
        Self: Sized,
        Self::Ok: TryAsyncGenerator<Error = Self::Error>,
    {
        TryFlattenAsyncGenerator {
            inner: TryFlattenAsyncGeneratorInner::Future(self),
        }
    }
}

impl<F: TryFuture> TryFutureExt for F {}

pub struct TryFlattenAsyncGenerator<F: TryFuture> {
    inner: TryFlattenAsyncGeneratorInner<F>,
}

enum TryFlattenAsyncGeneratorInner<F: TryFuture> {
    Future(F),
    AsyncGenerator(F::Ok),
}

impl<F> AsyncGenerator for TryFlattenAsyncGenerator<F>
where
    F: TryFuture,
    F::Ok: TryAsyncGenerator<Error = F::Error>,
{
    type Yield = <F::Ok as AsyncGenerator>::Yield;
    type Return = Result<<F::Ok as TryAsyncGenerator>::Ok, <F::Ok as TryAsyncGenerator>::Error>;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Self::Yield, Self::Return>> {
        let inner = unsafe { &mut self.get_unchecked_mut().inner };
        loop {
            match inner {
                TryFlattenAsyncGeneratorInner::Future(future) => {
                    let future = unsafe { Pin::new_unchecked(future) };
                    if let Poll::Ready(async_generator_res) = TryFuture::try_poll(future, cx) {
                        match async_generator_res {
                            Ok(async_generator) => {
                                *inner =
                                    TryFlattenAsyncGeneratorInner::AsyncGenerator(async_generator);
                                continue;
                            }
                            Err(err) => return Poll::Ready(GeneratorState::Complete(Err(err))),
                        }
                    }
                }
                TryFlattenAsyncGeneratorInner::AsyncGenerator(async_generator) => {
                    let async_generator = unsafe { Pin::new_unchecked(async_generator) };
                    if let Poll::Ready(state) =
                        TryAsyncGenerator::try_poll_resume(async_generator, cx)
                    {
                        return Poll::Ready(state);
                    }
                }
            }
            return Poll::Pending;
        }
    }
}

/// Convert a `Stream` into an `AsyncGenerator` which returns `()`.
pub fn from_stream<S: Stream>(stream: S) -> FromStream<S> {
    FromStream { stream }
}

pub struct FromStream<S> {
    stream: S,
}

impl<S: Stream> AsyncGenerator for FromStream<S> {
    type Yield = S::Item;
    type Return = ();

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<S::Item, ()>> {
        let stream = unsafe { self.map_unchecked_mut(|s| &mut s.stream) };
        match Stream::poll_next(stream, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(Some(x)) => Poll::Ready(GeneratorState::Yielded(x)),
            Poll::Ready(None) => Poll::Ready(GeneratorState::Complete(())),
        }
    }
}

/// Convert a `Future` into an `AsyncGenerator` which yields `Y`. `Y` can be any type since the
/// generator will never actually yield, only return the value returned by the `Future`.
pub fn from_future<Y, F: Future>(future: F) -> FromFuture<Y, F> {
    FromFuture {
        future,
        _phantom_yield: PhantomData,
    }
}

pub struct FromFuture<Y, F> {
    future: F,
    _phantom_yield: PhantomData<Y>,
}

impl<Y, F: Future> AsyncGenerator for FromFuture<Y, F> {
    type Yield = Y;
    type Return = F::Output;

    fn poll_resume(
        self: Pin<&mut Self>,
        cx: &mut task::Context,
    ) -> Poll<GeneratorState<Y, F::Output>> {
        let future = unsafe { self.map_unchecked_mut(|s| &mut s.future) };
        match Future::poll(future, cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(x) => Poll::Ready(GeneratorState::Complete(x)),
        }
    }
}
