//! This module provides a wrapper around `FuturesUnordered` and adds an `into_iter` method to it.
//! Ideally this method should be provided by FuturesUnordered itself since it already has similar
//! methods like `iter_mut`.
//! See: https://github.com/rust-lang-nursery/futures-rs/issues/1795

use super::*;

pub struct FuturesUnorderedWithIntoIter<F> {
    inner: FuturesUnordered<OptionFuture<F>>,
}

impl<F> FuturesUnorderedWithIntoIter<F> {
    pub fn new() -> FuturesUnorderedWithIntoIter<F>
    where
        F: Future + Unpin,
    {
        FuturesUnorderedWithIntoIter {
            inner: FuturesUnordered::new(),
        }
    }

    pub fn into_iter(mut self) -> impl Iterator<Item = F>
    where
        F: Unpin,
    {
        let mut futures = Vec::with_capacity(self.inner.len());
        for option_future in self.inner.iter_mut() {
            let future = option_future.future_opt.take().unwrap();
            futures.push(future);
        }
        futures.into_iter()
    }

    pub fn push(&mut self, future: F) {
        self.inner.push(OptionFuture {
            future_opt: Some(future),
        });
    }
}

impl<F> iter::FromIterator<F> for FuturesUnorderedWithIntoIter<F>
where
    F: Future + Unpin,
{
    fn from_iter<T: IntoIterator<Item = F>>(iter: T) -> FuturesUnorderedWithIntoIter<F> {
        FuturesUnorderedWithIntoIter {
            inner: iter
                .into_iter()
                .map(|future| OptionFuture {
                    future_opt: Some(future),
                })
                .collect(),
        }
    }
}

impl<F> FusedStream for FuturesUnorderedWithIntoIter<F>
where
    F: Future + Unpin,
{
    fn is_terminated(&self) -> bool {
        self.inner.is_terminated()
    }
}

impl<F> Stream for FuturesUnorderedWithIntoIter<F>
where
    F: Future + Unpin,
{
    type Item = F::Output;

    fn poll_next(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<Option<F::Output>> {
        let inner = Pin::new(&mut self.get_mut().inner);
        inner.poll_next(cx)
    }
}

struct OptionFuture<F> {
    future_opt: Option<F>,
}

impl<F> Future for OptionFuture<F>
where
    F: Future + Unpin,
{
    type Output = F::Output;

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<F::Output> {
        let future = Pin::new(self.get_mut().future_opt.as_mut().unwrap());
        future.poll(cx)
    }
}
