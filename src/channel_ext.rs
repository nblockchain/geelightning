use super::*;

#[ext_trait]
impl<T> OneshotSenderExt<T> for oneshot::Sender<T> {
    /// Borrows the `Sender` and creates a future which resolves to `()` if the corresponding
    /// `Receiver` is dropped.
    fn on_drop<'a>(&'a mut self) -> OneshotSenderOnDrop<'a, T> {
        OneshotSenderOnDrop {
            sender: self,
            terminated: false,
        }
    }
}

pub struct OneshotSenderOnDrop<'a, T> {
    sender: &'a mut oneshot::Sender<T>,
    terminated: bool,
}

impl<'a, T> Future for OneshotSenderOnDrop<'a, T> {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut task::Context) -> Poll<()> {
        let self_mut = self.get_mut();
        match self_mut.sender.poll_canceled(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(()) => {
                self_mut.terminated = true;
                Poll::Ready(())
            }
        }
    }
}

impl<'a, T> FusedFuture for OneshotSenderOnDrop<'a, T> {
    fn is_terminated(&self) -> bool {
        self.terminated
    }
}
