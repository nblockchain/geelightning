//! This module defines types for numerical units of measure, such as satoshis and millisatoshis.
//! For type-safety, these units should be used everywhere they apply instead of using raw int types
//! such as u32, u64 etc. They should only support checked, non-rounding operations.

use super::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Hash)]
pub struct Sat64 {
    sat: u64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Hash)]
pub struct Msat64 {
    msat: u64,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, Hash)]
pub struct SatPerKw32 {
    sat_per_kw: u32,
}

impl Sat64 {
    pub const MAX: Sat64 = Sat64::from_u64(2_099_999_997_690_000);

    pub const fn from_u64(sat: u64) -> Sat64 {
        Sat64 { sat }
    }

    pub fn to_u64(self) -> u64 {
        self.sat
    }

    pub fn try_to_msat64(self) -> Option<Msat64> {
        self.to_u64().checked_mul(1000).map(Msat64::from_u64)
    }

    pub fn checked_add(self, other: Sat64) -> Option<Sat64> {
        Some(Sat64 {
            sat: self.sat.checked_add(other.sat)?,
        })
    }

    pub fn checked_sub(self, other: Sat64) -> Option<Sat64> {
        Some(Sat64 {
            sat: self.sat.checked_sub(other.sat)?,
        })
    }

    pub fn div_mod_u64(self, n: u64) -> Option<(Sat64, Sat64)> {
        if n == 0 {
            return None;
        }

        let sat = self.to_u64();
        let div = Sat64::from_u64(sat / n);
        let rem = Sat64::from_u64(sat % n);
        Some((div, rem))
    }
}

impl Msat64 {
    pub const MAX: Msat64 = Msat64::from_u64(2_099_999_997_690_000_000);

    pub const fn from_u64(msat: u64) -> Msat64 {
        Msat64 { msat }
    }

    pub fn to_u64(self) -> u64 {
        self.msat
    }

    pub fn to_sat64_remainder(self) -> (Sat64, Msat64) {
        let msat = self.to_u64();
        let div = Sat64::from_u64(msat / 1000);
        let rem = Msat64::from_u64(msat % 1000);
        (div, rem)
    }

    pub fn checked_add(self, other: Msat64) -> Option<Msat64> {
        Some(Msat64 {
            msat: self.msat.checked_add(other.msat)?,
        })
    }

    pub fn checked_sub(self, other: Msat64) -> Option<Msat64> {
        Some(Msat64 {
            msat: self.msat.checked_sub(other.msat)?,
        })
    }
}

impl SatPerKw32 {
    pub const fn from_u32(sat_per_kw: u32) -> SatPerKw32 {
        SatPerKw32 { sat_per_kw }
    }

    pub fn to_u32(self) -> u32 {
        self.sat_per_kw
    }

    pub fn checked_mul_weight(self, weight: u64) -> Option<Sat64> {
        Some(Sat64::from_u64(
            (u64::from(self.sat_per_kw)).checked_mul(weight)? / 1000,
        ))
    }
}

impl PartialEq<Msat64> for Sat64 {
    fn eq(&self, other: &Msat64) -> bool {
        let self_msat = match self.try_to_msat64() {
            Some(self_msat) => self_msat,
            None => return false,
        };
        self_msat == *other
    }
}

impl PartialEq<Sat64> for Msat64 {
    fn eq(&self, other: &Sat64) -> bool {
        other.eq(self)
    }
}

impl PartialOrd<Msat64> for Sat64 {
    fn partial_cmp(&self, other: &Msat64) -> Option<cmp::Ordering> {
        let self_msat = match self.try_to_msat64() {
            Some(self_msat) => self_msat,
            None => return Some(cmp::Ordering::Greater),
        };
        self_msat.partial_cmp(other)
    }
}

impl PartialOrd<Sat64> for Msat64 {
    fn partial_cmp(&self, other: &Sat64) -> Option<cmp::Ordering> {
        let other_msat = match other.try_to_msat64() {
            Some(other_msat) => other_msat,
            None => return Some(cmp::Ordering::Less),
        };
        self.partial_cmp(&other_msat)
    }
}

impl fmt::Display for Sat64 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}sat", self.to_u64())
    }
}

impl fmt::Display for Msat64 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}msat", self.to_u64())
    }
}

impl fmt::Display for SatPerKw32 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}sat/kw", self.to_u32())
    }
}
