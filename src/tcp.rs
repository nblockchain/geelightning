//! This module provides methods for binding TCP sockets reusably so that we have a consistent
//! IP:port from the perspective of other peers on the network.
//!
//! For good run-down of what's going on here and why we need platform-specific code to do this see
//! this stackoverflow post:
//! https://stackoverflow.com/questions/14388706/how-do-so-reuseaddr-and-so-reuseport-differ

use super::*;

const ACCEPT_QUEUE_SIZE: u32 = 16;

#[async_trait]
#[ext_trait]
impl TcpStreamExt for TcpStream {
    /// Make a TCP connection to the remote address originating from the specified local address.
    /// The socket is reusably bound to the local address, allowing multiple sockets to be bound to
    /// the same local address so that our external endpoint is the same to all observers.
    async fn connect_reusable(
        local_addr: SocketAddr,
        remote_addr: SocketAddr,
    ) -> io::Result<TcpStream> {
        let socket = bind_reusable(local_addr)?;
        socket.connect(remote_addr).await
    }
}

#[async_trait]
#[ext_trait]
impl TcpListenerExt for TcpListener {
    /// Create `TcpListener` reusably bound to the given local address. Binding reusably allows us
    /// to also create outgoing TCP connections from this address.
    fn bind_reusable(local_addr: SocketAddr) -> io::Result<TcpListener> {
        let socket = bind_reusable(local_addr)?;
        socket.listen(ACCEPT_QUEUE_SIZE)
    }
}

fn bind_reusable(local_addr: SocketAddr) -> io::Result<TcpSocket> {
    let socket = match local_addr {
        SocketAddr::V4(..) => TcpSocket::new_v4()?,
        SocketAddr::V6(..) => TcpSocket::new_v6()?,
    };
    socket.set_reuseaddr(true)?;
    #[cfg(all(unix, not(target_os = "solaris"), not(target_os = "illumos")))]
    {
        socket.set_reuseport(true)?;
    }
    socket.bind(local_addr)?;
    Ok(socket)
}
