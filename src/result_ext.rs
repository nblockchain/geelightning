use super::*;

#[ext_trait]
impl<E> ResultIgnoreErrExt<E> for Result<(), E> {
    /// Drop the error of a result and return `()` if the result's `Ok` type is `()`.
    ///
    /// This can be used to silence unused value warnings but is safe than assiging to `_` because
    /// it forces the type to be a `Result`, eg. you can't accidently discard an `impl
    /// Future<Output = Result<(), _>>` using `ignore_err`.
    fn ignore_err(self) {
        drop(self);
    }
}
