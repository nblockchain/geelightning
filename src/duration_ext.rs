use super::*;

#[ext_trait]
impl DurationExt for Duration {
    // This method is not available on stable rust yet.
    // Waiting for feature(div_duration)
    fn div_duration_f64_(self, rhs: Duration) -> f64 {
        let lhs = self.as_secs_f64();
        let rhs = rhs.as_secs_f64();
        lhs / rhs
    }
}
